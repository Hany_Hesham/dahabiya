<?php
// contact us mailing function to admins
  if (!function_exists('mail_management')){
      function mail_management($client_req){
        $CI = get_instance();
        $CI->load->library('email');
        $CI->db->where(array('deleted'=>0,'disabled'=>0,'department'=>1));
        $users = $CI->db->get('users')->result_array();
        $emails=array();
            foreach ($users as $user) {
              $emails[]=$user['email'];
            }
           // die(print_r(implode(',', $emails)));
            $CI->email->from('info@dive.com');
            $CI->email->to(implode(',', $emails));
            //$CI->email->cc('mahmoud.mohier@gmail.com');
            $CI->email->subject("Web Site Client Request");
            $CI->email->message("Dear Sirs,
              <br/>
              Please contact client with below data<br>
              Client Name: {$client_req['client_name']}
              <br/>
              Subject: {$client_req['subject']}<br>
              Message: {$client_req['message']}<br>
              <br/>
            "); 
            //echo $CI->email->print_debugger();
            $mail_result = $CI->email->send();
            return $mail_result;
        }
    }
// booking mailing function to admins
  if (!function_exists('mail_management_booking')){
      function mail_management_booking($client_req){
        $CI = get_instance();
        $CI->load->library('email');
        $CI->db->where(array('deleted'=>0,'disabled'=>0,'department'=>1));
        $users = $CI->db->get('users')->result_array();
        $emails=array();
            foreach ($users as $user) {
              $emails[]=$user['email'];
            }
            $CI->email->from('zahabia_resort@yahoo.com');
            $CI->email->to(implode(',', $emails));
            $CI->email->subject("Web Site Booking Request");
            $CI->email->message(
                                  "Dear Sirs,
                                   <br/>
                                   Please contact client with below Booking data<br>
                                   Activity: {$client_req['activity_name']} 
                                   Sub acivity: {$client_req['sub_name']}
                                   Client Name: {$client_req['first_name']} {$client_req['last_name']}<br/>
                                   Email: {$client_req['email']}<br>
                                   Phone: {$client_req['phone']}<br>
                                   Booking Date: {$client_req['date']}<br>
                                   Pax Num: {$client_req['pax']}<br>
                                   Total Price: {$client_req['total_cost']}<br>
                                   <br/>"
                                ); 
            $mail_result = $CI->email->send();
            return $mail_result;
        }
    }
// replay mail function
    if (!function_exists('mail_clients')){
      function mail_clients($email,$subject,$message){
            $CI = get_instance();
            $CI->email->from('zahabia_resort@yahoo.com');
            $CI->email->to($email);
            $CI->email->cc('mahmoud.mohier@gmail.com');
            $CI->email->subject("{$subject}");
            $CI->email->message("<br/>{$message}<br>"); 
            $mail_result = $CI->email->send();
            return $mail_result;
        }
    }


?>