<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

  function collectAll() {
    $CI =& get_instance();
    $forms = $CI->RocketChat_model->getForms();
    $data = array();
    $counter_all = 0;
    foreach ($forms as $keys => $form) {
      $data[$keys]['form'] = $form['name'];
      $data[$keys]['message_id'] = $form['message_id'];
      $data[$keys]['count'] = 0;
      $data[$keys]['data'] = array();
      $counter = 0;
      $formsData = $CI->RocketChat_model->getFormData($form['database'], $form['date'], $form['deleted']);
      foreach ($formsData as $key => $formData) {
        if ($form['id'] == 1) {
          $rejectForms = $CI->RocketChat_model->getRejectForms($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_planned']);
        }elseif ($form['id'] == 4) {
          $rejectForms = $CI->RocketChat_model->getRejectForms($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_unplanned']);
        }elseif ($form['id'] != 8) {
          $rejectForms = $CI->RocketChat_model->getRejectForms($form['signature'], $form['id'], $form['signature_variable'], $formData['id']);
        }
        if ($rejectForms != 0) {
          unset($formsData[$key]); 
        }
      }
      foreach ($formsData as $key => $formData) {
        if ($form['id'] == 1) {
          $signForm = $CI->RocketChat_model->getSignForm($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_planned']);
        }elseif ($form['id'] == 4) {
          $signForm = $CI->RocketChat_model->getSignForm($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_unplanned']);
        }elseif ($form['id'] == 8) {
          $signForm = $CI->RocketChat_model->getSignAsset($form['signature'], $form['variable'], $form['signature_variable'], $formData['id']);
        }else{
          $signForm = $CI->RocketChat_model->getSignForm($form['signature'], $form['id'], $form['signature_variable'], $formData['id']);
        }
        if($form['id'] != 8) {
          if (isset($signForm[0]['role_id']) && $signForm[0]['role_id'] == 1) {
            if ($form['id'] == 1) {
              if (is_null($formData['code'])) {
                unset($formsData[$key]);
              }else{
                $requested = base_url($form['link'].$formData[$form['variable']]);
                $project = array('link' => $requested, 'name' => $formData['name']);
                //rocketSendFirst($CI->config->item('page_to_send'), $requested);
                array_push($data[$keys]['data'], $project);
                $counter++;
                $counter_all++;
              }
            } elseif ($form['id'] == 4) {
              if (!is_null($formData['code'])) {
                unset($formsData[$key]);
              }else{
                $requested = base_url($form['link'].$formData[$form['variable']]);
                $project = array('link' => $requested, 'name' => $formData['name']);
                //rocketSendFirst($CI->config->item('page_to_send'), $requested);
                array_push($data[$keys]['data'], $project);
                $counter++;
                $counter_all++;
              }
            } elseif ($form['id'] == 7) {
              if ($formData['changes'] == 1) {
                unset($formsData[$key]);
              }else{
                $requested = base_url($form['link'].$formData[$form['variable']]);
                //rocketSendFirst($CI->config->item('page_to_send'), $requested);
                array_push($data[$keys]['data'], $requested);
                $counter++;
                $counter_all++;
              }
            } elseif ($form['id'] == 16) {
              if ($formData['changes'] == 0) {
                unset($formsData[$key]);
              }else{
                $requested = base_url($form['link'].$formData[$form['variable']]);
                //rocketSendFirst($CI->config->item('page_to_send'), $requested);
                array_push($data[$keys]['data'], $requested);
                $counter++;
                $counter_all++;
              }
            }else{
              $requested = base_url($form['link'].$formData[$form['variable']]);
              //rocketSendFirst($CI->config->item('page_to_send'), $requested);
              array_push($data[$keys]['data'], $requested);
              $counter++;
              $counter_all++;
            }
          }else{
            unset($formsData[$key]);
          }
        }else{
          if ($signForm == 1) {
            $requested = base_url($form['link'].$formData[$form['variable']]);
            //rocketSendFirst($CI->config->item('page_to_send'), $requested);
            array_push($data[$keys]['data'], $requested);
            $counter++;
            $counter_all++;
          }else{
            unset($formsData[$key]);
          }
        }
        if ($counter > 0) {
          $data[$keys]['count'] = $counter;
        }
      }
    }
    $count = array();
    foreach ($data as $key => $value) {
      if ($value['count'] == 0) {
        unset($data[$key]);
      }else{
        $count[$key] = $value['count'];
      }
    }
    array_multisort($count, SORT_ASC, $data);
    $dataAll = array(
      'counter_all' => $counter_all, 
      'data_all' => $data
    );
    return $dataAll;
  }

  function collectForm($id) {
    $CI =& get_instance();
    $form = $CI->RocketChat_model->getForm($id);
    $data = array();
    $counter_all = 0;
    $data['form'] = '';
    $data['message_id'] = '';
    $data['count'] = 0;
    $data['data'] = array();
    $counter = 0;
    $formsData = $CI->RocketChat_model->getFormData($form['database'], $form['date'], $form['deleted']);
    foreach ($formsData as $key => $formData) {
      if ($form['id'] == 1) {
        $rejectForms = $CI->RocketChat_model->getRejectForms($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_planned']);
      }elseif ($form['id'] == 4) {
        $rejectForms = $CI->RocketChat_model->getRejectForms($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_unplanned']);
      }elseif ($form['id'] != 8) {
        $rejectForms = $CI->RocketChat_model->getRejectForms($form['signature'], $form['id'], $form['signature_variable'], $formData['id']);
      }
      if ($rejectForms != 0) {
        unset($formsData[$key]); 
      }
    }
    foreach ($formsData as $key => $formData) {
      if ($form['id'] == 1) {
        $signForm = $CI->RocketChat_model->getSignForm($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_planned']);
      }elseif ($form['id'] == 4) {
        $signForm = $CI->RocketChat_model->getSignForm($form['signature'], $form['id'], $form['signature_variable'], $formData['id'], $formData['change_unplanned']);
      }elseif ($form['id'] == 8) {
        $signForm = $CI->RocketChat_model->getSignAsset($form['signature'], $form['variable'], $form['signature_variable'], $formData['id']);
      }else{
        $signForm = $CI->RocketChat_model->getSignForm($form['signature'], $form['id'], $form['signature_variable'], $formData['id']);
      }
      if($form['id'] != 8) {
        if (isset($signForm[0]['role_id']) && $signForm[0]['role_id'] == 1) {
          if ($form['id'] == 1) {
            if (is_null($formData['code'])) {
              unset($formsData[$key]);
            }else{
              $requested = base_url($form['link'].$formData[$form['variable']]);
              $project = array('link' => $requested, 'name' => $formData['name']);
              array_push($data['data'], $project);
              $counter++;
              $counter_all++;
            }
          } elseif ($form['id'] == 4) {
            if (!is_null($formData['code'])) {
              unset($formsData[$key]);
            }else{
              $requested = base_url($form['link'].$formData[$form['variable']]);
              $project = array('link' => $requested, 'name' => $formData['name']);
              array_push($data['data'], $project);
              $counter++;
              $counter_all++;
            }
          } elseif ($form['id'] == 7) {
            if ($formData['changes'] == 1) {
              unset($formsData[$key]);
            }else{
              $requested = base_url($form['link'].$formData[$form['variable']]);
              array_push($data['data'], $requested);
              $counter++;
              $counter_all++;
            }
          } elseif ($form['id'] == 16) {
            if ($formData['changes'] == 0) {
              unset($formsData[$key]);
            }else{
              $requested = base_url($form['link'].$formData[$form['variable']]);
              array_push($data['data'], $requested);
              $counter++;
              $counter_all++;
            }
          }else{
            $requested = base_url($form['link'].$formData[$form['variable']]);
            array_push($data['data'], $requested);
            $counter++;
            $counter_all++;
          }
        }else{
          unset($formsData[$key]);
        }
      }else{
        if ($signForm == 1) {
          $requested = base_url($form['link'].$formData[$form['variable']]);
          array_push($data['data'], $requested);
          $counter++;
          $counter_all++;
        }else{
          unset($formsData[$key]);
        }
      }
      if ($counter > 0) {
        $data['form'] = $form['name'];
        $data['message_id'] = $form['message_id'];
        $data['count'] = $counter;
      }
    }
    return $data;
  }

  function headerMessage($allData) {
    $CI =& get_instance();
    $html = "All Form Counter [{$allData['counter_all']}]:-\n";
    $i = 1;
    foreach ($allData['data_all'] as $data_all) {
      $html.= "{$i}- {$data_all['form']} [{$data_all['count']}]\n";
      $i++;
    }
    rocketUpdate($html, $CI->config->item('page_to_send'), 'kSK4PSnuB2gFAugcv');
    //rocketSendFirst($CI->config->item('page_to_send'), $html);
  }

  function formMessage($data) {
    $CI =& get_instance();
    $html = "{$data['form']} [{$data['count']}]";
    rocketUpdate($html, $CI->config->item('page_to_send'), $data['message_id']);
    //rocketSendFirst($CI->config->item('page_to_send'), $html);
  }

  /*function rocketSendFirst($channel, $message){
    include(APPPATH . 'third_party/RocketChat/autoload.php');
    $CI =& get_instance();
    $client = new RocketChat\Client($CI->config->item('send_url'));
    $token = $client->api('user')->login($CI->config->item('user_access'), $CI->config->item('pass_access'));
    $client->setToken($token);
    $channel_result = $client->api('channel')->sendMessage($channel,$message);
  }*/

  function rocketSend($message, $channel, $id, $form, $type){
    include(APPPATH . 'third_party/RocketChat/autoload.php');
    $CI =& get_instance();
    $client = new RocketChat\Client($CI->config->item('send_url'));
    $token = $client->api('user')->login($CI->config->item('user_access'), $CI->config->item('pass_access'));
    $client->setToken($token);
    $channel_result = $client->api('channel')->sendMessage($channel,$message);
    $CI->RocketChat_model->addMessageId($form, $id, $type, $channel_result);
  }

  function rocketUpdate($message, $channel, $id){
    include(APPPATH . 'third_party/RocketChat/autoload.php');
    $CI =& get_instance();
    $client = new RocketChat\Client($CI->config->item('send_url'));
    $token = $client->api('user')->login($CI->config->item('user_access'), $CI->config->item('pass_access'));
    $client->setToken($token);
    $channel_result = $client->api('channel')->updateMessage($channel, $id, $message);
  }

  /*function rocketDelete($id, $form, $channel){
    include(APPPATH . 'third_party/RocketChat/autoload.php');
    $CI =& get_instance();
    $client = new RocketChat\Client($CI->config->item('send_url'));
    $token = $client->api('user')->login($CI->config->item('user_access'), $CI->config->item('pass_access'));
    $client->setToken($token);
    $message = $CI->RocketChat_model->getMessageId($id, $form);
    $finale = $message['formName'].' No. '.$message['form_id'].' ✅';
    $channel_result = $client->api('channel')->updateMessage($channel, $message['message_id'], $finale);
    $CI->RocketChat_model->deleteMessageId($message['message_id']);
  }*/

  function rocketDelete($id, $form, $channel){
    include(APPPATH . 'third_party/RocketChat/autoload.php');
    $CI =& get_instance();
    $client = new RocketChat\Client($CI->config->item('send_url'));
    $token = $client->api('user')->login($CI->config->item('user_access'), $CI->config->item('pass_access'));
    $client->setToken($token);
    $message = $CI->RocketChat_model->getMessageId($id, $form);
    //$finale = $message['formName'].' No. '.$message['form_id'].' ✅';
    $channel_result = $client->api('channel')->deleteChannel_msg($channel, $message['message_id']);
    $CI->RocketChat_model->deleteMessageId($message['message_id']);
  }

  /*function rocketDeleteFirst($id, $channel){
    include(APPPATH . 'third_party/RocketChat/autoload.php');
    $CI =& get_instance();
    $client = new RocketChat\Client($CI->config->item('send_url'));
    $token = $client->api('user')->login($CI->config->item('user_access'), $CI->config->item('pass_access'));
    $client->setToken($token);
    $channel_result = $client->api('channel')->deleteChannel_msg($channel, $id);
  }*/

  /*public function test() {
      $data = collectAll();
      headerMessage($data);
    }*/

?>