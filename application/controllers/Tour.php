<?php defined('BASEPATH') OR exit('No direct script access allowed');

  	class Tour extends CI_Controller {

	    public function __construct(){
	      	parent::__construct();
	    }

	    public function index(){
	    	login_checker();      	
			$maindata = notour_access_checker();
	      	$this->load->model('admin/Rooms_model');
      		$data['rooms']      = $this->Rooms_model->searchRoom($maindata);
      		$this->load->view('tour/index',$data);       
	    } 

	    public function add(){
	    	login_checker();      				 
			$maindata = notour_access_checker();
			if ($this->input->post('submit')) {
      			$this->load->model('front_office/Fborder_model');
				$fdata    =[
		          	'uid'             => $_SESSION['user_id'],
          			'hid'             => $maindata['hid'],
          			'date'            => $this->input->post('date'),
          			'status'          => '1',
		        ];  
		        $fb_id  =  $this->Fborder_model->add_fborder($fdata);
        		if ($fb_id) {
          			loger('Create', 27, 'Food & Beverage', $fb_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Food & Beverage Request # NO'.$fb_id.'');
          			if ($this->input->post('items')){
            			foreach ($this->input->post('items') as $key => $item){
              				$item['fb_id'] 							= $fb_id;
              				if ($this->input->post('meal') == 1) {
	              				$item['break'] 		= $this->input->post('booker');
	              				$item['date'] 		= $this->input->post('date');
              				}elseif ($this->input->post('meal') == 2) {
	              				$item['lunch'] 		= $this->input->post('booker');
	              				$item['date1'] 		= $this->input->post('date');
              				}elseif ($this->input->post('meal') == 3) {
	              				$item['royal'] 		= $this->input->post('booker');
	              				$item['date1'] 		= $this->input->post('date');
              				}elseif ($this->input->post('meal') == 4) {
	              				$item['dinner'] 		= $this->input->post('booker');
	              				$item['date2'] 		= $this->input->post('date');
              				}elseif ($this->input->post('meal') == 5) {
	              				$item['cold'] 		= $this->input->post('booker');
	              				$item['date3'] 		= $this->input->post('date');
              				}
              				$item_id = $this->Fborder_model->add_fborder_item($item);
              				if ($item_id) {
                				loger('Create', 27, 'Food & Beverage', $fb_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'added item:'.$item['guest'].'');
              				}
            			}
          			}
          			$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          			$this->signers($fb_id);
        		} 
			}
	    } 

	    public function signers($form_id){
			addEditSigners(27, $form_id);
			redirect($this->stage($form_id));
        } 

        public function stage($form_id){
		   	notify_signers(27, $form_id);
      		$url = base_url().'tour';
			redirect($url);
		}

	    public function login(){
	      	$this->load->model('admin/Auth_model');
			if($this->input->post('submit')){
				$this->form_validation->set_rules('username', 'username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('tour/login');
				}else{
					$data = array(
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password')
					);
					$result = $this->Auth_model->login($data);
					if ($result == TRUE) {
						$admin_data = array(
							'user_id' => $result['id'],
						 	'name'    => $result['username'],
						 	'is_admin_login' => TRUE
						);
						$this->session->set_userdata($admin_data);
	   					if ($this->session->has_userdata('red_url')) {
			                $red_url = $this->session->userdata('red_url');
			                $this->session->unset_userdata('red_url');
			                redirect($red_url);
				        }else{
							redirect('tour');
						}
					}else{
          				$this->session->set_flashdata(['alert'=>'Warning','msg'=>'Invalid Email or Password!']);
						$this->load->view('tour/login', $data);
					}
				}
			}else{
				$this->load->view('tour/login');
			}
		}	

	}

?>   