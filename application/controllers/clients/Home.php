<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends HY_Controller {
   
    public function __construct(){
	    parent::__construct();
	    $this->data['language'] 	= 'english';
	    if (isset($_COOKIE['language'])) {
		    $this->data['language'] = $_COOKIE['language'];
		}
		$this->data['facebook']	= $this->Home_model->get_sit_metaData('social_data_facebook' ,'single');
		$this->data['contact'] = $this->Home_model->get_sit_metaData('contact_us' ,'single');
		$this->data['location'] = $this->Home_model->get_sit_metaData('location' ,'single');
		$this->data['email'] = $this->Home_model->get_sit_metaData('email_us' ,'single');
		$this->data['site_title_logo'] = $this->Home_model->get_sit_metaData('site_title_logo' ,'single');
		$this->data['header']      	= 'clients/includes/header';
		$this->data['slider']     	= 'clients/includes/slider';
		$this->data['footer']      	= 'clients/includes/footer';
	}
		 
	public function index(){
		$data['rooms'] = $this->Home_model->get_rooms_byLimit(6);
		$data['offers'] = $this->Home_model->get_offers();
		$data['welcome_zahabia'] = $this->Home_model->get_sit_metaData('welcome_zahabia','single');
		$data['view']        		= 'clients/home/index';
		$this->load->view('clients/includes/layout',$data);
	}

	public function contact_us(){
		$data['events']       		= $this->Home_model->get_events();
		$data['view']        		= 'clients/home/contact_us';
		$this->load->view('clients/includes/layout',$data);
	}

	public function add_contact_us(){
	    $fdata = [
			'client_name'=> $this->input->post('client_name'),
			'email'=> $this->input->post('email'),
			'subject'=> $this->input->post('subject') !== null? $this->input->post('subject') : '',
			'message'=> $this->input->post('message'),
			'status' => 24
		];
	    $returned_id = $this->Home_model->add_client_mail($fdata);
		if ($returned_id) {         
			$this->session->set_flashdata(['msg' => 'contact_us_request', 'alert'=>'Your request has been sent
			 and our reservation team will contact you soon.']); 
			 redirect($this->agent->referrer());
		}         
	}

	public function rooms(){
		$data['categories'] = $this->Home_model->get_sit_metaData('rooms_categories','multi');
		$data['rooms'] = $this->Home_model->get_rooms();
		$data['view']        		= 'clients/home/rooms';
		$this->load->view('clients/includes/layout',$data);
	}

	public function room($id = FALSE){
		$data['room'] = $this->Home_model->get_room($id);
		$data['view']        		= 'clients/home/room';
		$this->load->view('clients/includes/layout',$data);
	}

	public function offers(){
		$data['offers'] = $this->Home_model->get_offers();
		$data['view']        		= 'clients/home/offers';
		$this->load->view('clients/includes/layout',$data);
	}

	public function offer($id = FALSE){
		$data['offer'] = $this->Home_model->get_offer($id);
		$data['view']        		= 'clients/home/offer';
		$this->load->view('clients/includes/layout',$data);
	}


	public function booking(){
		$data['search'] = $this->parseSearchData($this->input->get());
		$data['categories'] = $this->Home_model->get_sit_metaData('rooms_categories','multi');
		if( $this->input->get()){
			$data['rooms'] = $this->Home_model->get_available_rooms($data['search']);
		}else{
			$data['rooms'] = $this->Home_model->get_rooms();
		}
		$data['view'] = 'clients/home/booking';
		$this->load->view('clients/includes/layout',$data);
	}

	private function parseSearchData($req){
		if($this->input->get('dates')){
			$search['dates'] = explode('/', $this->input->get('dates'));
			$search['check_in'] = isset($search['dates'][0]) ? trim($search['dates'][0]) :'';
			$search['check_out'] = isset($search['dates'][1]) ? trim($search['dates'][1]): '';
			$search['nights'] = $this->getNumberOfNights($search['check_in'], $search['check_out']); 
		}elseif($this->input->get('check_in') && $this->input->get('check_out')){
			$search['check_in'] = $this->input->get('check_in');
			$search['check_out'] = $this->input->get('check_out');
			$search['nights'] = $this->getNumberOfNights($search['check_in'], $search['check_out']); 
		}
		$search['adults'] = $this->input->get('adults');
		$search['childs'] = $this->input->get('childs');
		$search['rooms'] = $this->input->get('rooms');
		return $search;
	}

	private function getNumberOfNights($dt1, $dt2) {
		return date_diff(
			date_create($dt2),  
			date_create($dt1)
		)->format('%a');
	}

	public function add_booking($room_id){
		$data['search'] = json_decode($this->input->post('search_'.$room_id));
		$data['reserved_rooms'] = json_decode($this->input->post('reserved_rooms_'.$room_id));
		$data['room'] = $this->Home_model->get_room($room_id);
		$data['countries'] = $this->Home_model->get_countries();
		$data['view'] = 'clients/home/add_booking';
		$this->load->view('clients/includes/layout',$data);
	}

	public function confirm_booking($code){
		$data['booking'] = $this->Home_model->get_booking_byCode($code);
		$data['search'] = new stdClass();
		$data['search']->check_in = $data['booking']['check_in'];
		$data['search']->check_out = $data['booking']['check_out'];
		$data['search']->nights = $data['booking']['nights'];
		$data['search']->adults = $data['booking']['adults'];
		$data['search']->childs = $data['booking']['childs'];
		$data['reserved_rooms'] = $data['booking']['rooms'];
		$data['room'] = $this->Home_model->get_room($data['booking']['room_id']);
		$data['countries'] = $this->Home_model->get_countries();
		$data['view'] = 'clients/home/add_booking';
		$this->load->view('clients/includes/layout',$data);
	}

	public function postBooking($room_id){
		if ($this->input->post()) {
			$booking_search = json_decode($this->input->post('booking_search'));  
			$total_rate = $this->input->post('reserved_rooms')*$booking_search->nights*$this->input->post('rate');
			$total_tax = $this->input->post('reserved_rooms')*$booking_search->nights*$this->input->post('tax');
			$total_price = $total_rate + $total_tax; 
        	$fdata =[
				'code' => get_code('booking'),
				'room_id' => $room_id,
				'offer_id' => $this->input->post('offer_id'),
          		'check_in' => $booking_search->check_in,
          		'check_out' => $booking_search->check_out,
          		'nights' => $booking_search->nights,
          		'adults' => $booking_search->adults,
          		'childs' => $booking_search->childs,
				'rooms' => $this->input->post('reserved_rooms'),
				'rate' => $this->input->post('rate'),
				'tax'  => $this->input->post('tax'),
				'currency' => $this->input->post('currency'),
				'total_rate' => $total_rate,
				'total_tax' => $total_tax,
				'total_price' => $total_price,
          		'first_name' => $this->input->post('first_name'),
          		'last_name' => $this->input->post('last_name'),
          		'email' => $this->input->post('email'),
				'country' => $this->input->post('country'),
          		'address' => $this->input->post('address'),
          		'city' => $this->input->post('city'),
          		'state' => $this->input->post('state'),
				'post_code' => $this->input->post('post_code'),
          		'phone' => $this->input->post('phone'),
          		'status' => '1',
        	];      
        	$booking_id  =  $this->Home_model->add_booking($fdata);
        	if ($booking_id) {         
				$this->session->set_flashdata(['msg' => 'reservation_request', 'alert'=>'Your reservation request has been sent
				 and our reservation team will contact you for payment confirmation.']); 
				redirect('clients/home/booking/');
        	}
      	}       
    }

	public function update_booking_payment($booking_id){
		$file_name   = do_upload("payment_image",'bookings','site_assets/uploads');
		$fdata = [
           'payment_image' => $file_name,
		   'payment_method' => $this->input->post('payment_method'),
		   'status' => 3,
		];
		$item_id = $this->Home_model->update_booking($fdata, $booking_id);
		$this->session->set_flashdata(['msg'=>'payment_confirmation','alert'=>'Your payment confirmation has been sent successfully.']); 
		redirect('clients/home/booking/');
	}

	public function add_offer_booking($offer_id){
		$search = $this->parseSearchData($this->input->get());
		$data['search'] = new stdClass();
		$data['search']->check_in = $search['check_in'];
		$data['search']->check_out = $search['check_out'];
		$data['search']->nights = $search['nights'];
		$data['search']->adults = $search['adults'];
		$data['search']->childs = $search['childs'];
		$data['reserved_rooms'] = $search['rooms'];
		$data['offer'] = $this->Home_model->get_offer($offer_id);
		if( $data['search']->nights != $data['offer']['nights']){
			$this->session->set_flashdata(['offer_nights_error' => 'offer_nights_error', 'alert'=>'offer only for '.$data['offer']['nights'].' nights.']); 
			redirect($this->agent->referrer());
		} 
		$data['room'] = $this->Home_model->get_room($data['offer']['room_id']);
		$data['countries'] = $this->Home_model->get_countries();
		$data['view'] = 'clients/home/add_offer_booking';
		$this->load->view('clients/includes/layout',$data);
	}

	public function postOfferBooking($room_id){
		if ($this->input->post()) {
			$offer_id = $this->input->post('offer_id');
			$booking_search = json_decode($this->input->post('booking_search'));  
			$total_rate = $this->input->post('reserved_rooms')*$this->input->post('rate');
			$total_tax = $this->input->post('reserved_rooms')*$this->input->post('tax');
			$total_price = $total_rate + $total_tax; 
        	$fdata =[
				'code' => get_code('booking'),
				'room_id' => $room_id,
				'offer_id' => $offer_id,
          		'check_in' => $booking_search->check_in,
          		'check_out' => $booking_search->check_out,
          		'nights' => $booking_search->nights,
          		'adults' => $booking_search->adults,
          		'childs' => $booking_search->childs,
				'rooms' => $this->input->post('reserved_rooms'),
				'rate' => $this->input->post('rate'),
				'tax'  => $this->input->post('tax'),
				'currency' => $this->input->post('currency'),
				'total_rate' => $total_rate,
				'total_tax' => $total_tax,
				'total_price' => $total_price,
          		'first_name' => $this->input->post('first_name'),
          		'last_name' => $this->input->post('last_name'),
          		'email' => $this->input->post('email'),
				'country' => $this->input->post('country'),
          		'address' => $this->input->post('address'),
          		'city' => $this->input->post('city'),
          		'state' => $this->input->post('state'),
				'post_code' => $this->input->post('post_code'),
          		'phone' => $this->input->post('phone'),
          		'status' => '1',
        	];      
        	$booking_id  =  $this->Home_model->add_booking($fdata);
        	if ($booking_id) {         
				$this->session->set_flashdata(['msg' => 'reservation_request', 'alert'=>'Your reservation request has been sent
				 and our reservation team will contact you for payment confirmation.']); 
				redirect('clients/home/booking/');
        	}
      	}       
    }

	public function confirm_offer_booking($code){
		$data['booking'] = $this->Home_model->get_booking_byCode($code);
		$data['search'] = new stdClass();
		$data['search']->check_in = $data['booking']['check_in'];
		$data['search']->check_out = $data['booking']['check_out'];
		$data['search']->nights = $data['booking']['nights'];
		$data['search']->adults = $data['booking']['adults'];
		$data['search']->childs = $data['booking']['childs'];
		$data['reserved_rooms'] = $data['booking']['rooms'];
		$data['offer'] = $this->Home_model->get_offer($data['booking']['offer_id']);
		$data['room'] = $this->Home_model->get_room($data['booking']['room_id']);
		$data['countries'] = $this->Home_model->get_countries();
		$data['view'] = 'clients/home/add_offer_booking';
		$this->load->view('clients/includes/layout',$data);
	}

	public function about(){
		$data['about_paragraph'] = $this->Home_model->get_sit_metaData('about','single');
		$data['points'] = $this->Home_model->get_sit_metaData('about_points','multi');
		$data['about_images'] = $this->Home_model->get_sit_metaData('about_images','multi');
		$data['view'] = 'clients/home/about.php';
		$this->load->view('clients/includes/layout',$data);
	}
    
	public function gallery(){
		$data['gallerys']  = $this->Home_model->get_gallarys();
		$data['categories'] = $this->Home_model->get_sit_metaData('gallery_categories','multi');
		$data['view']           = 'clients/home/gallery.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function language_converter($lang){
        setcookie('language', $lang, 2592000000, "/"); 
        redirect($this->agent->referrer());
	}

	// public function blogs(){
	// 	$data['posts']      		= $this->Home_model->get_posts();
	// 	$data['images']       		= $this->Home_model->get_gallarys();
	// 	$data['view']        		= 'clients/includes/blogs';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function blog($id){
	// 	$data['post']      			= $this->Home_model->get_post($id);
	// 	$data['images']       		= $this->Home_model->get_gallarys();
	// 	$data['view']        		= 'clients/includes/blog';
	// 	$this->load->view('clients/includes/layout',$data);
	// }


	// public function trip(){
	// 	$data['events']       		= $this->Home_model->get_events();
	// 	$data['images']       		= $this->Home_model->get_gallarys();
	// 	$data['view']        		= 'clients/includes/trip';
	// 	$this->load->view('clients/includes/layout',$data);
	// }
    
    // public function index_offers(){
	// 	$data['allOffers']      = get_special();
	// 	$this->load->view('clients/home/index_offers',$data);
	// }	

	// public function index_services(){
	// 	$data['services']       = $this->Home_model->get_sit_metaData('services','multi');
	// 	$this->load->view('clients/home/index_services',$data);
	// }	

	// public function index_gallery(){
	// 	$data['gallerys']       = $this->Home_model->get_sit_metaData('gallery','multi');
	// 	$data['points']         = $this->Home_model->get_gallery_unique();
	// 	$this->load->view('clients/home/index_gallery',$data);
	// }	

	// public function index_about(){
	// 	$data['about']       = $this->Home_model->get_sit_metaData('about','single');
	// 	$this->load->view('clients/home/index_about',$data);
	// }	

	
	// public function boat(){
	// 	$data['boat_infos']       	= $this->Home_model->get_boat_infos('1');
	// 	$data['view']        		= 'clients/includes/boat';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function packages(){
	// 	$data['packages']       	= $this->Home_model->get_boat_packages('1');
	// 	$data['view']        		= 'clients/includes/packages';
	// 	$this->load->view('clients/includes/layout',$data);
	// }
	// public function portfolio(){
	// 	$data['portfolio']       = $this->Home_model->get_sit_metaData('portfolio','multi');
	// 	$data['navbar']         = 'clients/includes/navbar';
	// 	$data['view']           = 'clients/home/portfolio.php';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

    // public function services(){
	// 	$data['services']       = $this->Home_model->get_sit_metaData('services','multi');
	// 	$this->load->view('clients/home/services',$data);
	// }

	// public function products(){
	// 	$data['categories']       = $this->Home_model->get_categories();
	// 	$this->load->view('clients/home/products',$data);
	// }

	
// public function gallary(){
	// 	$data['categories']       	= getallCategories();
	// 	$data['images']       		= $this->Home_model->get_gallarys();
	// 	$data['view']        		= 'clients/home/gallary';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	

}
?>
