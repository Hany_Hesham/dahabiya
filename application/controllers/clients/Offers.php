<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offers extends CI_Controller {
   
    public function __construct(){
	    parent::__construct();
	    $this->load->model('clients/Home_model');
	    $this->load->model('admin/General_model');
	    $this->load->helper('mailer_helper');
	    $this->data['language'] = 'english';
	    if (isset($_COOKIE['language'])) {
		    $this->data['language']      = $_COOKIE['language'];
		}
	    $this->data['activities'] = $this->General_model->get_activities();
	  }
		 
	public function index(){
		$data['sliders']       = $this->Home_model->get_sliders();
		$data['gallerys']       = $this->Home_model->get_sit_metaData('gallery','multi');
		$data['points']         = $this->Home_model->get_gallery_unique();
		$data['navbar']        = 'clients/includes/navbar';
		$data['view']          = 'clients/home/index.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function allOffers($refrance){
		$data['portfolio']       = $this->Home_model->get_sit_metaData('portfolio','multi');
		$data['offerData']      = get_subs($refrance);
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/allOffer.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function offer($refrance, $type){
		$data['offerData']      = get_subs($refrance);
		$data['allOffers']      = get_offers($data['offerData']['activity_id'], $data['offerData']['hotel_id'], $data['offerData']['sub_id']);
		if ($type == 1) {
        	$this->load->view('clients/home/offers',$data);
		}else{
        	$this->load->view('clients/home/offerz',$data);
		}
	}

	public function offerd($id){
		$data['offer']      	= $this->General_model->get_offer($id);
		$data['allOffers']      = get_offers($data['offer']['activity_id'], $data['offer']['hotel_id'], $data['offer']['sub_id']);
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/offer.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function book_trip(){
	    $formdata=[];
        parse_str($this->input->post('formdata'),$formdata); 
        $date=date_create($formdata['date']);
        $formdata['date'] =  date_format($date,"yy-m-d");    
        $formdata['status'] ='35';
        unset($formdata['s']);
        unset($formdata['subscriber_email']);
	    $result = mail_management_booking($formdata);
	    unset($formdata['activity_name']);
	    unset($formdata['sub_name']);
	    $returned_id = $this->Home_model->add_booking_trip($formdata);
	    echo json_encode(true);
	    exit();  
	            
	}

}
