<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Reports extends MY_Controller {

		  public function __construct(){
			  parent::__construct();
          $this->load->model('admin/Modules_model');
          $this->load->model('admin/Hotels_model');
          $this->load->model('Reports_model');
          $this->load->helper('date_helper');
          $this->load->helper('mpdf_helper');
  		    $this->data['module']      = $this->Modules_model->get_module_by('','Reports');
          $this->data['permission']  = user_access($this->data['module']['id']);
		  }
      public function index($report_name=''){
          access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,'admin/dashboard'); 
            $data['reports']         = $this->Reports_model->get_reports();
            $data['modules']         = $this->Reports_model->get_reports_modules(); 
            $data['departments']     = get_departments($this->data['dep_id']);  
            $data['statuss']         = get_statuss();  
            $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);          
            $data['report_name']     = $report_name; 
            $datestring              = '%Y-%m-%d';
            $data['from_date']       = date("Y-m-d", strtotime("first day of this month"));
            $data['to_date']         = mdate($datestring,time());
            $data['view']            = 'reports/reports_gen';
            $this->load->view('admin/includes/layout',$data);
      }  
      public function filter_generator($report_fun=''){
        $data = array();
        if ($report_fun == 'generate_reservations_resource_report'){
            $data['reservation_sources']     = $this->General_model->get_meta_data('reservation_resources');
          }elseif ($report_fun == 'generate_out_go_item_report') {
            $data['delivery_status']         = ['delivered','not delivered'];
          }elseif ($report_fun == 'generate_amenity_report') {
            $data['amenity_treatments']      = $this->General_model->get_meta_data('amenity_treatment');
          }elseif ($report_fun == 'generate_workshop_report') {
            $data['workshop_types']          = $this->General_model->get_meta_data('workshop_types');
          }elseif ($report_fun == 'generate_project_detailed_report' || $report_fun == 'generate_project_summary_report') {
            $data['project_original']          = array('Planned','UNPlanned');
            $data['project_equipment']         = array('yes','no');
            $data['project_renno']             = array('1','0');
            $data['project_progresses']        = $this->General_model->get_meta_data('project_progress');
            $data['project_delay']             = array(
                                                        array('id' => 1, 'name' => 'Waiting Signature Delay'),
                                                        array('id' => 2, 'name' => 'In Progress Delay'),
                                                        array('id' => 3, 'name' => 'Finished With Delay')
                                                      );
            $data['project_types']             = array(
                                                        array('id' => 1, 'name' => 'Mandatory'),
                                                        array('id' => 2, 'name' => 'Maintenance'),
                                                        array('id' => 3, 'name' => 'Enhancement')
                                                      );
            if ($report_fun == 'generate_project_detailed_report') {
                  $data['project_by_cost']           = array('Over Cost','Within The Cost');
            }
          }
        $this->load->view('reports/report_extra_filters',$data);
      } 
      public function mpdfMaker(){
        $data['tableData']    = json_decode($this->input->post('tableData'),true);
        $data['report_name']  = $this->input->post('reportname');
        $data['from_date']    = $this->input->post('fromDate');
        $data['to_date']      = $this->input->post('toDate');
        $data['hotels_count'] = count($this->input->post('hotelId'));
        $data['username']     =  $this->data['username'];
        if (count($this->input->post('hotelId')) >1 ) {
             $hotels      = $this->Hotels_model->get_hotels_by_ids($this->input->post('hotelId'));
             $data['hotel']=array();
             foreach ($hotels as $hotel) {
               $data['hotel']['name'][] = $hotel['code'];
               $data['hotel']['logo']   = 'SR-Master.png';
              }
             $data['hotel']['hotel_name']  = implode(',', $data['hotel']['name']);
          }elseif (count($this->input->post('hotelId'))==1) {
             $data['hotel']      = $this->Hotels_model->get_by_id($this->input->post('hotelId')[0]);
         }
       
        $generated_html=report_pdf_generator($data);   
        $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
          try {
            $mpdf = new \Mpdf\Mpdf([
              'margin_left' => 10,
              'margin_right' => 10,
              'margin_bottom' => 25,
              'margin_footer' => 10
            ]);
            $mpdf->SetProtection(array('print'));
            $mpdf->SetTitle($data['hotel']['hotel_name']);
            $mpdf->SetAuthor($data['hotel']['hotel_name']);
            $mpdf->SetWatermarkText("E-Signature");
            $mpdf->showWatermarkText = true;
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($generated_html);
            $mpdf->debug = true;
            ob_end_clean();
            $pdfFilePath = $data['report_name'].'.pdf';
            $mpdf->Output(FCPATH.'assets/pdf_generated/'.$pdfFilePath, "F");
         
          }catch (\Mpdf\MpdfException $e) { 
               echo $e->getMessage();
          }
         echo json_encode($pdfFilePath);
         exit();
      }
//===================================================== reservations Reports ============================================
      public function generate_reservations_resource_report(){
          $data['table_fun']     = 'reservations_resource_report_generator';
          $this->load->view('reports/reservations/reservations_report',$data);
      }    

      public function reservations_resource_report_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_reservation_report($this->data['uhotels'],$dt_att,$custom_search,'');
          $data          = array();
          $i = 1;
            foreach($rows as $row) {
              $arr = array(); 
                $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>Hotel Name: </strong><span>'.$row['hotel_name'].'</span><br>
                          <strong>Guest Name: </strong><span>'.$row['name'].'</span><br>
                          <strong>Agent: </strong><span>'.$row['agent'].'</span><br>
                          <strong>Arrival: </strong><span>'.$row['arrival'].'</span><br>
                          <strong>Departure: </strong><span>'.$row['departure'].'</span><br>
                          <strong>Resource Type: </strong><span>'.$row['resource_type'].'</span><br>';
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['no_room'].'</span>'; 
                $arr[] = '<strong>Adults: </strong><span>'.$row['adult'].'</span><br>
                          <strong>Childrens: </strong><span>'.$row['child'].'</span><br>';    
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['discount'].'</span>';    ;
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['types_name'].'</span>';  
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['board_name'].'</span>';
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['room_type'].'</span>';  
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['rate'].'</span>';
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['recived_by'].'</span>';              
                $data[] =$arr;
              $i++;
            }

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_report_reservation($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_reservation_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }

/*
=============================================== Financial Reports =========================================
=========================== out go reports  
========== out go itmes
*/

      public function generate_out_go_item_report(){
          $data['table_fun']     = 'out_go_item_report_generator';
          $this->load->view('reports/financial/out_go_item_report',$data);
      }    
      public function out_go_item_report_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_out_go_item_report($this->data['uhotels'],$dt_att,$custom_search,'');
          $data          = array();
          $i = 1;
            foreach($rows as $row) {
              if ($row['delivered']==1){$color='#000000';}else{$color='#b10909';}
              $return_date = (isset($row['new_changed_date'])?$row['new_changed_date']:$row['re_date']);
              if ($row['delivered_date']){$finish_date = $row['delivered_date'];}else{$finish_date = date("Y-m-d");}
              if ($finish_date > $return_date) {
                  $start_date = new DateTime(date($return_date));
                  $end_date   = new DateTime(date($finish_date));
                  $dd = date_diff($start_date,$end_date);
                  if ($dd->m>0) {
                         $delayed = "$dd->m month $dd->d day";
                    }else{
                         $delayed = "$dd->d day";
                  }
                }else{
                         $delayed = '';
              }
              $arr = array(); 
                $arr[] = '<span style="color:'.$color.';padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>Hotel Name: </strong><span>'.$row['hotel_name'].'</span><br>
                          <strong>Department: </strong><span>'.$row['dep_name'].'</span><br>
                          <strong>Address: </strong><span>'.$row['address'].'</span><br>
                          <strong>Form No: </strong>
                          <span><a target="_blank" class="text-cyan" href="'.base_url('financial/out_go/view/'.$row['out_id']).'">'.$row['out_id'].'</a></span>';
                $arr[] = '<span style="color:'.$color.'">'.$row['description'].'</span>';    
                $arr[] = '<span style="color:'.$color.'">'.$row['remarks'].'</span>';    ;
                $arr[] = '<span style="color:'.$color.'">'.$row['quantity'].'</span>';  
                $arr[] = '<span style="color:'.$color.'">'.$row['recieved'].'</span>';
                $arr[] = '<span style="color:'.$color.'">'.$row['date'].'</span>';  
                $arr[] = '<span style="color:'.$color.'">'.$row['re_date'].'</span>
                          '.(isset($row['new_changed_date'])?'<strong>Changed To '.$row['new_changed_date'].'</strong>':'').'';
                $arr[] = '<span style="color:'.$color.'">'.$row['delivered_date'].'</span>';
                $arr[] = '<span style="color:'.$color.'">'.$delayed.'</span>';            
                $data[] =$arr;
              $i++;
            }

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_report_out_go_items($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_out_go_item_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
      }
//======= changed return date
      public function generate_changed_return_date_report(){
          $data['table_fun']     = 'out_go_changed_return_date_generator';
          $this->load->view('reports/financial/out_go_changed_return_date_report',$data);
      }    
      public function out_go_changed_return_date_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_changed_return_date_report($this->data['uhotels'],$dt_att,$custom_search,'');
          $data          = array();
          $i = 1;
            foreach($rows as $row) {
              $changed_data = $this->Reports_model->get_changed_return_date($row['id']);
              $arr = array(); 
                $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>Hotel Name: </strong><span>'.$row['hotel_name'].'</span><br>
                          <strong>Department: </strong><span>'.$row['dep_name'].'</span><br>
                          <strong>Address: </strong><span>'.$row['address'].'</span><br>
                          <strong>Form No: </strong>
                          <span><a target="_blank" class="text-cyan" href="'.base_url('financial/out_go/view/'.$row['id']).'">'.$row['id'].'</a></span>';
                $arr[] = '<span>'.$row['timestamp'].'</span>';    
                $arr[] = '<span>'.$row['date'].'</span>';    ;
                $arr[] = '<span>'.$row['re_date'].'</span>';  
                $arr[] = '<span>'.$row['out_with'].'</span>';
                $arr[] = '<span>'.implode('<br>', $changed_data['dates']).'</span>';  
                $arr[] = '<span>'.implode('<br>', $changed_data['reasons']).'</span>';      
                $data[] =$arr;
              $i++;
            }

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_report_changed_return_date($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_changed_return_date_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }
/*
======================================== Front Office Reports ===============================================
========================= amenitys reports  
=============== amenity report
*/

      public function generate_amenity_report(){
          $data['table_fun']     = 'amenity_report_generator';
          $this->load->view('reports/front_office/amenity_report',$data);
      }    

      public function amenity_report_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_amenity_report($this->data['uhotels'],$dt_att,$custom_search,'');
          $data          = array();
          $i = 1;
            foreach($rows as $row) {
              $refls = $this->Reports_model->get_amenities_refl($row['amen_id']);
              $amenities = array();
              if ($row['other_ids']) {
                  foreach (json_decode($row['other_ids']) as $arr) {
                     array_push($amenities, $arr);
                   }
               }
              $all_amenities_ids = array_merge(array_values($amenities),array_values($refls));   
              $countes= array_count_values($all_amenities_ids);
              $all_amenities = $this->Reports_model->get_meta_data_by_ids($all_amenities_ids);
              $names  = array();
              foreach ($all_amenities as $amen) {
                  array_push($names, $amen['type_name']);
               }
              $arr = array(); 
                $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>Hotel Name: </strong><span>'.$row['hotel_name'].'</span><br>
                          <strong>Delivery Time: </strong><span>'.$row['date_time'].'</span><br>
                          <strong>Guest Name: </strong><span>'.$row['guest'].'</span><br>
                          <strong>Loctaion: </strong><span>'.$row['location'].'</span><br>
                          <strong>Type: </strong><span>'.$row['amenity_type'].'</span><br>
                          '.(!empty($row['reason'])?'<strong>Reason: </strong><span>'.$row['reason'].'</span><br>':'').'
                          <strong>Form No: </strong>
                          <span><a target="_blank" class="text-cyan" href="'.base_url('front_office/amenity/view/'.$row['amen_id']).'">'.$row['amen_id'].'</a></span>';
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['room'].'</span>';   
                $arr[] = '<strong>Adult: </strong><span>'.$row['pax'].'</span><br>
                          <strong>Child: </strong><span>'.$row['child'].'</span><br>';  
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['amenity_treatment'].'</span>';
                $arr[] = '<span style="color:'.$row['status_color'].'" class="float-right">'.implode('<br>',$countes).'</span>';
                $arr[] = '<span style="color:'.$row['status_color'].'">'.implode('<br>',$names).'</span>';
                $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';            
                $data[] =$arr;
              $i++;
            }

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_amenity_report($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_amenity_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }


//======= amenity summary report

      public function generate_amenity_summary_report(){
          $data['table_fun']     = 'amenity_summary_report_generator';
          $this->load->view('reports/front_office/amenity_summary_report',$data);
      }    

      public function amenity_summary_report_generator(){     
          $dt_att              = $this->datatables_att();
          $custom_search       = $this->input->post('searchBy');
          $treatments          = $this->Reports_model->get_all_treatments_in($this->data['uhotels'],$custom_search);
          $rooms_count         = $this->Reports_model->get_rooms_count($this->data['uhotels'],$custom_search);
          $amenity_treatments  = $this->General_model->get_meta_data('amenity_other');
          $rooms  = 0;
          foreach ($rooms_count as $room) {
            $rooms += $room['room_count'];
           }
          $data             = array();
          $i = 1;
          $room_count_row   = array();
          $room_count_row[] = '<strong class="text-dark font-18">Total Rooms: '.$rooms.'</strong>';   
          $room_count_row[] = '<span></span>';  
          $room_count_row[] = '<span></span>';
          $room_count_row[] = '<span></span>';
          $room_count_row[] = '<span></span>'; 
          $data[]           = $room_count_row;
          foreach($treatments as $row) {
            $treatment_row = array(); 
              $treatment_row[]  = '<strong>'.$row['amenity_treatment'].'</strong>';
              $treatment_row[]  = '<strong>'.$row['treatment_count'].'</strong>';   
              $treatment_row[]  = '<strong>'.$row['adult_pax_count'].'</strong>';  
              $treatment_row[]  = '<strong>'.$row['child_pax_count'].'</strong>';      
              $data[] = $treatment_row;
            $i++;
          }
           $amenitys_details = array(); 
              $amenitys_details[] = '<strong class="text-danger"></strong>';
              $amenitys_details[] = '<strong class="text-dark font-18">Amenitys Details</strong>';
              $amenitys_details[] = '<span></span>'; 
              $amenitys_details[] = '<span></span>';
            $data[] =$amenitys_details;
          foreach($amenity_treatments as $amenity) {
            $amenity_counter = $this->Reports_model->get_amenity_count($this->data['uhotels'],$custom_search,$amenity['id']);
              $amen_row = array(); 
              if ($amenity_counter['amenity_count']>0) {
                  $amen_row[] = '<strong class="text-danger">'.$amenity['type_name'].'</strong>';
                  $amen_row[] = '<strong class="text-danger">'.$amenity_counter['amenity_count'].'</strong>';
                  $amen_row[] = '<span></span>'; 
                  $amen_row[] = '<span></span>';
                $data[] =$amen_row;
               }
            $i++;
          }

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_amenity_report($this->data['uhotels'])),
              "recordsFiltered" => count($this->Reports_model->get_all_treatments_in($this->data['uhotels'],$custom_search)),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }




//========================================== Workshop Reports =============================================

//================ workshop report

      public function generate_workshop_report(){
          $data['table_fun']     = 'workshop_report_generator';
          $this->load->view('reports/maintenance/workshop_report',$data);
      }    

      public function workshop_report_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_workshop_report($this->data['uhotels'],$dt_att,$custom_search,'');
          $data          = array();
          $i = 1;
          $total_orders       = 0;
          $total_actual_cost  = 0;
            foreach($rows as $row) {
              $req_items           = $this->Reports_model->get_workshop_items($row['id']);
              $order_items         = $this->Reports_model->get_workshop_order_items($row['order_id']);
              $req_items_names     = array();
              $req_items_ready     = array();
              $req_items_recieve   = array();
              $order_items_names   = array();
              $order_price         = 0;
              $req_counter         = 1;
              $order_counter       = 1;
              foreach ($req_items as $req_item) {
                $req_items_names[]    = $req_counter.'. <span>'.$req_item['description'].'</span>';
                $ready_time           = (isset($req_item['ready_time'])?new DateTime($req_item['ready_time']):' ');
                $recieve_time         = (isset($req_item['recieve_time'])?new DateTime($req_item['recieve_time']):' ');
                $ready_at             = ($ready_time!=' '?$ready_time->format('Y-m-d'):''); 
                $recieved_at          = ($recieve_time!=' '?$recieve_time->format('Y-m-d'):'');  
                $req_items_ready[]    = '<span>'. $ready_at.'</span>';
                $req_items_recieve[]  = '<span>'.$recieved_at.'</span>';
                $req_counter++;
               }
               if ($order_items){
                   foreach ($order_items as $order_item) {
                    $order_items_names[]  = $order_counter.'. <span>'.$order_item['description'].'</span>';
                    $order_price += $order_item['price'];
                    $order_counter++;
                   }
               }
              $arr = array(); 
                $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>'.$row['hotel_name'].'</strong><br>
                          <strong>Type</strong><span>'.$row['workshop_type'].'</span><br>
                          <strong>Form No: </strong>
                          <span><a target="_blank" class="text-cyan" href="'.base_url('maintenance/workshop/view/'.$row['id']).'">'.$row['id'].'</a></span>';    
                $arr[] = '<span>'.implode('<br>',$req_items_names).'</span>';    
                $arr[] = '<span>'.implode('<br>',$order_items_names).'</span>';
                $arr[] = '<span>'.$order_price.'</span>';  
                $arr[] = '<span>'.$row['actual_cost'].'</span>';
                $arr[] = '<span>'.implode('<br>',$req_items_ready).'</span>';  
                $arr[] = '<span>'.implode('<br>',$req_items_recieve).'</span>';
                $arr[] = '<span>'.implode('<br>',$req_items_recieve).'</span>';         
                $data[] =$arr;
              $i++;
              $total_orders       += $order_price;
              $total_actual_cost  += $row['actual_cost'];
            }
            $totals_row = array(); 
              $totals_row[] = '<span></span>';
              $totals_row[] = '<span></span>';    
              $totals_row[] = '<strong>Total Workshop Sections</strong>';    
              $totals_row[] = '<span></span>';
              $totals_row[] = '<strong>'.$total_orders.'</strong>';  
              $totals_row[] = '<strong>'.$total_actual_cost.'</strong>';
              $totals_row[] = '<span></span>';  
              $totals_row[] = '<span></span>';
              $totals_row[] = '<span></span>';         
              $data[] =$totals_row;

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_report_workshop($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_workshop_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }
                 


//=========================================== Projects Reports ==================================================

//==================== project detailed report

      public function generate_project_detailed_report(){
          $data['table_fun']     = 'project_detailed_report_generator';
          $this->load->view('reports/projects_plans/project_detailed_report',$data);
      }    

      public function project_detailed_report_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_project_detailed_report($this->data['uhotels'],$dt_att,$custom_search,'');
          $data          = array();
          $i = 1;
          $total_budget_egp    = 0;
          $total_budget_usd    = 0;
          $total_budget_eur    = 0;
          $total_budget        = 0;
          $total_cost_egp      = 0;
          $total_cost_usd      = 0;
          $total_cost_eur      = 0;
          $total_cost          = 0;
          $total_true_egp      = 0;
          $total_true_usd      = 0;
          $total_true_eur      = 0;
          $total_true          = 0;
          $total_different     = 0;
            foreach($rows as $row) {
              $color = ''; $different = 0;
              if ($row['true'] > $row['budget']) {
                   $different = (float)$row['true'] - (float)$row['budget'];
                   $color     = 'danger';
              }else{
                   $different = (float)$row['budget']-(float)$row['true'];
                   $color     = 'dark';
              }
              $arr = array(); 
                $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>Hotel Name: </strong><span>'.$row['hotel_name'].'</span><br>
                          <strong>Department: </strong><span>'.$row['dep_name'].'</span><br>
                          <strong>Project Name: </strong><span>'.$row['name'].'</span><br>
                          <strong>Project Type: </strong><span>'.$row['origin_id'].'</span><br>
                          <strong>Project Year: </strong><span>'.$row['year'].'</span><hr>
                          <strong>Status: </strong><span>'.$row['status_name'].'</span><br>
                          '.($row['status']==1?'<strong>Signature On: </strong><span>'.$row['role_name'].'</span><br>':'').'
                          '.($row['reasons']!=''?' <strong>Reasons: </strong><span>'.$row['reasons'].'</span><br>':'').'
                          <strong>Project No: </strong>
                          <span><a target="_blank" class="text-cyan" href="'.base_url('projects_plans/projects/view/'.$row['id']).'">'.$row['id'].'</a></span>';
  
                $arr[] = '<span class="text-'.$color.'">'.money_formater($row['budget_EGP'],'EGP').'</span><br>
                          <span class="text-'.$color.'">'.($row['budget_USD']!=0?money_formater($row['budget_USD'],'USD'):'').'</span><br>
                          <span class="text-'.$color.'">'.($row['budget_EUR']!=0?money_formater($row['budget_EUR'],'EUR'):'').'</span><br>';
                $arr[] = '<span class="text-'.$color.'">'.money_formater($row['budget'],'EGP').'</span>';  

                $arr[] = '<span class="text-'.$color.'">'.money_formater($row['cost_EGP'],'EGP').'</span><br>
                          <span class="text-'.$color.'">'.($row['cost_USD']!=0?money_formater($row['cost_USD'],'USD'):'').'</span><br>
                          <span class="text-'.$color.'">'.($row['cost_EUR']!=0?money_formater($row['cost_EUR'],'EUR'):'').'</span><br>';
                $arr[] = '<span class="text-'.$color.'">'.money_formater($row['cost'],'EGP').'</span>'; 

                $arr[] = '<span class="text-'.$color.'">'.money_formater($row['true_EGP'],'EGP').'</span><br>
                          <span class="text-'.$color.'">'.($row['cost_USD']!=0?money_formater($row['true_USD'],'USD'):'').'</span><br>
                          <span class="text-'.$color.'">'.($row['cost_EUR']!=0?money_formater($row['true_EUR'],'EUR'):'').'</span><br>';
                $arr[] = '<span class="text-'.$color.'">'.money_formater($row['true'],'EGP').'</span>'; 
                  
                $arr[] = '<span class="text-'.$color.'">'.money_formater($different,'EGP').'</span>'; 
                $arr[] = '<span class="text-'.$color.'">'.$row['status_name'].'</span>'; 
                $arr[] = '<span class="text-'.$color.'">'.$row['progress_name'].'</span>'; 

                $arr[] = '<strong>Start At: </strong><span>'.$row['start'].'</span><br>
                          <strong>End At: </strong><span>'.$row['end'].'</span><br>
                          '.(!is_null($row['new_date'])?'<strong>New Date: </strong><span>'.$row['new_date'].'</span><br>':'').'
                          '.($row['reason'] !=''?'<strong>Reason: </strong><span>'.$row['reason'].'</span><br>':'').'
                          '.($row['done_date'] != '0000-00-00 00:00:00'?'<strong>Finished At: </strong><span>'.$row['done_date'].'</span><br>':'').' ';      
                $data[] =$arr;
                $i++;
                $total_budget_egp    += $row['budget_EGP'];
                $total_budget_usd    += $row['budget_USD'];
                $total_budget_eur    += $row['budget_EUR'];
                $total_budget        += $row['budget'];
                $total_cost_egp      += $row['cost_EGP'];
                $total_cost_usd      += $row['cost_USD'];
                $total_cost_eur      += $row['cost_EUR'];
                $total_cost          += $row['cost'];
                $total_true_egp      += $row['true_EGP'];
                $total_true_usd      += $row['true_USD'];
                $total_true_eur      += $row['true_EUR'];
                $total_true          += $row['true'];
                $total_different     += $row['true'];
            }
            $arr = array(); 
                $arr[] = '<span></span>';
                $arr[] = '<strong class="text-danger">Total Budgets And Costs</strong>';   
                $arr[] = '<strong>'.money_formater($total_budget_egp,'EGP').'</strong><br>
                          <strong>'.($total_budget_usd!=0?money_formater($total_budget_usd,'USD'):'').'</strong><br>
                          <strong>'.($total_budget_eur!=0?money_formater($total_budget_eur,'EUR'):'').'</strong>';
                $arr[] = '<strong>'.money_formater($total_budget,'EGP').'</strong>';  
                $arr[] = '<strong>'.money_formater($total_cost_egp,'EGP').'</strong><br>
                          <strong>'.($total_cost_usd!=0?money_formater($total_cost_usd,'USD'):'').'</strong><br>
                          <strong>'.($total_cost_eur!=0?money_formater($total_cost_eur,'EUR'):'').'</strong>';
                $arr[] = '<strong>'.money_formater($total_cost,'EGP').'</strong>'; 
                $arr[] = '<strong>'.money_formater($total_true_egp,'EGP').'</strong><br>
                          <strong>'.($total_cost_usd!=0?money_formater($total_true_usd,'USD'):'').'</strong><br>
                          <strong>'.($total_cost_eur!=0?money_formater($total_true_eur,'EUR'):'').'</strong>';
                $arr[] = '<strong>'.money_formater($total_true,'EGP').'</strong>'; 
                $arr[] = '<strong>'.money_formater($total_different,'EGP').'</strong>'; 
                $arr[] = '<span></span>';      
                $arr[] = '<span></span>';      
                $arr[] = '<span></span>';      
                $data[] =$arr;

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_report_project_detailed($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_project_detailed_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }

//=================== project Summary report

      public function generate_project_summary_report(){
          $data['table_fun']     = 'project_summary_report_generator';
          $this->load->view('reports/projects_plans/project_summary_report',$data);
      }    

      public function project_summary_report_generator(){     
          $dt_att        = $this->datatables_att();
          $custom_search = $this->input->post('searchBy');
          $rows          = $this->Reports_model->get_project_summary_report($this->data['uhotels'],$dt_att,$custom_search,'','summary');
          $data          = array();
          $i = 1;
          $total_budget_egp    = 0;
          $total_budget_usd    = 0;
          $total_budget_eur    = 0;
          $total_budget        = 0;
          $total_cost_egp      = 0;
          $total_cost_usd      = 0;
          $total_cost_eur      = 0;
          $total_cost          = 0;
            foreach($rows as $row) {
              $arr = array(); 
                $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
                $arr[] = '<strong>'.$row['hotel_name'].'</strong>';   
                $arr[] = '<span>'.money_formater($row['total_budget_epg'],'EGP').'</span><br>
                          <span>'.($row['budget_USD']!=0?money_formater($row['total_budget_usd'],'USD'):'').'</span><br>
                          <span>'.($row['budget_EUR']!=0?money_formater($row['total_budget_eur'],'EUR'):'').'</span><br>';
                $arr[] = '<span>'.money_formater($row['total_budget'],'EGP').'</span>';  
                $arr[] = '<span>'.money_formater($row['total_cost_epg'],'EGP').'</span><br>
                          <span>'.($row['cost_USD']!=0?money_formater($row['total_cost_usd'],'USD'):'').'</span><br>
                          <span>'.($row['cost_EUR']!=0?money_formater($row['total_cost_eur'],'EUR'):'').'</span><br>';
                $arr[] = '<span>'.money_formater($row['total_cost'],'EGP').'</span>';       
                $data[] =$arr;
                $i++;
                $total_budget_egp    += $row['total_budget_epg'];
                $total_budget_usd    += $row['total_budget_usd'];
                $total_budget_eur    += $row['total_budget_eur'];
                $total_budget        += $row['total_budget'];
                $total_cost_egp      += $row['total_cost_epg'];
                $total_cost_usd      += $row['total_cost_usd'];
                $total_cost_eur      += $row['total_cost_eur'];
                $total_cost          += $row['total_cost'];
            }
            $arr = array();    
                $arr[] = '<span></span>';     
                $arr[] = '<strong class="text-danger">Total Budgets And Costs</strong>';
                $arr[] = '<strong>'.money_formater($total_budget_egp,'EGP').'</strong><br>
                          <strong>'.($total_budget_usd!=0?money_formater($total_budget_usd,'USD'):'').'</strong><br>
                          <strong>'.($total_budget_eur!=0?money_formater($total_budget_eur,'EUR'):'').'</strong>';
                $arr[] = '<strong>'.money_formater($total_budget,'EGP').'</strong>';  
                $arr[] = '<strong>'.money_formater($total_cost_egp,'EGP').'</strong><br>
                          <strong>'.($total_cost_usd!=0?money_formater($total_cost_usd,'USD'):'').'</strong><br>
                          <strong>'.($total_cost_eur!=0?money_formater($total_cost_eur,'EUR'):'').'</strong>';
                $arr[] = '<strong>'.money_formater($total_cost,'EGP').'</strong>';  
                $data[] =$arr;

          $output = array(
              "draw"            => $dt_att['draw'],
              "recordsTotal"    => count($this->Reports_model->get_all_report_project_summary($this->data['uhotels'])),
              "recordsFiltered" => $this->Reports_model->get_project_summary_report( $this->data['uhotels'],$dt_att,$custom_search,'count','summary'),
              "data"            => $data
               );
          echo json_encode($output);
          exit();
        }


	}

?>	 