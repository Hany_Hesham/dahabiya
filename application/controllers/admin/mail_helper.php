<?php

	if (!function_exists('emailIt')){
	    function emailIt($module_id, $form_id, $emails, $message){
	    	$CI = get_instance();
	      	$CI->load->model('admin/Modules_model');
          	$CI->load->library('email');
          	$CI->load->helper('url');
      		$module = $CI->Modules_model->get_module_by($module_id);
      		$url = base_url().$module['link'].'/view/'.$form_id;
          	$CI->email->from('alerts@sunrise-resorts.com');
          	$CI->email->to($emails);
          	$CI->email->cc('hany.hisham@sunrise-resorts.com');
          	$CI->email->subject("{$module['name']} NO.#{$form_id}");
          	$CI->email->message("Dear Sirs,
	            <br/>
	            {$module['name']} NO.#{$form_id} {$message}, Please use the link below:
	            <a href='{$url}' target='_blank'>{$url}</a>
	            <br/>
          	"); 
          	//echo $CI->email->print_debugger();
          	$mail_result = $CI->email->send();
          	return $mail_result;
        }
    }

    if (!function_exists('notify_signers')){
	    function notify_signers($module_id, $form_id, $typed = FALSE) {
		    $CI = get_instance();
          	$CI->load->library('email');
          	$CI->load->helper('url');
      		$CI->load->model('admin/Modules_model');
	      	$module = $CI->Modules_model->get_module_by($module_id);
      		$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
          	$url = $module['link'].'/view/'.$form_id;
		    $message = "{$module['name']} No. #{$form_id}: {$url}";
	      	$signers = getSigners($module_id, $form_id, $typed);
		    $notified = FALSE;
		    $reason = '';
		    $signers = array_values($signers);
	      	foreach ($signers as $key => $signer) {
	      		$next = $key + 1;
	      		$last = $key - 1;
	        	if (isset($signer['queue']) && $signer['queue']) {
	        		$CI->Sign_model->updateNextSign($module['tdatabase'], $form_id, $signer['role_id']);
	          		$notified = TRUE;
	          		foreach ($signer['queue'] as $uid => $user) {
	            		//($message, $user['channel']);
	            		if ($signer['queue']['reason']) {
	            			$reason = 'It has been reback for '.$signer['queue']['reason'];
	            		}else{
	            			$reason = '';
	            		}
	            		if ($uid != 'reason') {
	            			if ((isset($signers[$last]['reject']) && ($signers[$last]['reject'] == 0)) || (!isset($signers[$last]['reject']))) {
		            			if (isset($form['hid'])) {
		            				notify($module_id, $uid, $form['hid'], null, $module['name'].' Signature '.$form_id.'', 'Please view and take required action', $url, $form_id, $module['name'].' Signature', $reason.'<br/>Please view and take required action', $signer['role_id'], 4);
		            			}else{
		            				notify($module_id, $uid, null, null, $module['name'].' Signature '.$form_id.'', 'Please view and take required action', $url, $form_id, $module['name'].' Signature', $reason.'<br/>Please view and take required action', $signer['role_id'], 4);
		            			}
	            			}
	            		}
	          		}
	          		break;
	        	}else{
	        		if (!isset($signers[$next])) {
		        		$CI->Sign_model->updateNextSign($module['tdatabase'], $form_id, 0);
		        		$CI->Sign_model->updateState($module['tdatabase'], $form_id, 2);
		        		
		        		//notify_signed($module_id, $form_id, $typed);
		        		//notify($module_id, $form['uid'], $form['hid'], null, $module['name'].' Approved '.$form_id.'', 'Your request has been approved', $url, $form_id, $module['name'].' Approved', $reason.'<br/>Please view and take required action', $signer['role_id'], 4);
		        	}
	        	}
	      	}
	    }
	}

	if (!function_exists('notify_signed')){
	    function notify_signed($module_id, $form_id, $typed = FALSE) {
		    $CI = get_instance();
          	$CI->load->library('email');
      		$CI->load->model('admin/Modules_model');
	      	$module = $CI->Modules_model->get_module_by($module_id);
      		$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
          	$url = $module['link'].'/view/'.$form_id;
	      	$signers = getSigners($module_id, $form_id, $typed);
	      	$signers = array_reverse($signers);
	      	unset($signers[0]);
	      	$signers = array_reverse($signers);
		    $notified = FALSE;
	      	foreach ($signers as $signer) {
	        	if (isset($signer['sign'])) {
	            	notify($module_id, $signer['sign']['id'], $form['hid'], null, $module['name'].' Edit '.$form_id.'', 'Please view Modifications', $url, $form_id, $module['name'].' Edit', 'Please view Modifications', null, 4);
	          	}
	      	}
	    }
	}

	if (!function_exists('notify_reject')){
	    function notify_reject($module_id, $form_id, $typed = FALSE) {
		    $CI = get_instance();
          	$CI->load->library('email');
      		$CI->load->model('admin/Modules_model');
	      	$module = $CI->Modules_model->get_module_by($module_id);
      		$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
          	$url = $module['link'].'/view/'.$form_id;
	      	$signers = getSigners($module_id, $form_id, $typed);
		    $notified = FALSE;
	      	foreach ($signers as $signer) {
	        	if (isset($signer['sign'])) {
	        		if ($signer['sign']['reject']) {
	            		notify($module_id, $form['uid'], $form['hid'], null, $module['name'].' Reject '.$form_id.'', 'Please view and take required action', $url, $form_id, $module['name'].' Reject', 'Please view and take required action', null, 4);
	            	}
	          	}
	      	}
	    }
	}

?>