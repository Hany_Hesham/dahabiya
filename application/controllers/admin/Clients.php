<?php

	defined('BASEPATH') OR exit('No direct script access allowed');



	class Clients extends MY_Controller {



		public function __construct(){

			parent::__construct();

            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/Clients_model');
	            $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
				$this->data['username']     = $this->global_data['sessioned_user']['username'];
				$this->data['module']       = $this->Modules_model->get_module_by('','العملاء ');
	            $this->data['permission']   = user_access($this->data['module']['id']);
	            $this->data['ubranches']    = get_ubranches($this->data['user_id']);
		  }

        public function index(){

		       access_checker($this->data['permission']['view'],0,0,0,0,'admin/dashboard');	

		        $data['assumed_id']    = get_file_code('','C');

		        $data['uploads']       = $this->General_model->get_file(4,$data['assumed_id']);

				$data['view']          = 'admin/clients/clients_index';

				$this->load->view('admin/includes/layout',$data);

			  }  


	     public function clients_ajax(){
	           
	          $dt_att  = $this->datatables_att();
	          $clients   = $this->Clients_model->get_clients($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	          $i=1;
	           foreach($clients as $client) {
	               $arr = array(); 
	               $tools=array();
	                if ($this->data['permission']['remove'] == 1 && $client['deleted']==0) {
	                      $tools[] = '<div class="float-right"><a href="javascript: void(0);" onclick="del('.$client['id'].',\'العميل   \',\'clients\',\'admin/clients\',\'del\')" titel="Delete client">
				                          <span style="color:red;"> إغلاق </span>
				                     </a> </div> ';
	                      }elseif($this->data['permission']['remove'] == 1 && $client['deleted']==1){
                           $tools[] = '<div class="float-right"><a href="javascript: void(0);" onclick="reOpen('.$client['id'].',\'العميل   \',\'clients\',\'admin/clients\',\'reopen\')">
					                          <span style="color:red;"> إعادة فتح </span>
					                     </a> </div> ';
	                      }
	                    $arr[] = '<span id="cont_email" class="float-right">'.$client['cont_email'].'</span>';  
	                    $arr[] = '<span id="address" class="info float-right">'.$client['address'].'</span>';
	                    $arr[] = '<span id="contact" class="float-right">'.$client['contact'].'</span>';
	                    $arr[] = '<span id="phone" class="info float-right">'.$client['phone'].'</span>';
	                    $arr[] = '<a  href="'.base_url('admin/clients/client/'. $client['id']).'" class="float-right" titel="View client"><span id="supp_name" class="info">'.$client['client_name'].'</span></a><br>'.implode("", $tools);
	                    $arr[] = '<span id="supp_id" style="padding-left:7%;" class="info float-right">'.$i.'</span>';
	                   
	                    $data[] =$arr;
	                    $i++;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Clients_model->get_all_clients()),
	                     "recordsFiltered" => $this->Clients_model->get_clients($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }


        public function client_process(){

			$data = [

			         'client_name'            => $this->input->post('client_name'),

			         'phone'                  => $this->input->post('phone'),

			         'address'                => $this->input->post('address'),

			         'contact'                => $this->input->post('contact'),

			         'cont_email'             => $this->input->post('cont_email')
			        
			        ];

				if (!$this->input->post('id')) {

					$supp_id = $this->Clients_model->add_client($data);
				   
				    if($supp_id){

				     $this->General_model->update_files($this->input->post('assumed_id'),4,$supp_id);	
				  	 
				  	 $this->session->set_flashdata(['alert'=>'نجاح','msg'=>'لقد تمت الإضافة بنجاح!']);

				  	 loger('create',4,'clients',$supp_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'created client no '.$supp_id.'');

		             redirect('admin/clients');

				     }
				     
				   }else{

	                $client   = $this->Clients_model->get_client($this->input->post('id'));
				   	
				   	$updated    = $this->Clients_model->update_client($this->input->post('id'),$data);

				   	if ($updated) {
				   		
				   	   $this->session->set_flashdata(['alert'=>'نجاح','msg'=>'لقد تم التعديل بنجاح!']);

				   	    loger('update',4,'clients',$client['id'],0,json_encode($client, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated client no '.$client['id'].'');

				   	 }


		               redirect('admin/clients/client/'.$this->input->post('id'));

			   }      
		  
	       }


	    public function client($supp_id){

          $data['client']  = $this->Clients_model->get_client($supp_id);

          access_checker(0,$this->data['permission']['view'],0,0,0,0,'admin/clients');

	         if ($data['client']['deleted'] == 1) {

	         	 $this->session->set_flashdata(['alert'=>'Sorry','msg'=>'This Record has been Deleted before!']);
	         	
	         	 redirect('admin/clients');
	           
	           }

	            $data['uploads']           = $this->General_model->get_file(4,$supp_id);

                $data['image_link']        =  base_url('assets/images/big/auth-bg.jpg"');

	            $data['file_path']         = base_url('assets/uploads/clients');

				$data['view'] = 'admin/clients/client_view';

			    $this->load->view('admin/includes/layout',$data);
	      }


	     public function client_transactions($supp_id){
	           
	          $dt_att         = $this->datatables_att();
	          $transactions   = $this->Clients_model->get_client_transactions($supp_id,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	          $total_po    = 0;
	          $total_payed = 0;
	          $total_diff  = 0;
	           foreach($transactions as $trans) {
	               $arr = array(); 
                      $arr[] = '<span class="info float-right">'.(float)((float)$trans['total_cost']-(float)$trans['payed']).'</span>';  
                      $arr[] = '<span class="info float-right">'.(float)$trans['payed'].'</span>';    
                      $arr[] = '<span class="info float-right">'.(float)$trans['total_cost'].'</span>';
                      $arr[] = '<span class="info float-right">'.$trans['status_name'].'</span>';
                      $arr[] = '<span class="info float-right">'.$trans['delivery_date'].'</span>';
	                  $arr[] = '<a  href="'.base_url('prs/pr/'. $trans['id']).'" class="info float-right">'.$trans['serial'].'</span></a>';
	                 $data[] =$arr;
	                 $total_po    += (float)$trans['total_cost'];
	                 $total_payed += (float)$trans['payed'];
	                 $total_diff  += (float)((float)$trans['total_cost']-(float)$trans['payed']);
	                }
	                 $totals_arr = array(); 
                      $totals_arr[] = '<strong class="info float-right text-danger">'.$total_diff.'</strong>';  
                      $totals_arr[] = '<strong class="info float-right text-danger">'.$total_payed.'</strong>';    
                      $totals_arr[] = '<strong class="info float-right text-danger">'.$total_po.'</strong>';
                      $totals_arr[] = '<strong class="info float-right font-20 text-danger">الإجمالى  </strong>';
                      $totals_arr[] = '<span class="info float-right"></span>';
	                  $totals_arr[] = '<span class="info float-right"></span>';
	                 $data[] =$totals_arr;
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Clients_model->get_all_client_transactions($supp_id)),
	                     "recordsFiltered" => $this->Clients_model->get_client_transactions($supp_id,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }           



	    public function del($id){
            
           access_checker(0,0,0,0,$this->data['permission']['remove'],0,'admin/clients');

            $client   = $this->Clients_model->get_client($id);

	        $this->db->update('clients', array('deleted' => 1), "id = ".$id);

         	loger('إغلاق',$this->data['module']['id'],'clients',$id,0,0,0,0,0,'لقد تم إغلاق '.$client['supp_name'].'');

	        $this->session->set_flashdata(['alert'=>'نجاح','msg'=>'لقد تم الإغلاق بنجاح!']);

		  }

		public function reopen($id){
            
          access_checker(0,0,0,0,$this->data['permission']['remove'],0,'admin/clients');

            $client   = $this->Clients_model->get_client($id);

	        $this->db->update('clients', array('deleted' => 0), "id = ".$id);

         	loger('إعادة فتح',$this->data['module']['id'],'clients',$id,0,0,0,0,0,'إعادة فتح  '.$client['supp_name'].'');

	        $this->session->set_flashdata(['alert'=>'نجاح','msg'=>'لقد تم إعادة الفتح!']);

		}






	}

?>	 