<?php

	defined('BASEPATH') OR exit('No direct script access allowed');



	class Companies extends MY_Controller {



		public function __construct(){

			parent::__construct();

            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/Companies_model');
	            $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
				$this->data['username']     = $this->global_data['sessioned_user']['username'];
				$this->data['module']       = $this->Modules_model->get_module_by('','الإعدادات  ');
	            $this->data['permission']   = user_access($this->data['module']['id']);
	            $this->data['ubranches']    = get_ubranches($this->data['user_id']);
		  }

        public function index(){

		       access_checker($this->data['permission']['view'],0,0,0,0,'admin/dashboard');	

				$data['view']          = 'admin/backend/companies_index';

				$this->load->view('admin/includes/layout',$data);

			  }  


	     public function companies_ajax(){
	           
	          $dt_att  = $this->datatables_att();
	          $companies   = $this->Companies_model->get_companies_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	           foreach($companies as $company) {
	               $arr = array(); 
	               $tools=array();

	                    $arr[] = '<span id="companyRank" class="badge badge-danger float-right" >'.$company['rank'].'</span>'; 
	                    $arr[] = '<a data-toggle="modal" href="#smallmodal" id="dep" class="edit-dep float-right"><strong id="companyName" class="info" style="font-size:14px;">'.$company['company_name'].'</strong></a>';
	                    $arr[] = '<span class="info float-right" id="companyId">'.$company['id'].'</span>';
	                   
	                    $data[] =$arr;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Companies_model->get_all_companies()),
	                     "recordsFiltered" => $this->Companies_model->get_companies_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }


        public function company_process(){

			$data = [

			         'company_name'        => $this->input->post('company_name'),

			         'rank'                => $this->input->post('rank'),
			        
			        ];

				if (!$this->input->post('id')) {

					$company_id = $this->Companies_model->add_company($data);
				   
				    if($company_id){
				  	 
				  	 $this->session->set_flashdata(['alert'=>'نجاح','msg'=>'لقد تمت الإضافة بنجاح!']);

				  	 loger('create',$this->data['module']['id'],'companies',$company_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'created company no '.$company_id.'');

		             redirect('admin/companies');

				     }
				     
				   }else{

	                $company      = $this->Companies_model->get_company($this->input->post('id'));
				   	
				   	$updated    = $this->Companies_model->update_company($this->input->post('id'),$data);

				   	if ($updated) {
				   		

				   	    loger('update',$this->data['module']['id'],'companys',$company['id'],0,json_encode($company, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated company no '.$company['id'].'');

				   	   $this->session->set_flashdata(['alert'=>'نجاح','msg'=>'لقد تم التعديل بنجاح!']);

		               redirect('admin/companies');

				   	 }



			   }      
		  
	       }

	}

?>	 