<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Modules extends MY_Controller {

		public function __construct(){
	      	parent::__construct();
	      	$this->load->model('admin/Modules_model');
	      	$this->load->model('admin/Hotels_model');
	      	$this->load->model('admin/Departments_model');
	      	$this->data['module']        = $this->Modules_model->get_module_by(104);
	      	$this->data['permission']    = user_access($this->data['module']['id']);
	    }

        public function index(){
		    access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');	
			$data['view']          = 'admin/modules/modules_index';
			$this->load->view('admin/includes/layout',$data);
    	} 

	    public function modules_ajax(){
	    	$dt_att               = $this->datatables_att();
		    $dt_att['module_id']  = $this->data['module']['id'];
		    $rows                 = $this->Modules_model->get_modulesz($dt_att, '');
		    $data = array();
	        foreach($rows as $row) {
	            $arr = array(); 
        		$tools=array();
		        if ($this->data['permission']['edit'] == 1) {   
		          	$tools[] = '<span class="wait-hover"><a href="javascript:void(0);" onclick="getmodule('.$row['id'].')"><span style="color:blue;">Edit</span></a> | ';
		        }
	            if ($this->data['permission']['view'] == 1) {
	                $tools[] .='<a href="javascript:void(0);" onclick="getsignature('.$row['id'].')"><span style="color:green;">View</span></a>'; 
	            }
	            $arr[] = '<span class="info" id="moduleId'.$row['id'].'">'.$row['id'].'</span>';
	            $arr[] = '<div class=" after-hover"><strong id="moduleName'.$row['id'].'"  style="font-size:14px;">'.$row['name'].'</strong><br>'.implode("", $tools).'</div>';
	            $arr[] = '<span id="menu_group" class="badge badge-danger" >'.$row['menu_group'].'</span>
	            	<span id="g_view'.$row['id'].'" class="hidden" >'.$row['g_view'].'</span>
	            	<span id="view'.$row['id'].'" class="hidden" >'.$row['view'].'</span>
	            	<span id="private_view'.$row['id'].'" class="hidden" >'.$row['private_view'].'</span>
	            	<span id="creat'.$row['id'].'" class="hidden" >'.$row['creat'].'</span>
	            	<span id="edit'.$row['id'].'" class="hidden" >'.$row['edit'].'</span>
	            	<span id="remove'.$row['id'].'" class="hidden" >'.$row['remove'].'</span>
	            	<span id="f_change'.$row['id'].'" class="hidden" >'.$row['f_change'].'</span>
	            	<span id="link'.$row['id'].'" class="hidden" >'.$row['link'].'</span>
	            	<span id="view_link'.$row['id'].'" class="hidden" >'.$row['view_link'].'</span>
	            	<span id="tdatabase'.$row['id'].'" class="hidden" >'.$row['tdatabase'].'</span>
	            	<span id="type'.$row['id'].'" class="hidden" >'.$row['type'].'</span>
	            	<span id="menu_icon'.$row['id'].'" class="hidden" >'.$row['menu_icon'].'</span>
	            	<span id="text_color'.$row['id'].'" class="hidden" >'.$row['text_color'].'</span>
	            	<span id="background_color'.$row['id'].'" class="hidden" >'.$row['background_color'].'</span>
	            	<span id="menu_group_icon'.$row['id'].'" class="hidden" >'.$row['menu_group_icon'].'</span>
	            	<span id="rank'.$row['id'].'" class="hidden" >'.$row['rank'].'</span>
	            '; 
	            if ($row['deleted'] ==1) {
                	$arr[] ='<label class="customcheckbox">
                          <input type="hidden" value="0" name="disabled" class="listCheckbox">
                            <input type="checkbox" name="disabled" class="listCheckbox"
                                value="1" class="switch-input" checked  onclick="changes('.$row['id'].',\'Module\',\'modules\',\'deleted\',\'0\',\'status_change\')">
                             <span class="checkmark"></span>
                          </label>';
              	}else{
              		$arr[] ='<label class="customcheckbox">
                          <input type="hidden" value="0" name="disabled" class="listCheckbox">
                            <input type="checkbox" name="disabled" class="listCheckbox"
                                value="1" class="switch-input" onclick="changes('.$row['id'].',\'Module\',\'modules\',\'deleted\',\'1\',\'status_change\')">
                            <span class="checkmark"></span>
                         </label>';
              	}
	            $data[] =$arr;
	        }
	        $output = array(
       			"draw" => $dt_att['draw'],
       			"recordsTotal"    => count($this->Modules_model->get_all_modules()),
       			"recordsFiltered" => $this->Modules_model->get_modules($dt_att, 'count'),
       			"data" => $data
      		);
	        echo json_encode($output);
	        exit();
        }

        public function sign_index($module_id){
		    access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');	
		    $data['module_id']          = $module_id;
			$data['base_units']         = $this->Modules_model->get_all_modules();
			$data['hotels']           	= $this->Hotels_model->get_hotels();
      		$data['departments']   		= $this->Departments_model->get_all_departments();
      		$data['roles']   			= $this->Modules_model->get_allroles();
			$this->load->view('admin/modules/sign_index',$data);
    	} 

	    public function sign_ajax($modules_id){
	    	$dt_att               = $this->datatables_att();
		    $dt_att['module_id']  = $this->data['module']['id'];
		    $rows                 = $this->Modules_model->get_signs($modules_id, $dt_att, '');
	        $data = array();
	        foreach($rows as $row) {
	            $arr = array(); 
	            $tools=array();
	            if ($this->data['permission']['view'] == 1) {
	                $tools[] .='<a href="javascript:void(0);" onclick="getsignroles('.$row['id'].')"><span style="color:green;">Roles</span></a>'; 
	            }
	            $arr[] = '<span class="info" id="unitId">'.$row['id'].'</span>';
	            $arr[] = '<a id="'.$row['id'].'" data-toggle="modal" href="#smallmodal" class="edit-type">
	                <strong id="signName"  style="font-size:14px;">'.$row['name'].'</strong>
	            </a><br>'.implode("", $tools).'
	            	<span id="sign_id" class="hidden" >'.$row['id'].'</span>
	            	<span id="sign_name" class="hidden" >'.$row['name'].'</span>
	            	<span id="modules_id" class="hidden" >'.$row['module_id'].'</span>
	            	<span id="hid" class="hidden" >'.$row['hotel_id'].'</span>
	            	<span id="h_condition" class="hidden" >'.$row['hotel_condition'].'</span>
	            	<span id="did" class="hidden" >'.$row['dep_code'].'</span>
	            	<span id="d_condition" class="hidden" >'.$row['department_condition'].'</span>
	            	<span id="priority" class="hidden" >'.$row['priority'].'</span>
	            	<span id="special" class="hidden" >'.$row['special'].'</span>
	            	<span id="special_condition" class="hidden" >'.$row['special_condition'].'</span>
	            	<span id="special_value" class="hidden" >'.$row['special_value'].'</span>
	            	<span id="special1" class="hidden" >'.$row['special1'].'</span>
	            	<span id="special_condition1" class="hidden" >'.$row['special1_condition'].'</span>
	            	<span id="special_value1" class="hidden" >'.$row['special1_value'].'</span>
	            	<span id="special2" class="hidden" >'.$row['special2'].'</span>
	            	<span id="special_condition2" class="hidden" >'.$row['special2_condition'].'</span>
	            	<span id="special_value2" class="hidden" >'.$row['special2_value'].'</span>
	            	<span id="special3" class="hidden" >'.$row['special3'].'</span>
	            	<span id="special_condition3" class="hidden" >'.$row['special3_condition'].'</span>
	            	<span id="special_value3" class="hidden" >'.$row['special3_value'].'</span>
	            '; 
	            if ($row['deleted'] ==1) {
                	$arr[] ='<label class="customcheckbox">
                          <input type="hidden" value="0" name="disabled" class="listCheckbox">
                            <input type="checkbox" name="disabled" class="listCheckbox"
                                value="1" class="switch-input" checked  onclick="changes('.$row['id'].',\'Type\',\'sign_types\',\'deleted\',\'0\',\'status_changer\')">
                             <span class="checkmark"></span>
                          </label>';
              	}else{
              		$arr[] ='<label class="customcheckbox">
                          <input type="hidden" value="0" name="disabled" class="listCheckbox">
                            <input type="checkbox" name="disabled" class="listCheckbox"
                                value="1" class="switch-input" onclick="changes('.$row['id'].',\'Type\',\'sign_types\',\'deleted\',\'1\',\'status_changer\')">
                            <span class="checkmark"></span>
                         </label>';
              	}
	            $data[] =$arr;
	        }
	        $output = array(
	            "draw" => $dt_att['draw'],
	            "recordsTotal"    => count($this->Modules_model->get_all_signs($modules_id)),
	            "recordsFiltered" => $this->Modules_model->get_signs($modules_id, $dt_att, 'count'),
	            "data" => $data
	        );
	        echo json_encode($output);
	        exit();
        }

        public function modules_process(){
			$fdata = [
			    'name'            	=> $this->input->post('name'),
			    'link'              => $this->input->post('link'),
			    'view_link'         => $this->input->post('view_link'),
			    'tdatabase'         => $this->input->post('tdatabase'),
			    'type'              => $this->input->post('type'),
			    'menu_icon'         => $this->input->post('menu_icon'),
			    'text_color'        => $this->input->post('text_color'),
			    'background_color'  => $this->input->post('background_color'),
			    'menu_group'        => $this->input->post('menu_group'),
			    'menu_group_icon'   => $this->input->post('menu_group_icon'),
			    'rank'              => $this->input->post('rank'),
			];
        	if ($this->input->post('g_view')) {
        		$fdata['g_view']   		= $this->input->post('g_view');
        	}else{
        		$fdata['g_view'] =0;
        	}
        	if ($this->input->post('view')) {
        		$fdata['view']    		= $this->input->post('view');
        	}else{
        		$fdata['view'] =0;
        	}
        	if ($this->input->post('private_view')) {
        		$fdata['private_view'] 	= $this->input->post('private_view');
        	}else{
        		$fdata['private_view'] =0;
        	}
        	if ($this->input->post('creat')) {
        		$fdata['creat'] 		= $this->input->post('creat');
        	}else{
        		$fdata['creat'] =0;
        	}
        	if ($this->input->post('edit')) {
        		$fdata['edit'] 			= $this->input->post('edit');
        	}else{
        		$fdata['edit'] =0;
        	}
        	if ($this->input->post('remove')) {
        		$fdata['remove'] 		= $this->input->post('remove');
        	}else{
        		$fdata['remove'] =0;
        	}
        	if ($this->input->post('f_change')) {
        		$fdata['f_change'] 		= $this->input->post('f_change');
        	}else{
        		$fdata['f_change'] =0;
        	}
			if (!$this->input->post('id')) {
				$module_id = $this->Modules_model->add_module($fdata);
				if($module_id){
				  	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
				  	loger('Create', $this->data['module']['id'], $this->data['module']['name'], $module_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Module #'.$module_id.'');
		            redirect('admin/modules/');
				}
			}else{
	            $module   	= $this->Modules_model->get_module($this->input->post('id'));
				$updated    = $this->Modules_model->update_module($this->input->post('id'),$fdata);
				if ($updated) {
				   	loger('Update', $this->data['module']['id'], $this->data['module']['name'], $module['id'], 0,json_encode($module, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Module #'.$module['id'] );
			   	   	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
				}
	            redirect('admin/modules/');
			}      
	    }

    	public function status_change($id,$column,$value){
            $this->db->update('modules', array($column => $value), "id = ".$id);
            if ($this->input->is_ajax_request()) {
            	if ($value == 1) { $disabled ='Disabled';}else{$disabled ='Enabled';}
	            loger(''.$disabled.'',$this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0, ''.$disabled.' Module #'.$id.'');
           	}
        	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is Updated Successfully!']);
			redirect('admin/modules/');
		}

		public function status_changer($id,$column,$value){
            $this->db->update('sign_types', array($column => $value), "id = ".$id);
            if ($this->input->is_ajax_request()) {
            	if ($value == 1) { $disabled ='Disabled';}else{$disabled ='Enabled';}
	            loger(''.$disabled.'',$this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0, ''.$disabled.' Type #'.$id.'');
           	}
        	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is Updated Successfully!']);
			redirect('admin/modules/');
		}

		public function get_modulecol(){
      		if ($this->input->post()) {
        		$module_id  = $this->input->post('module_id');
      		}
      		$module = $this->Modules_model->get_module_by($module_id);
      		$parts 	= $this->Modules_model->get_columns($module['tdatabase']);
  			$select = '<option value="">Choose a Column</option>';
  			foreach ($parts as $part) {
    			$select .= '<option value="'.$part.'">'.strtoupper($part).'</option>';
  			}      
		    echo json_encode($select);          
		    exit();
    	} 

        public function sign_process(){
			$fdata = [
			    'hotel_id'            		=> $this->input->post('hotel_id'),
			    'hotel_condition' 		    => $this->input->post('hotel_condition'),
			    'module_id'         		=> $this->input->post('module_id'),
			    'name'         				=> $this->input->post('name'),
			    'dep_code'         			=> $this->input->post('dep_code'),
			    'department_condition'		=> $this->input->post('department_condition'),
			    'priority'         			=> $this->input->post('priority'),
			    'special'        			=> $this->input->post('special'),
			    'special_condition'  		=> $this->input->post('special_condition'),
			    'special_value'        		=> $this->input->post('special_value'),
			    'special1'        			=> $this->input->post('special1'),
			    'special1_condition'  		=> $this->input->post('special1_condition'),
			    'special1_value'        	=> $this->input->post('special1_value'),
			    'special2'        			=> $this->input->post('special2'),
			    'special2_condition'  		=> $this->input->post('special2_condition'),
			    'special2_value'        	=> $this->input->post('special2_value'),
			    'special3'        			=> $this->input->post('special3'),
			    'special3_condition'  		=> $this->input->post('special3_condition'),
			    'special3_value'        	=> $this->input->post('special3_value'),
			];
			if (!$this->input->post('id')) {
				$type_id = $this->Modules_model->add_signtype($fdata);
				if($type_id){
				  	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
				  	loger('Create', $this->data['module']['id'], $this->data['module']['name'], $type_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Sign Type #'.$type_id.'');
		            redirect('admin/modules/');
				}
			}else{
	            $type   	= $this->Modules_model->get_signtype($this->input->post('id'));
				$updated    = $this->Modules_model->update_signtype($this->input->post('id'),$fdata);
				if ($updated) {
				   	loger('Update', $this->data['module']['id'], $this->data['module']['name'], $type['id'], 0,json_encode($type, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Sign Type #'.$type['id'] );
			   	   	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
				}
	            redirect('admin/modules/');
			}      
	    }

	    public function role_process($type_id){
	    	foreach ($this->input->post('items') as $key => $item) {
	          if (isset($item['id'])) {
	            $item_data            = $this->Modules_model->get_role($item['id']);
	            $item['type']     	  = $type_id;
	            $item_name            = $item_data['role_name'];
	            if ($item['role'] == 0) {
	            	$this->db->delete('sign_role', array('id' => $item['id']));
	            }
	            $updated              = $this->Modules_model->edit_role($item, $item['id']);
	            if ($updated > 0) {
	              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $type_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Role: '.$item_name);
	            }
	          }else{
	          	if ($item['role'] != 0) {
		            $item['type']     	  = $type_id;
		            $item_id              = $this->Modules_model->add_role($item);
		            if ($item_id) {
		              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $type_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['role'].'');
		            }
		        }
	          }
	        }
			$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
	        redirect('admin/modules/');
	    }

	}

?>	 