<?php

	defined('BASEPATH') OR exit('No direct script access allowed');



	class Suppliers extends MY_Controller {



		public function __construct(){

			parent::__construct();

            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/Suppliers_model');
	            $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
				$this->data['username']     = $this->global_data['sessioned_user']['username'];
				$this->data['module']       = $this->Modules_model->get_module_by('','Suppliers');
	            $this->data['permission']   = user_access($this->data['module']['id']);
	            $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		  }

        public function index(){

		       access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');	

		        $data['assumed_id']    = get_file_code('','S');

		        $data['groups']        = get_hotels_groups();

		        $data['uploads']       = $this->General_model->get_file(4,$data['assumed_id']);

				$data['view']          = 'admin/suppliers/suppliers_index';

				$this->load->view('admin/includes/layout',$data);

			  }  


	     public function suppliers_ajax(){
	           
	          $dt_att  = $this->datatables_att();
	          $suppliers   = $this->Suppliers_model->get_suppliers($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	           foreach($suppliers as $supp) {
	               $arr = array(); 
	               $tools=array();
	                if ($this->data['permission']['remove'] == 1) {
	                      $tools[] = '<div><a href="javascript: void(0);" onclick="del('.$supp['id'].',\'supplier\',\'suppliers\',\'admin/suppliers\',\'del\')" titel="Delete supplier">
				                          <span class="fas fa-trash-alt"  style="color:red;"></span>
				                     </a> | ';
	                      }
	                
	                if ($this->data['permission']['edit'] == 1) {   
	                    $tools[] .='<a >
		                              <span class="fas fa-edit"></span>
		                            </a></div>'; 
	                      }
	                    $arr[] = '<span id="supp_id" style="padding-left:7%;" class="info">'.$supp['id'].'</span>';
	                    $arr[] = '<a  href="'.base_url('admin/suppliers/supplier/'. $supp['id']).'" titel="View supplier"><span id="supp_name" class="info">'.$supp['supp_name'].'</span></a><br>'.implode("", $tools);
	                    $arr[] = '<span id="phone" class="info">'.$supp['interface'].'</span>';
	                    $arr[] = '<span id="phone" class="info">'.$supp['hotel_group'].'</span>';
	                    $arr[] = '<span id="phone" class="info">'.$supp['phone'].'</span>';
	                    $arr[] = '<span id="address" class="info">'.$supp['address'].'</span>';
	                    $arr[] = '<span id="contact">'.$supp['contact'].'</span>';
	                    $arr[] = '<span id="cont_email">'.$supp['cont_email'].'</span>';  
	                   
	                    $data[] =$arr;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Suppliers_model->get_all_suppliers()),
	                     "recordsFiltered" => $this->Suppliers_model->get_suppliers($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }


        public function supplier_process(){

			$data = [

			         'supp_name'            => $this->input->post('supp_name'),

			         'interface'            => $this->input->post('interface'),

			         'group_id'             => $this->input->post('group_id'),

			         'phone'                => $this->input->post('phone'),

			         'address'              => $this->input->post('address'),

			         'contact'              => $this->input->post('contact'),

			         'cont_email'           => $this->input->post('cont_email')
			        
			        ];

				if (!$this->input->post('id')) {

					$supp_id = $this->Suppliers_model->add_supplier($data);
				   
				    if($supp_id){

				     $this->General_model->update_files($this->input->post('assumed_id'),4,$supp_id);	
				  	 
				  	 $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);

				  	 loger('create',4,'Suppliers',$supp_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'created supplier no '.$supp_id.'');

		             redirect('admin/suppliers');

				     }
				     
				   }else{

	                $supplier   = $this->Suppliers_model->get_supplier($this->input->post('id'));
				   	
				   	$updated    = $this->Suppliers_model->update_supplier($this->input->post('id'),$data);

				   	if ($updated) {
				   		
				   	   $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);

				   	    loger('update',4,'Suppliers',$supplier['id'],0,json_encode($supplier, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated supplier no '.$supplier['id'].'');

				   	 }


		               redirect('admin/suppliers/supplier/'.$this->input->post('id'));

			   }      
		  
	       }


	    public function supplier($supp_id){

          $data['supplier']  = $this->Suppliers_model->get_supplier($supp_id);

          access_checker(0,$this->data['permission']['view'],0,0,0,0,0,0,'admin/suppliers');

	         if ($data['supplier']['deleted'] == 1) {

	         	 $this->session->set_flashdata(['alert'=>'Sorry','msg'=>'This Record has been Deleted before!']);
	         	
	         	 redirect('admin/suppliers');
	           
	           }

	            $data['uploads']           = $this->General_model->get_file(4,$supp_id);

                $data['image_link']        =  base_url('assets/images/big/auth-bg.jpg"');

	            $data['file_path']         = base_url('assets/uploads/suppliers');

	            $data['groups']            = get_hotels_groups();

				$data['view'] = 'admin/suppliers/supplier_view';

			    $this->load->view('admin/includes/layout',$data);
	      }      

	    public function supplier_transactions($supp_id){
	           
	          $dt_att         = $this->datatables_att();
	          $transactions   = $this->Suppliers_model->get_supplier_transactions($supp_id,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	           foreach($transactions as $trans) {
	               $arr = array(); 
	                  $arr[] = '<a  href="'.base_url('delivery/del_note/'. $trans['delivery_id']).'" class="info">'.$trans['delivery_serial'].'</span></a>';
                      $arr[] = '<span id="hotel_name" style="padding-left:7%;">'.$trans['hotel_name'].'</span>';
                      $arr[] = '<span id="dep_name" class="info">'.$trans['dep_name'].'</span>';
                      $arr[] = '<span id="item_name" class="info">'.$trans['item_name'].'</span>';
                      $arr[] = '<span id="delivery_date" class="info">'.$trans['serial'].'</span>';
                      $arr[] = '<span id="creationDate" class="info">'.$trans['description'].'</span>';
                      $arr[] = '<span id="recieved" class="info">'.$trans['recieved'].'</span>';
                      $arr[] = '<span id="net_amount" class="info">'.$trans['net_amount'].' EGP'.'</span>';           
	                   
	                 $data[] =$arr;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Suppliers_model->get_all_supplier_transactions($supp_id)),
	                     "recordsFiltered" => $this->Suppliers_model->get_supplier_transactions($supp_id,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }   
  



        public function del($id){
            
            access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'admin/suppliers');

	        $this->db->update('suppliers', array('deleted' => 1), "id = ".$id);

         	loger('delete',4,'Suppliers',$id,0,0,0,0,0,'Deleted supplier no '.$id.'');

	        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);

		}






	}

?>	 