<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Auth_model');
			$this->load->model('admin/User_model');
		}
		public function index(){
			redirect('admin/auth/login');
		}


		public function login(){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('username', 'username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->load->view('admin/auth/login');
				}
				else {
					$data = array(
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password')
					);
					$result = $this->Auth_model->login($data);
					if ($result == TRUE) {
						$admin_data = array(
							'user_id' => $result['id'],
						 	'name'    => $result['username'],
						 	'is_admin_login' => TRUE
						);
						$this->session->set_userdata($admin_data);
	   					if ($this->session->has_userdata('red_url')) {
			                $red_url = $this->session->userdata('red_url');
			                $this->session->unset_userdata('red_url');
			                redirect($red_url);
				        }else{
							redirect('admin/site_manager');
						}

					}
					else{
						$data['msg'] = 'Invalid Email or Password!';
						$this->load->view('admin/auth/login', $data);
					}
				}
			}
			else{
				$this->load->view('admin/auth/login');
			}
		}	

		public function change_pwd(){
			$id = $this->session->userdata('admin_id');
			 $this->global_data['user'] = $this->User_model->get_user_by_id($id, TRUE);
            $this->load->vars($this->global_data);
			if($this->input->post('submit')){
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|matches[password]');
				if ($this->form_validation->run() == FALSE) {
					$data['view'] = 'admin/auth/change_pwd';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
					);
					$result = $this->auth_model->change_pwd($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Password has been changed successfully!');
						redirect(base_url('admin/auth/change_pwd'));
					}
				}
			}
			else{
				$data['view'] = 'admin/auth/change_pwd';
				$this->load->view('admin/layout', $data);
			}
		}
				
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('admin/auth/login'), 'refresh');
		}


			
	} 


?>