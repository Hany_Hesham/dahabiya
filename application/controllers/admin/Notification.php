<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Notification extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
	        $this->load->model('admin/Notification_model');
	        $this->load->library('user_agent');
	        $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
			$this->data['username']     = $this->global_data['sessioned_user']['username'];
			$this->data['module']       = $this->Modules_model->get_module_by('','Notification');
	        $this->data['permission']   = user_access($this->data['module']['id']);
	        $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		}

        public function index($module_id=''){
		    access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');
		    $data['module_id'] = $module_id;
			$data['view'] = 'admin/notification/notifications_view';
		    $this->load->view('admin/includes/layout',$data);
		}  

	    public function notification_ajax($module_id=''){
	        $dt_att  = $this->datatables_att();
	        $notifications    = $this->Notification_model->get_notifications($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'',$module_id);
	        $data = array();
	        foreach($notifications as $notification) {
	            $arr = array(); 
	            $arr[] = '<span id="" style="padding-left:7%;" class="info">'.$notification['id'].'</span>';
	            $arr[] = '<strong style="font-size:16px;"class="info">'.ucfirst($notification['head']).'</strong>';
                $arr[] = '<span id="">'.$notification['timestamp'].'</span>';  
	           	$arr[] = '<strong style="font-size:16px;"class="info">'.$notification['module_name'].'</strong>';
	           	$forms = explode('/', $notification['link']);
	            $arr[] = '<span id="" class="info">'.$notification['module_name'].' No #.'.$forms[2].'</span>';
	            if ($notification['to_uid']) {
	            	$arr[] = '<span id="">'.getUser($notification['to_uid']).'</span>';
	            }else{
	            	$arr[] = '<span id=""> </span>';
	            }
	            if ($notification['to_dep_id']) {
	            	$arr[] = '<span id="">'.getDepartment($notification['to_dep_id']).'</span>';
	            }else{
	            	$arr[] = '<span id=""> </span>';
	            }
	            if ($notification['to_hid']) {
	            	$arr[] = '<span id="">'.getHotel($notification['to_hid']).'</span>';
	            }else{
	            	$arr[] = '<span id=""> </span>';
	            }
	            $arr[] = '<span id="">'.$notification['fullname'].'</span>';
	            $arr[] = '<span id="">'.$notification['message'].'</span>'; 
	            if ($notification['status'] == 1) {
	                $arr[] = '<a class="edit-group" onclick="upRead('.$notification['id'].', 0)" href="javascript: void(0);" titel="Send Email"><strong style="font-size:16px;"class="info"><i class="fas fa-envelope"></i></strong></a><br/><span id=""> Read By '.getUser($notification['readedby']).'</span>';
	            }else{
		            $arr[] = '<a class="edit-group" onclick="upRead('.$notification['id'].', 1)" href="javascript: void(0);" titel="Send Email"><strong style="font-size:16px;"class="info"><i class="fas fa-envelope"></i></strong></a>';
	            }
	            $data[] =$arr;
	       	}
	        $output = array(
	            "draw" => $dt_att['draw'],
	            "recordsTotal"    => count($this->Notification_model->get_all_notifications($module_id)),
	            "recordsFiltered" => $this->Notification_model->get_notifications($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count', $module_id),
	            "data" => $data
	        );
	        echo json_encode($output);
	        exit();
        }

        public function read(){
        	$notification_id = $this->input->post('notification_id');
        	$statue = $this->input->post('statue');
		    $this->Notification_model->updateRead($notification_id, $statue);
	        echo json_encode(true);
	        exit();
	    }

	}

?>	 