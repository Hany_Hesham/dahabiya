<?php

	defined('BASEPATH') OR exit('No direct script access allowed');



	class Hotels extends MY_Controller {



		public function __construct(){

			parent::__construct();
            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/Hotels_model');
	        $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
			$this->data['username']     = $this->global_data['sessioned_user']['username'];
			$this->data['module']       = $this->Modules_model->get_module_by(105);
	        $this->data['permission']   = user_access($this->data['module']['id']);
	        $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		}

        public function index(){
		        // $data['assumed_id']    = get_file_code('','H');
		        // $data['uploads']       = $this->General_model->get_file(1,$data['assumed_id']);
		       access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');	

		        $data['groups']        = get_hotels_groups();

				$data['view']          = 'admin/backend/hotels_index';

				$this->load->view('admin/includes/layout',$data);

			  }  


	     public function hotels_ajax(){
	           
	          $dt_att  = $this->datatables_att();
	          $hotels   = $this->Hotels_model->get_hotels_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	           foreach($hotels as $hotel) {
	               $arr = array(); 
	               $tools=array();

	                    $arr[] = '<span class="info" id="hotelId">'.$hotel['id'].'</span>';
	                    $arr[] = '<a data-toggle="modal" href="#smallmodal" id="hotel" class="edit-hotel"><strong id="hotelName" class="info" style="font-size:14px;">'.$hotel['hotel_name'].'</strong></a>';
	                    $arr[] = '<span class="hidden" id="group_id">'.$hotel['group_id'].'</span><span class="info" id="hotelGroup">'.$hotel['hotel_group'].'</span>';
	                    $arr[] = '<span id="hotelCode" >'.$hotel['code'].'</span>'; 
	                   
	                    $data[] =$arr;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Hotels_model->get_all_hotels()),
	                     "recordsFiltered" => $this->Hotels_model->get_hotels_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }


        public function hotel_process(){

			$data = [

			         'hotel_name'          => $this->input->post('hotel_name'),

			         'group_id'            => $this->input->post('group_id'),

			         'code'                => $this->input->post('code')
			        
			        ];

				if (!$this->input->post('id')) {

					$hotel_id = $this->Hotels_model->add_hotel($data);
				   
				    if($hotel_id){
				  	 
				  	 $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);

				  	 loger('create',1,'Hotels',$hotel_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'created Hotel no '.$hotel_id.'');

		             redirect('admin/hotels');

				     }
				     
				   }else{

	                $hotel      = $this->Hotels_model->get_by_id($this->input->post('id'));
				   	
				   	$updated    = $this->Hotels_model->update_hotel($this->input->post('id'),$data);

				   	if ($updated) {
				   		

				   	    loger('update',1,'Hotels',$hotel['id'],0,json_encode($hotel, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated hotel no '.$hotel['id'].'');

				   	   $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);

		               redirect('admin/hotels');

				   	 }



			   }      
		  
	       }

	}

?>	 