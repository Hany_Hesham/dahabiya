<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this
            ->load
            ->model('admin/User_model');
        $this
            ->load
            ->model('admin/branches_model');
        $this
            ->load
            ->model('admin/Modules_model');
        $this
            ->load
            ->library('user_agent');
        $this->data['user_id'] = $this->global_data['sessioned_user']['id'];
        $this->data['username'] = $this->global_data['sessioned_user']['username'];
        $this->data['module'] = $this
            ->Modules_model
            ->get_module_by('', 'users');
        $this->data['permission'] = user_access($this->data['module']['id']);
        $this->data['ubranches'] = get_ubranches($this->data['user_id']);
    }

    public function index()
    {

        access_checker($this->data['permission']['view'], 0, 0, 0, 0, 'admin/dashboard');

        $data['view'] = 'admin/users/users_index';

        $this
            ->load
            ->view('admin/includes/layout', $data);

    }

    public function users_ajax()
    {
        $users_ubranches = $this
            ->User_model
            ->get_users_by_granted_branches($this->data['ubranches']);
        $uids = array();
        foreach ($users_ubranches as $user)
        {
            $uids[] = $user['user_id'];
        }
        $dt_att = $this->datatables_att();
        $users = $this
            ->User_model
            ->get_users($uids, $dt_att['start'], $dt_att['length'], $dt_att['search'], $dt_att['order'], $dt_att['col_name'], '');
        $data = array();
        foreach ($users as $user)
        {
            $arr = array();
            $tools = array();
            if ($this->data['permission']['remove'] == 1)
            {
                $tools[] = '<div><a href="javascript: void(0);" title="Delete User" onclick="del(' . $user['id'] . ',\'' . $user['username'] . '\',\'users\',\'admin/users\',\'del\')">
				                          <span class="ti-trash float-right"  style="color:red;"> </span>
				                     </a>';
            }

            if ($this->data['permission']['edit'] == 1)
            {
                $tools[] .= '<a href="' . base_url('admin/users/user/' . $user['id']) . '" title="View User">
		                                 <span class="fab fa-phabricator float-right"> | </span>
		                            </a></div>';
            }

            if ($user['disabled'] == 1)
            {
                $arr[] = '<label class="customcheckbox float-right">
		                          <input type="hidden" value="0" name="disabled" class="listCheckbox">
		                            <input type="checkbox" name="disabled" class="listCheckbox"
		                                value="1" class="switch-input" checked  onclick="changer(' . $user['id'] . ',\'' . $user['username'] . '\',\'users\',\'disabled\',\'0\')">
		                             <span class="checkmark"></span>
                                  </label>';
            }
            else
            {

                $arr[] = '<label class="customcheckbox float-right">
		                          <input type="hidden" value="0" name="disabled" class="listCheckbox">
		                            <input type="checkbox" name="disabled" class="listCheckbox"
		                                value="1" class="switch-input" onclick="changer(' . $user['id'] . ',\'' . $user['username'] . '\',\'users\',\'disabled\',\'1\')">
		                            <span class="checkmark"></span>
                                 </label>';
            }

            if ($user['is_admin'] == 1)
            {
                $arr[] = '<span class="badge badge-danger float-right">Admin</span>';
            }
            else
            {
                $arr[] = '<span class="badge badge-dark float-right">User</span>';
            }

            $arr[] = '<span class="float-right" style =" color:#2962FF" >' . $user['email'] . '</span>';
            $arr[] = '<span class="float-right" style =" color:#2962FF" >' . $user['fullname'] . '</span>';
            $arr[] = '<span class="float-right" style =" color:#2962FF" >' . $user['username'] . '</span><br>' . implode("", $tools);
            $arr[] = '<span class="float-right" style="color:#2962FF;padding-left:20%;">' . $user['id'] . '</span>';

            $data[] = $arr;
        }

        $output = array(
            "draw" => $dt_att['draw'],
            "recordsTotal" => count($this
                ->User_model
                ->get_all_users($uids)) ,
            "recordsFiltered" => $this
                ->User_model
                ->get_users($uids, $dt_att['start'], $dt_att['length'], $dt_att['search'], $dt_att['order'], $dt_att['col_name'], 'count') ,
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    public function user($uid = false)
    {

        access_checker(0, 0, 0, $this->data['permission']['edit'], 0, 0, 'admin/users');

        $this->load->model('admin/Modules_model');

        $data['roles'] = get_roles();

        $data['departments'] = get_departments();

        $data['branches'] = $this
            ->branches_model
            ->get_branches($this->data['ubranches']);

        $data['user'] = $this
            ->User_model
            ->get_user_by_id($uid);

        $data['selected_branches'] = $this
            ->User_model
            ->get_user_branches($uid);

        foreach ($data['selected_branches'] as $key => $s_branch)
        {

            $selected_roles = $this
                ->User_model
                ->get_user_roles($s_branch['branch_id'], $uid);

            $data['selected_branches'][$key]['selected_roles'] = json_decode(implode(',', $selected_roles) , true);

        }
		
        $data['modules'] = $this->Modules_model->get_modules();

        if (isset($data['user']))
        {
            if (isset($data['user']['permission']) && $data['user']['permission'] == 0)
            {
                foreach ($data['modules'] as $key => $module)
                {
                    $data['modules'][$key]['permission'] = $this
                        ->User_model
                        ->get_group_permissions($data['user']['role_id'], $module['id']);
                }
            }
            else
            {
                foreach ($data['modules'] as $key => $module)
                {
                    $data['modules'][$key]['permission'] = $this
                        ->User_model
                        ->get_access_modules($uid, $module['id']);
                }
            }
        }
        $data['view'] = 'admin/users/user_add';
        $this
            ->load
            ->view('admin/includes/layout', $data);
    }

    public function add()
    {
        $user_id = $this
            ->input
            ->post('id');

        $data = array(

            'username' => $this
                ->input
                ->post('username') ,

            'fullname' => $this
                ->input
                ->post('fullname') ,

            'email' => $this
                ->input
                ->post('email') ,

            'role_id' => $this
                ->input
                ->post('role_id') ,

            'department' => $this
                ->input
                ->post('department_id') ,

            'password' => password_hash($this
                ->input
                ->post('password') , PASSWORD_BCRYPT) ,

            'created_at' => date('Y-m-d : h:m:s') ,

            'updated_at' => date('Y-m-d : h:m:s') ,

        );

        if ($user_id == null)
        {

            $uid = $this
                ->User_model
                ->add_user($data);

            foreach ($this
                ->input
                ->post('branches[]') as $hotel)
            {

                $this
                    ->User_model
                    ->add_user_branches($uid, $hotel);
            }

            $this
                ->session
                ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تمت الإضافة بنجاح  !']);

            redirect('admin/users/user/' . $uid);

        }
        else
        {

            if (!$this
                ->input
                ->post('password'))
            {

                unset($data['password']);
            }

            $this
                ->User_model
                ->delete_user_branches($user_id);

            foreach ($this
                ->input
                ->post('branches[]') as $hotel)
            {

                $this
                    ->User_model
                    ->add_user_branches($user_id, $hotel);
            }

            $this
                ->User_model
                ->edit_user($data, $user_id);

            $this
                ->session
                ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تم التعديل بنجاح  !']);

            redirect('admin/users/user/' . $user_id);

        }

    }

    public function hotel_roles()
    {

        foreach ($this
            ->input
            ->post('branches') as $key => $hotel_roles)
        {
            //die(print_r($hotel_roles));
            if (isset($hotel_roles['role_id']))
            {
                # code...
                $form_data = json_encode($hotel_roles['role_id'], JSON_UNESCAPED_UNICODE);

                $updated = $this
                    ->User_model
                    ->update_user_branches($hotel_roles['id'], $form_data);

            }

        }

        $this
            ->session
            ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تم التعديل بنجاح  !']);

        redirect($this
            ->agent
            ->referrer());
    }

    public function add_permission($user_id)
    {

        foreach ($this
            ->input
            ->post('accs') as $module)
        {

            $module['user_id'] = $user_id;

            if (!isset($module['id']))
            {

                $Permission = $this
                    ->User_model
                    ->add_permission($module);
            }
            else
            {

                $updated = $this
                    ->User_model
                    ->update_permission_by_user_id($module['id'], $user_id, $module);

            }

        }

        if ($Permission || $updated)
        {

            status_changer('users', 'permission', '1', $user_id);

            $this
                ->session
                ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تم التعديل بنجاح  !']);

            redirect(base_url('admin/users'));

        }
        else
        {

            redirect(base_url('admin/users/user/' . $user_id));

        }

    }

    public function del($id)
    {

        $this
            ->db
            ->update('users', array(
            'deleted' => 1
        ) , "id = " . $id);

        $this
            ->session
            ->set_flashdata(['alert' => 'نجاح', 'msg' => 'Record is Deleted Successfully!']);

        redirect(base_url('admin/users'));

    }

    public function status_change($id, $column, $value)
    {

        $this
            ->db
            ->update('users', array(
            $column => $value
        ) , "id = " . $id);

        $this
            ->session
            ->set_flashdata(['alert' => 'نجاح', 'msg' => 'Record is Deleted Successfully!']);

        redirect(base_url('admin/users'));

    }

    public function bulk()
    {

        $this
            ->load
            ->model('admin/Modules_model');

        $data['roles'] = get_roles();

        $data['users'] = $this
            ->User_model
            ->get_all_users($this->data['ubranches']);

        $data['modules'] = $this
            ->Modules_model
            ->get_modules();

        $data['role'] = $this
            ->User_model
            ->get_role($this
            ->input
            ->post('role_id'));

        foreach ($data['modules'] as $key => $module)
        {

            $data['modules'][$key]['permission'] = $this
                ->User_model
                ->get_group_permissions($data['role']['id'], $module['id']);

        }

        $data['view'] = 'admin/users/add_bulk';

        $this
            ->load
            ->view('admin/includes/layout', $data);

    }

    public function add_bulk()
    {

        foreach ($this
            ->input
            ->post('users') as $user_id)
        {

            $user = $this
                ->User_model
                ->get_user_by_id($user_id);

            foreach ($this
                ->input
                ->post('accs') as $module)
            {

                $module['user_id'] = $user_id;

                if ($user['permission'] == 0)
                {

                    $Permission = $this
                        ->User_model
                        ->add_permission($module);

                }
                else
                {

                    $updated = $this
                        ->User_model
                        ->update_permission_bulk($module['module_id'], $user_id, $module);

                }

            }

            status_changer('users', 'permission', '1', $user_id);

        }

        if ($Permission || $updated)
        {

            $this
                ->session
                ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تمت الإضافة بنجاح  !']);

            redirect(base_url('admin/users'));

        }
        else
        {

            redirect(base_url('admin/users/bulk'));

        }

    }

    public function group()
    {

        $this
            ->load
            ->model('admin/Modules_model');

        $data['roles'] = get_roles();

        $data['modules'] = $this
            ->Modules_model
            ->get_modules();

        $data['role'] = $this
            ->User_model
            ->get_role($this
            ->input
            ->post('role_id'));

        // die(print_r($data['role']));
        foreach ($data['modules'] as $key => $module)
        {

            $data['modules'][$key]['permission'] = $this
                ->User_model
                ->get_group_permissions($data['role']['id'], $module['id']);

        }

        $data['view'] = 'admin/users/add_group';

        $this
            ->load
            ->view('admin/includes/layout', $data);

    }

    public function add_role_name()
    {

        $this
            ->load
            ->library('user_agent');

        $name = $this
            ->input
            ->post('role_name');

        $role_id = $this
            ->User_model
            ->add_role_name($name);

        if ($role_id)
        {

            $this
                ->session
                ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تمت الإضافة بنجاح  !']);

            redirect($this
                ->agent
                ->referrer());
        }

    }

    public function add_group()
    {

        $role_id = $this
            ->input
            ->post('role');

        foreach ($this
            ->input
            ->post('accs') as $module)
        {

            $module['role_id'] = $role_id;

            if (!isset($module['id']))
            {

                $Permission = $this
                    ->User_model
                    ->add_group_permission($module);

            }
            else
            {

                $updated = $this
                    ->User_model
                    ->update_group_permission_by_id($module['id'], $role_id, $module);

            }

        }

        if ($Permission || $updated)
        {

            status_changer('user_groups', 'permission', '1', $role_id);

            $this
                ->session
                ->set_flashdata(['alert' => 'نجاح', 'msg' => 'لقد تمت الإضافة بنجاح  !']);

            redirect(base_url('admin/users/group'));

        }
        else
        {

            redirect(base_url('admin/users/group'));

        }

    }

}

?>
