<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Test extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/Test_model');
            $this->load->model('admin/Rooms_model');
		}

        public function hideOldModules($year){
            $moduleArray = array(32, 33, 34, 35, 36, 78, 99);
            $modules = $this->Modules_model->getAllModules('user');
            foreach ($modules as $module) {
                if(!in_array($module['id'], $moduleArray)){
                    $counter = $this->Test_model->updateDeletForms($module['tdatabase'],$year);
                    if(count($counter) > 0){
                        $updatedForms = array();
                        $updatedForms['year'] = $year;
                        $updatedForms['name'] = $module['name'];
                        $updatedForms['forms'] = json_encode($counter);
                        $this->Test_model->insertChanges($updatedForms);
                    }
                }    
            }
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Module has been updated Successfully!']);
            redirect('admin/dashboard');
        } 


        public function update_modules($stage){
            $modules = $this->Modules_model->get_all_modules();
            foreach ($modules as $module) {
                if ($stage == 1) {
                    $this->Test_model->update_role_id($module['tdatabase'],$module['id']);
                }elseif($stage == 2){
                    $this->Test_model->update_approved_status($module['tdatabase'],$module['id']);
                }elseif($stage == 3){
                    $this->Test_model->update_reject_status($module['tdatabase'],$module['id']);
                }
            }
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Module has been updated Successfully!']);
            redirect('admin/dashboard');
        } 


        public function update_module($table,$module_id,$stage){
            if ($stage==1) {
                    $this->Test_model->update_role_id($table,$module_id);
             }elseif($stage==2){
                    $this->Test_model->update_approved_status($table,$module_id);
             }elseif($stage==3){
                    $this->Test_model->update_reject_status($table,$module_id);
             }
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Module has been updated Successfully!']);
            redirect('admin/dashboard');
        } 

        public function update_files($table, $field, $module_dir){
            $this->Test_model->copy_files($table, $field, $module_dir);
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Files has been updated Successfully!']);
            redirect('admin/dashboard');
        } 

        public function update_allfiles($module_id, $module_dir){
            $this->Test_model->copy_allfiles($module_id, $module_dir);
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Files has been updated Successfully!']);
            redirect('admin/dashboard');
        }  

    public function update_role() {
        $this->db->select('*');
        $module_data = $this->db->get('assets_transfer')->result_array();
        foreach ($module_data as $data) {
            $this->db->select('*');
            $this->db->where('module_id', 76);
            $this->db->where('form_id', $data['id']);
            $this->db->where('typed', 1);
            $this->db->where('user_id IS NULL');
            $this->db->order_by('rank'); 
            $this->db->limit('1');      
            $signatures = $this->db->get('signatures')->row();
            if($signatures){
                $this->db->where('id', $data['id']);
                $this->db->update('assets_transfer', array('role_id' =>$signatures->role_id , 'status' => 1, 'stage' => 'First'));

            } else {
                continue;
            }       
        }
    }

    public function update_role1() {
        $this->db->select('*');
        $module_data = $this->db->get('assets_transfer')->result_array();
        foreach ($module_data as $data) {
            $this->db->select('*');
            $this->db->where('module_id', 76);
            $this->db->where('form_id', $data['id']);
            $this->db->where('typed', 2);
            $this->db->where('user_id IS NULL');
            $this->db->order_by('rank'); 
            $this->db->limit('1');      
            $signatures = $this->db->get('signatures')->row();
            if($signatures){
                $this->db->where('id', $data['id']);
                $this->db->update('assets_transfer', array('role_id' =>$signatures->role_id , 'status' => 1, 'stage' => 'Second'));

            } else {
                continue;
            }       
        }
    }
		public function tester(){
			$test =$this->Rooms_model->get_employe_data(3, 6369);
            die(print_r($test));
	    } 

	}

?>	 