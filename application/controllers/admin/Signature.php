  <?php defined('BASEPATH') OR exit('No direct script access allowed');

   class Signature extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('admin/Signature_model');
      $this->load->model('admin/Hotels_model');
      $this->load->model('admin/Departments_model');
      $this->data['module']               = $this->Modules_model->get_module_by(46);
      $this->data['permission']           = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,'admin/dashboard');
      $data['view']   = 'admin/signature/signature_index';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function signature_ajax(){
      $dt_att               = $this->datatables_att();
      $dt_att['module_id']  = $this->data['module']['id'];
      $custom_search        = $this->input->post('searchBy');
      $rows                 = $this->Signature_model->get_signatures($dt_att, $custom_search, '');
      $data                 = array();
      $i                    = 1;
      foreach($rows as $row) {
        $arr        = array(); 
        $tools      =array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[]  = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'signature\',\'signature\',\'admin/signature\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" titel="Edit item"><span>Edit</span></a></div>'; 
        }
        $arr[]      = '<span style="padding-left:7%;">'.$i.'</span>';
        $arr[]      = '<div class="after-hover"><a  href="'.base_url('admin/signature/view/'. $row['id']).'" target="_blank" titel="View Item"><strong>'.$row['name'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[]      = '<span>'.$row['modules_name'].'</span>';
        if ($row['hotel_id']) {
          $arr[]      = '<span>Hotel '.$row['hotel_condition'].' '.$row['hotel_name'].'</span>';
        }else{
          $arr[]      = '<span></span>';
        }
        if ($row['dep_code']) {
          $arr[]      = '<span>Department '.$row['department_condition'].' '.$row['dep_name'].'</span>';
        }else{
          $arr[]      = '<span></span>';
        }
        if ($row['special']) {
          $arr[]      = '<span>'.$row['special'].' '.$row['special_condition'].' '.$row['special_value'].'</span>';
        }else{
          $arr[]      = '<span></span>';
        }
        if ($row['limitation']) {
          $arr[]      = '<span>Cost '.$row['limit_condition'].' '.$row['limitation'].'</span>';
        }else{
          $arr[]      = '<span></span>';
        }
        $data[]     = $arr;
        $i++;
      }
      $output             =   array(
        "draw"            =>  $dt_att['draw'],
        "recordsTotal"    =>  count($this->Signature_model->get_all_signature()),
        "recordsFiltered" =>  $this->Signature_model->get_signatures($dt_att, $custom_search, 'count'),
        "data"            =>  $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'admin/signature');
      $data['roles']              = get_roles();
      $data['modules']            = $this->General_model->get_modules();
      $data['hotels']             = $this->Hotels_model->get_all_hotels();
      $data['departments']        = $this->Departments_model->get_all_departments();
      $data['gen_id']             = get_file_code('files','signature');
      $data['uploads']            = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'             => $this->data['user_id'],
          'status'          => '1',
        ];      
        $signature_id  =  $this->Signature_model->add_signature($fdata);
        if ($signature_id) {
          loger('Create',$this->data['module']['id'], $this->data['module']['name'],$signature_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Signature signature #NO'.$signature_id.'');  
          foreach ($this->input->post('items') as $key => $item) {
            $item['signature_id'] = $signature_id;
            $name = $item['type_name'];
            unset($item['type_name']);
            $item_id = $this->Signature_model->add_signature_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $signature_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$name.'');
            }
          }
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $signature_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $signature_id);
        }
      }
      $data['view'] = 'admin/signature/signature_process';
      $this->load->view('admin/includes/layout',$data);       
    }

    public function edit(){
      if ($this->input->post('id')) {
        $id           = $this->input->post('id');
        $name         = $this->input->post('type_name');
        $item_data    = $this->Signature_model->get_signature_item($id);
        $fdata        =[
          'first'         => $this->input->post('first'),
          'second'        => $this->input->post('second'),
          'third'         => $this->input->post('third'),
          'fourth'        => $this->input->post('fourth'),
          'fifth'         => $this->input->post('fifth'),
          'sixth'         => $this->input->post('sixth'),
          'seventh'       => $this->input->post('seventh'),
          'eighth'        => $this->input->post('eighth'),
          'ninth'         => $this->input->post('ninth'),
        ];
        $updated          = $this->Signature_model->edit_signature_item($fdata,$id);
        if ($updated > 0) {
          loger('update',$this->data['module']['id'],$this->data['module']['name'],$item_data['signature_id'],$id,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated Item: '.$name);
        }
        echo json_encode(true);
        exit();
      }  
    }    

    public function view($signature_id){
      access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');
      $data['signature']               = $this->Signature_model->get_signature($signature_id);
      if (!$data['signature']) {
        $this->session->set_flashdata(['alert'=>'Failure','msg'=>'Sorry there is no record with this number!']);
        redirect('admin/signature');
      }
      $data['roles']                = $this->General_model->get_meta_data('roles');
      $data['uploads']              = $this->General_model->get_file_from_table($this->data['module']['id'],$signature_id,'files');
      $this->data['form_id']        = $signature_id;
      $data['messaged']             = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'],$this->data['form_id']);
      $this->data['commentsCount']  = count($data['messaged']);
      $data['view']                 = 'admin/signature/view_signature';
      $this->load->view('admin/includes/layout',$data);
    }

    public function view_body_signature($signature_id){
      access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');
      $data['signature_id']            = $signature_id; 
      $data['departments']          = $this->General_model->get_meta_data('signature_department');
      $data['roles']              = $this->General_model->get_meta_data('roles');
      $this->load->view('admin/signature/view_body_signature',$data);
    }  

    public function signers_items($signature_id){
      $data['signatures']           = getSigners($this->data['module']['id'],$signature_id);
      $data['module']               = $this->data['module'];
      $data['signature_path']       = $this->data['signature_path'];
      $data['form_id']              = $signature_id;
      $this->load->view('admin/html_parts/signers',$data);
    } 

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Signature_model->get_signature_item($id);
        $this->Signature_model->remove_item($id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['signature_id'], $item['id'], json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted item:'.$item['name'].'');
        echo json_encode(true);
        exit();
      }  
    }

  }

?>