<?php

	defined('BASEPATH') OR exit('No direct script access allowed');



	class Departments extends MY_Controller {



		public function __construct(){

			parent::__construct();

            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/Departments_model');
	            $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
				$this->data['username']     = $this->global_data['sessioned_user']['username'];
				$this->data['module']       = $this->Modules_model->get_module_by('','Admin');
	            $this->data['permission']   = user_access($this->data['module']['id']);
	            $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		  }

        public function index(){

		       access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');	

				$data['view']          = 'admin/backend/departments_index';

				$this->load->view('admin/includes/layout',$data);

			  }  


	     public function deps_ajax(){
	           
	          $dt_att  = $this->datatables_att();
	          $departments   = $this->Departments_model->get_departments_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	           foreach($departments as $dep) {
	               $arr = array(); 
	               $tools=array();

	                    $arr[] = '<span class="info" id="depId">'.$dep['id'].'</span>';
	                    $arr[] = '<a data-toggle="modal" href="#smallmodal" id="dep" class="edit-dep"><strong id="depName" class="info" style="font-size:14px;">'.$dep['dep_name'].'</strong></a>';
	                    $arr[] = '<span id="depCode" >'.$dep['code'].'</span>';
	                    $arr[] = '<span id="depRank" class="badge badge-danger" >'.$dep['rank'].'</span>'; 
	                   
	                    $data[] =$arr;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Departments_model->get_all_departments()),
	                     "recordsFiltered" => $this->Departments_model->get_departments_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }


        public function dep_process(){

			$data = [

			         'dep_name'            => $this->input->post('dep_name'),

			         'code'                => $this->input->post('code'),

			         'rank'                => $this->input->post('rank'),
			        
			        ];

				if (!$this->input->post('id')) {

					$dep_id = $this->Departments_model->add_department($data);
				   
				    if($dep_id){
				  	 
				  	 $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);

				  	 loger('create',1,'Departments',$dep_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'created department no '.$dep_id.'');

		             redirect('admin/departments');

				     }
				     
				   }else{

	                $dep      = $this->Departments_model->get_department($this->input->post('id'));
				   	
				   	$updated    = $this->Departments_model->update_department($this->input->post('id'),$data);

				   	if ($updated) {
				   		

				   	    loger('update',1,'Departments',$dep['id'],0,json_encode($dep, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated department no '.$dep['id'].'');

				   	   $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);

		               redirect('admin/departments');

				   	 }



			   }      
		  
	       }

	}

?>	 