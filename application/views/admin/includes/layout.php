<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="20x20" href="<?php echo base_url('assets/images/favicon.png');?>">
    <title>Site Manager</title>
    <!-- Custom CSS -->
    <script src="<?php echo base_url('assets/libs/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/custom/main_scripts.js')?>"></script>
 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/extra-libs/multicheck/multicheck.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/select2/dist/css/select2.min.css')?>">
    <link href="<?php echo base_url('assets/libs/flot/css/float-chart.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.min.css');?>" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/Buttons/css/buttons.dataTables.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/Buttons/css/buttons.bootstrap4.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css')?>"> 
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/Responsive/css/responsive.bootstrap.min.css')?>">    
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/media/css/dataTables.bootstrap4.min.css')?>">

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/fileinput.min.css">
    
    <script src="<?= base_url() ?>assets/js/fileinput.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
 
     <link href="<?php echo base_url('assets/libs/magnific-popup/dist/magnific-popup.css')?>" rel="stylesheet">


     <!---------------------------------------------Selec picker -------------------------------->
    <link  href="<?php echo base_url('assets/extra-libs/selectpicker/dist/css/bootstrap-select.min.css')?>" rel="stylesheet">
     <!-- <link  href="<?php echo base_url('assets/extra-libs/selectpicker/dist/css/ajax-bootstrap-select.css')?>" rel="stylesheet"> -->
     
     <!---------------------------------------------datepiker -------------------------------->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>">
    
    <script src="<?php echo base_url('assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>

    <style type="text/css">a {text-decoration: none !important}</style>
    <link href="<?php echo base_url('assets/editor/css/summernote.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/editor/css/summernote-bs4.css')?>" rel="stylesheet">

</head>
<body>
    <!-- <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-sidebartype="mini-sidebar" class="mini-sidebar">
    <!--  <div id="main-wrapper">   -->  
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar no-print" data-navbarbg="skin5">
            <input type="hidden" id="baseurl" name="baseurl" value="<?php echo base_url(); ?>" />
           <?php $this->load->view('admin/includes/navbar');?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <?php $this->load->view('admin/includes/sidebar');?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
         <div class="page-wrapper">
            
           <?php $this->load->view($view);?>
           
           <footer class="footer text-center no-print" style="background-color:#fff;">
                <strong>Copyrights &copy; 2021 All Rights Reserved by HTM3.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

    
   
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.pie.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.time.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.stack.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.crosshair.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')?>"></script> 

     <script src="<?php echo base_url('assets/libs/datatables/media/js/jquery.js')?>"></script> 
<!----------------------------------------- data tables js files ------------------------------------------------------------------->
     <script src="<?php echo base_url('assets/libs/datatables/media/js/jquery.dataTables.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/dataTables.buttons.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.bootstrap4.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.print.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.flash.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.colVis.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.html5.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/pdfmake.min.js')?>"></script> 
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/jszip.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/vfs_fonts.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/media/js/dataTables.bootstrap4.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/ColReorder/js/colReorder.dataTables.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Responsive/js/dataTables.responsive.min.js')?>"></script>
<!-- ---------------------------------------------------------------------------------------------------------------------------------->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/libs/popper.js/dist/umd/popper.min.js');?>"></script>
    <script src="<?php echo base_url('assets/libs/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/sparkline/sparkline.js')?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets/js/waves.js')?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets/js/sidebarmenu.js')?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets/js/custom.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/toastr/build/toastr.min.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/datatable-checkbox-init.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/jquery.multicheck.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/select2/dist/js/select2.full.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/select2/dist/js/select2.min.js')?>"></script>

    <script src="<?php echo base_url('assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/magnific-popup/meg.init.js')?>"></script>

     <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/bootstrap-select.min.js')?>"src="/path/to/datepicker.js"></script>
    <!-- <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/ajax-bootstrap-select.js')?>"></script> -->
    <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/i18n/defaults-en_US.min.js')?>"src="/path/to/datepicker.js"></script>
    <script src="<?php echo base_url('assets/js/custom/printThis.js')?>"></script>

    <script type="text/javascript">
        $(document).ready(function(){
           $('.selectpicker').selectpicker();
           $('.selectpicker').selectpicker('setStyle', 'btn btn-dark'); 
        });

//        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
//   $('.selectpicker').selectpicker('mobile');
// }

    </script>
 
    <script type="text/javascript">
      

      $(".select2").select2();
      
       getNotifications();
    </script>
    <script src="<?php echo base_url('assets/editor/js/summernote.js')?>"></script>
    <script src="<?php echo base_url('assets/editor/js/summernote-bs4.js')?>"></script>
</body>
</html>