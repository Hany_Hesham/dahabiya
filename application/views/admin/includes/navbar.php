       <nav class="no-print navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="<?php echo base_url('admin/site_manager');?>">
                        <b class="logo-icon p-l-10">
                            <img  style="width:45px;height:40px;" src="<?php echo base_url('assets/images/logo-sm.png');?>" alt="homepage" class="light-logo" />
                           
                        </b>
                        <span class="logo-text">
                             <strong style="color:#f1a22d;font-size:24px;">Site</strong><strong style="color:#fff;font-size:24px;">Manager</strong>
                        </span>
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5" style="background-color:#fff !important;">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="m-r-10 mdi mdi-stackexchange font-24" style="color:#000;"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                           
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item"> <a class="nav-link waves-effect waves-light" onclick="openFullscreen()" href="javascript:void(0)"><i class="fas fa-expand-arrows-alt"  style="color:#000;"></i></a>
                            <div class="app-search position-absolute">
                            </div>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                 
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                         <li class="nav-item dropdown">
                            <a class="nav-link waves-effect waves-light" href="<?php echo base_url();?>" target="_blanck"> <i class="m-r-10 mdi mdi-access-point-network font-24" style="color:#000;"></i>
                            </a>
                        </li>


                        <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle waves-effect waves-light" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="m-r-10 mdi mdi-alarm-snooze font-24" style="color:#000;"></i><span id="notifiCount" class="badge badge-pill badge-success" style="margin-left:0px;margin-bottom:20px;"></span>
                            </a>
                           <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown notifications-div" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="" id="notifications">
                                             <!-- Message -->
                                             <div class="dropdown-divider"></div>
                                                <h5 class="m-b-0 centered">الإخطارات  </h5>
                                                <div class="dropdown-divider"></div><h5 class="m-b-0 centered">لا توجد إخطارات  </h5><br>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>


                       
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-light pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url('assets/images/users/download.png');?>" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <div class="dropdown-divider"></div>
                                <strong class="float-right text-danger" style="margin-right:15px;">
                                    <?php echo $this->global_data['sessioned_user']['username']?>                                        
                                </strong>
                                <a class="dropdown-item text-right" href="<?php echo base_url('admin/auth/logout');?>"><i class="fa fa-power-off m-r-5 m-l-5 text-danger" style="color:#000;"></i> تسجيل خروج  </a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>

<script type="text/javascript">
     
</script>