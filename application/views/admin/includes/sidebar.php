
<?php
   $slider_permission = user_access(1);
   $booking_permission = user_access(5);
   $rooms_permission = user_access(15);
   $offers_permission = user_access(2);
   $about_permission = user_access(14);
   $language_permission = user_access(25);
   $gallary_permission  = user_access(4);
   $clients_req_permission      = user_access(21);
   $users_permission          = user_access(6);
   $setting_permission          = user_access(11);
   //  $competitors_permission = user_access(18);
   //  $voters_permission = user_access(26);
   //  $winners_permission = user_access(24);
   //  $partners_permission = user_access(3);
  //  $event_permission            = user_access(6);
  //  $products_permission         = user_access(16);
  //  $portfolio_permission        = user_access(17);
  //  $contact_permission          = user_access(19);
  //  $footer_permission           = user_access(20);
  //  $team_permission             = user_access(23);
?>

<aside class="left-sidebar no-print  float-right" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" >
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager');?>" aria-expanded="false">
                               <i class="fas fa-bars"></i><span class="hide-menu"><strong>Dashboard</strong></span>
                           </a>
                        </li>
                        <?php if($booking_permission['view']==1){?>
                          <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/booking_manager');?>" aria-expanded="false"><i class="fas fa-tasks"></i><span class="hide-menu"><strong>Booking Manager</strong></span>
                            </a>
                          </li>
                        <?php }?>    
                        <?php if($clients_req_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/clients_req_manager');?>" aria-expanded="false"><i class="fas fa-users"></i><span class="hide-menu"><strong>Clients Requests Manager</strong></span></a>
                          </li>
                         <?php }?>
                        <?php if($rooms_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/rooms_manager');?>" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu"><strong>Rooms Manager</strong></span></a>
                          </li>
                        <?php }?>
                        <?php if($offers_permission['view']==1){?>
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/offers_manager');?>" aria-expanded="false"><i class="mdi mdi-star-outline"></i><span class="hide-menu"><strong>Offers Manager</strong></span></a>
                          </li>
                        <?php }?> 
                         <?php if($about_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/about_us_manager');?>" aria-expanded="false"><i class="mdi mdi-angularjs"></i><span class="hide-menu"><strong>About US & Info Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($slider_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/slider_manager');?>" aria-expanded="false"><i class="fas fa-sliders-h"></i><span class="hide-menu"><strong>Sliders</strong></span></a>
                          </li>
                        <?php }?>
                        <?php if($gallary_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/gallary_manager');?>" aria-expanded="false"><i class="fas fa-laptop"></i><span class="hide-menu"><strong>Gallery</strong></span></a>
                          </li>
                        <?php }?>
                        <?php if($language_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/lang_manager');?>" aria-expanded="false"><i class="fas fa-globe"></i><span class="hide-menu"><strong>Languages Translator</strong></span></a>
                          </li>
                        <?php }?>
                        <?php if($users_permission['view']==1){?>
                          <li class="sidebar-item">
                            <a href="<?php echo base_url('admin/users');?>" class="sidebar-link"><i class="fas fa-users"></i><span class="hide-menu"><strong>Users</strong></span></a>
                          </li> 
                        <?php }?>
                        <?php if($setting_permission['view']==1){?>
                          <li class="sidebar-item">
                            <a href="<?php echo base_url('admin/log_activity');?>" class="sidebar-link"><i class="fas fa-shield-alt"></i><span class="hide-menu"><strong>Listener</strong></span></a>
                          </li>
                          <li class="sidebar-item">
                            <a href="<?php echo base_url('admin/log_activity/backup_me');?>" class="sidebar-link"><i class="fas fa-database"></i><span class="hide-menu"><strong>DB Backup</strong></span></a>
                          </li>  
                         <?php }?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>