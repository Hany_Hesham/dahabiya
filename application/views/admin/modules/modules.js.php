<?php 
  initiate_modal(
    'lg','Add New Module','admin/modules/modules_process',
    '<input id="module_id" type="hidden" value="" name="id">'
  )
?>
<script type="text/javascript">
  initDTable(
    "modules-list-table",
    [{"name":"id"},
    {"name":"name"},
    {"name":"menu_group"},
    {"name":"deleted"},],
    "<?php echo base_url("admin/modules/modules_ajax") ?>"
  );
  function getsignature(id){
    var url = getUrl()+'admin/modules/sign_index/'+id;
    jQuery.ajaxSetup({async:false});
    $( "#signatureTable" ).empty();
    $( "#signatureTable" ).load( ""+url+"" );
  }
  function getsignature(id){
    var url = getUrl()+'admin/modules/sign_index/'+id;
    jQuery.ajaxSetup({async:false});
    $( "#signatureTable" ).empty();
    $( "#signatureTable" ).load( ""+url+"" );
  }
  function getmodule(id){
    $('#smallmodal').modal('show');
    $('#smallmodal .modal-body').empty();
    $('#smallmodal .modal-header .modal-title').empty();
    $('form').attr('action', ''+getUrl()+'admin/modules/modules_process');
    var module_id               = $('#moduleId'+id).text();
    var module_name             = $('#moduleName'+id).text();
    var g_view                  = $('#g_view'+id).text();
    var g_viewchecker           = '';
    var view                    = $('#view'+id).text();
    var viewchecker             = '';
    var private_view            = $('#private_view'+id).text();
    var private_viewchecker     = '';
    var creat                   = $('#creat'+id).text();
    var creatchecker            = '';
    var edit                    = $('#edit'+id).text();
    var editchecker             = '';
    var remove                  = $('#remove'+id).text();
    var removechecker           = '';
    var f_change                = $('#f_change'+id).text();
    var f_changechecker         = '';
    var link                    = $('#link'+id).text();
    var view_link               = $('#view_link'+id).text();
    var tdatabase               = $('#tdatabase'+id).text();
    var type                    = $('#type'+id).text();
    var menu_icon               = $('#menu_icon'+id).text();
    var text_color              = $('#text_color'+id).text();
    var background_color        = $('#background_color'+id).text();
    var menu_group              = $('#menu_group'+id).text();
    var menu_group_icon         = $('#menu_group_icon'+id).text();
    var rank                    = $('#rank'+id).text();
    if (g_view == 1) {
      g_viewchecker             = 'checked';
    }
    if (view == 1) {
      viewchecker               = 'checked';
    }
    if (private_view == 1) {
      private_viewchecker       = 'checked';
    }
    if (creat == 1) {
      creatchecker              = 'checked';
    }
    if (edit == 1) {
      editchecker               = 'checked';
    }
    if (remove == 1) {
      removechecker             = 'checked';
    }
    if (f_change == 1) {
      f_changechecker           = 'checked';
    }
    $('#smallmodal .modal-header .modal-title').append('Edit Module');
    $('#smallmodal .modal-body').append('<input id="module_id" type="hidden" value="'+module_id+'" name="id"><input  id="module_name" value="'+module_name+'" type="text" name="name" class="form-control" placeholder="Name" required><br><label class="customcheckbox">General View<input type="checkbox" id="g_view" name="g_view" value="1" class="listCheckbox" '+g_viewchecker+'><span class="checkmark"></span></label><br><label class="customcheckbox">View<input type="checkbox" id="view" name="view" value="1" class="listCheckbox" '+viewchecker+'><span class="checkmark"></span></label><br><label class="customcheckbox">Private View<input type="checkbox" id="private_view" name="private_view" value="1" class="listCheckbox" '+private_viewchecker+'><span class="checkmark"></span></label><br><label class="customcheckbox">Creat<input type="checkbox" id="creat" name="creat" value="1" class="listCheckbox" '+creatchecker+'><span class="checkmark"></span></label><br><label class="customcheckbox">Edit<input type="checkbox" id="edit" name="edit" value="1" class="listCheckbox" '+editchecker+'><span class="checkmark"></span></label><br><label class="customcheckbox">Remove<input type="checkbox" id="remove" name="remove" value="1" class="listCheckbox" '+removechecker+'><span class="checkmark"></span></label><br><label class="customcheckbox">Change<input type="checkbox" id="f_change" name="f_change" value="1" class="listCheckbox" '+f_changechecker+'><span class="checkmark"></span></label><br><input id="link" value="'+link+'" value="'+link+'" type="text" name="link" class="form-control" placeholder="Link" required><br><input id="view_link" value="'+view_link+'" value="'+view_link+'" type="text" name="view_link" class="form-control" placeholder="View Link"><br><input id="tdatabase" value="'+tdatabase+'" value="'+tdatabase+'" type="text" name="tdatabase" class="form-control" placeholder="Database" required><br><input id="type" value="'+type+'" value="'+type+'" type="text" name="type" class="form-control" placeholder="Type" required><br><input id="menu_icon" value="'+menu_icon+'" type="text" name="menu_icon" class="form-control" placeholder="Icon" required><br><input id="text_color" value="'+text_color+'" value="'+text_color+'" type="text" name="text_color" class="form-control" placeholder="Text Color"><br><input id="background_color" value="'+background_color+'" type="text" name="background_color" class="form-control" placeholder="Background Color"><br><input id="menu_group" value="'+menu_group+'" type="text" name="menu_group" value="'+menu_group+'" class="form-control" placeholder="Group" required><br><input id="menu_group_icon" value="'+menu_group_icon+'" type="text" name="menu_group_icon" class="form-control" placeholder="Group Icon" required><br><input id="rank" value="'+rank+'" value="'+rank+'" type="text" name="rank" class="form-control" placeholder="Rank" required><br>');
  }
  $(document).ready(function(){
    $("#add-new").click(function() {
      $('#smallmodal').modal('show');
      $('#smallmodal .modal-body').empty();
      $('#smallmodal .modal-header .modal-title').empty();
      $('form').attr('action', ''+getUrl()+'admin/modules/modules_process');
      $('#smallmodal .modal-header .modal-title').append('Add Module');
      $('#smallmodal .modal-body').append('<input id="module_id" type="hidden" value="" name="id"><input  id="module_name" type="text" name="name" class="form-control" placeholder="Name" required><br><label class="customcheckbox">General View<input type="checkbox" id="g_view" name="g_view" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><label class="customcheckbox">View<input type="checkbox" id="view" name="view" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><label class="customcheckbox">Private View<input type="checkbox" id="private_view" name="private_view" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><label class="customcheckbox">Creat<input type="checkbox" id="creat" name="creat" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><label class="customcheckbox">Edit<input type="checkbox" id="edit" name="edit" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><label class="customcheckbox">Remove<input type="checkbox" id="remove" name="remove" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><label class="customcheckbox">Change<input type="checkbox" id="f_change" name="f_change" value="1" class="listCheckbox"><span class="checkmark"></span></label><br><input id="link" type="text" name="link" class="form-control" placeholder="Link" required><br><input id="view_link" type="text" name="view_link" class="form-control" placeholder="View Link" required><br><input id="tdatabase" type="text" name="tdatabase" class="form-control" placeholder="Database" required><br><input id="type" type="text" name="type" class="form-control" placeholder="Type" required><br><input id="menu_icon" type="text" name="menu_icon" class="form-control" placeholder="Icon" required><br><input id="text_color" type="text" name="text_color" class="form-control" placeholder="Text Color" required><br><input id="background_color" type="text" name="background_color" class="form-control" placeholder="Background Color" required><br><input id="menu_group" type="text" name="menu_group" class="form-control" placeholder="Group" required><br><input id="menu_group_icon" type="text" name="menu_group_icon" class="form-control" placeholder="Group Icon" required><br><input id="rank" type="text" name="rank" class="form-control" placeholder="Rank" required><br>');
    });      
  });    
</script>