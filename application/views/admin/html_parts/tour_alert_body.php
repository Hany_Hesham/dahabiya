<?php if($this->session->flashdata('msg') != ''): ?>
  <div class="form-row" style="margin-bottom: 31px;">
    <div class="input-group mb-3">
      <div style="text-align: center; color:<?php if($this->session->flashdata('alert')=='succsess'){echo'#386e2a';}else{echo '#a82222';}?>">
        <span style="text-align: center; color:<?php if($this->session->flashdata('alert')=='succsess'){echo'#386e2a';}else{echo '#a82222';}?>">
          <?php echo $this->session->flashdata('msg'); ?>
        </span>
      </div>
    </div> 
  </div>
<?php endif; ?>