  <script type="text/javascript">
        var $el1 = $("#offers");
        $("#offers").fileinput({
            uploadUrl: "<?php echo base_url($cr_path.'/upload/'.$module_id.'/'.$temp_id.'/'.$folder);?>",
            uploadAsync: true,
            minFileCount: 1,
            uploadAsync:true,
            maxFileCount: 100,
            // showUpload: false, // hide upload button
            // showRemove: false, // hide remove button
            overwriteInitial: false,
            initialPreviewAsData: true,
            uploadExtraData:{table:'<?php echo $table?>'},
            initialPreview: [
              <?php foreach($uploads as $upload): ?>
                "<div class='file-preview-text'>" +
                "<h2><i class='glyphicon glyphicon-file'></i></h2>" +
                "<a href='<?php echo base_url('site_assets/images/'.$folder.'/'.$upload['file_name']);?>'><?php echo $upload['file_name'] ?></a>" + "</div>",
              <?php endforeach ?>
            ],
            initialPreviewConfig: [
              <?php foreach($uploads as $upload): ?>
                {url:"<?php echo base_url($cr_path.'/remove_file/'.$temp_id.'/'.$upload['id'].'/'.$module_id);?>", 
                key: "<?php echo $table?>"},
              <?php endforeach; ?>
          ],
       }).on("filebatchselected", function(event, files) {
            $el1.fileinput("upload");
        });

//          $('#offers').on('change', function(event) {
//     console.log("change");
// });
  </script>    