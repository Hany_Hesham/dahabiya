<?php 
  if (count($logers) > 0) {
    foreach($logers as $loger){
      $date = $loger['timestamp'];
      $exist = 1;
    }
  }else{
    $date = 'No Data';
    $exist = 0;
  }
?>
<button data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $date?>" value="<?php echo $id?>" id="<?php echo $id?>"  class="card-hover btn btn-dark btn-lg btn-circle allLoger"><i class="far fa-chart-bar"></i></button>
<?php 
  $loged = '<tbody>';
  $x = 1;
  foreach($logers as $loger){ 
    $loged .= '<tr><td>'.$x.'</td><td>'.$loger['user_name'].'</td><td>'.$loger['oldData'].'</td><td>'.$loger['newData'].'</td><td>'.$loger['timestamp'].'</td></tr>';  
    $x++;    
  }          
  $loged .= '</tbody>'; 
?>           
<?php initiate_modal('','','','<table class="table table-hover"><thead style="background-color:#0f597a;color:#FFF"><tr><th width="1%">#</th><th width="9%" style="font-size:13px;">Updated By</th><th width="40%" style="font-size:13px;">From</th><th width="40%" style="font-size:13px;">To</th><th width="10%" style="font-size:13px;">Updated By</th></tr></thead>'.$loged.'</table>','allLoger'.$id.''
)?>
<script type="text/javascript">
  $('.allLoger').click(function(){
    let id = $(this).attr('id');
    $('#allLoger'+id).modal('show');
    $('.modal-title').empty();
    $('.modal-title').append('Loger');
    $('.modal-footer').hide();
  });
</script>