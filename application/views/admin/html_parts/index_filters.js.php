<script type="text/javascript">
    
  $('[data-toggle="datepicker"]').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
  });
  $('[data-toggle="datepicker"]').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
  });
  $('.filter-indexs').change(function () {
      indexsFilter();
  });  
  $('[data-toggle="datepicker"]').datepicker().on('changeDate', function(e) {
       indexsFilter();
  });
  function dateInitial(){
      this.fromDate = '<?php echo (isset($from_date))? $from_date : ''?>'; 
      this.toDate   = '<?php echo (isset($from_date))? $to_date : ''?>'; 
  }
  function tableConstructor(){
     this.indexTable          = $('.indexTable').attr('id'); 
     this.index_fun           = $('#tableFun').text(); 
     this.tablCols            = $('.tableCol').map(function(){
                                     return {name:$(this).attr('id')};
                                 }).get();
     this.indexName           = $('#indexName').text();
     this.total               = $('#totalCount').text();
     this.hotel_id            = $('#hotel_id').val();
     this.to_hotel_id         = $('#to_hotel_id').val();
     this.dep_code            = $('#dep_code').val();
     this.fromDate            = $('#fromDate').val();
     this.toDate              = $('#toDate').val();
     this.role_id             = $('#role_id').val();
     this.status_id           = $('#status_id').val();
     this.arrivalDate         = $('#arrivalDate').val();
     this.departureDate       = $('#departureDate').val();
     this.fromFormDate        = $('#fromFormDate').val();
     this.toFormDate          = $('#toFormDate').val();
     this.items_search        = $('#items_search').val();
     this.shop_upload_types   = $('#shop_upload_types').val();
       if (this.fromDate == '' || this.toDate == '') {
           let intiDate = new dateInitial();
           if (this.fromDate == ''){this.fromDate = intiDate.fromDate}
           if (this.toDate == ''){this.toDate = intiDate.toDate}   
        }
      $('#fromDate').val(this.fromDate);
      $('#toDate').val(this.toDate);  
  }  
  function indexsFilter(){
     let filters    = new tableConstructor();
     let table      = $('#'+filters.indexTable+'').DataTable();
     let indexData  = {reportName:filters.indexName,total:filters.total};
     let data       = {searchBy:{
                                  hotel_id:filters.hotel_id,
                                  to_hotel_id:filters.to_hotel_id,
                                  dep_code:filters.dep_code,
                                  fromDate:filters.fromDate,
                                  toDate:filters.toDate,
                                  role_id:filters.role_id,
                                  status_id:filters.status_id,
                                  arrivalDate:filters.arrivalDate,
                                  departureDate:filters.departureDate,
                                  fromFormDate:filters.fromFormDate,
                                  toFormDate:filters.toFormDate,
                                  items_search:filters.items_search,
                                  shop_upload_types:filters.shop_upload_types,
                                }
                      };
     if (filters.hotel_id !='' ||  filters.to_hotel_id != ''  || filters.dep_code != '' || filters.fromDate != ''|| filters.toDate != '' 
         || filters.role_id != '' || filters.status_id !='' || filters.arrivalDate != '' || filters.departureDate !=''
         || filters.fromFormDate != '' || filters.toFormDate !='' || filters.items_search !='' || filters.shop_upload_types !='') {
       table.destroy();
       initDTableButtons(
                          ""+filters.indexTable+"",
                          filters.tablCols,
                          "<?php echo base_url("")?>"+filters.index_fun,
                          data,'',indexData
                        );
      } 
  }
</script>