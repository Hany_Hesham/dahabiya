<div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h5 class="page-title">Languages Translator</h5>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item">Languages Translator</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                         <table id="items-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th width="5%"><strong>#</strong></th>
                                  <th width="25%"><strong>English</strong></th>
                                  <th width="25%"><strong>German</strong></th>
                                  <th width="25%"><strong>French</strong></th>
                                  <th width="20%"><strong>Arabic</strong></th>
                                  <th class="text-center" width="1%">
                                    <a href="<?php echo base_url('admin/site_manager/blank_translate')?>" name="remove" id="addRow" class="btn btn-light btn-sm btn-circle btn_remove">
                                      <i class="mdi mdi-database-plus"></i>
                                    </a>
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <textarea rows="3" name="english" class="form-control"></textarea>
  <?php initiate_modal(
                         'lg','Languages Translator','admin/site_manager/lang_translator',
                          '<input id="row_id" type="hidden" name="id">
                           <label class="text-right control-label col-form-label" style="font-size:12px;">*English</label>
                           <textarea id="english-language-input" rows="3" name="english" class="form-control"></textarea><br>
                           <label class="text-right control-label col-form-label" style="font-size:12px;">*German</label>
                           <textarea id="german-language-input" rows="3" name="german" class="form-control"></textarea><br>
                           <label class="text-right control-label col-form-label" style="font-size:12px;">*French</label>
                           <textarea id="french-language-input" rows="3" name="french" class="form-control"></textarea><br>
                          <label class="text-right control-label col-form-label" style="font-size:12px;">*Arabic</label>
                          <textarea id="arabic-language-input" rows="3" name="arabic" class="form-control"></textarea>
                            <br>'

                         )?>

<script type="text/javascript">
      initDTable(
                 "items-list-table",
                    [{"name":"id"},
                     {"name":"english"},
                     {"name":"german"},
                     {"name":"french"},
                     {"name":"arabic"},],
                  "<?php echo base_url("admin/site_manager/lang_manager_ajax") ?>"
           );
      function fillLanguageModal(formData){
         $('#smallmodal').modal('show');
         $(".modal-body #row_id").val(formData.id);
         $(".modal-body #english-language-input").val(formData.english);
         $(".modal-body #german-language-input").val(formData.german);
         $(".modal-body #french-language-input").val(formData.french);
         $(".modal-body #arabic-language-input").val(formData.arabic);
      }    
    </script>