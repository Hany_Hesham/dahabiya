<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                <div class="col-sm-4">
                    <div class="float-left">
                        <h5 class="page-title">Add Offer</h5>      
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="ml-auto float-right">
                        <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager/offers_manager');?>">Offers Manager</a></li>
                            <li class="breadcrumb-item">Add Offer</li>
                        </ol>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <?php if(!isset($offer)) {?>
                    <?php echo form_open(base_url('admin/site_manager/add_offer'), 'class="form-horizontal" id="addEditOfferForm" enctype="multipart/form-data"');  ?> 
                <?php }else{?>    
                    <?php echo form_open(base_url('admin/site_manager/edit_offer/'.$offer['id']), 'class="form-horizontal" id="addEditOfferForm" enctype="multipart/form-data"');  ?> 
                <?php }?>    
                  <div class="card-body">
                      <div class="row">
                            <div class="form-group m-t-20 col-md-6">
                              <label>*Offer Title</label>
                              <input type="text" name="title" class="form-control" id="Offer Title" 
                              value="<?php echo isset($offer) ? $offer['title']: ''?>" placeholder="Enter Title" required>
                            </div>
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Description</label>
                                <input type="text" name="description" class="form-control" id="Offer Description" 
                                value="<?php echo isset($offer) ? $offer['description']: ''?>" placeholder="Enter Description" required>
                            </div>
                      </div>
                      <div class="row">
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Currency</label>
                                <select  id="category" name="currency" class="form-control" required>
                                    <option value="EGP" <?php echo isset($offer) && $offer['currency'] == 'EGP' ? 'selected':''?>>EGP</option>
                                    <option value="USD" <?php echo isset($offer) && $offer['currency'] == 'USD' ? 'selected':''?>>$</option>
                                    <option value="EUR" <?php echo isset($offer) && $offer['currency'] == 'EUR' ? 'selected':''?>>Euro</option>   
                                </select>
                            </div>
                            <div class="form-group m-t-20 col-md-4">
                              <label>*Rate</label>
                              <input type="number" step="any" name="price" class="form-control" id="price" 
                              value="<?php echo isset($offer) ? $offer['price']: ''?>" placeholder="Enter Rate" required>
                            </div>
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Tax</label>
                                <input type="number" step="any" name="tax" class="form-control" id="tax"
                                 value="<?php echo isset($offer) ? $offer['tax']: ''?>" placeholder="Enter Tax" required>
                            </div>
                      </div>
                      <div class="row">
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Nights</label>
                                <input type="number" name="nights" class="form-control" id="nights"
                                 value="<?php echo isset($offer) ? $offer['nights']: ''?>" required>
                            </div>
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Adults</label>
                                <input type="number" name="adults" class="form-control" id="adults"
                                 value="<?php echo isset($offer) ? $offer['adults']: ''?>" required>
                            </div>
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Childerns</label>
                                <input type="number" name="childs" class="form-control" id="childs"
                                 value="<?php echo isset($offer) ? $offer['childs']: ''?>" required>
                            </div>
                      </div>
                      <div class="row">
                        <div class="form-group m-t-20 col-md-4">
                            <label>*Image</label>
                            <input type="file" name="image" class="form-control" id="image">
                        </div>
                        <div class="form-group m-t-40 col-md-4"><br>
                            <?php echo isset($offer['image']) && $offer['image'] !='' ? getImageViewer(base_url('site_assets/images/offers/'.$offer['image'])) : "";?>
                        </div>
                      </div>
                      <?php if(isset($offer['features'])){?>
                        <div class="row">
                            <div class="col-md-6">
                                    <h4>Offers Includes  
                                        <a href="javascript:void(0)" onclick="AddEditOfferFeature(``)" class='btn btn-outline-dark btn-sm'>
                                            <strong><i class='mdi mdi-folder-plus' style="font-size:18px;"></i></strong>
                                        </a>
                                    </h4>
                                    <table class="table">
                                    <tbody>
                                        <?php $i=0; foreach($offer['features'] as $feature){
                                            if($feature['category'] == 'included'){ 
                                            $i++;
                                        ?>
                                            <tr>
                                                <td width="5%"><?php echo $i;?></td>
                                                <td width="95%">
                                                    <?php echo $feature['feature']?><br>
                                                    <a href="javascript:void(0)" onclick="AddEditOfferFeature(`<?php echo $feature['id']?>`)" class='btn btn-primary btn-sm'>
                                                        <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                                    </a>
                                                    <a href="<?php echo base_url('admin/site_manager/delete_offer_feature/'.$offer['id'].'/'.$feature['id'])?>" class='btn btn-danger btn-sm'>
                                                        <strong>Delete <i class='mdi mdi-delete'></i></strong>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } }?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h4>Offers Conditions
                                    <a href="javascript:void(0)" onclick="AddEditOfferFeature(``)" class='btn btn-outline-dark btn-sm'>
                                        <strong><i class='mdi mdi-folder-plus' style="font-size:18px;"></i></strong>
                                    </a>
                                </h4>
                                <table class="table">
                                    <tbody>
                                        <?php $i=0; foreach($offer['features'] as $feature){
                                            if($feature['category'] == 'conditions'){ 
                                            $i++;
                                        ?>
                                            <tr>
                                                <td width="5%"><?php echo $i;?></td>
                                                <td width="95%">
                                                    <?php echo $feature['feature']?><br>
                                                    <a href="javascript:void(0)" onclick="AddEditOfferFeature(`<?php echo $feature['id']?>`)" class='btn btn-primary btn-sm'>
                                                        <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                                    </a>
                                                    <a href="<?php echo base_url('admin/site_manager/delete_offer_feature/'.$offer['id'].'/'.$feature['id'])?>" class='btn btn-danger btn-sm'>
                                                        <strong>Delete <i class='mdi mdi-delete'></i></strong>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      <?php }?>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <input type="submit" name="submit" value="Save" class="btn btn-primary" style="width:100%"/>
                        </div>
                    </div>
                <?php echo form_close( ); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="offerFeatureModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_offer_feature'), 'class="form-horizontal" id="addEditOfferFeature" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Add & Edit Offer conditions</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <input id="offer_id" type="hidden" name="offer_id" class="form-control"> 
                 <input id="feature_id" type="hidden" name="id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Category</label>
                      <select  id="category" name="category" class="selectpicker form-control" data-container="body" 
                      data-live-search="true" title="Select category" data-hide-disabled="true" data-actions-box="true"
                       data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                        <option value="included">Included</option>
                        <option value="conditions">Conditions</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Feature</label>
                      <input  id="feature" type="text" name="feature" class="form-control " placeholder="feature" required>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
<script>
    function AddEditOfferFeature(id) {
      if(id == ""){
         $('#addEditOfferForm').trigger("reset");
         $('#offerFeatureModal').modal('show');
         $('#offerFeatureModal .modal-body #offer_id').val(`<?php echo $offer['id']?>`);
       }  
      var url="<?php echo base_url('admin/site_manager/get_rowDataAjax/Site_manager_model/get_offer_feature/');?>";      
      $.ajax({
          url: url,
          dataType: "json",
          type : 'POST',
          data:{search:id},
            success: function(data){
                $('#offerFeatureModal').modal('show');
                let {id, offer_id, category, feature} = data;
                $('#offerFeatureModal .modal-body #offer_id').val(offer_id);
                $('#offerFeatureModal .modal-body #feature_id').val(id);
                $('#offerFeatureModal .modal-body #category').val(category);
                $('#offerFeatureModal .modal-body #feature').val(feature);
                $('.selectpicker').selectpicker('refresh');
              }
          });
     }
</script>