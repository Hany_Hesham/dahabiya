
<div class="page-breadcrumb">
  <div class="row">
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
              <div class="col-sm-4">
                <div class="float-left">
                    <h4 class="page-title">Gallery Manager
                      <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editgallery_Modal(``)'>
                        <strong> <i class='mdi mdi-book-plus'></i></strong>
                      </a>
                    </h4>      
                </div>
              </div>
              <div class="col-sm-12">
                <div class="ml-auto float-right">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item">Gallery Manager</li>
                      </ol>
                    </nav>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
</div>

<div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="gallery-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="30%"><strong>title</strong></th>
                              <th width="30%"><strong>Category</strong></th>
                              <th width="10%"><strong>Rank</strong></th>
                              <th width="30%"><strong>Image</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $i=0; if(isset($gallerys)){ foreach($gallerys as $gallery){$i++?>
                              <tr>
                                <td><?php echo $i?></td>
                                <td>
                                  <strong> <?php echo $gallery['title']?> </strong><br>
                                    <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editgallery_Modal(`<?php echo $gallery["id"] ?>`)'>
                                      <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                    </a> 
                                    <a href='<?php echo base_url('admin/site_manager/delete_gallary/'.$gallery['id'])?>' class='btn btn-danger btn-sm'>
                                      <strong>Delete <i class='mdi mdi-trash'></i></strong>
                                    </a> 
                                </td>  
                                <td> <?php echo $gallery['gallery_category']?></td>
                                <td> <?php echo $gallery['rank']?></td>
                                <td> 
                                  <div style="width:50%;height:50%;">
                                    <?php echo isset($gallery['img']) && $gallery['img'] !='' ? getImageViewer(base_url('site_assets/images/gallery/'.$gallery['img'])) : ""?>
                                  <div>
                                </td>
                              </tr>
                            <?php } }?>
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="gallery_Modal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_gallary'), 'class="form-horizontal" id="EditGalleryForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Add & Edit gallery</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="gallery_id" type="hidden" name="id" class="form-control">
                  <div class="form-group row">
                        <div class="col-sm-12">
                          <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                          <input id="title" type="text" name="title" class="form-control">
                        </div>
                  </div>
                  <div class="form-group row">
                        <div class="col-sm-12">
                          <label class="text-right control-label col-form-label" style="font-size:12px;">*Rank</label>
                          <input id="rank" type="text" name="rank" class="form-control">
                        </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Category</label>
                      <select class="form-control" name="categories" id="categories">
                          <?php foreach($categories as $category){?>
                            <option value="<?php echo $category['id']?>"><?php echo $category['title']?></option>
                          <?php }?>  
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                        <div class="col-sm-12">
                          <label class="text-right control-label col-form-label" style="font-size:12px;">*Image</label>
                          <input type="file" name="img" class="form-control">
                        </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
  <script type="text/javascript">
    initDTable_normal(
          'gallery-list-table',0, 
          [{"name":"id"},
          {"name":"title"},
          {"name":"category"},
          {"name":"rank"},
          {"name":"img"},]);
    
    function Editgallery_Modal(id) {
      console.log(id);
      if(id == ''){
        $('#gallery_Modal').modal('show');
        $('#EditGalleryForm').trigger("reset");
        return false;
      }
      var url="<?php echo base_url('admin/site_manager/get_rowDataAjax/Site_manager_model/get_gallary_item/');?>";      
      $.ajax({
          url: url,
          dataType: "json",
          type : 'POST',
          data:{search:id},
            success: function(data){
              $('#gallery_Modal').modal('show');
                let {id, title, categories, rank} = data;
                $('#gallery_Modal .modal-body #gallery_id').val(id);
                $('#gallery_Modal .modal-body #title').val(title);
                $('#gallery_Modal .modal-body #categories').val(categories);
                $('#gallery_Modal .modal-body #rank').val(rank);
              }
          });
     }
 
  </script>
          

  