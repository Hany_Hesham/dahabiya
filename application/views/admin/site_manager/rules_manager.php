<link href="<?php echo base_url('assets/css/summernote-bs4.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/summernote.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/summernote.js')?>"></script>
<script src="<?php echo base_url('assets/js/summernote-bs4.js')?>"></script>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Rules Manager
                        <a href='<?php echo base_url('admin/site_manager/add_rule')?>'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-book-plus'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Rules Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br> 
                      <table id="rules-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="25%"><strong>rule</strong></th>
                              <th width="40%"><strong>Content</strong></th>
                              <th width="10%"><strong>Rank</strong></th>
                              <th width="15%"><strong>Active</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="ruleModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_rule'), 'class="form-horizontal" id="addEditruleForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">New rule</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="rule_id" type="hidden" name="rule_id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Type</label>
                      <select id="type" name="type" class="form-control"  required>
                        <option value="weekly">Weekly</option>
                        <option value="yearly">Yearly</option>
                      </select>                    
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input  id="title" type="text" name="title" class="form-control " placeholder="Title" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Description</label>
                      <textarea  class="form-control" name="description" id="description" rows="4"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Start Date</label>
                      <input  id="start_date" type="date" name="start_date" class="form-control " placeholder="Start Date" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*End Date</label>
                      <input  id="end_date" type="date" name="end_date" class="form-control " placeholder="End Date" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Price</label>
                      <input  id="price" step="any" type="number" name="price" class="form-control " placeholder="Price" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Currency</label>
                      <select  id="currency" name="currency" class="form-control"  required>
                        <option value="EGP">EGP</option>
                        <option value="USD">$</option>
                        <option value="EUR">Euro</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Image</label>
                      <input  id="image" type="file" name="image" class="form-control " placeholder="Image" required>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
<script type="text/javascript">
    initDTable(
                "rules-list-table",
                  [{"name":"id"},
                    {"name":"title"},
                    {"name":"content"},
                    {"name":"rank"},
                    {"name":"disabled"},],
                "<?php echo base_url("admin/site_manager/rules_ajax/") ?>"
          );
    function addEditruleModal(rule) {
      $('#ruleModal').modal('show');
      console.log(rule);
      if(rule != undefined){
        let {id, type, title, description, start_date, end_date, price, currency} = rule;
        $('#ruleModal .modal-body #rule_id').val(id);
        $('#ruleModal .modal-body #type').val(type);
        $('#ruleModal .modal-body #title').val(title);
        $('#ruleModal .modal-body #description').val(description);
        $('#ruleModal .modal-body #start_date').val(start_date);
        $('#ruleModal .modal-body #end_date').val(end_date);
        $('#ruleModal .modal-body #price').val(price);
        $('#ruleModal .modal-body #currency').val(currency);
        $('#ruleModal .modal-body #image').attr('required', false);
      }else{
        $('#addEditruleForm').trigger("reset");
      }
     }
  </script>