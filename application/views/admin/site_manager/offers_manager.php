<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Offers Manager
                        <a href='<?php echo base_url('admin/site_manager/add_offer')?>'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-book-plus'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Offers Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br> 
                      <table id="offers-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="30%"><strong>Offer</strong></th>
                              <th width="15%"><strong>Nights</strong></th>
                              <th width="15%"><strong>Adults</strong></th>
                              <th width="15%"><strong>Cilderns</strong></th>
                              <th width="15%"><strong>Image</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    initDTable(
                "offers-list-table",
                  [{"name":"id"},
                    {"name":"title"},
                    {"name":"nights"},
                    {"name":"adults"},
                    {"name":"childs"},
                    {"name":"id"},],
                "<?php echo base_url("admin/site_manager/offers_ajax/") ?>"
          );
  </script>