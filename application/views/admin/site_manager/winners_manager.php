<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Winners Manager
                        <a href='javascript: void(0);' onclick='addEditWinnerModal()'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-book-plus'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Winners Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="winners-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="20%"><strong>Competitior Name</strong></th>
                              <th width="20%"><strong>Competition</strong></th>
                              <th width="15%"><strong>Active</strong></th>
                              <th width="20%"><strong>Email</strong></th>
                              <th width="10%"><strong>Rank</strong></th>
                              <th width="20%"><strong>Image</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="winnerModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_winner'), 'class="form-horizontal" id="addEditWinnerForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">New Winner</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="winner_id" type="hidden" name="id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Rank</label>
                      <input  id="rank" type="text" name="rank" class="form-control " placeholder="rank" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Competitor</label>
                      <select  id="competitor_id" name="competitor_id" class="selectpicker form-control" data-container="body" 
                      data-live-search="true" title="Select Winner" data-hide-disabled="true" data-actions-box="true"
                       data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Competitions</label>
                      <select  id="competition_id" name="competition_id" class="selectpicker form-control" data-container="body" 
                      data-live-search="true" title="Select Competition" data-hide-disabled="true" data-actions-box="true"
                       data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                      </select>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
<script type="text/javascript">
    initDTable(
                "winners-list-table",
                  [{"name":"id"},
                    {"name":"name"},
                    {"name":"disabled"},
                    {"name":"competition_title"},
                    {"name":"email"},
                    {"name":"rank"},
                    {"name":"id"},],
                "<?php echo base_url("admin/site_manager/winners_ajax/") ?>"
          );
    function addEditWinnerModal(winner) {
      $('#winnerModal').modal('show');
      if(winner != undefined){
        let {id, competitor_id, competition_id, rank} = winner;
        $('#winnerModal .modal-body #winner_id').val(id);
        $('#winnerModal .modal-body #rank').val(rank);
        $('#winnerModal .modal-body #competitor_id').val(competitor_id);
        $('#winnerModal .modal-body #competition_id').val(competition_id);
        $('.selectpicker').selectpicker('refresh');
      }else{
        $('#addEditWinnerForm').trigger("reset");
      }
     }
    <?php if(isset($competitors)){?>
        $(document).ready(function(){
            let competitors = JSON.parse(`<?php echo json_encode($competitors)?>`);
            $("#competitor_id").append(`<option disabled>Select Winner</option>`);
            for(let competitor of competitors){
                $("#competitor_id").append(`<option value="${competitor.id}" data-subtext="${competitor.email}">${competitor.name} ${competitor.middle_name} ${competitor.last_name} </option>`);
            }
            $('.selectpicker').selectpicker('refresh');           
        });
    <?php }?>

    <?php if(isset($competitions)){?>
        $(document).ready(function(){
            let competitions = JSON.parse(`<?php echo json_encode($competitions)?>`);
            $("#competitor_id").append(`<option disabled>Select Winner</option>`);
            for(let competition of competitions){
                $("#competition_id").append(`<option value="${competition.id}">${competition.title}</option>`);
            }
            $('.selectpicker').selectpicker('refresh');           
        });
    <?php }?>
  </script>