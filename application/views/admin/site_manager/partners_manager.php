<link href="<?php echo base_url('assets/css/summernote-bs4.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/summernote.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/summernote.js')?>"></script>
<script src="<?php echo base_url('assets/js/summernote-bs4.js')?>"></script>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Partners Manager
                        <a href='javascript: void(0);' onclick='EditPartnersModal()'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-book-plus'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Partners Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="about_us-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="40%"><strong>Title</strong></th>
                              <th width="50%"><strong>Image</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; foreach($partners as $partner){ $i++?>
                                <tr>
                                    <td>
                                    <?php echo $i?>
                                    </td>
                                    <td>
                                        <?php echo $partner['title']?><br><br>
                                        <div class='info'>
                                            <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='EditPartnersModal(`<?php echo json_encode($partner) ?>`)'>
                                            <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                            </a> 
                                            <a href='<?php echo base_url('admin/site_manager/delete_partner/'.$partner['id'])?>' class='btn btn-danger btn-sm'>
                                            <strong>Delete <i class='mdi mdi-archive'></i></strong>
                                            </a> 
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn default btn-outline image-popup-vertical-fit el-link" 
                                            href="<?php if($partner['img']!='0'){echo base_url('site_assets/uploads/partners/'.$partner['img']);}elseif($partner['img']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>">
                                            <img style="width:100%;height:60px;" src="<?php if($partner['img']!='0'){echo base_url('site_assets/uploads/partners/'.$partner['img']);}elseif($partner['img']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>" alt="No Attach">
                                        </a>
                                    </td>
                                </tr>
                            <?php }?>
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="partners_Modal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_partner'), 'class="form-horizontal" id="partners_form" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Add And Edit Partner</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="partner_id" type="hidden" name="partner_id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input id="title" type="text" name="title" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Image</label>
                      <input  id="img" type="file" name="img" class="form-control " placeholder="Image">
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
  <script type="text/javascript">
    initDTable_normal(
          'about_us-list-table',0, 
          [{"name":"id"},
          {"name":"title"},
          {"name":"image"},]);
    
    function EditPartnersModal(partner) {
        $('#partners_Modal').modal('show');
        if(partner != undefined){
            partner = JSON.parse(partner);
            let {id, title} = partner;
            $('#partners_Modal .modal-body #partner_id').val(id);
            $('#partners_Modal .modal-body #title').val(title);
      }else{
            $('#partners_Modal .modal-body #partner_id').val('');
            $('#partners_form').trigger("reset");
      }
     }
 
  </script>
          

  