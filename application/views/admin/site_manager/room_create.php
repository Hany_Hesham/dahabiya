<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                <div class="col-sm-4">
                    <div class="float-left">
                        <h5 class="page-title">Add Room</h5>      
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="ml-auto float-right">
                        <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager/rooms_manager');?>">Rooms Manager</a></li>
                            <li class="breadcrumb-item">Add Room</li>
                        </ol>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <?php if(!isset($room)) {?>
                    <?php echo form_open(base_url('admin/site_manager/add_room'), 'class="form-horizontal" id="addEditroomForm" enctype="multipart/form-data"');  ?> 
                <?php }else{?>    
                    <?php echo form_open(base_url('admin/site_manager/edit_room/'.$room['id']), 'class="form-horizontal" id="addEditroomForm" enctype="multipart/form-data"');  ?> 
                <?php }?>    
                  <div class="card-body">
                      <div class="row">
                            <div class="form-group m-t-20 col-md-6">
                              <label>*Room Title</label>
                              <input type="text" name="room" class="form-control" id="Room Title" 
                              value="<?php echo isset($room) ? $room['room']: ''?>" placeholder="Enter Title" required>
                            </div>
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Inventory</label>
                                <input type="number" name="inventory" class="form-control" id="inventory" 
                                value="<?php echo isset($room) ? $room['inventory']: ''?>" placeholder="Enter Inventory" required>
                            </div>
                      </div>
                      <div class="row">
                            <div class="form-group m-t-20 col-md-6">
                            <label>*Category</label>
                            <select  id="category" name="category" class="form-control" required>
                                <?php foreach($categories as $catgeory){?>
                                    <option value="<?php echo $catgeory['id']?>"
                                        <?php echo isset($room) && $room['category'] == $catgeory['id'] ? 'selected':''?> >
                                        <?php echo $catgeory['title']?>
                                    </option>
                                <?php }?>    
                            </select>
                            </div>
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Rank</label>
                                <input type="number" name="rank" class="form-control" id="rank" 
                                value="<?php echo isset($room) ? $room['rank']: ''?>" placeholder="Enter Rank" required>
                            </div>
                      </div>
                      <div class="row">
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Currency</label>
                                <select  id="category" name="currency" class="form-control" required>
                                    <option value="EGP" <?php echo isset($room) && $room['currency'] == 'EGP' ? 'selected':''?>>EGP</option>
                                    <option value="USD" <?php echo isset($room) && $room['currency'] == 'USD' ? 'selected':''?>>$</option>
                                    <option value="EUR" <?php echo isset($room) && $room['currency'] == 'EUR' ? 'selected':''?>>Euro</option>   
                                </select>
                            </div>
                            <div class="form-group m-t-20 col-md-4">
                              <label>*Rate</label>
                              <input type="number" step="any" name="price" class="form-control" id="price" 
                              value="<?php echo isset($room) ? $room['price']: ''?>" placeholder="Enter Rate" required>
                            </div>
                            <div class="form-group m-t-20 col-md-4">
                                <label>*Tax</label>
                                <input type="number" step="any" name="tax" class="form-control" id="tax"
                                 value="<?php echo isset($room) ? $room['tax']: ''?>" placeholder="Enter Tax" required>
                            </div>
                      </div>
                      <div class="row">
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Adults</label>
                                <input type="number" name="adult" class="form-control" id="adults"
                                 value="<?php echo isset($room) ? $room['adult']: ''?>" required>
                            </div>
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Childerns</label>
                                <input type="number" name="child" class="form-control" id="childs"
                                 value="<?php echo isset($room) ? $room['child']: ''?>" required>
                            </div>
                      </div>
                      <div class="row">
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Bed Description</label>
                                <input type="text" name="bed" class="form-control" id="bed" 
                                value="<?php echo isset($room) ? $room['bed']: ''?>" required>
                            </div>
                            <div class="form-group m-t-20 col-md-6">
                                <label>*Room Space Description</label>
                                <input type="text" name="space" class="form-control" id="space" 
                                value="<?php echo isset($room) ? $room['space']: ''?>" required>
                            </div>
                      </div><hr>
                      <?php if(isset($room['features'])){?>
                      <div class="row">
                           <div class="col-md-6">
                                <h4>Room Features  
                                    <a href="javascript:void(0)" onclick="AddEditRoomFeature(``)" class='btn btn-outline-dark btn-sm'>
                                        <strong><i class='mdi mdi-folder-plus' style="font-size:18px;"></i></strong>
                                    </a>
                                </h4>
                                <table class="table">
                                 <tbody>
                                    <?php $i=0; foreach($room['features'] as $feature){
                                        if($feature['category'] == 'room features'){ 
                                        $i++;
                                    ?>
                                        <tr>
                                            <td width="5%"><?php echo $i;?></td>
                                            <td width="95%">
                                                <?php echo $feature['feature']?><br>
                                                <a href="javascript:void(0)" onclick="AddEditRoomFeature(`<?php echo $feature['id']?>`)" class='btn btn-primary btn-sm'>
                                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                                </a>
                                                <a href="<?php echo base_url('admin/site_manager/delete_room_feature/'.$room['id'].'/'.$feature['id'])?>" class='btn btn-danger btn-sm'>
                                                    <strong>Delete <i class='mdi mdi-delete'></i></strong>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } }?>
                                 </tbody>
                             </table>
                          </div>
                          <div class="col-md-6">
                            <h4>Room Amenities
                                <a href="javascript:void(0)" onclick="AddEditRoomFeature(``)" class='btn btn-outline-dark btn-sm'>
                                    <strong><i class='mdi mdi-folder-plus' style="font-size:18px;"></i></strong>
                                </a>
                            </h4>
                             <table class="table">
                                 <tbody>
                                    <?php $i=0; foreach($room['features'] as $feature){
                                        if($feature['category'] == 'room amenities'){ 
                                        $i++;
                                    ?>
                                        <tr>
                                            <td width="5%"><?php echo $i;?></td>
                                            <td width="95%">
                                                <?php echo $feature['feature']?><br>
                                                <a href="javascript:void(0)" onclick="AddEditRoomFeature(`<?php echo $feature['id']?>`)" class='btn btn-primary btn-sm'>
                                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                                </a>
                                                <a href="<?php echo base_url('admin/site_manager/delete_room_feature/'.$room['id'].'/'.$feature['id'])?>" class='btn btn-danger btn-sm'>
                                                    <strong>Delete <i class='mdi mdi-delete'></i></strong>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } }?>
                                 </tbody>
                             </table>
                          </div>
                      </div>
                      <?php }?>
                      <div class="row">
                          <div class="col-md-12">
                              <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                              <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                          </div>
                      </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <input type="submit" name="submit" value="Save" class="btn btn-primary" style="width:100%"/>
                        </div>
                    </div>
                <?php echo form_close( ); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="roomFeatureModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_room_feature'), 'class="form-horizontal" id="addEditroomFeature" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Add & Edit Room Features</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <input id="room_id" type="hidden" name="room_id" class="form-control"> 
                 <input id="feature_id" type="hidden" name="id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Category</label>
                      <select  id="category" name="category" class="selectpicker form-control" data-container="body" 
                      data-live-search="true" title="Select category" data-hide-disabled="true" data-actions-box="true"
                       data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                        <option value="room features">room features</option>
                        <option value="room amenities">room amenities</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Feature</label>
                      <input  id="feature" type="text" name="feature" class="form-control " placeholder="feature" required>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
<?php upfiles_js('admin/site_manager',$uploads,15,$gen_id,'rooms','site_files');?>
<script>
    function AddEditRoomFeature(id) {
      if(id == ""){
         $('#addEditroomFeature').trigger("reset");
         $('#roomFeatureModal').modal('show');
         $('#roomFeatureModal .modal-body #room_id').val(`<?php echo $room['id']?>`);
       }  
      var url="<?php echo base_url('admin/site_manager/get_rowDataAjax/Site_manager_model/get_room_feature/');?>";      
      $.ajax({
          url: url,
          dataType: "json",
          type : 'POST',
          data:{search:id},
            success: function(data){
                $('#roomFeatureModal').modal('show');
                let {id, room_id, category, feature} = data;
                $('#roomFeatureModal .modal-body #room_id').val(room_id);
                $('#roomFeatureModal .modal-body #feature_id').val(id);
                $('#roomFeatureModal .modal-body #category').val(category);
                $('#roomFeatureModal .modal-body #feature').val(feature);
                $('.selectpicker').selectpicker('refresh');
              }
          });
     }
</script>