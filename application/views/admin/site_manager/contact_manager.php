
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h4 class="page-title">Contact Info Manager</h4>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
       <?php  echo form_open(base_url('admin/site_manager/contact_manager'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                     <div class="col-sm-12">
                        <div class="table-responsive m-t-40" style="clear: both;">
                          <div style="width:100%;">
                            <table id="Items-list-table" class="table table-hover">
                              <thead class="thead-dark">
                                  <tr>
                                      <th width="5%"  class="hidden">#</th>
                                      <th width="15%" style="font-size:13px;">Header</th>
                                      <th width="30%" style="font-size:13px;">First Line</th>
                                      <th width="30%" style="font-size:13px;">Second Line</th>
                                  </tr>
                              </thead>
                              <tbody>
                               <?php 
                                if (isset($contacts)) {$i=1;$ids = '';foreach($contacts as $row){ $ids= $i.','.$ids;?>
                                  <tr class="rowIds" id="row<?php echo $i?>">
                                    <td class="hidden"><?php echo $i?></td>
                                    <td>
                                      <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                                      <input type="hidden" name="items[<?php echo $i?>][meta]" value="contact_us"/>
                                      <input type="text" name="items[<?php echo $i?>][header]" class="form-control" value="<?php echo $row['header']?>" readonly/>
                                    </td>
                                    <td>
                                      <input type="text" name="items[<?php echo $i?>][title]" class="form-control" value="<?php echo $row['title']?>" required/>
                                    </td>
                                    <td>
                                       <input type="text" name="items[<?php echo $i?>][paragraph]" class="form-control" value="<?php echo $row['paragraph']?>" required/>
                                    </td>
                                  </tr>
                                <?php $i++;} }?>  
                              </tbody>
                          </table>
                          <input class="form-control " type="hidden" id="allstoredIds"  value="<?php echo (isset($contacts)) ? $ids :''?>">
                        </div>
                      <br>
                    </div>
                  </div>  
                </div> 
              </div>
            </div>
          </div>
        </div> 
        <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-12"> 
                  <input type="submit" name="submit" class="btn btn-dark btn-lg" value="Save" style="float:right;width:12%;">
                </div>
              </div>
            </div>
          </div> 
      </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
    <?php echo form_close( ); ?>
    <?php $this->load->view('admin/html_parts/loader_div');?>       
    <?php $this->load->view('admin/site_manager/site_manager.js.php');?> 
   