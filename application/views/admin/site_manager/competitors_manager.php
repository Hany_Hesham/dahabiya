<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Competitors Manager</h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Competitors Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="competitors-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="27%"><strong>Competitor Info</strong></th>
                              <th width="15%"><strong>City</strong></th>
                              <th width="15%"><strong>School</strong></th>
                              <th width="15%"><strong>Birth Date</strong></th>
                              <th width="10%"><strong>Age</strong></th>
                              <th width="15%"><strong>Active</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    initDTable(
        "competitors-list-table",
            [{"name":"id"},
            {"name":"name"},
            {"name":"city"},
            {"name":"school"},
            {"name":"birth_date"},
            {"name":"age"},
            {"name":"disabled"},],
        "<?php echo base_url("admin/site_manager/competitors_ajax/") ?>"
    );
   
  </script>