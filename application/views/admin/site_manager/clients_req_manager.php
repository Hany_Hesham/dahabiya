<head>
  <script src="https://cdn.tiny.cloud/1/hx65hac5gjrbdc2fzw9lpmo6rg1z2592ymlpiswwq42og9nl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
</head>
<div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h5 class="page-title">Clients Requests Manager</h5>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item">Clients Requests Manager</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body"><br>  
                         <table id="client_emails-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th width="5%"><strong>#</strong></th>
                                  <th width="12%"><strong>Client Name</strong></th>
                                  <th width="12%"><strong>Email</strong></th>
                                  <th width="20%"><strong>Subject</strong></th>
                                  <th width="35%"><strong>Message</strong></th>
                                  <th width="12%"><strong>Status</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="mailermodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/client_mailer'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">New Mail</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <label class="text-right control-label col-form-label" style="font-size:12px;">*Email</label>
                  <input id="client_id" type="hidden" name="client_id" class="form-control" required>
                  <input id="email" type="text" name="email" class="form-control " placeholder="Email" required readonly>
                  <label class="text-right control-label col-form-label" style="font-size:12px;">*subject</label>
                  <input  id="subject" type="text" name="subject" class="form-control " placeholder="Subject" required>
                  <label class="text-right control-label col-form-label" style="font-size:12px;">*email Body</label>
                  <textarea  class="form-control message-content" name="message" id="modal-message" rows="4"></textarea>
                  <br>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button id="mailer-modalSubmit" type="submit" type="submit" name="submit" class="btn btn-primary">Send</button>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
  <div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document ">
            <div class="modal-content">
                <div class="modal-body">
                  <h5 class="text-center" id="loading-part">
                    <div class="" id="process-loader">
                       <img src="<?php echo base_url();?>assets/images/spinner.gif"
                           style = " width: 30%;height:30%;">
                    </div>
                    Please Wait
                  </h5>
                  <div id="success-part">
                    <h1 class="text-center">
                      <strong style="font-size:100px;color:#3EC1D5"><i class="fa fa-check-circle"></i></strong>
                    </h1>
                    <h6 class="text-center">Success</h6>
                  </div>
                </div>
            </div>
        </div>
    </div>

  <script type="text/javascript">
      // $(document).ready(function() {
      //  $('#summernote').summernote({
      //       tabsize:1,
      //       height: 50,
      //       width: 800
      //     });
      // });
      initDTable(
                 "client_emails-list-table",
                    [{"name":"id"},
                     {"name":"client_name"},
                     {"name":"email"},
                     {"name":"subject"},
                     {"name":"message"},
                     {"name":"mail_status"},],
                  "<?php echo base_url("admin/site_manager/clients_req_ajax/") ?>"
           );

      function get_mailerView(id,email) {
       $('#mailermodal').modal('show');
       $('#mailermodal .modal-body #modal-message,#mailermodal .modal-body #subject').val('');
       $('#mailermodal .modal-body #email').val(email);
       $('#mailermodal .modal-body #client_id').val(id);
     }

     $(function () {
      $('form').on('submit', function (e) { 
      $('#mailermodal').modal('hide');
      $('#success-modal').modal('show');
      $('#success-modal .modal-body #success-part').hide();
      $('#success-modal .modal-body #loading-part').show();
      let clientsEmailsTable = $('#client_emails-list-table').DataTable();
         e.preventDefault();      
         var formdata = $('form').serialize();
         $.ajax({
           type: 'post',
           url: '<?php echo base_url("admin/site_manager/client_mailer") ?>',
           data: {formdata:formdata},
           success: function(data) { 
             setTimeout(function(){
              $('#success-modal .modal-body #loading-part').hide();
              $('#success-modal .modal-body #success-part').show();
              }, 1000);
              clientsEmailsTable.ajax.reload();
               
               }
          });
      });
   });
      
    </script>
    <script>
      tinymce.init({
      selector: '.message-content',
      plugins: 'print preview paste importcss searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
      imagetools_cors_hosts: ['picsum.photos'],
      menubar: 'file edit view insert format tools table help',
      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
      toolbar_sticky: true,
      image_advtab: true,
      link_list: [
        { title: 'My page 1', value: 'https://www.tiny.cloud' },
        { title: 'My page 2', value: 'http://www.moxiecode.com' }
      ],
      image_list: [
        { title: 'My page 1', value: 'https://www.tiny.cloud' },
        { title: 'My page 2', value: 'http://www.moxiecode.com' }
      ],
      image_class_list: [
        { title: 'None', value: '' },
        { title: 'Some class', value: 'class-name' }
      ],
      importcss_append: true,
      file_picker_callback: function (callback, value, meta) {
        /* Provide file and text for the link dialog */
        if (meta.filetype === 'file') {
          callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
        }

        /* Provide image and alt text for the image dialog */
        if (meta.filetype === 'image') {
          callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
        }

        /* Provide alternative source and posted for the media dialog */
        if (meta.filetype === 'media') {
          callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
        }
      },
      templates: [
            { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
        { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
        { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
      ],
      template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
      template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
      height: 350,
      image_caption: true,
      quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
      noneditable_noneditable_class: 'mceNonEditable',
      toolbar_mode: 'sliding',
      contextmenu: 'link image imagetools table',
      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });

</script>