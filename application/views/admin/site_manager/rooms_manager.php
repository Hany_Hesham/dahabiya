<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Rooms Manager
                        <a href='<?php echo base_url('admin/site_manager/add_room')?>'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-book-plus'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Rooms Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br> 
                      <table id="rooms-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="30%"><strong>Room</strong></th>
                              <th width="15%"><strong>Category</strong></th>
                              <th width="15%"><strong>Inventory</strong></th>
                              <th width="15%"><strong>Rank</strong></th>
                              <th width="15%"><strong>Active</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    initDTable(
                "rooms-list-table",
                  [{"name":"id"},
                    {"name":"room"},
                    {"name":"room_category"},
                    {"name":"inventory"},
                    {"name":"rank"},
                    {"name":"disabled"},],
                "<?php echo base_url("admin/site_manager/rooms_ajax/") ?>"
          );
  </script>