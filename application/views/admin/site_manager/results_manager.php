<link href="<?php echo base_url('assets/css/summernote-bs4.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/summernote.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/summernote.js')?>"></script>
<script src="<?php echo base_url('assets/js/summernote-bs4.js')?>"></script>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Results Manager
                        <a href='<?php echo base_url('admin/site_manager/export_competition_projects/'.$competition_id)?>'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-file-excel'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Results Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="results-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                            <th width="3%"><strong>#</strong></th>
                              <th width="20%"><strong>Competitor</strong></th>
                              <th width="20%"><strong>Project</strong></th>
                              <th width="10%"><strong>Project Link</strong></th>
                              <th width="15%"><strong>Programming Score</strong></th>
                              <th width="15%"><strong>Logic Score</strong></th>
                              <th width="15%"><strong>Votes Score</strong></th>
                              <th width="15%"><strong>Total Score</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/edit_project_score'), 'class="form-horizontal" id="EditResultForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Edit Competitor Results</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="project_id" type="hidden" name="project_id" class="form-control">
                  <input id="competition_id" type="hidden" name="competition_id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Programming Score</label>
                      <input  id="design_score" type="number" step="any" name="design_score" class="form-control " placeholder="Design Score" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Logic Score</label>
                      <input  id="logic_score" type="number" step="any" name="logic_score" class="form-control " placeholder="Logic Score" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Votes Score</label>
                      <input  id="presentation_score" type="number" step="any" name="presentation_score" class="form-control " placeholder="Design Score" required>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
<script type="text/javascript">
    initDTable(
                "results-list-table",
                  [{"name":"id"},
                    {"name":"email"},
                    {"name":"code"},
                    {"name":"project_link"},
                    {"name":"design_score"},
                    {"name":"logic_score"},
                    {"name":"presentation_score"},
                    {"name":"total_score"},],
                "<?php echo base_url("admin/site_manager/results_ajax/".$competition_id) ?>", 7
          );
    function addEditResultModal(result) {
      $('#resultModal').modal('show');
      if(result != undefined){
        let {id, competition_id, design_score, logic_score, presentation_score} = result;
        $('#resultModal .modal-body #project_id').val(id);
        $('#resultModal .modal-body #competition_id').val(competition_id);
        $('#resultModal .modal-body #design_score').val(design_score);
        $('#resultModal .modal-body #logic_score').val(logic_score);
        $('#resultModal .modal-body #presentation_score').val(presentation_score);
      }else{
        $('#EditResultForm').trigger("reset");
      }
     }
  </script>