<!-- <head> -->
  <!-- <script src="https://cdn.tiny.cloud/1/hx65hac5gjrbdc2fzw9lpmo6rg1z2592ymlpiswwq42og9nl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
<!-- </head> -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                <div class="col-sm-4">
                    <div class="float-left">
                        <h5 class="page-title">Add Rule</h5>      
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="ml-auto float-right">
                        <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager/rules_manager');?>">Rules Manager</a></li>
                            <li class="breadcrumb-item">Add Rule</li>
                        </ol>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <?php if(!isset($rule)) {?>
                    <?php echo form_open(base_url('admin/site_manager/add_rule'), 'class="form-horizontal" id="addEditruleForm" enctype="multipart/form-data"');  ?> 
                <?php }else{?>    
                    <?php echo form_open(base_url('admin/site_manager/edit_rule/'.$rule['id']), 'class="form-horizontal" id="addEditruleForm" enctype="multipart/form-data"');  ?> 
                <?php }?>    
                  <div class="card-body">
                        <div class="form-group m-t-20">
                            <label>Rank</label>
                            <input type="number" name="rank" class="form-control" id="rank" value="<?php echo isset($rule) ? $rule['rank']: ''?>" placeholder="Enter Rank">
                        </div>
                        <div class="form-group m-t-20">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="<?php echo isset($rule) ? $rule['title']: ''?>" placeholder="Enter Title">
                        </div>
                        <div class="form-group m-t-20">
                            <label for="cono1">Content</label>
                            <textarea id="content" name="content" class="form-control" rows="4"><?php echo isset($rule['content']) ? $rule['content']: ''?>
                            </textarea>
                        </div>
                        <div class="form-group m-t-20">
                            <label>Rules File</label>
                            <input type="file" name="file_name" class="form-control" id="file_name" placeholder="Upload rules file">
                        </div>
                        <div class="form-group m-t-20">
                            <label>Video Link</label>
                            <input type="text" name="video_link" class="form-control" value="<?php echo isset($rule['video_link']) ? $rule['video_link']: ''?>"  id="video_link" placeholder="Enter Video Link">
                        </div>
                        <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                        <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                            
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
                        </div>
                    </div>
                <?php echo form_close( ); ?>
            </div>
        </div>
    </div>
</div>
<?php upfiles_js('admin/site_manager',$uploads,27,$gen_id,'rules','site_files');?>

<script>
//     tinymce.init({
//     selector: '#content',
//     plugins: 'print preview paste importcss searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
//   imagetools_cors_hosts: ['picsum.photos'],
//   menubar: 'file edit view insert format tools table help',
//   toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
//   toolbar_sticky: true,
//   image_advtab: true,
//   link_list: [
//     { title: 'My page 1', value: 'https://www.tiny.cloud' },
//     { title: 'My page 2', value: 'http://www.moxiecode.com' }
//   ],
//   image_list: [
//     { title: 'My page 1', value: 'https://www.tiny.cloud' },
//     { title: 'My page 2', value: 'http://www.moxiecode.com' }
//   ],
//   image_class_list: [
//     { title: 'None', value: '' },
//     { title: 'Some class', value: 'class-name' }
//   ],
//   importcss_append: true,
//   file_picker_callback: function (callback, value, meta) {
//     /* Provide file and text for the link dialog */
//     if (meta.filetype === 'file') {
//       callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
//     }

//     /* Provide image and alt text for the image dialog */
//     if (meta.filetype === 'image') {
//       callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
//     }

//     /* Provide alternative source and posted for the media dialog */
//     if (meta.filetype === 'media') {
//       callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
//     }
//   },
//   templates: [
//         { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
//     { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
//     { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
//   ],
//   template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
//   template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
//   height: 350,
//   image_caption: true,
//   quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
//   noneditable_noneditable_class: 'mceNonEditable',
//   toolbar_mode: 'sliding',
//   contextmenu: 'link image imagetools table',
//   content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
//  });

</script>