<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Booking Manager</h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Booking Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="booking-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="25%"><strong>Guest Info</strong></th>
                              <th width="30%"><strong>Booking</strong></th>
                              <th width="20%"><strong>Rates</strong></th>
                              <th width="10%"><strong>Payment Method</strong></th>
                              <th width="10%"><strong>Voucher Image</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    initDTable(
                "booking-list-table",
                  [{"name":"id"},
                    {"name":"first_name"},
                    {"name":"code"},
                    {"name":"total_rate"},
                    {"name":"payment_method"},
                    {"name":"payment_method"},],
                "<?php echo base_url("admin/site_manager/booking_ajax/") ?>"
          );
  </script>