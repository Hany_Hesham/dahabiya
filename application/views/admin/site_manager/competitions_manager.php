<link href="<?php echo base_url('assets/css/summernote-bs4.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/summernote.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/summernote.js')?>"></script>
<script src="<?php echo base_url('assets/js/summernote-bs4.js')?>"></script>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Competitions Manager
                        <a href='javascript: void(0);' onclick='addEditCompetitionModal()'>
                            <strong style="font-size: 25px;"> <i class='mdi mdi-book-plus'></i></strong>
                        </a> 
                      </h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Competitions Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="competitions-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="25%"><strong>Competition Info</strong></th>
                              <th width="10%"><strong>Active</strong></th>
                              <th width="10%"><strong>Type</strong></th>
                              <th width="20%"><strong>Competition Dates</strong></th>
                              <th width="8%"><strong>Results File</strong></th>
                              <th width="10%"><strong>Price</strong></th>
                              <th width="10%"><strong>Image</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="competitionModal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/add_competition'), 'class="form-horizontal" id="addEditCompetitionForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">New Competition</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="competition_id" type="hidden" name="competition_id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Type</label>
                      <select id="type" name="type" class="form-control"  required>
                        <option value="weekly">Weekly</option>
                        <option value="yearly">Yearly</option>
                      </select>                    
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input  id="title" type="text" name="title" class="form-control " placeholder="Title" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Description</label>
                      <textarea  class="form-control" name="description" id="description" rows="4"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Start Date</label>
                      <input  id="start_date" type="date" name="start_date" class="form-control " placeholder="Start Date" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*End Date</label>
                      <input  id="end_date" type="date" name="end_date" class="form-control " placeholder="End Date" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Vote Start Date</label>
                      <input  id="vote_start_date" type="date" name="vote_start_date" class="form-control " placeholder="Start Date" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Vote End Date</label>
                      <input  id="vote_end_date" type="date" name="vote_end_date" class="form-control " placeholder="End Date" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Price</label>
                      <input  id="price" step="any" type="number" name="price" class="form-control " placeholder="Price" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Currency</label>
                      <select  id="currency" name="currency" class="form-control"  required>
                        <option value="EGP">EGP</option>
                        <option value="USD">$</option>
                        <option value="EUR">Euro</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Image</label>
                      <input  id="image" type="file" name="image" class="form-control " placeholder="Image" required>
                    </div>
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Results File</label>
                      <input  id="results_file" type="file" name="results_file" class="form-control " placeholder="results file">
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
<script type="text/javascript">
    initDTable(
                "competitions-list-table",
                  [{"name":"id"},
                    {"name":"title"},
                    {"name":"active"},
                    {"name":"type"},
                    {"name":"start_date"},
                    {"name":"results_file"},
                    {"name":"price"},
                    {"name":"id"},],
                "<?php echo base_url("admin/site_manager/competitions_ajax/") ?>"
          );
    function addEditCompetitionModal(competition) {
      $('#competitionModal').modal('show');
      if(competition != undefined){
        let {id, type, title, description, start_date, end_date, vote_start_date,
            vote_end_date, price, currency} = competition;
        $('#competitionModal .modal-body #competition_id').val(id);
        $('#competitionModal .modal-body #type').val(type);
        $('#competitionModal .modal-body #title').val(title);
        $('#competitionModal .modal-body #description').val(description);
        $('#competitionModal .modal-body #start_date').val(start_date);
        $('#competitionModal .modal-body #end_date').val(end_date);
        $('#competitionModal .modal-body #vote_start_date').val(vote_start_date);
        $('#competitionModal .modal-body #vote_end_date').val(vote_end_date);
        $('#competitionModal .modal-body #price').val(price);
        $('#competitionModal .modal-body #currency').val(currency);
        $('#competitionModal .modal-body #image').attr('required', false);
      }else{
        $('#addEditCompetitionForm').trigger("reset");
      }
     }

  //    $(function () {
  //     $('form').on('submit', function (e) { 
  //     $('#mailermodal').modal('hide');
  //     let clientsEmailsTable = $('#competitions-list-table').DataTable();
  //        e.preventDefault();      
  //        var formdata = $('form').serialize();
  //        $.ajax({
  //          type: 'post',
  //          url: '<?//php echo base_url("admin/site_manager/client_mailer") ?>',
  //          data: {formdata:formdata},
  //          success: function(data) { 
  //             clientsEmailsTable.ajax.reload();
               
  //              }
  //         });
  //     });
  //  });
  </script>