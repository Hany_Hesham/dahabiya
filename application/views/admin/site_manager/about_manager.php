
<div class="page-breadcrumb">
  <div class="row">
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
              <div class="col-sm-4">
                <div class="float-left">
                    <h4 class="page-title">About Us Manager</h4>      
                </div>
              </div>
              <div class="col-sm-12">
                <div class="ml-auto float-right">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                      </ol>
                    </nav>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
</div>

<div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="about_us-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="20%"><strong>Header</strong></th>
                              <th width="60%"><strong>Content</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td> 
                                <?php echo $about_us['header']?>
                                <div class='info'>
                                  <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editabout_us_Modal(`<?php echo $about_us["id"] ?>`)'>
                                  <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                  </a> 
                                </div>
                              </td>
                              <td> <?php echo $about_us['paragraph']?></td>
                            </tr>
                            <?php $i=1; foreach($about_us_points as $about_us_point){ $i++?>
                              <tr>
                                <td><?php echo $i?></td>
                                <td> 
                                  <?php echo $about_us_point['header']?>
                                  <div class='info'>
                                    <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editabout_us_Modal(`<?php echo $about_us_point["id"] ?>`)'>
                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                    </a> 
                                  </div>
                                </td>
                                <td> <?php echo $about_us_point['paragraph']?></td>
                              </tr>
                              <?php }?>
                              <tr>
                                <td><?php echo $i+1?></td>
                                <td> 
                                  <?php echo $social_data_facebook['title']?>
                                  <div class='info'>
                                    <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editabout_us_Modal(`<?php echo $social_data_facebook["id"] ?>`)'>
                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                    </a> 
                                  </div>
                                </td>
                                <td> <?php echo $social_data_facebook['paragraph']?></td>
                              </tr>
                            <tr>
                              <td><?php echo $i+2?></td>
                              <td>
                                 <?php echo $contact['title']?>
                                  <div class='info'>
                                    <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editabout_us_Modal(`<?php echo $contact["id"] ?>`)'>
                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                    </a> 
                                  </div>
                              </td>
                              <td> <?php echo $contact['paragraph']?></td>
                            </tr>
                            <tr>
                              <td><?php echo $i+3?></td>
                              <td>
                                 <?php echo $location['title']?>
                                  <div class='info'>
                                    <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editabout_us_Modal(`<?php echo $location["id"] ?>`)'>
                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                    </a> 
                                  </div>
                              </td>
                              <td> <?php echo $location['paragraph']?></td>
                            </tr>
                            <tr>
                              <td><?php echo $i+4?></td>
                              <td>
                                 <?php echo $email['title']?>
                                  <div class='info'>
                                    <a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='Editabout_us_Modal(`<?php echo $email["id"] ?>`)'>
                                    <strong>Edit <i class='mdi mdi-pencil'></i></strong>
                                    </a> 
                                  </div>
                              </td>
                              <td> <?php echo $email['paragraph']?></td>
                            </tr>
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="about_us_Modal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/edit_about_us'), 'class="form-horizontal" id="EditAboutForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Edit</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="about_id" type="hidden" name="id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Header</label>
                      <input id="header" type="text" name="header" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Paragraph</label>
                      <textarea  class="form-control" name="paragraph" id="paragraph" rows="4"></textarea>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
  <script type="text/javascript">
    initDTable_normal(
          'about_us-list-table',"", 
          [{"name":"id"},
          {"name":"header"},
          {"name":"paragraph"},]);
    
    function Editabout_us_Modal(id) {
      var url="<?php echo base_url('admin/site_manager/get_rowDataAjax/Site_manager_model/get_site_metaData_byId/');?>";      
      $.ajax({
          url: url,
          dataType: "json",
          type : 'POST',
          data:{search:id},
            success: function(data){
              $('#about_us_Modal').modal('show');
                let {id, header, paragraph} = data;
                $('#about_us_Modal .modal-body #about_id').val(id);
                $('#about_us_Modal .modal-body #header').val(header);
                $('#about_us_Modal .modal-body #paragraph').val(paragraph);
              }
          });
     }
 
  </script>
          

  