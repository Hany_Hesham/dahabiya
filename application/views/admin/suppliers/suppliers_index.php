
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">Suppliers</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Suppliers</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->data['permission']['creat'] == 1){?>
                         <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-lg">
                                 <i class="fa fa-cloud"></i>&nbsp; <strong>Add Supplier</strong></button> <br> 
                         <div class="dropdown-divider"></div><br><br>
                       <?php }?>
                         <table id="suppliers-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="8%"><strong>#</strong></th>
                                  <th scope="col" width="25%"><strong>Supplier Name</strong></th>
                                  <th scope="col" width="15%"><strong>Supplier Interface</strong></th>
                                  <th scope="col" width="10%"><strong>Group</strong></th>
                                  <th scope="col" width="10%"><strong>Phone</strong></th>
                                  <th scope="col" width="10%"><strong>Address</strong></th>
                                  <th scope="col" width="10%"><strong>Contact Person</strong></th>
                                  <th scope="col" width="15%"><strong>Contact Email</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php 
         $select = '<select id="group_id" name="group_id" class="selectpicker form-control custom-select"  data-container="body" data-live-search="true" title="Select Hotel Group" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="width: 100%; height:36px;" required>';
         $select .= '<option value="">Select Item Unit</option>';    
              foreach ($groups as $group) {
                  $select .= '<option value="'.$group['id'].'">'.$group['hotel_group'].'</option>';
                }
         $select .= '</select><br>';
         initiate_modal(
                         'lg','Add New Supplier','admin/suppliers/supplier_process',
                          '<input id="supp_id" type="hidden" name="supp_id">
                           <input  id="suppName" onkeyup="fieldChecker(\'suppName\',\'admin/suppliers/check_field/suppliers/supp_name\')" type="text" name="supp_name" class="form-control " placeholder="Item Supplier" required><br>
                           <input  id="interface" onkeyup="fieldChecker(\'interface\',\'admin/suppliers/check_field/suppliers/interface\')" type="text" name="interface" class="form-control " placeholder="Supplier interface" required><br>
                            '. $select.'<br>
                           <input id="phone" type="number" name="phone" class="form-control" placeholder="Phone" ><br>
                           <input id="address" type="text" name="address" class="form-control" placeholder="Address" ><br>
                           <input id="contact" type="text" name="contact" class="form-control" placeholder="Contacted Person" ><br>
                           <input id="cont_email" type="email" name="cont_email" class="form-control" placeholder="Contact Email" ><br>
                            <input type="hidden" name="assumed_id" value="'.$assumed_id.'" />
                                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />         
                            <br>'
                         )
  ?>

<?php upfiles_js('admin/suppliers',$uploads,4,$assumed_id,'suppliers');?>

<script type="text/javascript">
      initDTable(
                 "suppliers-list-table",
                    [{"name":"id"},
                     {"name":"supp_name"},
                     {"name":"interface"},
                     {"name":"hotel_group"},
                     {"name":"phone"},
                     {"name":"address"},
                     {"name":"contact"},
                     {"name":"cont_email"},],
                  "<?php echo base_url("admin/suppliers/suppliers_ajax/") ?>"
           );
      
    </script>