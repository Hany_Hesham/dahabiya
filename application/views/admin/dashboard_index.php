<?php
    $slider_permission = user_access(1);
    $booking_permission = user_access(5);
    $rooms_permission = user_access(15);
    $offers_permission = user_access(2);
    $about_permission = user_access(14);
    $language_permission = user_access(25);
    $gallary_permission  = user_access(4);
    $clients_req_permission      = user_access(21);
    
    // $competitors_permission = user_access(18);
    // $voters_permission = user_access(26);
    // $winners_permission = user_access(24);
    // $partners_permission = user_access(3);
    // $gallary_permission          = user_access(4);
    // $event_permission            = user_access(6);
    // $products_permission         = user_access(16);
    // $portfolio_permission        = user_access(17);
    // $contact_permission          = user_access(19);
    // $footer_permission           = user_access(20);
    // $team_permission             = user_access(23);
?>

<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
     <?php if($booking_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/booking_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-tasks" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Booking Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($clients_req_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/clients_req_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-users" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Clients Requests Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($rooms_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/rooms_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-book" style="font-size:130%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Rooms Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
       <?php if($offers_permission['view'] == 1){?> 
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/offers_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-star-outline" style="font-size:130%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Offers Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
        <?php }?> 
      <!-- <?php if($competitors_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/competitors_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-account-card-details" style="font-size:130%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Competitors Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
      <?php if($voters_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/voters_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="far fa-thumbs-up" style="font-size:155%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Voters Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
      <?php if($winners_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/winners_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-trophy" style="font-size:155%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Winners Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  -->
      <?php if($about_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/about_us_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-angularjs" style="font-size:123%;"></i></h1>
                    <h6 class="text-white"><?php echo 'About US & Info Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
      <?php if($slider_permission['view'] == 1){?> 
         <div class="col-md-6 col-lg-2 col-xlg-3">
           <a href="<?php echo base_url('admin/site_manager/slider_manager');?>">
             <div class="card card-hover">
                 <div class="box bg-dark text-center">
                     <h1 class="font-light text-white"><i class="fas fa-sliders-h" style="font-size:150%;"></i></h1>
                     <h6 class="text-white"><?php echo 'Sliders'?></h6>
                 </div>
             </div>
           </a>
         </div>   
       <?php }?> 
       <?php if($gallary_permission['view'] == 1){?> 
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/gallary_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-laptop" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Gallery'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
       <!-- <?php if($partners_permission['view'] == 1){?> 
         <div class="col-md-6 col-lg-2 col-xlg-3">
           <a href="<?php echo base_url('admin/site_manager/partners_manager');?>">
             <div class="card card-hover">
                 <div class="box bg-dark text-center">
                     <h1 class="font-light text-white"><i class="mdi mdi-animation" style="font-size:130%;"></i></h1>
                     <h6 class="text-white"><?php echo 'Partners Manager'?></h6>
                 </div>
             </div>
           </a>
         </div>   
       <?php }?>  -->
      <?php if($language_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/lang_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-globe" style="font-size:160%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Languages Translator'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <!--  <?php if($package_permission['view'] == 1){?> 
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/package_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-address-book" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Packages'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>   
      <?php if($event_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/event_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-circle" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Events'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>   -->
      <!-- <?php if($products_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/category_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-list-ol" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Products'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
       <?php if($portfolio_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/portfolio_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-laptop" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Portfolio'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($team_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/team_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-address-book" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Team'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($blog_permission['view'] == 15){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/blog_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-warehouse" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Blog'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
      <?php if($contact_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/contact_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-user-circle" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Contact'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($footer_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/footer_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-copyright" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Footer'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($bookings_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/bookings_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-users" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Bookings Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($offer_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/offer_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-globe" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Offers Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> -->
    </div> 
  </div>