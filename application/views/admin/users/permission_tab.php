   <?php if(isset($user['id'])){?>
     <div class="card-body card-block">
       <?php echo form_open(base_url('admin/users/add_permission/'.$user['id']), 'class="form-horizontal"');  ?> 
       <table id="permission-list-table" class="table" style="margin-top:2%;width:100%;">
         <thead>
             <tr>
                <th width="5%" class="text-right"><strong style="font-size:16px;">الحذف   </strong></th>
                <th width="5%" class="text-right"><strong style="font-size:16px;"> التعديل  </strong></th>
                <th width="5%" class="text-right"><strong style="font-size:16px;"> الإضافة  </strong></th>
                <th width="5%" class="text-right"><strong style="font-size:16px;">الرؤية  </strong></th>
                <th class="text-right"><strong style="font-size:16px;"> القسم  </strong></th>
             </tr>
          </thead>
          <tbody>
             <?php foreach ($modules as $key => $module) {?> 
              <tr>
                  <td width="5%">
                    <?php if($modules[$key]['remove'] == 1) :?>
                        <label class="customcheckbox switch-info float-right">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][remove]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][remove]"
                                value="1" class="listCheckbox" 
                               <?php echo ( isset($modules[$key]['permission']['remove']) 
                                    && $modules[$key]['permission']['remove'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['edit'] == 1) :?>
                        <label class="customcheckbox float-right">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][edit]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][edit]"
                                value="1" class="listCheckbox" 
                               <?php echo ( isset($modules[$key]['permission']['edit']) && $modules[$key]['permission']['edit'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                   <td width="5%">
                    <?php if($modules[$key]['creat'] == 1) :?>
                        <label class="customcheckbox float-right">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][creat]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][creat]"
                                value="1" class="listCheckbox" 
                               <?php echo ( isset($modules[$key]['permission']['creat']) && $modules[$key]['permission']['creat'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['view'] == 1) :?>
                        <label class="customcheckbox float-right">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][view]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][view]"
                                value="1" class="listCheckbox" 
                               <?php echo ( isset($modules[$key]['permission']['view']) && $modules[$key]['permission']['view'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="10%" class="text-right">
                     <?php if($user['permission'] == 1) :?>
                        <input  type="hidden" name="accs[<?php echo $module['id']?>][id]" value="<?php echo $modules[$key]['permission']['id']?>">
                      <?php endif;?>
                       <input  type="hidden" name="accs[<?php echo $module['id']?>][module_id]" value="<?php echo $modules[$key]['id']?>" style="width:25px;height: 25px;">
                         <strong><?php echo $module['name']?></strong>  
                  </td>
              </tr>
              <?php }?>
          </tbody>
         </table>
        <button type="submit" name="submit" class="btn btn-primary btn-lg" style="float:right;margin-top:5%;">
                <i class="fa fa-dot-circle-o"></i> Save
       </button>
       <?php echo form_close( ); ?>
    </div>
   <?php }?>