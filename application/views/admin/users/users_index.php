    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title"> قائمة الأعضاء  </h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">الرئيسية  </a></li>
                                <li class="breadcrumb-item active" aria-current="page">أعضاء</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                      <a class="nav-link dropdown-toggle btn btn-outline-dark col-md-3 float-right" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><strong style="font-size:14px;">قائمة الإضافة  </strong></a>
                       <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item text-right" href="<?php echo base_url('admin/users/user');?>"> إضافة عضو جديد  </a>
                            <?php if($sessioned_user['is_admin']==1){?>
                              <!-- <div class="dropdown-divider"></div> -->
                                <!-- <a class="dropdown-item" href="<?php echo base_url('admin/users/bulk');?>">Bulk Action</a> -->
                                <!-- <div class="dropdown-divider"></div> -->
                                <a class="dropdown-item text-right" href="<?php echo base_url('admin/users/group');?>"> الجروبات   </a>
                              <?php }?>
                         </div>
                       <div class="dropdown-divider"></div><br><br><br>
                         <table id="users-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th width="5%" class="text-right"><strong>ممنوع  </strong></th>
                                  <th width="10%" class="text-right"><strong>نوع  </strong></th>
                                  <th width="20%"class="text-right"><strong>البريد الإلكتروني  </strong></th>
                                  <th width="15%"class="text-right"><strong>الأسم  بالكامل  </strong></th>
                                  <th width="15%" class="text-right"><strong>أسم   الحساب   </strong></th>
                                  <th width="10%" class="text-right"><strong>#</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
    
    <script type="text/javascript">
      initDTable(
                 "users-list-table",
                    [{"name":"disabled"},
                     {"name":"is_admin"},
                     {"name":"email"},
                     {"name":"fullname"},
                     {"name":"username"},
                     {"name":"id"},],
                  "<?php echo base_url("admin/users/users_ajax") ?>"
           );
    </script>