
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">العملاء </h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">الرئيسية</a></li>
                              <li class="breadcrumb-item active" aria-current="page">العملاء  </li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->data['permission']['creat'] == 1){?>
                         <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-lg float-right">
                                 <i class="fa fa-cloud"></i>&nbsp; <strong>إضافة  عميل </strong></button> <br> 
                         <div class="dropdown-divider"></div><br><br>
                       <?php }?>
                         <table id="clients-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="20%" class="text-right"><strong>العنوان اللإلكتروني </strong></th>
                                  <th scope="col" width="15%" class="text-right"><strong>العنوان </strong></th>
                                  <th scope="col" width="15%" class="text-right"><strong>إسم المتواصل معه</strong></th>
                                  <th scope="col" width="10%" class="text-right"><strong>التليفون </strong></th>
                                  <th scope="col" width="15%" class="text-right"><strong>إسم العميل </strong></th>
                                  <th scope="col" width="5%"  class="text-right"><strong>#</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php 
         initiate_modal(
                         'lg','إضافة  عميل جديد ','admin/clients/client_process',
                          '<input id="supp_id" type="hidden" name="supp_id">
                           <input  id="client_name" onkeyup="fieldChecker(\'client_name\',\'admin/clients/check_field/clients/client_name\')" type="text" name="client_name" class="form-control " placeholder="إسم العميل " required dir="rtl"><div class="invalid-feedback">Please change the name it is already exist</div>
                           <input id="phone" type="text" name="phone" class="form-control" placeholder="التليفون " dir="rtl" required><br>
                           <input id="address" type="text" name="address" class="form-control" placeholder="العنوان " dir="rtl"><br>
                           <input id="contact" type="text" name="contact" class="form-control" placeholder="المتواصل معه " dir="rtl"><br>
                           <input id="cont_email" type="email" name="cont_email" class="form-control" placeholder="العنوان الإلكتروني " dir="rtl"><br>
                            <input type="hidden" name="assumed_id" value="'.$assumed_id.'" />
                                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />         
                            <br>'
                         )
  ?>

<?php upfiles_js('admin/clients',$uploads,$this->data['module']['id'],$assumed_id,'clients');?>

<script type="text/javascript">
      initDTable(
                 "clients-list-table",
                    [
                     {"name":"cont_email"},
                     {"name":"address"},
                     {"name":"contact"},
                     {"name":"phone"},
                     {"name":"client_name"},
                     {"name":"id"},],
                  "<?php echo base_url("admin/clients/clients_ajax/") ?>"
                  ,'','5'
           );
      
    </script>