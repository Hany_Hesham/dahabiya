<?php 
  $select = '<option value=""></option>';
  foreach ($roles as $role) {
    $select .= '<option value="'.$role['id'].'">'.$role['name'].'</option>';
  }
?>
<script type="text/javascript">
  let select    = '<?php echo $select?>';

  function storeSelected(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#head').append('<th><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)" class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></th>');
    $('#row').append('<td class="role'+rowId+'"><select type="text" class="form-control" name="items['+rowId+'][role]" title="Select Role" required>'+select+'</select></td>');
  }

  function removeRow(typeID, rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#Items-list-table'+typeID+' .rowd'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
        $('#Items-list-table'+typeID+' .rowd'+rowId+'').remove(); 
      }
      var ids = $('#allstoredIds'+typeID).val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds'+typeID).val(nids);
    }else{
      return false;
    }
  }

  function editRow(typeID){ 
    $('#typeNameEdit'+typeID).show();
    $('#typeNameView'+typeID).hide();
  }

  function cancelRow(typeID){ 
    $('#typeNameEdit'+typeID).hide();
    $('#typeNameView'+typeID).show();
  }

  function editSubmitRow(id, typeID){
    var url =getUrl()+'admin/signature/edit/'+id;
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data: {
        id         :   id,
        type_name  :   $('#items-'+typeID+'-type_name').val(),
        first      :   $('#items-'+typeID+'-first').val(),
        second     :   $('#items-'+typeID+'-second').val(),
        third      :   $('#items-'+typeID+'-third').val(),
        fourth     :   $('#items-'+typeID+'-fourth').val(),
        fifth      :   $('#items-'+typeID+'-fifth').val(),
        sixth      :   $('#items-'+typeID+'-sixth').val(),
        seventh    :   $('#items-'+typeID+'-seventh').val(),
        eighth     :   $('#items-'+typeID+'-eighth').val(),
        ninth      :   $('#items-'+typeID+'-ninth').val(),
      },
      success: function(data){
        first   = $('#items-'+typeID+'-first option:selected').text();
        second  = $('#items-'+typeID+'-second option:selected').text();
        third   = $('#items-'+typeID+'-third option:selected').text();
        fourth  = $('#items-'+typeID+'-fourth option:selected').text();
        fifth   = $('#items-'+typeID+'-fifth option:selected').text();
        sixth   = $('#items-'+typeID+'-sixth option:selected').text();
        seventh = $('#items-'+typeID+'-seventh option:selected').text();
        eighth  = $('#items-'+typeID+'-eighth option:selected').text();
        ninth   = $('#items-'+typeID+'-ninth option:selected').text();
        $('#first'+id).empty();
        $('#second'+id).empty();
        $('#third'+id).empty();
        $('#fourth'+id).empty();
        $('#fifth'+id).empty();
        $('#sixth'+id).empty();
        $('#seventh'+id).empty();
        $('#eighth'+id).empty();
        $('#ninth'+id).empty();
        $('#first'+id).text(first);
        $('#second'+id).text(second);
        $('#third'+id).text(third);
        $('#fourth'+id).text(fourth);
        $('#fifth'+id).text(fifth);
        $('#sixth'+id).text(sixth);
        $('#seventh'+id).text(seventh);
        $('#eighth'+id).text(eighth);
        $('#ninth'+id).text(ninth);
        $('#typeNameEdit'+typeID).hide();
        $('#typeNameView'+typeID).show();
      }
    });
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'admin/signature/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#admin-list-table #rows'+rowId+'').remove(); 
      }
    });
  }
</script>