<?php $this->load->view('clients/includes/inner_slider') ?>

  <!-- Content -->
  <section>
      <div class="container">
          <div class="row">
              <div class="col-lg-6">
                  <h3 class="text-uppercase"><?php echo translate('Get In Touch', $this->data['language']) ?></h3>
                  <div class="row m-t-40">
                      <div class="col-lg-6">
                          <address>
                              <strong><?php echo translate('ZAHABYA HOTEL Inc', $this->data['language']) ?>.</strong><br>
                              <?php echo translate($this->data['location']['paragraph'], $this->data['language']) ?><br>
                              <abbr title="Phone">P:</h4> <?php echo $this->data['contact']['paragraph'] ?>
                          </address>
                      </div>
                  </div>
                  <div class="social-icons m-t-30 social-icons-colored">
                      <ul>
                          <li class="social-facebook"><a target="_blanck" href="<?php echo $this->data['facebook']['link']?>"><i class="fab fa-facebook-f"></i></a></li>
                      </ul>
                  </div>
              </div>
              <div class="col-lg-6">
                  <form  action="<?php echo base_url('clients/home/add_contact_us')?>" role="form" method="post">
                      <div class="row">
                          <div class="form-group col-md-6">
                              <label for="name"><?php echo translate('Name', $this->data['language']) ?></label>
                              <input type="text" aria-required="true" name="client_name" required class="form-control required name" placeholder="<?php echo translate('Enter your Name', $this->data['language']) ?>">
                          </div>
                          <div class="form-group col-md-6">
                              <label for="email"><?php echo translate('Email', $this->data['language']) ?></label>
                              <input type="email" aria-required="true" name="email" required class="form-control required email" placeholder="<?php echo translate('Enter your Email', $this->data['language']) ?>">
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label for="subject"><?php echo translate('Your Subject', $this->data['language']) ?></label>
                              <input type="text" name="subject" required class="form-control required" placeholder="<?php echo translate('Subject...', $this->data['language']) ?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="message"><?php echo translate('Message', $this->data['language']) ?></label>
                          <textarea type="text" name="message" required rows="5" class="form-control required" placeholder="<?php echo translate('Enter your Message', $this->data['language']) ?>"></textarea>
                      </div>
                      <button class="btn" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Send message</button>
                  </form>
              </div>
          </div>
      </div>
  </section>
  <!-- end: CONTENT -->
  <!-- MAP -->
  <section class="no-padding">
  <iframe  src="https://www.google.com/maps/embed/v1/place?q=Zahabia+Hotel+%26+Beach+Resort&key=AIzaSyCa4so4cfKS3JUkSnqJWI0j-aqInzf4wC0" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
      <!-- Google Map -->
      <!-- end: Google Map -->
  </section>
  <!-- end: MAP -->
    