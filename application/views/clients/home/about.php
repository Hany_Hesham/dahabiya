<?php $this->load->view('clients/includes/inner_slider') ?>
<section>
  <div class="container">
    <div class="row">
      <div class="content col-lg-5">
      <div class="post-item-description">
        <h2><?php echo translate('About Us', $this->data['language']) ?>
         <br><?php echo translate('Our History', $this->data['language']) ?><br> 
         <?php echo translate('Mission & Vision', $this->data['language']) ?></h2>
          <p><?php echo translate($about_paragraph['paragraph'], $this->data['language']) ?>
          </p>
          <div class="blockquote">
            <?php foreach($points as $point){?>
              <p>
                <i class="icon-check-square"></i> 
                <cite><?php echo translate($point['paragraph'], $this->data['language']) ?></cite>
              </p>
            <?php }?>
          </div>
        </div>
      </div>
      <div class="content col-lg-7">
        <!-- Blog -->
        <div class="carousel dots-inside arrows-visible" data-items="1" data-lightbox="gallery">
            <?php foreach($about_images as $row){?>
              <a href="<?php echo base_url('site_assets/images/about/'.$row['img'])?>" data-lightbox="gallery-image">
                  <img alt="image" src="<?php echo base_url('site_assets/images/about/'.$row['img'])?>">
              </a>
            <?php }?>
        </div>
      </div>
    </div>
  </div>
</section>