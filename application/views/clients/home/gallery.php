
<?php $this->load->view('clients/includes/inner_slider') ?>

<section id="page-content">
    <div class="container">
      <!-- Portfolio Filter -->
        <nav class="grid-filter gf-outline" data-layout="#portfolio">
            <ul>
                <li class="active"><a href="#" data-category="*">Show All</a></li>
                <?php foreach($categories as $category){?>
                    <li>
                        <a href="#" data-category=".<?php echo $category['id']?>">
                            <?php echo translate($category['header'], $this->data['language']) ?>
                        </a>
                    </li>
                <?php }?> 
            </ul>
            <div class="grid-active-title">Show All</div>
        </nav>
      <!-- end: Portfolio Filter -->
      <!-- Portfolio -->
        <div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="0">
          <!-- portfolio item -->
          <?php foreach($gallerys as $gallery){?>
            <div class="portfolio-item <?php echo $gallery['categories']?>">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-slider">
                        <div class="carousel" data-items="1" data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-autoplay="1500">
                            <a href="#"><img src="<?php echo base_url('site_assets/images/gallery/'.$gallery['img'])?>" alt="<?php echo $gallery['title']?>"></a>
                        </div>
                    </div>
                    <div class="portfolio-description" data-lightbox="gallery">
                        <a title="<?php echo $gallery['title']?>" data-lightbox="gallery-image" href="<?php echo base_url('site_assets/images/gallery/'.$gallery['img'])?>"><i class="icon-copy"></i></a>
                    </div>
                </div>
            </div>
          <?php }?>
        </div>
      <!-- end: Portfolio -->
    </div>
</section>