<?php $this->load->view('clients/includes/inner_slider'); ?>
<style>
    .list-group input[type="radio"]:checked + .list-group-item {
        background-color: #cdcdcd;
    }
</style>
<section id="shop-checkout">
    <div class="container">
        <div class="shop-cart">
           <div class="seperator" style="margin-top: 5px;"><div class="icon-holder"><i class="icon-book-open"> </i> </div>
            </div>
                <div class="row">             
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4><?php echo translate($room['room'], $this->data['language'])?></h4>
                                <span style="font-size: 13px;">
                                    <?php echo translate('Check-in', $this->data['language']).':
                                    &nbsp;<strong>'.$search->check_in.'</strong> '.translate('from', $this->data['language']).' 14.00 pm'?> 
                                </span><br>
                                <span style="font-size: 13px;">
                                    <?php echo translate('Check-out', $this->data['language']).': 
                                    &nbsp;<strong>'.$search->check_out.'</strong> '.translate('until', $this->data['language']).' 12.00 pm'?>
                                </span>
                                <br>
                                <span>
                                    <?php echo translate('Rooms', $this->data['language']).':&nbsp;'?>
                                    <strong><?php echo $reserved_rooms?></strong>
                                </span>
                                <br>
                                <span>
                                    <?php echo translate('Adults', $this->data['language']).':&nbsp;'?> 
                                    <strong><?php echo $search->adults?></strong>
                                </span>
                                <br>
                                <span>
                                    <?php echo translate('Childerns', $this->data['language']).':&nbsp;'?> 
                                    <strong><?php echo $search->childs?></strong>
                                </span>
                                <h5>
                                    <a style="color: #bb0f0f;" href="#collapseFour" data-toggle="collapse" class="collapsed" aria-expanded="false"> 
                                    <?php echo translate('Room Details', $this->data['language'])?> <i class="icon-arrow-down-circle"></i>
                                    </a>
                                </h5>
                            </div>    
                            <div class="content col-lg-6">
                                <div data-lightbox="gallery" class="row" style="margin-top: 10px;">
                                    <div class="col-lg-12">
                                        <div class="grid-item">
                                            <div class="grid-item-wrap">
                                                <div class="grid-image"> 
                                                    <img src="<?php echo base_url('site_assets/images/rooms/'.$room['files'][0]['file_name'])?>" alt="">
                                                </div>
                                                <?php foreach($room['files'] as $file) {?>
                                                <div class="grid-description">
                                                        <a title="<?php echo $room['room']?>" data-lightbox="gallery-image" href="<?php echo base_url('site_assets/images/rooms/'.$file['file_name'])?>" class="btn btn-light btn-rounded">Zoom</a>
                                                    </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>       
                        <div class="row" style="margin-top: 0px;">
                            <div class="col-lg-12">
                                <div style="height: 0px;" aria-expanded="false" id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="post-item-description">
                                            <div class="row">
                                                <div class="col-xl-6 col-lg-6 col-md-4">
                                                    <div class="widget">
                                                        <h4><?php echo translate('Room', $this->data['language'])?></h4>
                                                        <ul class="list">
                                                            <?php foreach($room['features'] as $feature){
                                                                if($feature['category'] == 'room features'){?>
                                                                    <li><span class="room-description"><i class="fa fa-check"></i> <?php echo translate($feature['feature'], $this->data['language'])?></span><br></li>
                                                            <?php } }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-4">
                                                    <div class="widget">
                                                        <h4> <?php echo translate('Room amenities', $this->data['language'])?></h4>
                                                        <ul class="list">
                                                            <?php foreach($room['features'] as $feature){
                                                                if($feature['category'] == 'room amenities'){?>
                                                                    <li><span class="room-description"><i class="fa fa-check"></i> <?php echo translate($feature['feature'], $this->data['language'])?></span><br></li>
                                                            <?php } }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <h4><?php echo translate('Booking Total', $this->data['language'])?></h4>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="cart-product-name">
                                                    <strong><?php echo $room['room']?></strong>
                                                </td>
                                                <td class="cart-product-name text-right">
                                                    <span class="amount">
                                                        <?php echo money_formater($room['price'],$room['currency']) ?>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cart-product-name">
                                                    <strong><?php echo translate('Rooms', $this->data['language'])?></strong>
                                                </td>
                                                <td class="cart-product-name  text-right">
                                                    <span class="amount"><?php echo $reserved_rooms?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cart-product-name">
                                                    <strong><?php echo translate('nights', $this->data['language'])?></strong>
                                                </td>
                                                <td class="cart-product-name  text-right">
                                                    <span class="amount"><?php echo $search->nights?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cart-product-name">
                                                    <strong><?php echo translate('Sub Total', $this->data['language'])?></strong>
                                                </td>
                                                <td class="cart-product-name  text-right">
                                                    <span class="amount">
                                                        <?php echo money_formater(($search->nights*$room['price']*$reserved_rooms),$room['currency']) ?>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cart-product-name">
                                                    <strong><?php echo translate('Taxes', $this->data['language'])?></strong>
                                                </td>
                                                <td class="cart-product-name  text-right">
                                                    <span class="amount"> <?php echo money_formater(($search->nights*$room['tax']*$reserved_rooms),$room['currency']) ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="cart-product-name">
                                                    <strong>Total</strong>
                                                </td>
                                                <td class="cart-product-name text-right">
                                                    <span class="amount color lead"><strong>
                                                    <?php echo money_formater(
                                                        (($search->nights*$room['price']*$reserved_rooms)+($search->nights*$room['tax']*$reserved_rooms)),
                                                        $room['currency']) ?>
                                                    </strong></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 no-padding">
                        <form action="<?php echo base_url('clients/home/postBooking/'.$room['id'])?>" method="post">
                           <input type="hidden" name="booking_search" value='<?php echo json_encode($search)?>' />
                           <input type="hidden" name="reserved_rooms" value='<?php echo $reserved_rooms?>' />
                           <input type="hidden" name="rate" value='<?php echo $room['price']?>' />
                           <input type="hidden" name="tax" value='<?php echo $room['tax']?>' />
                           <input type="hidden" name="currency" value='<?php echo $room['currency']?>' />
                           <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="upper"><?php echo translate('Your Information', $this->data['language'])?></h4>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label><?php echo translate('First Name', $this->data['language'])?> *</label>
                                    <input type="text" class="form-control" name="first_name" value="<?php echo isset($booking) ? $booking['first_name']: ''?>"
                                    placeholder="<?php echo translate('First Name', $this->data['language'])?>" required
                                    <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label><?php echo translate('Last Name', $this->data['language'])?> *</label>
                                    <input type="text" class="form-control" name="last_name" value="<?php echo isset($booking) ? $booking['last_name']: ''?>"
                                     placeholder="<?php echo translate('Last Name', $this->data['language'])?>" required
                                     <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label><?php echo translate('Email', $this->data['language'])?> *</label>
                                    <input type="email" class="form-control" name="email"  value="<?php echo isset($booking) ? $booking['email']: ''?>"
                                    placeholder="<?php echo translate('Email', $this->data['language'])?>" required
                                    <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label><?php echo translate('Country of residence', $this->data['language'])?> *</label>
                                    <select  name="country" required>
                                        <?php foreach($countries as $country){?>
                                            <option value="<?php echo $country['country_code']?>" 
                                                <?php echo isset($booking) ? 'disabled': ''?>
                                                <?php echo isset($booking) &&  $booking['country'] == $country['country_code'] ? 'selected': ''?>
                                                <?php echo !isset($booking) &&  $country['country_code']  == 'EG' ? 'selected': ''?>>
                                                <?php echo translate($country['country_name'], $this->data['language'])?>
                                           </option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label><?php echo translate('Address', $this->data['language'])?> *</label>
                                    <input type="text" class="form-control" name="address" value="<?php echo isset($booking) ? $booking['address']: ''?>"
                                    placeholder="<?php echo translate('Address', $this->data['language'])?>" required
                                    <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label><?php echo translate('Town / City', $this->data['language'])?> *</label>
                                    <input type="text" class="form-control"  name="city" value="<?php echo isset($booking) ? $booking['city']: ''?>"
                                    placeholder="<?php echo translate('Town / City', $this->data['language'])?>" required
                                    <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label><?php echo translate('State / County', $this->data['language'])?> *</label>
                                    <input type="text" class="form-control" name="state" value="<?php echo isset($booking) ? $booking['state']: ''?>"
                                     placeholder="<?php echo translate('State / County', $this->data['language'])?>" required
                                     <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label><?php echo translate('Postcode / Zip', $this->data['language'])?></label>
                                    <input type="text" class="form-control"  name="post_code" value="<?php echo isset($booking) ? $booking['post_code']: ''?>"
                                     placeholder="<?php echo translate('Postcode / Zip', $this->data['language'])?>"
                                     <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label><?php echo translate('Phone', $this->data['language'])?> *</label>
                                    <input type="text" class="form-control" name="phone" value="<?php echo isset($booking) ? $booking['phone']: ''?>"
                                    placeholder="<?php echo translate('Phone', $this->data['language'])?>" required
                                    <?php echo isset($booking) ? 'readonly': ''?>>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <input type="submit" class="btn btn-dark book_now_btn" value="<?php echo translate('Confirm Booking', $this->data['language']) ?>"
                                    <?php echo isset($booking) ? 'disabled': ''?>/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php if( isset($booking)){?>
                <div class="seperator"><i class="fa fa-credit-card"></i></div>
                <form action="<?php echo base_url('clients/home/update_booking_payment/'.$booking['id'])?>" method="post"
                enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="upper">Payment Method</h4>
                            <div class="list-group">
                                <input type="radio" name="payment_method" value="Bank Transfer" id="Radio1"
                                <?php  echo $booking['payment_method'] == 'Bank Transfer' ? 'checked' : '' ?>/>
                                <label class="list-group-item" for="Radio1">Direct Bank Transfer</label>
                                <input type="radio" name="payment_method" value="Cheque" id="Radio2" 
                                <?php  echo $booking['payment_method'] == 'Cheque' ? 'checked' : '' ?>/>
                                <label class="list-group-item" for="Radio2">Cheque Payment</label>
                                <input type="radio" name="payment_method" value="paypal" id="Radio3" 
                                <?php  echo $booking['payment_method'] == 'paypal' ? 'checked' : '' ?>/>
                                <label class="list-group-item" for="Radio3"><img width="90" alt="paypal" src="<?php echo base_url('site_assets/images/shop/paypal-logo.png')?>"></label>
                            </div>
                            <input name="payment_image" type="file" required/>
                            <?php if(isset($booking['payment_image']) &&  $booking['payment_image'] != ''){?>
                                <img src="<?php echo base_url('site_assets/uploads/bookings/'.$booking['payment_image'])?>" 
                                class="img-fluid img-thumbnail" style="height: 100px;width:100px;">
                            <?php }?>
                            <br>
                            <input type="submit" class="btn btn-dark book_now_btn" value="<?php echo translate('Confirm Booking Payment', $this->data['language']) ?>"
                            <?php echo isset($booking) && $booking['status'] == 3 ? 'disabled':'' ?>  />
                            
                        </div>
                    </div>
                </form>
            <?php }?>
        </div>
    </div>
</section>

<script>
 
</script>