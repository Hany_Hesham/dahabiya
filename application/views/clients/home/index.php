<!-- Inspiro Slider -->
<?php $this->load->view($this->data['slider']);?> 
<!--end: Inspiro Slider -->
<!-- WELCOME -->
<section id="welcome" class="p-b-0">
	<div class="container">
    <div class="heading-text heading-section text-center m-b-40" data-animate="fadeInUp">
      <h2><?php echo translate($welcome_zahabia['header'], $this->data['language']) ?></h2>
      <span class="lead"><?php echo translate($welcome_zahabia['paragraph'], $this->data['language']) ?></span>
    </div>
    <div class="row">
    <div class="col-12 col-lg-2"></div>
      <div class="col-12 col-lg-8">
        <div class="row" data-animate="fadeInUp">
          <div class="col-lg-12">
            <img class="img-fluid" src="<?php echo base_url('site_assets/images/index-merged-images.png')?>" alt="Welcome to POLO">
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-2"></div>
    </div>
	</div>
</section>
<!-- end: WELCOME -->

<!-- hotel_accomodation -->
  <?php $this->load->view('clients/home/index_parts/hotel_accomodation');?> 
<!-- end: hotel_accomodation -->

<!-- hotel_facilities -->
  <?php $this->load->view('clients/home/index_parts/hotel_facilities');?> 
<!-- end: hotel_facilities -->

<!-- CLIENTS -->

<!-- end: offers -->

<section id="page-content">
    <div class="container">
    <div class="heading-text heading-section text-center">
			<h2><?php echo translate('HOT OFFERS', $this->data['language'])?></h2>
        <div id="portfolio" class="grid-layout portfolio-2-columns" data-margin="20">
            <!-- portfolio item -->
              <?php foreach($offers as $offer){?>
                <div class="portfolio-item no-overlay ct-rooms">
                  <div class="portfolio-item-wrap">
                    <div class="portfolio-slider">
                        <div class="carousel dots-inside dots-dark arrows-dark" data-items="1" data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-autoplay="1500">
                            <a href="#"><img src="<?php echo base_url('site_assets/images/offers/'.$offer['image'])?>" alt=""></a>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-12 col-lg-8">
                        <span class="offer-title" style="float: left;"><?php echo translate($offer['title'], $this->data['language'])?></span>
                      </div>
                        <div class="col-12 col-lg-4">
                          <a style="float: right;margin-top:15px;" href="<?php echo base_url('clients/home/offer/'.$offer['id'])?>" class="item-link"><?php echo translate('See More', $this->data['language']) ?> <i class="icon-chevron-right"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
              <?php }?>    
          </div>
        </div>
    </div>
</section>