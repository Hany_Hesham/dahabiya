<?php $this->load->view('clients/includes/inner_slider') ?>
<section>
  <div class="container">
    <div class="row">
        <div class="content col-lg-7">
            <!-- Blog -->
            <div class="carousel dots-inside arrows-visible" data-items="1" data-lightbox="gallery">
                <?php foreach($room['files'] as $file) {?>
                    <a href="#"><img src="<?php echo base_url('site_assets/images/rooms/'.$file['file_name'])?>" alt=""></a>
                <?php }?>
            </div>
            <a style="text-align: left !important;pointer-events: none;">
                <h3 class="room-title"> <?php echo translate($room['room'], $this->data['language']) ?></h3>
                <span class="room-description"><i class="fa fa fa-bed"></i> <?php echo translate($room['bed'], $this->data['language']) ?></span><br>
                <span class="room-description"><i class="fa fa fa-ruler-combined"></i> <?php echo translate($room['space'], $this->data['language']) ?></span><br>
                <span class="room-description"><i class="fa fa-user-friends"></i>
                    <?php echo $room['adult'].' '. translate( 'adults and', $this->data['language']).'
                        '.$room['child'].' '.translate( 'child', $this->data['language']) ?>  
                </span><br>                       
            </a>
        </div>
        <div class="content col-lg-5">
            <div class="post-item-description">
                <h2>Room Features</h2>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-4">
                        <div class="widget">
                            <h4><?php echo translate('Room', $this->data['language'])?></h4>
                            <ul class="list">
                                <?php foreach($room['features'] as $feature){
                                    if($feature['category'] == 'room features'){?>
                                        <li><span class="room-description"><i class="fa fa-check"></i> <?php echo translate($feature['feature'], $this->data['language'])?></span><br></li>
                                <?php } }?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-4">
                        <div class="widget">
                            <h4> <?php echo translate('Room amenities', $this->data['language'])?></h4>
                            <ul class="list">
                                <?php foreach($room['features'] as $feature){
                                    if($feature['category'] == 'room amenities'){?>
                                        <li><span class="room-description"><i class="fa fa-check"></i> <?php echo translate($feature['feature'], $this->data['language'])?></span><br></li>
                                <?php } }?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="<?php echo base_url('clients/home/booking')?>" class="btn btn-dark" style="float: right;width:100%;">
                    <span style="font-weight: 500; font-size:16px;"><i class="icon-list"></i> <?php echo translate('Book Now', $this->data['language'])?> </span></a>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>