<main class="page-content">
  <section style="background-image: url(<?php echo base_url('site_assets/images/dived/1298266-top-freediving-wallpaper-1920x1080-for-ipad-pro.jpg');?>);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
    <div class="shell">
      <div class="page-title">
        <h2><?php echo translate($offerData['hotel_name'], $this->data['language']) ?></h2>
      </div>
    </div>
  </section>
  <section id="mainPageBody">
  </section>
</main>
<script type="text/javascript">
  getViewAjax('clients/offers','offer','<?php echo $offerData['id'].'/1'?>','mainPageBody');
  function getViewAjax(controller,fun,form_id,insteadOf=''){ 
    var baseurl = document.getElementById('baseurl').value;
    var url = baseurl+controller+'/'+fun+'/'+form_id;
    jQuery.ajaxSetup({async:false});
    $('#'+insteadOf+'').empty();
    $('#'+insteadOf+'').load( ""+url+"" );
  } 
</script>