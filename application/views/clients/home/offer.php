<?php 
   $this->load->view('clients/includes/inner_slider'); 
   $maxNumber = 21; 
 ?>
<section>
    <div class="container">
        <div class="row">
            <div class="content col-lg-5">
                <div class="post-item-description">
                    <div class="carousel dots-inside arrows-visible" data-items="1" data-lightbox="gallery">
                        <a href="<?php echo base_url('site_assets/images/offers/'.$offer['image'])?>"
                            data-lightbox="gallery-image">
                            <img alt="image" src="<?php echo base_url('site_assets/images/offers/'.$offer['image'])?>">
                        </a>
                    </div>
                    <h4><?php echo translate($offer['title'], $this->data['language'])?></h4>
                    <span>
                        <?php echo translate('Adults', $this->data['language']).':&nbsp;'?> 
                        <strong><?php echo$offer['adults']?></strong>
                    </span><br>
                    <span>
                        <?php echo translate('Childerns', $this->data['language']).':&nbsp;'?> 
                        <strong><?php echo $offer['childs']?></strong>
                    </span><br>
                    <span>
                        <?php echo translate('Nights', $this->data['language']).':&nbsp;'?> 
                        <strong><?php echo $offer['nights']?></strong>
                    </span><br>
                    <strong>
                        <?php echo translate('Offer Price', $this->data['language']) ?>:
                        <sapn style="font-weight: 400; font-style: italic;">
                            <?php echo money_formater($offer['price'],$offer['currency']) ?>
                        </sapn>
                    </strong>
                    <?php if($offer['description'] != ''){?>
                    <p><?php echo translate($offer['description'], $this->data['language'])?></p>
                    <?php }?>
                </div>
            </div>
            <div class="content col-lg-7">
                <!-- Blog -->

                <div class="post-item-description">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-4">
                            <div class="widget">
                                <h4><?php echo translate('INCLUDED', $this->data['language'])?></h4>
                                <ul class="list">
                                    <?php foreach($offer['features'] as $feature){
                                if ($feature['category'] == 'included'){?>
                                    <li>
                                        <span class="room-description"><i class="fa fa-check"></i>
                                            <?php echo translate($feature['feature'], $this->data['language'])?>
                                        </span><br>
                                    </li>
                                    <?php }}?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-4">
                            <div class="widget">
                                <h4><?php echo translate('Conditions', $this->data['language'])?></h4>
                                <ul class="list">
                                    <?php foreach($offer['features'] as $feature){
                                if ($feature['category'] == 'conditions'){?>
                                    <li>
                                        <span class="room-description">
                                            <i class="fa fa-check"></i>
                                            <?php echo translate($feature['feature'], $this->data['language'])?>
                                        </span><br>
                                    </li>
                                    <?php }}?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('alert')){?>
                  <div role="alert" class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button>
                    <?php echo translate($this->session->flashdata('alert'), $this->data['language']) ?>. 
                  </div>
                <?php }?>  
                <div class="card-header"
                    style="background-color: #3c60b1;padding-bottom:5px;padding-top:10px;box-shadow: 0px 5px 5px 9px #8f8f8f;">
                    <form action="<?php echo base_url("clients/home/add_offer_booking/".$offer['id'])?>" method="get">
                        <div class="form-row" style="margin-top:15px;">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="input-group offer-dateRangePiker" id="form_daterangepicker_2">
                                        <input type="text" name="dates" onclick="initDatePiker()" class="form-control"
                                            placeholder="Check-in - Check-out" required="">
                                        <div class="input-group-append">
                                            <div class="input-group-text"><i class="icon-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <select name="adults" class="form-control" required="">
                                    <option value="1"
                                        <?php echo isset($offer['adults']) &&  $offer['adults'] == 1 ?'selected' : '' ?>>
                                        1 <?php echo translate('adult', $this->data['language']) ?></option>
                                    <?php for($i=2; $i< $maxNumber; $i++){?>
                                    <option value="<?php echo $i?>"
                                        <?php echo isset($offer['adults']) &&  $offer['adults'] == $i ?'selected' : '' ?>
                                        <?php echo $i > $offer['adults'] ? 'disabled': ''?>>
                                        <?php echo $i.' '. translate('adults', $this->data['language'])?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <select name="childs" class="form-control" required="">
                                    <option value="0"
                                        <?php echo isset($offer['childs']) &&  $offer['childs'] == 1 ?'selected' : '' ?>>
                                        0 <?php echo translate('children', $this->data['language']) ?></option>
                                    <?php for($i=1; $i< $maxNumber; $i++){?>
                                    <option value="<?php echo $i?>"
                                        <?php echo isset($offer['childs']) &&  $offer['childs'] == $i ?'selected' : '' ?>
                                        <?php echo $i > $offer['childs'] ? 'disabled': ''?>>
                                        <?php echo $i.' '. translate('childrens', $this->data['language'])?></option>
                                    <?php }?>

                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <select name="rooms" class="form-control" required="">
                                    <option value="1"
                                        <?php echo isset($offer['rooms']) &&  $offer['rooms'] == 1 ?'selected' : '' ?>>1
                                        <?php echo translate('room', $this->data['language']) ?></option>
                                    <?php for($i=2; $i< $maxNumber; $i++){?>
                                    <option value="<?php echo $i?>"
                                        <?php echo isset($offer['rooms']) &&  $offer['rooms'] == $i ?'selected' : '' ?>
                                        <?php echo  $i < $offer['rooms']  ? 'disabled': ''?>>
                                        <?php echo $i.' '.translate('rooms', $this->data['language'])?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="submit" class="btn btn-warning book_now_btn"
                                    value="<?php echo translate('Book Now', $this->data['language'])?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
function initDatePiker() {
    $('.offer-dateRangePiker').daterangepicker({
        minDate: '01/19/2022'
    }, function(start, end) {
        $(".offer-dateRangePiker .form-control").val(start.format("YYYY-MM-DD") + " / " + end.format(
            "YYYY-MM-DD"));
    });
}
</script>