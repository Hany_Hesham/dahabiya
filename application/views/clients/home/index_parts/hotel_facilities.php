<style>
	.icon-box.border .icon {
		border: 3px solid #fff1f1;
		text-align: center;
		border-radius: 50%;
		background-color: transparent;
	}
</style>
<?php 
	$facilities = $this->Home_model->get_sit_metaData('facilities', 'multi');
	$facility_groups = array_chunk($facilities,3); 
 ?>
<section class="text-light p-t-150 p-b-150" style="background-image: url('<?php echo base_url('site_assets/images/banner/DSC_3140-min.jpg')?>');background-repeat: no-repeat; background-size: cover;">
    <div class="bg-overlay"></div>
    <div class="container">
	    <div class="heading-text heading-section" style="margin-top: -100px !important;">
			<h2><?php echo translate('Hotel Facilities', $this->data['language']) ?></h2>
		</div>
		<?php for($i=0 ; $i < count($facility_groups); $i++){?>
			<div class="row">
				<?php foreach($facility_groups[$i] as $facility_group){?>
					<div class="col-lg-4">
					    <div class="icon-box effect medium border small">
							<div class="icon">
								<a href="javascript:void(0)"><i class="<?php echo $facility_group['img']?>"></i></a>
							</div>
							<h3 style="margin-top: 15px;"><?php echo translate($facility_group['title'], $this->data['language'])?></h3>
						</div>
					</div>
				<?php }?>
			</div>
		<?php }?> 
	</div>
</section>