<!-- Accomodation -->
<section id="page-content">
	<div class="container">
		<div class="heading-text heading-section">
			<h2><?php echo translate('Hotel Accomodation', $this->data['language']) ?></h2>
			<span class="lead">
            <?php echo translate('The accommodation features a 24-hour front desk. All units at the hotel are equipped with a seating area,
             a flat-screen TV with satellite channels', $this->data['language']) ?></span>
		</div>
        <!-- Portfolio -->
        <div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="20">
            <!-- portfolio item -->
            <?php foreach($rooms as $room){?>
                <div class="portfolio-item no-overlay <?php echo $room['category_class']?>">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-slider">
                            <div class="carousel dots-inside dots-dark arrows-dark" data-items="1" data-loop="true" data-autoplay="true" data-autoplay="1500">
                                <?php foreach($room['files'] as $file) {?>
                                    <a href="#"><img src="<?php echo base_url('site_assets/images/rooms/'.$file['file_name'])?>" alt=""></a>
                                <?php }?>
                            </div>
                        </div>
                        <div>
                            <a style="text-align: left !important;pointer-events: none;">
                                <h3 style="font-size: 12px;" class="room-title"> <?php echo translate($room['room'], $this->data['language']) ?></h3>
                            </a>
                            <a href="<?php echo base_url('clients/home/room/'.$room['id'])?>" class="item-link"><?php echo translate('See More', $this->data['language']) ?> <i class="icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</section>
<!-- end: Accomodation -->