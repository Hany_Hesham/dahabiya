<?php $this->load->view('clients/includes/inner_slider') ?>
<style>
   
</style>
<!-- Content -->
<section id="page-content">
    <div class="container">
        <!-- Portfolio -->
        <div id="portfolio" class="grid-layout portfolio-2-columns" data-margin="20">
            <!-- portfolio item -->
            <?php foreach($offers as $offer){?>
                <div class="portfolio-item no-overlay ct-rooms">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-slider">
                        <div class="carousel dots-inside dots-dark arrows-dark" data-items="1" data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-autoplay="1500">
                            <a href="#"><img src="<?php echo base_url('site_assets/images/offers/'.$offer['image'])?>" alt=""></a>
                        </div>
                    </div>
                    <div style="margin-top: 20px;">
                        <span class="offer-title"><?php echo translate($offer['title'], $this->data['language'])?></span><br><br>
                        <a  class="btn btn-outline btn-dark" href="<?php echo base_url('clients/home/offer/'.$offer['id']) ?>">
                            <i class="fa fa-list"></i>
                            <span><?php echo translate('Details', $this->data['language'])?></span>
                        </a>
                    </div>    
                    </div>
                </div>
            <?php }?>    
          </div>
        </div>
        <!-- end: Portfolio -->
    </div>
</section> <!-- end: Content -->