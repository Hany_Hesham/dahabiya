  <main class="page-content">
    <section style="background-image: url(<?php echo base_url('site_assets/images/dived/1298266-top-freediving-wallpaper-1920x1080-for-ipad-pro.jpg');?>);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
      <div class="shell">
        <div class="page-title">
          <h2><?php echo translate('Portfolio', $this->data['language']) ?></h2>
        </div>
      </div>
    </section>
    <section>
      <div class="section-50 section-sm-bottom-75 section-lg-bottom-100 bg-whisper">
        <div class="shell">
          <div class="range">
            <div class="cell-xs-12">
              <?php foreach($portfolio as $row){?>
                <div class="product product-item-fullwidth">
                  <div class="product-slider">
                    <div class="product-slider-inner">
                      <div  class="owl-carousel owl-style-minimal">
                        <div class="item">
                          <figure>
                             <img src="<?php echo base_url('site_assets/images/portfolio/'.$row['img'])?>" alt="" width="400" height="360"/> 
                          </figure>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="product-main">
                    <div class="product-main-inner">
                      <div class="product-body">
                        <h5 class="product-header"><?php echo translate($row['header'], $this->data['language']) ?></h5>
                        <div class="product-rating">
                          <ul class="list-rating">
                            <?php for($i=1; $i<= $row['title'];$i++){?>
                              <li><span class="icon icon-xxs material-icons-star"></span></li>
                            <?php }?>
                            <?php if ((float)$row['title'] < 5) {
                                     for($j=0; $j<= (5-(float)$row['title']);$j++){?>
                                       <li><span class="icon icon-xxs material-icons-star_border"></span></li>
                            <?php } }?>
                          </ul>
                          <span class="text-light"><?php echo translate('reviews', $this->data['language']) ?></span> </div>
                        <div class="product-description">
                          <p><?php echo translate($row['paragraph'], $this->data['language']) ?></p>
                        </div>
                      </div>
                      <div class="product-aside">
                      </div>
                    </div>
                  </div>
                </div>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
