
<?php 
    $this->load->view('clients/includes/inner_slider');
    $maxNumber = 21; 
?>
<!-- Shop products -->
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-lg-8">
                <div class="row"  style="background-color: #FFF;">
                    <div class="col-lg-12">
                        <h3 class=" p-t-10">
                        <?= $this->session->flashdata('msg'); ?>
                            <?php echo translate('Book Your Stay', $this->data['language']) ?>
                        </h3>
                        <p>
                            <?php echo translate('accommodations found for you from', $this->data['language']) ?>
                            date -
                            <?php echo translate('till', $this->data['language']) ?>
                            date

                        </p>
                    </div>
                </div><br>
                <div class="row"  style="background-color: #FFF;">
                    <div class="col-lg-12">
                        <div class="order-select">
                            <p class="">
                                <strong>
                                    <?php echo translate("Filter By Room Category", $this->data['language']) ?>
                                </strong>
                            </p>
                            <nav class="grid-filter gf-outline" data-layout="#portfolio">
                                <ul>
                                    <li class="active"><a href="#" data-category="*"><?php echo translate('ALL', $this->data['language']) ?></a></li>
                                    <?php foreach($categories as $category){?>
                                        <li>
                                            <a href="#" data-category=".<?php echo $category['paragraph']?>">
                                                <?php echo translate($category['header'], $this->data['language']) ?>
                                            </a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div><br>
                <!--Product list-->
                <div class="shop" >
                    <div id="portfolio" class="grid-layout portfolio-1-columns" data-margin="20">
                <!-- portfolio item -->
                        <?php foreach($rooms as $room){?>
                            <div class="portfolio-item no-overlay <?php echo $room['category_class']?>">
                                <div class="portfolio-item-wrap">
                                    <?php if(isset($room['available'])){?>
                                        <form action="<?php echo base_url('clients/home/add_booking/'.$room['id'])?>" method="post">
                                            <input type="hidden" name="search_<?php echo $room['id']?>" value='<?php echo json_encode($search)?>' />
                                            <div class="row">
                                                <div class="form-group col-3 col-lg-2">
                                                <input type="number" class="form-control" name="reserved_rooms_<?php echo $room['id']?>" value="<?php echo $search['rooms']?>" required>
                                                </div>
                                                <div class="form-group col-9 col-lg-7">
                                                    <p class="p-t-5" > <?php echo '&nbsp &nbsp'. translate('of', $this->data['language']).' '. $room['available'].' '.translate('accommodations available', $this->data['language']) ?>.</p>
                                                </div>
                                                <!-- <div class="form-group col-sm-3">
                                                </div> -->
                                                <div class="form-group col-3 col-lg-3">
                                                    <input type="submit"  class="btn btn-warning" value="<?php echo translate( 'Reserve', $this->data['language'])?>"/>
                                                </div>
                                            </div>
                                        </form>
                                        <br>
                                    <?php }?>    
                                    <div class="portfolio-slider">
                                        <div class="carousel dots-inside dots-dark arrows-dark" data-items="1" data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-autoplay="1500">
                                            <?php foreach($room['files'] as $file) {?>
                                                <a href="javascript:void(0)"><img src="<?php echo base_url('site_assets/images/rooms/'.$file['file_name'])?>" alt=""></a>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div>
                                        <a style="text-align: left !important;pointer-events: none;">
                                            <h3 class="room-title"> <?php echo translate($room['room'], $this->data['language']) ?></h3>
                                            <span class="room-description"><i class="fa fa fa-bed"></i> <?php echo translate($room['bed'], $this->data['language']) ?></span><br>
                                            <span class="room-description"><i class="fa fa fa-ruler-combined"></i> <?php echo translate($room['space'], $this->data['language']) ?></span><br>
                                            <span class="room-description"><i class="fa fa-user-friends"></i>
                                                <?php echo $room['adult'].' 
                                                '. translate( 'adults, or', $this->data['language']).' 
                                                '.$room['adult'].' '. translate( 'adults and', $this->data['language']).'
                                                '.$room['child'].' '.translate( 'child', $this->data['language']) ?>  
                                            </span><br><br> 
                                           <strong>
                                               <?php echo translate('Prices start at', $this->data['language']) ?>: 
                                               <sapn style="font-weight: 400; font-style: italic;">
                                                    <?php echo money_formater($room['price'],$room['currency']) ?>
                                                </sapn> 
                                                <sapn style="font-weight: 600; font-style: italic;"> <?php echo translate('Per night', $this->data['language']) ?></sapn>
                                            </strong>                      
                                        </a><br><br>
                                        <a href="<?php echo base_url('clients/home/room/'.$room['id'])?>" class="btn btn-dark"><i class="icon-list"></i><?php echo translate( 'Details', $this->data['language'])?></a>
                                    </div>
                                </div><hr>
                            </div>
                        <?php }?>
                    </div>
                    <!-- end: Pagination -->
                </div>
                <!--End: Product list-->
            </div>
            <!-- end: Content-->
            <!-- Sidebar-->
            <div class="sidebar sticky-sidebar col-lg-4" style="background-color: #FFF; box-shadow: 0px 5px 5px 9px #ebebeb;">
                <!--widget newsletter-->
                <div class="widget widget-archive">
                    <h4 class="widget-title  p-t-10">
                        <?php echo translate('Book Your Stay', $this->data['language']) ?>
                    </h4>
                    <form action="<?php echo base_url('clients/home/booking')?>" method="get">
                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <label for="name">Check-in: *</label>
                                <input type="date" class="form-control" name="check_in"  min="<?= date('Y-m-d'); ?>"
                                 value="<?php echo isset($search['check_in']) ? $search['check_in'] : '' ?>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <label for="name">Check-out: *</label>
                                <input type="date" class="form-control" name="check_out"  min="<?= date('Y-m-d'); ?>"
                                value="<?php echo isset($search['check_out']) ? $search['check_out'] : '' ?>" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <select name="adults" class="form-control" required>
                                    <option value="1" <?php echo isset($search['adults']) &&  $search['adults'] == 1 ?'selected' : '' ?>>1 <?php echo translate('adult', $this->data['language']) ?></option>
                                    <?php for($i=2; $i< $maxNumber; $i++){?>
                                        <option value="<?php echo $i?>" <?php echo isset($search['adults']) &&  $search['adults'] == $i ?'selected' : '' ?>><?php echo $i.' '. translate('adults', $this->data['language'])?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <select name="childs" class="form-control" required>
                                    <option value="0" <?php echo isset($search['childs']) &&  $search['childs'] == 0 ?'selected' : '' ?>>0 <?php echo translate('children', $this->data['language']) ?></option>
                                    <?php for($i=1; $i< $maxNumber; $i++){?>
                                        <option value="<?php echo $i?>"  <?php echo isset($search['childs']) &&  $search['childs'] == $i ?'selected' : '' ?>><?php echo $i.' '. translate('childrens', $this->data['language'])?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <select name="rooms" class="form-control" required>
                                    <option value="1" <?php echo isset($search['rooms']) &&  $search['rooms'] == 1 ?'selected' : '' ?>>1 <?php echo translate('room', $this->data['language']) ?></option>
                                    <?php for($i=2; $i< $maxNumber; $i++){?>
                                        <option value="<?php echo $i?>" <?php echo isset($search['rooms']) &&  $search['rooms'] == $i ?'selected' : '' ?>><?php echo $i.' '.translate('rooms', $this->data['language'])?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-warning book_now_btn" value="<?php echo translate('Check Rates', $this->data['language']) ?>"/>
                    </form>
                </div>
                <!-- end: Sidebar-->
            </div>
        </div>
</section>
<!-- <div class="modal fade no-padding" id="success-booking-modal" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button aria-hidden="true" style="text-align:right;padding:5px;" data-dismiss="modal" class="close" type="button">×</button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="text-l text-theme  m-b-30">Thank You </h2>
                        <p class="m-b-20">
                            <?//php echo translate($this->session->flashdata('msg'), $this->data['language']) ?>
                        </p>
                        <img id="success-image" style=" display: block; margin-left: auto; margin-right: auto; width: 50%;"
                        src="<?//php echo base_url('site_assets/images/animation_500_kw7u44j1.gif')?>" alt="">
                    </div>
                    <div class="col-12 col-md-6" >
                    <img id="success-image" style="  margin-left: auto; margin-right: auto; width: 100%;"
                        src="<?//php echo base_url('site_assets/images/gallery/DSC_3140-min-success.jpg')?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<script>
   
</script>