<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row gutter-40 col-mb-80">
				<!-- Post Content
				============================================= -->
				<div class="postcontent col-lg-9 order-lg-last">
					<div class="single-post mb-0">
						<!-- Single Post
						============================================= -->
						<div class="entry clearfix">
							<!-- Entry Title
							============================================= -->
							<div class="entry-title">
								<h2><?php echo $post['title'] ?></h2>
							</div><!-- .entry-title end -->
							<!-- Entry Meta
							============================================= -->
							<div class="entry-meta">
								<ul>
									<li><i class="icon-calendar3"></i><?php echo date('d', strtotime($post['timestamp'])) ?> <?php echo date('M', strtotime($post['timestamp'])) ?> <?php echo date('Y', strtotime($post['timestamp'])) ?></li>
									<li><i class="icon-folder-open"></i><?php echo $post['category'] ?>
								</ul>
							</div><!-- .entry-meta end -->
							<!-- Entry Image
							============================================= -->
							<div class="entry-image">
								<a href="#"><img src="<?php echo base_url('site_assets/images/blog/full/'.$post['image'])?>" alt="Blog Single"></a>
							</div><!-- .entry-image end -->
							<!-- Entry Content
							============================================= -->
							<div class="entry-content mt-0">
								<p><?php echo $post['description'] ?></p>
								<br>
								<p><?php echo $post['post'] ?></p>
								<!-- Post Single - Content End -->
								<div class="clear"></div>
							</div>
						</div><!-- .entry end -->
					</div>
				</div><!-- .postcontent end -->
				<!-- Sidebar
				============================================= -->
				<div class="sidebar col-lg-3">
					<div class="sidebar-widgets-wrap">
						<div class="widget clearfix">
							<h4>Gallery</h4>
							<div class="masonry-thumbs grid-container grid-4" data-lightbox="gallery">
								<?php foreach ($images as $image):?>
									<a href="<?php echo base_url('site_assets/images/portfolio/full/'.$image['img'])?>" data-lightbox="gallery-item" class="grid-item">
										<img src="<?php echo base_url('site_assets/images/portfolio/4/'.$image['thumb'])?>" alt="<?php echo $image['title'] ?>">
									</a>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div><!-- .sidebar end -->
			</div>
		</div>
	</div>
</section><!-- #content end -->