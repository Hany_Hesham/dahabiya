<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="INSPIRO" />
    <meta name="description" content="Themeforest Template Polo, html template">
    <link rel="icon" type="image/png" href="<?php echo base_url('site_assets/images/gallery/4e461400-1765-45b9-abf3-7a915bbf6e5a.jpg')?>">   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Document title -->
    <title>Zahabia Resort</title>
    <!-- Stylesheets & Fonts -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/vendors/linericon/style.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('site_assets/vendors/owl-carousel/owl.carousel.min.css')?>"> 
    <link rel="stylesheet" href="<?php echo base_url('site_assets/vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.css')?>">
    <!-- <link rel="stylesheet" href="<?//php echo base_url('site_assets/vendors/nice-select/css/nice-select.css')?>"> -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/vendors/owl-carousel/owl.carousel.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/style-out-source.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/responsive.css')?>">
    <link href="<?php echo base_url('site_assets/css/plugins.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('site_assets/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('site_assets/css/custom.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('site_assets/plugins/daterangepicker/daterangepicker.css')?>" rel="stylesheet">
    <style>
        /* width */
        ::-webkit-scrollbar {
        width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
        background: #f1f1f1; 
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
        background: #888; 
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
        background: #555; 
        }
    </style>
</head>

<body>
    <!-- Body Inner -->
    <div class="body-inner">
    <input type="hidden" id="baseurl" name="baseurl" value="<?php echo base_url(); ?>" />
        <!-- Header -->
        <?php $this->load->view($this->data['header']);?> 
        <!-- end: Header -->
        <?php $this->load->view($view);?>
        <!-- Footer -->
        <?php $this->load->view($this->data['footer']);?>   
        <div class="modal fade no-padding" id="success-booking-modal" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button aria-hidden="true" style="text-align:right;padding:5px;" data-dismiss="modal" class="close" type="button">×</button>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <h2 class="text-l text-theme  m-b-30">Thank You </h2>
                                <!-- <h2 class="text-l text-theme  m-b-30">70%</h2> -->
                                <p class="m-b-20">
                                    <?php echo translate($this->session->flashdata('alert'), $this->data['language']) ?>
                                </p>
                                <img id="success-image" style=" display: block; margin-left: auto; margin-right: auto; width: 50%;"
                                src="<?php echo base_url('site_assets/images/animation_500_kw7u44j1.gif')?>" alt="">
                            </div>
                            <div class="col-12 col-md-6" >
                            <img id="success-image" style="  margin-left: auto; margin-right: auto; width: 100%;"
                                src="<?php echo base_url('site_assets/images/gallery/DSC_3140-min-success.jpg')?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Footer -->
    </div>
    <!-- end: Body Inner -->
    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
    <!--Plugins-->
    <script src="<?php echo base_url('site_assets/js/jquery.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/plugins.js')?>"></script>
    <!--Template functions-->
    <script src="<?php echo base_url('site_assets/vendors/owl-carousel/owl.carousel.min.js')?>"></script>
    <script src="<?php echo base_url('site_assets/vendors/bootstrap-datepicker/bootstrap-datetimepicker.min.js')?>"></script>
    <!-- <script src="<?//php echo base_url('site_assets/vendors/nice-select/js/jquery.nice-select.js')?>"></script> -->
    <script src="<?php echo base_url('site_assets/vendors/lightbox/simpleLightbox.min.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/popper.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/jquery.ajaxchimp.min.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/mail-script.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/mail-script.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/stellar.js')?>"></script>
    <!-- <script src="<?//php echo base_url('site_assets/js/custom-out-source.js')?>"></script> -->
    <script src="<?php echo base_url('site_assets/js/custom.js')?>"></script>
    <script src="<?php echo base_url('site_assets/js/functions.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('site_assets/plugins/gmap3/gmap3.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('site_assets/plugins/gmap3/map-styles.js')?>"></script>
    <script src="<?php echo base_url('site_assets/plugins/moment/moment.min.js')?>"></script>
    <script src="<?php echo base_url('site_assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
    <link href="<?php echo base_url("site_assets/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <script src="<?php echo base_url("site_assets/plugins/dropzone/dropzone.js")?>"></script>
</body>
<script>
	$(document).ready(function(){
		$('.my-dateRangePiker').daterangepicker({ minDate: '01/19/2022'}, function (start, end) {
            $(".my-dateRangePiker .form-control").val(start.format("YYYY-MM-DD") + " / " + end.format("YYYY-MM-DD"));
        });
        <?php if( $this->session->flashdata('msg') != ''){ ?>
            $('#success-booking-modal').modal('show');
        <?php }?>
	});
</script>

</html>
