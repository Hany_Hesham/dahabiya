<section id="page-title">
	<div class="container clearfix">
		<h1>Gallery Filter</h1>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Gallery Filter</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="grid-filter-wrap">
				<!-- Grid Filter
				============================================= -->
				<ul class="grid-filter" data-container="#demo-gallery">
					<li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
					<?php foreach ($categories as $key => $category):?>
						<li><a href="#" data-filter="<?php echo $key ?>"><?php echo $category ?></a></li>
					<?php endforeach; ?>
				</ul><!-- .grid-filter end -->
			</div>
			<div id="demo-gallery" class="masonry-thumbs grid-container grid-3" data-big="2" data-lightbox="gallery">
				<?php foreach ($images as $image):?>
					<a href="<?php echo base_url('site_assets/images/portfolio/full/'.$image['img'])?>" data-lightbox="gallery-item" class="grid-item<?php echo getCategories($image['categories']) ?>">
						<img src="<?php echo base_url('site_assets/images/portfolio/4/'.$image['thumb'])?>" alt="<?php echo $image['title'] ?>">
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section><!-- #content end -->