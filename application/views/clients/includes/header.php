<header id="header" data-transparent="true" data-fullwidth="true" class="dark submenu-light">
	<div class="header-inner">
		<div class="container">
			<!--Logo-->
			<div id="logo">
				<a href="<?php echo base_url('clients/home')?>">
					<span class="logo-default"> <img src="<?php echo base_url('site_assets/images/genral/'.$this->data['site_title_logo']['img'])?>"
					width="210" height="80"/> </span>
					<span class="logo-dark"> <img src="<?php echo base_url('site_assets/images/genral/'.$this->data['site_title_logo']['img'])?>"
					width="210" height="80"/> </span>
				</a>
			</div>
			<!--End: Logo-->
			<!--Header Extras-->
			<div class="header-extras">
				<ul>
					<li>
						<div class="p-dropdown">
							<a href="#"><i class="icon-globe"></i><span>EN</span></a>
							<ul class="p-dropdown-content">
								<li>
									<a href="<?php echo base_url('clients/home/language_converter/english');?>">
								     <?php echo translate('English', $this->data['language']) ?>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('clients/home/language_converter/arabic');?>">
									    <?php echo translate('Arabic', $this->data['language']) ?>
								    </a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<!--end: Header Extras-->
			<!--Navigation Resposnive Trigger-->
			<div id="mainMenu-trigger">
				<a class="lines-button x"><span class="lines"></span></a>
			</div>
			<!--end: Navigation Resposnive Trigger-->
			<!--Navigation-->
			<div id="mainMenu">
				<div class="container">
					<nav>
						<ul>
							<li><a href="<?php echo base_url('clients/home') ?>">
							        <?php echo translate('Home', $this->data['language']) ?>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('clients/home/rooms') ?>">
								    <?php echo translate('Our Rooms', $this->data['language']) ?>
							    </a>
							</li>
							<li>
								<a href="<?php echo base_url('clients/home/offers') ?>">
								    <?php echo translate('Offers & Pakages', $this->data['language']) ?>
							    </a>
							</li>
							<li>
								<a href="<?php echo base_url('clients/home/gallery') ?>">
								    <?php echo translate('Gallery', $this->data['language']) ?>
							    </a>
							</li>
							<li>
								<a href="<?php echo base_url('clients/home/about') ?>">
								    <?php echo translate('About US', $this->data['language']) ?>
							    </a>
							</li>
							<li>
								<a href="<?php echo base_url('clients/home/contact_us') ?>">
								    <?php echo translate('Contact Us', $this->data['language']) ?>
							    </a>
							</li>
							<li>
								<a href="<?php echo base_url('clients/home/booking') ?>">
								    <?php echo translate('Book Your Stay', $this->data['language']) ?>
							    </a>
							</li>
							<!-- <li class="dropdown mega-menu-item"><a href="#">Book Your Stay</a>
								<ul class="dropdown-menu">
									<li class="mega-menu-content">
										<div class="row">
											<div class="col-lg-5">
												<ul>
													<li class="mega-menu-title">Single Product</li>
													<li><a href="shop-single-product.html">Fullwidth</a></li>
													<li><a href="shop-single-product-sidebar-left.html">Left Sidebar</a></li>
													<li><a href="shop-single-product-sidebar-right.html">Right Sidebar</a></li>
													<li><a href="shop-single-product-sidebar-both.html">Both Sidebars</a></li>
												</ul>
											</div>
											<div class="col-lg-3">
												<h4 class="text-theme">BIG SALE<small>Up to</small></h4>
												<h2 class="text-lg text-theme lh80 m-b-30">70%</h2>
												<p class="m-b-0">
													The most happiest time of the day!. Morbi sagittis, sem quis ipsum dolor sit amet lacinia faucibus.
												</p><a class="btn btn-shadow btn-rounded btn-block m-t-10">
													    <?php echo translate('Book NOW!', $this->data['language']) ?>
											        </a><small class="t300">
													<p class="text-center m-0"><em>* Limited time Offer</em></p>
												</small>
											</div>
										</div>
									</li>
								</ul>
							</li> -->
						</ul>
					</nav>
				</div>
			</div>
			<!--end: Navigation-->
		</div>
	</div>
</header>