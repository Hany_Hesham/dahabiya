<footer id="footer">
	<div class="footer-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-4  col-lg-2 col-md-4">
					<!-- Footer widget area 1 -->
					<div class="widget  widget-contact-us" style="background-image: url('<?php echo base_url('site_assets/images/world-map-dark.png')?>'); background-position: 50% 20px; background-repeat: no-repeat">
						<h4><?php echo translate('About Zahabia Resort', $this->data['language']) ?></h4>
						<ul class="list-icon">
							<li>
							  <i class="fa fa-map-marker-alt"></i> 
							  <?php echo translate($this->data['location']['paragraph'], $this->data['language']) ?>
							</li>
							<li><i class="fa fa-phone"></i> <?php echo $this->data['contact']['paragraph'] ?></li>
							<li><i class="far fa-envelope"></i> <a href="mailto:<?php echo $this->data['email']['paragraph'] ?>"> <?php echo $this->data['email']['paragraph'] ?></a> </li>
						</ul>
					</div>
					<!-- end: Footer widget area 1 -->
				</div>

				<div class="col-xl-2 col-lg-2 col-md-4">
					<!-- Footer widget area 1 -->
					<div class="widget">
						<h4><?php echo translate('Site Map', $this->data['language']) ?></h4>
						<ul class="list">
							<li><a href="<?php echo base_url('clients/home/rooms') ?>"><?php echo translate('Our Rooms', $this->data['language']) ?></a></li>
							<li><a href="<?php echo base_url('clients/home/offers') ?>"> <?php echo translate('Offers & Pakages', $this->data['language']) ?></a></li>
							<li><a href="<?php echo base_url('clients/home/gallery') ?>"> <?php echo translate('Gallery', $this->data['language']) ?></a></li>
							<li><a href="<?php echo base_url('clients/home/about') ?>"><?php echo translate('About US', $this->data['language']) ?></a></li>
							<li><a href="<?php echo base_url('clients/home/contact_us') ?>"><?php echo translate('Contact Us', $this->data['language']) ?></a></li>
							<li><a href="<?php echo base_url('clients/home/booking') ?>"><?php echo translate('Book Your Stay', $this->data['language']) ?></a></li>
						</ul>
					</div>
					<!-- end: Footer widget area 1 -->
				</div>
				



				<div class="col-lg-6">
					<form action="<?php echo base_url('clients/home/add_contact_us')?>" role="form" method="post">
						<div class="input-group mb-2">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
							</div>
							<input type="text" aria-required="true" name="client_name" class="form-control required name" placeholder="Enter your Name" required>
						</div>
						<div class="input-group mb-2">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
							</div>
							<input type="email" aria-required="true" required name="email" class="form-control required email" placeholder="Enter your Email">
						</div>
						<div class="form-group mb-2">
							<textarea type="text" name="message" rows="5" class="form-control required"  required placeholder="Enter your Message"></textarea>
						</div>
						<div class="form-group">
							<button class="btn" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Send message</button>
						</div>
					</form>

				</div>


			</div>
		</div>
	</div>
	<div class="copyright-content">
		<div class="container">

			<div class="row">
				<div class="col-lg-6">
					<!-- Social icons -->
					<div class="social-icons social-icons-colored float-left">
						<ul>
							<li class="social-facebook"><a target="_" href="<?php echo $this->data['facebook']['paragraph']?>"><i class="fab fa-facebook-f"></i></a></li>
						</ul>
					</div>
					<!-- end: Social icons -->
				</div>

				<div class="col-lg-6">
					<div class="copyright-text text-right">&copy; 2022 2H&M. All Rights Reserved.</div>
				</div>
			</div>
		</div>
	</div>
</footer>