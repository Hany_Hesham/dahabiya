<!-- Page Title
============================================= -->
<section id="page-title">
	<div class="container clearfix">
		<h1>Our Trip</h1>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Trip</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="single-event">
				<div class="row col-mb-50">
					<div class="col-md-8 col-lg-9">
						<div class="entry-image mb-0">
							<a href="#"><img src="<?php echo base_url('site_assets/images/portfolio/4/5.png')?>" alt="Event Single"></a>
							<div class="entry-overlay d-flex align-items-center justify-content-center">
								<span class="d-none d-md-flex">Avalible </span><div class="countdown d-block d-md-flex" data-year="2020" data-month="12"></div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-lg-3">
						<div class="card event-meta mb-3">
							<div class="card-header"><h5 class="mb-0">Trip Info:</h5></div>
							<div class="card-body">
								<ul class="iconlist mb-0">
									<li><i class="icon-calendar3"></i> 8 Double Cabin</li>
									<li><i class="icon-time"></i> 6 Royal Cabin</li>
									<li><i class="icon-map-marker2"></i> Aswan</li>
									<li><i class="icon-dollar"></i> <strong>200</strong></li>
								</ul>
							</div>
						</div>
						<a href="<?php echo base_url();?>booking" class="btn btn-success btn-block btn-lg">Book Now</a>
					</div>
					<div class="w-100"></div>
					<div class="col-md-6">
						<h3>Details</h3>
						<p>Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.</p>
						<br>
						<br>
						<h4>Inclusions</h4>
						<div class="row col-mb-30">
							<div class="col-md-6">
								<ul class="iconlist mb-0">
									<li><i class="icon-ok"></i> WIFI</li>
									<li><i class="icon-ok"></i> Drinks</li>
									<li><i class="icon-ok"></i> Sightseeing</li>
								</ul>
							</div>
							<div class="col-md-6">
								<ul class="iconlist mb-0">
									<li><i class="icon-ok"></i> Accomodation</li>
									<li><i class="icon-ok"></i> All Meals</li>
									<li><i class="icon-ok"></i> Adventure Activities</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-6 min-vh-50">
						<img src="<?php echo base_url('site_assets/images/FOOO9316.JPG')?>">
						<?php foreach ($events as $event):?>
							<img data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $event['location'] ?>: <?php echo $event['describtion'] ?>"style="position: absolute; top: <?php echo $event['toped'] ?>%; left: <?php echo $event['lefted'] ?>%;" src="<?php echo base_url('site_assets/images/'.$event['image'])?>">
						<?php endforeach; ?>
					</div>
					<div class="w-100"></div>
					<div class="col-md-5">
						<h4>Gallery</h4>
						<!-- Events Single Gallery Thumbs
						============================================= -->
						<div class="masonry-thumbs grid-container grid-4" data-lightbox="gallery">
							<?php $i=1; foreach ($images as $image):?>
								<?php if ($i <= 12):?>
									<a href="<?php echo base_url('site_assets/images/portfolio/full/'.$image['img'])?>" data-lightbox="gallery-item" class="grid-item">
										<img src="<?php echo base_url('site_assets/images/portfolio/4/'.$image['thumb'])?>" alt="<?php echo $image['title'] ?>">
									</a>
								<?php endif; ?>
							<?php $i++; endforeach; ?>
						</div><!-- Event Single Gallery Thumbs End -->
					</div>
					<div class="col-md-7">
						<h4>Events Timeline</h4>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Day</th>
										<th>Location</th>
										<th>Info</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($events as $event):?>
										<tr>
											<td><span class="badge badge-danger">Day <?php echo $i ?></span></td>
											<td><?php echo $event['location'] ?></td>
											<td><?php echo $event['describtion'] ?></td>
										</tr>
									<?php $i++; endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- #content end -->