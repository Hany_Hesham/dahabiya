<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row gutter-40 col-mb-80">
				<!-- Post Content
				============================================= -->
				<div class="postcontent col-lg-9">
					<!-- Posts
					============================================= -->
					<div id="posts" class="post-timeline">
						<?php foreach ($posts as $post):?>
							<div class="entry">
								<div class="entry-timeline">
									<?php echo date('d', strtotime($post['timestamp'])) ?><span><?php echo date('M', strtotime($post['timestamp'])) ?></span>
									<div class="timeline-divider"></div>
								</div>
								<div class="entry-image">
									<a href="<?php echo base_url('site_assets/images/blog/full/'.$post['image'])?>" data-lightbox="image"><img src="<?php echo base_url('site_assets/images/blog/standard/'.$post['image'])?>" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-title">
									<h2><a href="<?php echo base_url('blog/'.$post['id']);?>"><?php echo $post['title'] ?></a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><i class="icon-folder-open"></i><?php echo $post['category'] ?></li>
									</ul>
								</div>
								<div class="entry-content">
									<p><?php echo $post['description'] ?></p>
									<a href="<?php echo base_url('blog/'.$post['id']);?>" class="more-link">Read More</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div><!-- #posts end -->
				</div><!-- .postcontent end -->
				<!-- Sidebar
				============================================= -->
				<div class="sidebar col-lg-3">
					<div class="sidebar-widgets-wrap">
						<div class="widget clearfix">
							<h4>Gallery</h4>
							<div class="masonry-thumbs grid-container grid-4" data-lightbox="gallery">
								<?php foreach ($images as $image):?>
									<a href="<?php echo base_url('site_assets/images/portfolio/full/'.$image['img'])?>" data-lightbox="gallery-item" class="grid-item">
										<img src="<?php echo base_url('site_assets/images/portfolio/4/'.$image['thumb'])?>" alt="<?php echo $image['title'] ?>">
									</a>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div><!-- .sidebar end -->
			</div>
		</div>
	</div>
</section><!-- #content end -->