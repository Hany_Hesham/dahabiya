<section class="fullscreen" data-bg-parallax="<?php echo base_url('site_assets/images/slider/DSC_3122-min - Copy.jpg') ?>">
    <div class="bg-overlay"></div>
    <div class="shape-divider" data-style="1" data-height="300"></div>
    <div class="container-wide">
        <div class="container-fullscreen">
            <div class="text-middle">
                <div class="heading-text text-light col-lg-6">
                    <h2 class="font-weight-800"><span><?php echo translate('Enjoy A luxory Experience', $this->data['language']) ?></span></h2>
                </div>
            </div>
        </div>
    </div>
</section>
