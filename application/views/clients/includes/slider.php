<?php 
    $sliders = $this->Home_model->get_sliders();
    $maxNumber = 20;
?>
<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-fade="true">
	<?php foreach($sliders as $slider){?>
		<div class="slide kenburns" data-bg-image="<?php echo base_url('site_assets/images/slider/'.$slider['img'])?>">
			<div class="bg-overlay"></div>
			<div class="container">
				<div class="slide-captions text-center text-light">
					<!-- Captions -->
					<h1><?php echo translate($slider['first_title'], $this->data['language']) ?></h1>
					<p><?php echo translate($slider['second_title'], $this->data['language']) ?></p>
					<div>
						<a href=".hotel_booking_area" class="btn btn-warning scroll-to">
							<?php echo translate('Book Now', $this->data['language']) ?>
						</a>
					</div>
					<!-- end: Captions -->
				</div>
			</div>
		</div>
	<?php }?>
</div>
<div class="card hotel_booking_area position" style="background-color:#3c60b1; border:0px; border-radius:0px;margin-top:15px;">
	<div class="card-header" style="background-color: #3c60b1;padding-bottom:5px;padding-top:10px;box-shadow: 0px 5px 5px 9px #8f8f8f;" >
		<h5 class="card-title" style="color: #FFF;"><?php echo translate('Book Your Room', $this->data['language']) ?></h5>
		<form action="<?php echo base_url('clients/home/booking')?>" method="get">
			<div class="form-row">
				<div class="col-lg-4">
					<div class="form-group">
						<div class="input-group my-dateRangePiker" id="form_daterangepicker_2">
							<input type="text" name="dates" class="form-control" placeholder="<?php echo translate('Check-in - Check-out', $this->data['language']) ?>"
							required>
							<div class="input-group-append">
								<div class="input-group-text"><i class="icon-calendar"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group col-md-2">
					<select name="adults" class="form-control" required>
					    <option value="1">1 <?php echo translate('adult', $this->data['language']) ?></option>
						<?php for($i=2; $i< $maxNumber; $i++){?>
							<option value="<?php echo $i?>"><?php echo $i.' '. translate('adults', $this->data['language'])?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group col-md-2">
					<select name="childs" class="form-control" required>
					    <option value="0">0 <?php echo translate('children', $this->data['language']) ?></option>
						<?php for($i=1; $i< $maxNumber; $i++){?>
							<option value="<?php echo $i?>"><?php echo $i.' '. translate('childrens', $this->data['language'])?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group col-md-2">
					<select name="rooms" class="form-control" required>
					    <option value="1">1 <?php echo translate('room', $this->data['language']) ?></option>
						<?php for($i=2; $i< $maxNumber; $i++){?>
							<option value="<?php echo $i?>"><?php echo $i.' '.translate('rooms', $this->data['language'])?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group col-md-2">
					<input type="submit" class="btn btn-warning book_now_btn" value="<?php echo translate('Check Rates', $this->data['language']) ?>"/>
				</div>
			</div>
		</form>
	</div>
</div>