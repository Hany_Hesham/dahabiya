<!-- Page Title
============================================= -->
<section id="page-title">
	<div class="container clearfix">
		<h1>DAHABEYA QUEEN FARIDA</h1>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Boat</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div id="side-navigation" class="row" data-plugin="tabs">
				<div class="col-md-6 col-lg-4">
					<ul class="sidenav">
						<?php foreach ($boat_infos as $info):?>
							<li><a href="#<?php echo $info['code'] ?>"><i class="<?php echo $info['icon'] ?>"></i><?php echo $info['title'] ?><i class="icon-chevron-right"></i></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="col-md-6 col-lg-8">
					<?php foreach ($boat_infos as $info):?>
						<div id="<?php echo $info['code'] ?>">
							<h3><?php echo $info['title'] ?></h3>
							<?php if (!is_null($info['image'])):?>
								<img class="alignright img-responsive" src="<?php echo base_url('site_assets/images/slider/'.$info['image'])?>" alt="Image" style="height: 400px; width:400px;">
							<?php endif; ?>
							<?php echo $info['remarks'] ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section><!-- #content end -->