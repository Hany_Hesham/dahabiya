<!-- Page Title
============================================= -->
<section id="page-title">
	<div class="container clearfix">
		<h1>Packages</h1>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Packages</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row">
				<?php foreach ($packages as $package):?>
					<div class="entry event col-12">
						<div class="grid-inner row align-items-center no-gutters p-4">
							<div class="entry-image col-md-4 mb-md-0">
								<a href="#">
									<img src="<?php echo base_url('site_assets/images/portfolio/4/'.$package['image'])?>" alt="Inventore voluptates velit totam ipsa tenetur">
									<div class="entry-date"><?php echo getDay($package['date']) ?><span><?php echo getMonth($package['date']) ?></span></div>
								</a>
							</div>
							<div class="col-md-8 pl-md-4">
								<div class="entry-title title-sm">
									<h2><a href="#"><?php echo $package['title'] ?></a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><span class="badge badge-warning px-1 py-1"><?php echo $package['status_name'] ?></span></li>
										<li><a href="#"><i class="icon-time"></i><?php echo $package['time'] ?></a></li>
										<li><a href="#"><i class="icon-map-marker2"></i><?php echo $package['location'] ?></a></li>
									</ul>
								</div>
								<div class="entry-content">
									<p><?php echo $package['remarks'] ?></p>
									<a href="#" class="btn btn-secondary" disabled="disabled">Book Now</a>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section><!-- #content end -->