<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<h3 class="center">Booking Form</h3>
			<div class="card border-0 shadow-lg rounded-lg" style="background-color: #000000;">
				<img class="card-img-top" src="<?php echo base_url('site_assets/images/portfolio/full/1.jpg')?>" alt="Image">
				<div class="card-body dark" style="padding: 60px">
					<div class="form-widget" data-alert-type="inline">
						<div class="form-result"></div>
						<?php echo form_open(base_url('addBook'), 'id="booking-registration" class="row mb-0" enctype="multipart/form-data"'); ?>
							<div class="form-process"></div>
							<div class="col-12 bottommargin">
								<div id="tab-reservation" class="list-group list-group-horizontal d-flex justify-content-center" role="tablist">
									<a class="list-group-item list-group-item-action w-auto mx-0 mx-sm-3 active" data-toggle="list" href="#tab-reservation-select-dates" role="tab">1. Select Dates</a>
									<a class="list-group-item list-group-item-action w-auto mx-0 mx-sm-3" data-toggle="list" href="#tab-reservation-select-room" role="tab">2. Select Room</a>
									<a class="list-group-item list-group-item-action w-auto mx-0 mx-sm-3" data-toggle="list" href="#tab-reservation-contact" role="tab">3. Contact Details</a>
								</div>
							</div>
							<div class="col-lg-9 mt-4 mt-lg-0">
								<div class="tab-content" id="nav-tabContent">
									<div class="tab-pane fade show active" id="tab-reservation-select-dates" role="tabpanel" aria-labelledby="tab-reservation-select-dates">
										<div class="row">
											<div class="input-daterange travel-date-group col-12 bottommargin-sm">
												<div class="row">
													<div class="col-md-6 col-6">
														<label for="booking-registration-check-in">Check In:</label>
														<input type="text" value="" class="sm-form-control required border-form-control text-left" id="booking-registration-check-in" name="check_in" placeholder="DD/MM/YYYY">
													</div>
													<div class="col-md-6 col-6">
														<label for="booking-registration-check-out">Check Out:</label>
														<input type="text" value="" class="sm-form-control required border-form-control text-left" id="booking-registration-check-out" name="check_out" placeholder="DD/MM/YYYY">
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="mb-4">
													<label>Select Cabins:</label>
													<select class="sm-form-control border-form-control required" name="cabins" id="booking-registration-rooms">
														<option value="" selected="selected"></option>
														<?php foreach($cabins as $cabin){?>
                    										<option value="<?php echo $cabin['title']?>" <?php echo set_select('cabins',$cabin['id'] ); ?>>
                    											<?php echo $cabin['title']?>
                    										</option>
                  										<?php }?>
													</select>
												</div>
											</div>
											<div class="col-6">
												<div class="mb-4">
													<label>Select Persons:</label>
													<select class="sm-form-control border-form-control required" name="persons" id="booking-registration-persons">
														<option value="" selected="selected"></option>
														<option value="1 Person">1 Person</option>
														<option value="2 Person">2 Persons</option>
													</select>
												</div>
											</div>
											<div class="col-12">
												<a href="#" class="button button-rounded button-light bg-white tab-action-btn-next text-dark float-right">Select Package</a>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="tab-reservation-select-room" role="tabpanel" aria-labelledby="tab-reservation-select-room">
										<div class="row">
											<div class="col-12">
												<div class="list-group btn-group-vertical btn-group-toggle mb-3" data-toggle="buttons">
													<?php foreach($packages as $package){?>
														<label class="btn list-group-item d-flex justify-content-between align-items-center lh-condensed">
															<span class="d-block">
																<input type="radio" name="package" data-item="Standard" id="booking-registration-<?php echo $package['id']?>" value="<?php echo $package['title']?>" data-toggle="<?php echo $package['price']?>">
																<span class="<?php echo $package['title']?>">
																	<?php echo $package['price']?>
																</span>
																<h4 class="my-0"><?php echo $package['title']?></h4>
																<small class="text-muted"><?php echo $package['location']?></small>
															</span>
															<h4 class="mb-0 fcolor"><?php echo $package['price']?></h4>
														</label>
													<?php }?>
													<input type="hidden" name="price" id="price" value="">
												</div>
											</div>
											<div class="col-12">
												<a href="#" class="button button-rounded button-light bg-white tab-action-btn-next text-dark float-right">Select Contact Details</a>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="tab-reservation-contact" role="tabpanel" aria-labelledby="tab-reservation-contact">
										<div class="row">
											<div class="col-6 mb-4">
												<label>First Name:</label>
												<input type="text" name="first_name" id="booking-registration-first-name" class="sm-form-control border-form-control required" value="" placeholder="Enter Your First Name">
											</div>
											<div class="col-6 mb-4">
												<label>Last Name:</label>
												<input type="text" name="last_name" id="booking-registration-last-name" class="sm-form-control border-form-control required" value="" placeholder="Enter Your Last Name">
											</div>
											<div class="col-12 mb-4">
												<label>Email Address:</label>
												<input type="email" name="email" id="booking-registration-email" class="sm-form-control border-form-control required" value="" placeholder="Enter Your Email Address">
											</div>
											<div class="col-12 mb-4">
												<label>Full  Address:</label>
												<input type="text" name="address" id="checkout-form-shipping-street" class="form-control border-form-control required" value="" placeholder="Enter Your Full Address">
											</div>
											<div class="col-12 mb-4">
												<label>Phone:</label><br>
												<input type="text" name="phone" id="checkout-form-shipping-phone" class="form-control border-form-control required" value="" placeholder="Enter Your Phone Number">
											</div>
											<div class="col-12">
            									<input type="submit" name="submit" class="button button-rounded button-light bg-white text-dark float-right submit" value="Book Now">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="list-group list-group-flush">
									<div class="list-group-item">
										<h5 class="mb-0">Duration: <span class="time-value font-weight-semibold fcolor ml-1">Choose Date</span></h5>
									</div>
									<div class="list-group-item">
										<h5 class="mb-0">Cabin: <span class="rooms-value font-weight-semibold fcolor ml-1"></span></h5>
									</div>
									<div class="list-group-item">
										<h5 class="mb-0">Persons: <span class="persons-value font-weight-semibold fcolor ml-1"></span></h5>
									</div>
									<div class="list-group-item">
										<h5 class="mb-0">Package: <span class="room-type-value font-weight-semibold fcolor ml-1"></span></h5>
									</div>
								</div>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- #content end -->