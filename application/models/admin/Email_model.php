<?php

	class Email_model extends CI_Model{

		public function get_emails($start,$length,$search=false,$order='',$col_name='',$filter_count='',$single=''){
		    $query = 'SELECT mails_queue.*, modules.name AS module_name, users.fullname
		        FROM mails_queue
		        LEFT JOIN modules ON mails_queue.module_id = modules.id
		        LEFT JOIN users ON mails_queue.uid = users.id
		    ';
		    if (($single['module_id'] !='' && $single['form_id'] !='') || $search) {             
	  	       	$query .=' WHERE';
	  	    }
	  	    if ($single['module_id'] !='' && $single['form_id'] !='') {
               	$query .=' module_id='.$single['module_id'].' AND form_id = '.$single['form_id'].'';
            }     
            if (($single['module_id'] !='' && $single['form_id'] !='') &&  $search) {
               	$query .=' AND';
            }      
	  	    if ($search) {
	  	       	$query .=' (type like "%'.$search.'%" OR modules.name like "%'.$search.'%" OR emails like "%'.$search.'%" OR form_id like "%'.$search.'%" OR users.fullname like "%'.$search.'%" OR message like "%'.$search.'%" OR sent_at like "%'.$search.'%" OR timestamp like "%'.$search.'%") ';
	  	    }
          	if ($filter_count == 'count') { 
      	     	$records = $this->db->query($query);
                return $records->num_rows();
	        }else{
	           	$query .='  ORDER BY '.$col_name.' '.$order.'';
      	     	$query .=' LIMIT '.$start.','.$length.'';
	           	$records = $this->db->query($query);
	           	return $records->result_array();
	       	}   
	   	}

        public function get_all_emails($single=''){
			if ($single['module_id'] !='' && $single['form_id'] !='') {
				return $this->db->get_where('mails_queue',array('module_id'=>$single['module_id'],'form_id'=>$single['form_id']))->result_array();
			}else{
			   	return $this->db->get('mails_queue')->result_array();
			}
		}  

		public function get_email($id){
			$this->db->select('mails_queue.*, modules.name AS module_name, users.fullname');
			$this->db->join('modules','mails_queue.module_id = modules.id','left');
			$this->db->join('users','mails_queue.uid = users.id','left');
			$this->db->where('mails_queue.id',$id);
			return $this->db->get('mails_queue')->result_array();
		}  

  	}

?>	