<?php

class Companies_model extends CI_Model{
	
	  	function __contruct(){
			parent::__construct;
	  	  }
		
		public function get_companies_ajax($start,$length,$search=false,$order,$col_name,$filter_count){

	            $query = 'SELECT companies.* FROM companies';
		       if ($search) {
		       	$query .= ' WHERE (company_name like "'.$search.'%" OR rank like "'.$search.'%")';
                }

                $query .= ' ORDER BY '.$col_name.' '.$order.'';

          	   if ($filter_count == 'count') { 

      	     	 $records = $this->db->query($query);
                 return $records->num_rows();

	           }else{

      	     	 $query .=' LIMIT '.$start.','.$length.'';
	             $records = $this->db->query($query);
	             return $records->result_array();

	            }   

       }


	    public function get_all_companies(){
			
			return $this->db->get('companies')->result_array();

	    }  

	    public function get_company($id){

			return $this->db->get_where('companies',array('id'=>$id))->row_array();

	    }  

	    public function add_company($data){

				$this->db->insert('companies', $data);

				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

			 }  


		public function update_company($dep_id,$data){

			 	$this->db->where('companies.id', $dep_id);
			 				
				$this->db->update('companies', $data);
				
				 if ($this->db->affected_rows() >= 0) {
	                  
	                   return $this->db->affected_rows();
	              } 
	        
	          }	 	 

}
?>
