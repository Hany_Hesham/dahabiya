<?php

  class Signature_model extends CI_Model{

    public function get_signatures($t_attr, $custom_search='', $filter_count){
      $query = 'SELECT sign_types.*, modules.name AS modules_name, hotels.hotel_name, departments.dep_name
        FROM sign_types
        LEFT JOIN modules ON sign_types.module_id = modules.id
        LEFT JOIN hotels ON sign_types.hotel_id = hotels.id
        LEFT JOIN departments ON sign_types.dep_code = departments.id
        WHERE sign_types.deleted = 0';
      if ($t_attr['search']) {
        $query .= '  AND (sign_types.id like "%'.$t_attr['search'].'%" OR sign_types.name like "%'.$t_attr['search'].'%" OR sign_types.special like "%'.$t_attr['search'].'%" OR sign_types.limitation like "%'.$t_attr['search'].'%" OR modules.name like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%")';
      }
      $rows = $this->db->query($query)->result_array();
      $signatureids   = array();
      foreach ($rows as $row) {
        array_push($signatureids, $row['id']);
      }
      $this->db->select('sign_types.*, modules.name AS modules_name, hotels.hotel_name, departments.dep_name');
      $this->db->join('modules','sign_types.module_id = modules.id','left');
      $this->db->join('hotels','sign_types.hotel_id = hotels.id','left');
      $this->db->join('departments','sign_types.dep_code = departments.id','left');
      if (count($signatureids) != 0) {
        $this->db->group_start();
        $signature_ids_chunk = array_chunk($signatureids,25);
        foreach($signature_ids_chunk as $signature_ids) {
          $this->db->or_where_in('sign_types.id',$signature_ids);
        }
        $this->db->group_end();
      }else{
        return array();
      }
      $this->db->order_by($t_attr['col_name'],$t_attr['order']);
      if ($filter_count == 'count') {
        return $this->db->get("sign_types")->num_rows();
      }else{
        if ($t_attr['length'] == -1) {$t_attr['length'] = '10000000000000000';}
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("sign_types")->result_array();
      }
    }

    function get_all_signature(){
      return $this->db->get_where('sign_types',array('sign_types.deleted'=>0))->result_array();
    }       

    public function add_signature($data){
      $this->db->insert('sign_types', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }  

    public function add_signature_role($data){
      $this->db->insert('sign_role', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }  

    function get_signature($signature_id) {
      $this->db->select('signature.*, status.status_name, status.status_color, users.fullname, user_groups.name AS role_name');
      $this->db->join('users','signature.uid = users.id','left');
      $this->db->join('user_groups','signature.role_id = user_groups.id','left');
      $this->db->join('status','signature.status = status.id','left');
      $this->db->where(array('signature.deleted !='=>1,'signature.id'=>$signature_id));   
      return $this->db->get('signature')->row_array();
    }

    function remove_item($id) {
      $this->db->update('signature', array('deleted'=> 1), "id = ".$id);
    }

    function get_signature_items($signature_id, $type) {
      $this->db->select('signature.*, meta_data.type_name AS role_first, meta_data1.type_name AS role_second, meta_data2.type_name AS role_third, meta_data3.type_name AS role_fourth, meta_data4.type_name AS role_fifth, meta_data5.type_name AS role_sixth, meta_data6.type_name AS role_seventh, meta_data7.type_name AS role_eighth, meta_data8.type_name AS role_ninth');
      $this->db->join('meta_data','signature.first = meta_data.id','left');
      $this->db->join('meta_data AS meta_data1','signature.second = meta_data1.id','left');
      $this->db->join('meta_data AS meta_data2','signature.third = meta_data2.id','left');
      $this->db->join('meta_data AS meta_data3','signature.fourth = meta_data3.id','left');
      $this->db->join('meta_data AS meta_data4','signature.fifth = meta_data4.id','left');
      $this->db->join('meta_data AS meta_data5','signature.sixth = meta_data5.id','left');
      $this->db->join('meta_data AS meta_data6','signature.seventh = meta_data6.id','left');
      $this->db->join('meta_data AS meta_data7','signature.eighth = meta_data7.id','left');
      $this->db->join('meta_data AS meta_data8','signature.ninth = meta_data8.id','left');
      $this->db->where(array('signature.deleted !='=>1,'signature.signature_id'=>$signature_id,'signature.type_id'=>$type));
      return $this->db->get('signature')->row_array();
    }

    function edit_signature($data, $signature_id) {
      $this->db->where('signature.id', $signature_id);   
      $this->db->update('signature', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }

    function get_signature_item($item_id) {
      $this->db->select('signature.*');
      $this->db->where(array('signature.deleted !='=>1,'signature.id'=>$item_id));
      return $this->db->get('signature')->row_array();
    }

    function edit_signature_item($data, $item_id) {
      $this->db->where('signature.id', $item_id);   
      $this->db->update('signature', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }

  }

?>