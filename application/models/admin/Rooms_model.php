<?php

	class Rooms_model extends CI_Model{

        public function get_employe_data($hid, $clock_id){
	        $configured_db = switch_mssql_db($hid);
	        $DB = $this->load->database($configured_db, TRUE);
	        if ($DB->conn_id) {
        		$DB->select('Employees.EmployeeCode, Employees.Name, Employees.Arabic_Name, Employees.Sex, Employees.BirthDate, Employees.LocationCode, Employees.SectionCode, Sections.Name as section_name, Employees.DepartmentCode, Departments.Name as department_name, Employees.PositionCode, Positions.Name as position_name, Employees.DivisionCode, Divisions.Name as division_name, Employees.LevelCode, Employees.HiringDate, Employees.ContractExpireDate, Employees.ArabicFullAddress, Employees.Mobile, VacationRequest.CurrentBalance,Employees.BasicSalary');
            	$DB->join('Sections', 'Sections.SectionCode = Employees.SectionCode', 'left');
            	$DB->join('Departments', 'Departments.DepartmentCode = Employees.DepartmentCode', 'left');
            	$DB->join('Positions', 'Positions.PositionCode = Employees.PositionCode', 'left');
            	$DB->join('Divisions', 'Divisions.DivisionCode = Employees.DivisionCode', 'left');
            	$DB->join('VacationRequest', 'VacationRequest.EmployeeCode = Employees.EmployeeCode');
            	$DB->where('Employees.EmployeeCode', $clock_id);
            	$DB->order_by('VacationRequest.Serial', 'DESC');
            	$DB->limit('1');
            	$employees = $DB->get('Employees')->row_array();
            	return $employees;
            }
	    }

	    public function get_data($hid, $agent, $type = FALSE){
	        $configured_db = switch_oracle_db($hid);
	        $DB = $this->load->database($configured_db, TRUE);
	        if ($DB->conn_id) {
	        	$DB->select('YRES_EXPDEPTIME, YRES_EXPARRTIME, NAME, YDET_ADULTNO, TOTALKIDS, YRMS_SHORTDESC, XCOU_LONGDESC, YCAT_SHORTDESC, YCAT_LONGDESC, CREATE_DATE, YRES_SALEDATE, TRAVELAGENT_NAME');
	            $DB->where('INHOUSE', 1);
	            if ($type) {
	            	if ($type == 'agent') {
		            	$DB->where('TRAVELAGENT_NAME', $agent);
	            	}elseif ($type == 'room') {
		            	$DB->where('YRMS_SHORTDESC', $agent);
	            	}
	            }
	            $rooms = $DB->get('ACT_REP_YRES_INFOS')->result_array();    
	            return $rooms;
	        }
	    }

	    /*public function get_data($data) {
	        $configured_db = switch_oracle_db($data['hid']);
	        $DB = $this->load->database($configured_db, TRUE);
	        if ($DB->conn_id) {
	            $DB->select('NAME, EMAIL, TELEPHONE, YRES_EXPDEPTIME, YRES_EXPARRTIME, CREATE_DATE, XCID_BIRTHTIME, YDET_ADULTNO, TOTALKIDS, TRAVELAGENT_NAME, TRAVELAGENT_ID, COMPANY_NAME, SOURCE_LONGDESC, YCAT_LONGDESC, YCAT_SHORTDESC, YRMS_SHORTDESC, XCOU_LONGDESC, YRES_ID, YRES_CALCSTATUS, YRCH_SHORTDESC, XCMA_SHORTDESC, WLAN_ID, YBLH_SHORTDESC, YRES_XCMS_ID, VIP_SHORTDESC, YRES_CRSRESNR');
	            $DB->join('V8_MAILING_CUSTOMERS_COMM', 'V8_MAILING_CUSTOMERS_COMM.XCMS_ID = ACT_REP_YRES_INFOS.YRES_XCMS_ID');
	            $DB->where('INHOUSE', 1);
	            $DB->or_where('CHECKED_OUT_TODAY', 1);
	            $DB->or_where("TO_CHAR(YRES_EXPDEPTIME, 'YYYY-MM-DD') =", $data['from_date']);
	            $DB->or_where("TO_CHAR(YRES_EXPARRTIME, 'YYYY-MM-DD') BETWEEN '". $data['from_date'] ."' AND '". $data['to_date'] ."' ");
	            $DB->or_where('CREATE_DATE', $data['from_date']);
	            $reservations = $DB->get('ACT_REP_YRES_INFOS')->result_array();
	            return $reservations;
	        }
	    }

	    public function get_data($hid){
	        $configured_db = switch_oracle_db($hid);
	        $DB = $this->load->database($configured_db, TRUE);
	        if ($DB->conn_id) {
	            $DB->select('YRES_EXPDEPTIME, YRES_EXPARRTIME, NAME, YDET_ADULTNO, TOTALKIDS, YRMS_SHORTDESC, XCOU_LONGDESC, YCAT_SHORTDESC, YCAT_LONGDESC, CREATE_DATE, YRES_SALEDATE, TRAVELAGENT_NAME');
	            $DB->where('INHOUSE', 1);
	            $rooms = $DB->get('ACT_REP_YRES_INFOS')->result_array();    
	            return $rooms;
	        }
	    }*/

    	public function get_opera_data($hid, $agent, $type = FALSE){
	        $configured_db = switch_oracle_db($hid);
	        $DB = $this->load->database($configured_db, TRUE);
	        if ($DB->conn_id) {
	        	$DB->select('nr.DEPARTURE, nr.ARRIVAL, nr.GUEST_NAME, nr.ADULTS, nr.CHILDREN, nr.ROOM, nr.COUNTRY_DESC, nr.INSERT_DATE, nr.ROOM_CATEGORY_LABEL, nr.TRAVEL_AGENT_NAME');
	            $DB->where('nr.RESV_STATUS', 'CHECKED IN');
	            if ($type) {
	            	if ($type == 'agent') {
		            	$DB->where('nr.TRAVEL_AGENT_NAME', $agent);
	            	}elseif ($type == 'room') {
		            	$DB->where('nr.ROOM', $agent);
	            	}
	            }
	            $rooms = $DB->get('NAME_RESERVATION nr')->result_array();  
	            return $rooms;
	        }
	    }

    	public function searchRoom($data){
    		if (isset($data['agent'])) {
    			$hid 		= $data['hid'];
    			$searcher 	= $data['agent'];
    			$type 		= 'agent';
    		}elseif (isset($data['room'])) {
    			$hid 		= $data['hid'];
    			$searcher 	= $data['room'];
    			$type 		= 'room';
    		}else{
    			$hid 		= $data;
    			$searcher 	= FALSE;
    			$type 		= FALSE;
    		}
	        if ($hid == '53') {
	           $rooms = $this->get_opera_data($hid, $searcher, $type);
	        }else{
	           $rooms = $this->get_data($hid, $searcher, $type);
	        }
            $output_array = array();
            foreach ($rooms as $room) {
	            $roomz = array();
	            if ($hid == '53') {
            		if (isset($room['ROOM']) && $room['ROOM'] != null) {
	                    $roomz['departure']   	= date("Y-m-d", strtotime($room['DEPARTURE']));
	                    $roomz['arrival']     	= date("Y-m-d", strtotime($room['ARRIVAL']));
	                    $roomz['guest'] 		= $room['GUEST_NAME'];
	                    $roomz['pax'] 			= $room['ADULTS'];
	                    $roomz['child'] 		= $room['CHILDREN'];
	                    $roomz['room'] 			= $room['ROOM'];
	                    $roomz['nationality'] 	= $room['COUNTRY_DESC'];
	                    $roomz['create_date'] 	= date("Y-m-d", strtotime($room['INSERT_DATE']));
	                    $roomz['travel_agent'] 	= $room['TRAVEL_AGENT_NAME'];
	                    $roomz['room_category'] = $room['ROOM_CATEGORY_LABEL'];
	            		$output_array[] 		= $roomz;
	                }
	            }else{
            		if (isset($room['YRMS_SHORTDESC']) && $room['YRMS_SHORTDESC'] != null) {
	                    $roomz['departure']    	= date("Y-m-d", strtotime($room['YRES_EXPDEPTIME']));
	                    $roomz['arrival']     	= date("Y-m-d", strtotime($room['YRES_EXPARRTIME']));
	                    $roomz['guest'] 		= $room['NAME'];
	                    $roomz['pax'] 			= $room['YDET_ADULTNO'];
	                    $roomz['child'] 		= $room['TOTALKIDS'];
	                    $roomz['room'] 			= $room['YRMS_SHORTDESC'];
	                    $roomz['nationality'] 	= $room['XCOU_LONGDESC'];
	                    $roomz['create_date'] 	= date("Y-m-d", strtotime($room['YRES_SALEDATE']));
	                    $roomz['travel_agent'] 	= $room['TRAVELAGENT_NAME'];
	                    $roomz['room_category'] = $room['YCAT_LONGDESC'].'('.$room['YCAT_SHORTDESC'].')';
			            $output_array[] 		= $roomz;
	                }
	            }
	            if ($type == 'room') {
	            	return $roomz;
	            }
            }
	        return $output_array;
        }

        /*
			##INTEFACE DATA COLUMNS##

			public function suite8($hid){
		        $configured_db = switch_oracle_db($hid);
		        $DB = $this->load->database($configured_db, TRUE);
		        if ($DB->conn_id) {
		        	$DB->select('NAME, EMAIL, TELEPHONE, YRES_EXPDEPTIME, YRES_EXPARRTIME, CREATE_DATE, XCID_BIRTHTIME, YDET_ADULTNO, TOTALKIDS, TRAVELAGENT_NAME, COMPANY_NAME, SOURCE_LONGDESC, YCAT_LONGDESC, YCAT_SHORTDESC, YRMS_SHORTDESC, XCOU_LONGDESC, YRES_ID, YRES_CALCSTATUS, YRCH_SHORTDESC, XCMA_SHORTDESC, WLAN_WEBCULTURE, PACKAGES_LONG, YBLH_SHORTDESC, YRES_XCMS_ID, VIP_SHORTDESC, YRES_CRSRESNR');
		        	 $DB->join('V8_MAILING_CUSTOMERS_COMM', 'V8_MAILING_CUSTOMERS_COMM.XCMS_ID = ACT_REP_YRES_INFOS.YRES_XCMS_ID');
		            $DB->join('WLAN', 'WLAN.WLAN_ID = ACT_REP_YRES_INFOS.WLAN_ID');
		            $DB->where('INHOUSE', 1);
		            $rooms = $DB->get('ACT_REP_YRES_INFOS')->result_array();    
		            return $rooms;
		        }
		    }

	    	public function opera($hid){
		        $configured_db = switch_oracle_db($hid);
		        $DB = $this->load->database($configured_db, TRUE);
		        if ($DB->conn_id) {
		        	$DB->select('nr.TITLE, nr.GUEST_FIRST_NAME, nr.GUEST_NAME, nr.EMAIL, nr.PHONE_NO, nr.INSERT_DATE, nr.ARRIVAL, nr.DEPARTURE, nr.ADULTS, nr.CHILDREN, nr.BIRTH_DATE, rn.CUSTOM_REFERENCE, srmv.PMS_ROOM_CAT_DESC, nr.TRAVEL_AGENT_NAME, nr.COMPANY_NAME, nr.SOURCE_NAME, nr.ROOM_CATEGORY_LABEL, nr.ROOM, nr.COUNTRY_DESC, nr.CONFIRMATION_NO, nr.RESV_STATUS, nr.RESV_NAME_ID, nr.GUEST_NAME_ID, nr.MARKET_CODE, nr.RATE_CODE, nr.BLOCK_CODE, nr.VIP_STATUS, nr.LANGUAGE_NAME');
		        	$DB->join('RESERVATION_NAME rn', 'rn.RESV_NAME_ID = nr.RESV_NAME_ID');
		            $DB->join('SC_ROOMTYPE_MAP_VIEW srmv', 'srmv.PMS_ROOM_CATEGORY = nr.ROOM_CATEGORY', 'left');
		            $DB->where('nr.RESV_STATUS', 'CHECKED IN');
		            $rooms = $DB->get('NAME_RESERVATION nr')->result_array();  
		            return $rooms;
		        }
		    }
        */

	}

?>	