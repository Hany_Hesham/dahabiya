<?php

	class RocketChat_model extends CI_Model{

  		function __contruct(){
			parent::__construct;
			$this->load->database();
		}

		function getForms() {
			$this->db->select('data_signature.*');
			$query = $this->db->get('data_signature');
			return $query->result_array();
		}

		function getForm($id) {
			$this->db->select('data_signature.*');
			$this->db->where('data_signature.id', $id);
			$query = $this->db->get('data_signature');
			return $query->row_array();
		}

		function getFormData($database, $date, $deleted = FALSE) {
			$this->db->select($database.'.*');
			$this->db->where($database.'.'.$date.'>=', '2018-01-01');
			if ($deleted) {
				$this->db->where($database.'.deleted', 0);	
			}
			$query = $this->db->get($database);
			return $query->result_array();
		}

		function getRejectForms($database, $formId, $variable, $id, $change = FALSE) {
			$this->db->select($database.'.*');
			$this->db->where($database.'.'.$variable, $id);
			if ($formId == 1 || $formId == 4) {
				$this->db->where($database.'.changed', $change);
			}
			$this->db->where($database.'.reject', 1);
			$query = $this->db->get($database);
			return $query->num_rows();
		}

		function getSignForm($database, $formId, $variable, $id, $change = FALSE) {
			$this->db->select($database.'.*');
			$this->db->where($database.'.'.$variable, $id);
			if ($formId == 1 || $formId == 4) {
				$this->db->where($database.'.changed', $change);
			}
			if ($formId == 1 || $formId == 3 || $formId == 4 || $formId == 8) {
				$this->db->where($database.'.user_id IS NULL', NULL);
			}else{
				$this->db->where($database.'.user_id', 0);
			}
			$this->db->order_by($database.'.rank', 'ASC');
			$query = $this->db->get($database);
			return $query->result_array();
		}

		function getSignAsset($database, $variable, $signVariable, $id) {
			$this->db->select($database.'.*');
			$this->db->where($database.'.'.$variable, $id);
			$this->db->where($database.'.'.$signVariable, 0);
			$this->db->where($database.'.inv_dpt_mgr_id !=', 0);
			$this->db->where($database.'.issue_date >=', '2018-07-01');
			$query = $this->db->get($database);
			return $query->num_rows();
		}

		function getSunriseOwning($id, $change, $type) {
			$this->db->select('owning_signatures.*');
			$this->db->where('owning_signatures.project_id', $id);
			$this->db->where('owning_signatures.changed', $change);
			$this->db->where('owning_signatures.type', $type);
			$this->db->where('owning_signatures.user_id', NULL);
			$this->db->order_by('owning_signatures.rank', 'DESC');
			$query = $this->db->get('owning_signatures');
			return $query->result_array();
		}

		function getOwning($id, $type) {
			$this->db->select('company_owning_signature.*');
			$this->db->where('company_owning_signature.project_id', $id);
			$this->db->where('company_owning_signature.type', $type);
			$this->db->where('company_owning_signature.user_id', NULL);
			$this->db->order_by('company_owning_signature.rank', 'DESC');
			$query = $this->db->get('company_owning_signature');
			return $query->result_array();
		}

		function addMessageId($form, $id, $type, $message_id) {
		    $data = array('form_type_id' => $form, 'form_id' => $id, 'type' => $type, 'message_id' => $message_id);
		    $this->db->insert('data_signature_message', $data);
			return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		}

		function getMessageId($id, $form) {
			$this->db->select('data_signature_message.*, data_signature.name as formName');
			$this->db->join('data_signature', 'data_signature_message.form_type_id = data_signature.id','left');
			$this->db->where('data_signature_message.form_id', $id);
			$this->db->where('data_signature_message.form_type_id', $form);
			$query = $this->db->get('data_signature_message');
			return $query->row_array();
		}

		function deleteMessageId($message_id) {
		    $this->db->where('message_id', $message_id);
			$this->db->delete('data_signature_message');
		}

	}

?>