<?php

	class Test_model extends CI_Model {

		function __contruct(){
			parent::__construct;
	  	}

	  	public function update_roles() {
		    $module_data = $this->db->get('users')->result_array();
		    foreach ($module_data as $data) {
		    	$this->db->where('user_hotels.user_id', $data['id']);
		        $this->db->update('user_hotels', array('role_id' => '["'.$data['role_id'].'"]'));     
		    }
		}

		public function updateDeletForms($table, $year){
			$statusArray = array(2, 3);
			$this->db->select(''.$table.'.id');
			$this->db->like(''.$table.'.timestamp', $year);
			$this->db->where_not_in(''.$table.'.status', $statusArray);
			$this->db->where(''.$table.'.deleted', 0);
			$updated = $this->db->get(''.$table.'')->result_array();
            if ($updated) {
                $this->db->like(''.$table.'.timestamp', $year);
                $this->db->where_not_in(''.$table.'.status', $statusArray);
                $this->db->where(''.$table.'.deleted', 0);
    			$this->db->update(''.$table.'', array('deleted' => 1));
            }
			return $updated;
		}

		public function insertChanges($data){
			$this->db->insert('hideoldmodules', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		} 

		public function update_deps() {
		    $module_data = $this->db->get('users')->result_array();
		    foreach ($module_data as $data) {
		        $this->db->select('*');
		    	$this->db->where('departments.role_id', $data['role_id']);
		    	$deps = $this->db->get('departments')->result_array();
		    	if ($deps) {
		    		$departments=array();
			    	foreach ($deps as $dep) {
			    		$departments[]=$dep['id'];
			    	}
		            $this->db->where('users.id', $data['id']);
		        	$this->db->update('users', array('department' => json_encode($departments)));     
		    	}
		    }
		}

		public function update_role_id($table,$module_id) {
        	$module_data = $this->db->get(''.$table.'')->result_array();
        	foreach ($module_data as $data) {
            	$this->db->select('*');
            	$this->db->where('signatures.module_id', $module_id);
                $this->db->where('signatures.form_id', $data['id']);
                $this->db->where('signatures.deleted', 0);
            	$this->db->where('signatures.user_id IS NULL');
            	$this->db->order_by('signatures.rank', 'ASC');
            	$this->db->limit('1');     
            	$signatures = $this->db->get('signatures')->row();
        		if(!$signatures){
                	$this->db->select('*');
               		$this->db->where('signatures.module_id', $module_id);
                	$this->db->where('signatures.form_id', $data['id']);
                	$this->db->where('signatures.user_id IS NOT NULL');
                	$this->db->order_by('signatures.rank', 'DESC');
                	$this->db->limit('1');     
                	$signaturess = $this->db->get('signatures')->row();
                	if ($signaturess) {
                		$this->db->where('id', $data['id']);
                		$this->db->update(''.$table.'', array('role_id' => $signaturess->role_id, 'status' => 1));
                	}
        		}else{
            		$this->db->where('id', $data['id']);
                	$this->db->update(''.$table.'', array('role_id' => $signatures->role_id, 'status' => 1));
        		}       
       		}
    	}

    	
	  	public function update_reject_status($table,$module_id) {
		    $module_data = $this->db->get(''.$table.'')->result_array();
		    foreach ($module_data as $data) {
		        $this->db->select('*');
		        $this->db->where('signatures.module_id', $module_id);
                $this->db->where('signatures.deleted', 0);
		        $this->db->where(array('signatures.form_id'=>$data['id'],'signatures.reject'=>1));
		        $this->db->limit('1');     
		        $signatures = $this->db->get('signatures')->row();
		        if($signatures){
		            $this->db->where(''.$table.'.id', $data['id']);
		            $this->db->update(''.$table.'', array('role_id' => 0, 'status' => 3));
		        } else {
		            continue;
		        }      
		    }
		}


    	public function update_approved_status($table,$module_id) {
        	$module_data = $this->db->get(''.$table.'')->result_array();
        	foreach ($module_data as $data) {
            	$this->db->select('*');
                $this->db->where('signatures.module_id', $module_id);
                $this->db->where('signatures.deleted', 0);
                $this->db->where(array('signatures.form_id'=>$data['id'],'signatures.reject'=>0));
            	$this->db->order_by('signatures.rank', 'DESC');
            	$this->db->limit('1');     
            	$signatures = $this->db->get('signatures')->row();
        		if($signatures && $signatures->user_id){
                	$this->db->where('id', $data['id']);
                	$this->db->update(''.$table.'', array('role_id' => 0, 'status' => 2));
       			} else {
        			continue;
        		}      
        	}
    	}

        public function copy_files($table, $field, $module_dir){
            $array = array();
            $this->db->select('*');
            $files = $this->db->get(''.$table.'')->result_array();
            foreach ($files as $key => $file) {
                if (isset($files[$key][$field]) && ($files[$key][$field]) && ($files[$key][$field] != '')) {
                    $dir_name = '/home/sunrise/public_html/newsign/assets/uploads/'.$module_dir;
                    $file_name = '/home/sunrise/public_html/newsign/assets/uploads/'.$module_dir.'/'.$files[$key][$field];
                    $copy_name = '/home/sunrise/public_html/e-signature/assets/uploads/files/'.$files[$key][$field];
                    if (file_exists($copy_name) && file_exists($dir_name) && !file_exists($file_name)) {
                        copy($copy_name, $file_name);
                        $array[] = '[old:'.$copy_name.', new:'.$file_name.']';
                    }
                }
            }
            // die(print_r($array));
            return $array;
        }

        public function copy_allfiles($module_id, $module_dir){
            $array = array();
            $this->db->select('*');
            $this->db->where('module_id', $module_id);
            $files = $this->db->get('files')->result_array();
            foreach ($files as $key => $file) {
                if (isset($files[$key]['file_name']) && ($files[$key]['file_name']) && ($files[$key]['file_name'] != '')) {
                    $dir_name = '/home/sunrise/public_html/newsign/assets/uploads/'.$module_dir;
                    $file_name = '/home/sunrise/public_html/newsign/assets/uploads/'.$module_dir.'/'.$files[$key]['file_name'];
                    $copy_name = '/home/sunrise/public_html/e-signature/assets/uploads/files/'.$files[$key]['file_name'];
                    if (file_exists($copy_name) && file_exists($dir_name) && !file_exists($file_name)) {
                        copy($copy_name, $file_name);
                        $array[] = '[old:'.$copy_name.', new:'.$file_name.']';
                    }
                }
            }
            // die(print_r($array));
            return $array;
        }

  	}

?>	