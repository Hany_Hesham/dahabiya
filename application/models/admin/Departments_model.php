<?php

class Departments_model extends CI_Model{
	
	  	function __contruct(){
			parent::__construct;
	  	  }
		
		public function get_departments_ajax($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
		       if ($search) {

		    	         $query = 'SELECT departments.* FROM departments
			  	    	        WHERE  
			  	    	        (dep_name like "'.$search.'%" OR code like "'.$search.'%" OR rank like "'.$search.'%") 
			  	    	        ORDER BY '.$col_name.' '.$order.'';

		          	if ($filter_count == 'count') { 

		      	     	 $records = $this->db->query($query);
		                 return $records->num_rows();

			           }else{

		      	     	 $query .=' LIMIT '.$start.','.$length.'';
			             $records = $this->db->query($query);
			             return $records->result_array();

			            }   

		          }elseif(!$search){

		          		 $this->db->select('departments.*');
	                     $this->db->order_by($col_name,$order);
	              
	              if ($filter_count == 'count') {
	    
		                 return $this->db
		              	 ->get("departments")
		                 ->num_rows();
	              
	              }else{

			             return $this->db
			             ->limit($length,$start)
			             ->get("departments")
			             ->result_array();
	              	
	              }   
	          }
	    }


	    public function get_all_departments(){
			
			return $this->db->get('departments')->result_array();

	    }  

	    public function get_department($id){

			return $this->db->get_where('departments',array('id'=>$id))->row_array();

	    }  

	    public function get_department_by_devision($dev_id){

			return $this->db->get_where('departments',array('devision_id'=>$dev_id))->result_array();

	    }  

	    public function add_department($data){

				$this->db->insert('departments', $data);

				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

			 }  


		public function update_department($dep_id,$data){

			 	$this->db->where('departments.id', $dep_id);
			 				
				$this->db->update('departments', $data);
				
				 if ($this->db->affected_rows() >= 0) {
	                  
	                   return $this->db->affected_rows();
	              } 
	        
	          }	 	 

}
?>
