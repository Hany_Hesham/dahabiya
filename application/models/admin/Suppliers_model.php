<?php

	class Suppliers_model extends CI_Model{

		public function get_suppliers($start,$length,$search=false,$order,$col_name,$filter_count){
		       if ($search) {

		    	         $query = 'SELECT suppliers.*,hotels_group.hotel_group FROM suppliers
		    	                LEFT Join hotels_group ON suppliers.group_id = hotels_group.id
			  	    	        WHERE suppliers.deleted = 0 AND 
			  	    	        (supp_name like "'.$search.'%" OR phone like "'.$search.'%" OR address like "'.$search.'%"
			  	    	        OR contact like "'.$search.'%"  OR cont_email like "'.$search.'%" OR hotels_group.hotel_group LIKE "%'.$search.'%") 
			  	    	        ORDER BY '.$col_name.' '.$order.'';

		          	if ($filter_count == 'count') { 

		      	     	 $records = $this->db->query($query);
		                 return $records->num_rows();

			           }else{

		      	     	 $query .=' LIMIT '.$start.','.$length.'';
			             $records = $this->db->query($query);
			             return $records->result_array();

			            }   

		          }elseif(!$search){

		          		 $this->db->select('suppliers.*,hotels_group.hotel_group');
		          		 $this->db->join('hotels_group','suppliers.group_id = hotels_group.id','LEFT');
	                     $this->db->where(array('suppliers.deleted'=> 0));
	                     $this->db->order_by($col_name,$order);
	              
	              if ($filter_count == 'count') {
	    
		                 return $this->db
		              	 ->get("suppliers")
		                 ->num_rows();
	              
	              }else{

			             return $this->db
			             ->limit($length,$start)
			             ->get("suppliers")
			             ->result_array();
	              	
	              }   
	          }
	    }


        public function get_all_suppliers(){
			
			return $this->db->get_where('suppliers',array('deleted'=>0))->result_array();

		  }  


		public function add_supplier($data){

			$this->db->insert('suppliers', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		 }  

		public function get_supplier($supp_id){
              $this->db->select('suppliers.*,hotels_group.hotel_group');
              $this->db->join('hotels_group','suppliers.group_id = hotels_group.id','left');
              $this->db->where('suppliers.id',$supp_id);
	          return $this->db->get('suppliers')->row_array();
          	
           }  


        public function get_supplier_transactions($supp_id,$start,$length,$search=false,$order,$col_name,$filter_count){
		         $query = 'SELECT delivery_items.*,delivery.serial as delivery_serial,hotels.hotel_name,departments.dep_name,delivery.dep_code,group_items.item_name,group_items.serial,group_items.description,units.unit_name,suppliers.supp_name
							 FROM delivery_items
							 LEFT JOIN delivery ON delivery_items.delivery_id = delivery.id
							 LEFT JOIN hotels ON delivery.hid = hotels.id
							 LEFT JOIN departments ON delivery.dep_code = departments.code
							 LEFT JOIN group_items ON delivery_items.item_id = group_items.id
							 LEFT JOIN units ON delivery_items.unit_id = units.id
							 LEFT JOIN suppliers ON delivery.supplier_id = suppliers.id
							 WHERE delivery.supplier_id = '.$supp_id.' AND delivery_items.type != 4';
			  	    	        
			  	    if ($search) {
			  	    	$query .= ' AND (delivery.serial like "%'.$search.'%" OR group_items.item_name like "%'.$search.'%" OR group_items.serial like "%'.$search.'%" OR description like "%'.$search.'%" OR suppliers.supp_name like "%'.$search.'%" OR hotels.hotel_name like "%'.$search.'%")';
			  	       } 	

		          	if ($filter_count == 'count') { 
		      	     	 $records = $this->db->query($query);
		                 return $records->num_rows();
			           }else{
		      	     	 $query .=' ORDER BY '.$col_name.' '.$order.'';
		      	     	 $query .=' LIMIT '.$start.','.$length.'';
			             $records = $this->db->query($query);
			             return $records->result_array();

			            }   

	        }


        public function get_all_supplier_transactions($supp_id){
        	$this->db->select('delivery_items.*,delivery.supplier_id');
        	$this->db->join('delivery','delivery_items.delivery_id = delivery.id','LEFT');
            $this->db->where('delivery.supplier_id',$supp_id);
            return $this->db->get('delivery_items')->result_array();

		  }        

         
        public function update_supplier($supp_id,$data){

		 	$this->db->where('suppliers.id', $supp_id);
		 				
			$this->db->update('suppliers', $data);
			
			 if ($this->db->affected_rows() >= 0) {
                  
                   return $this->db->affected_rows();
              } 
        
          }	 


          public function search_suppliers($values){
             $query = 'SELECT suppliers.*,hotels_group.hotel_group
		          	   	FROM  suppliers
						LEFT JOIN hotels_group ON suppliers.group_id = hotels_group.id 
	  	    	        WHERE suppliers.deleted = 0  AND 
	  	    	        (supp_name like "'.trim($values['text']).'%" OR interface like "'.trim($values['text']).'%")';

             $query .=' LIMIT 20';
             
             $records = $this->db->query($query);
             
             return $records->result_array();

//AND (suppliers.group_id = 5 OR group_id = '.$values['group_id'].')
        }  
      
      




  }
?>	