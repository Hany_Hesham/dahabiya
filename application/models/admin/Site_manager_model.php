<?php

	class Site_manager_model extends CI_Model{
	
	    public function get_site_metaData($meta,$type){
			$this->db->order_by('site_meta_data.rank', 'ASC');
        	if ($type=='single') {
				return $this->db->get_where('site_meta_data',array('meta'=>$meta))->row_array();
        	}elseif($type=='multi'){
                return $this->db->get_where('site_meta_data',array('meta'=>$meta))->result_array();
        	}
        }    

        public function get_site_metaData_item($id,$meta){
		   	return $this->db->get_where('site_meta_data',array('id'=>$id,'meta'=>$meta))->row_array();
        } 

		public function get_site_metaData_byId($id){
			return $this->db->get_where('site_meta_data',array('id'=>$id))->row_array();
	    } 

        public function update_site_metaData_item($data,$item_id){
            $this->db->where('site_meta_data.id', $item_id);
		    $this->db->update('site_meta_data', $data);
			if ($this->db->affected_rows() >= 0) {
	            return $this->db->affected_rows();
	        } 
	    }	    

	    public function add_site_metaData_item($data){
          	$this->db->insert('site_meta_data', $data);
          	return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
        }  

// //------------------------------------------ Offers manager -----------------------------------------
//         public function get_offers(){
//           	$this->db->select('offers.*, activity.name AS activity_name, hotel.name AS hotel_name, sub_activity.name AS sub_name');
//           	$this->db->join('activity','offers.activity_id = activity.id','left');
//           	$this->db->join('hotel','offers.hotel_id = hotel.id','left');
//           	$this->db->join('sub_activity','offers.sub_id = sub_activity.id','left');
// 	        $this->db->where('offers.deleted', 0);
// 	      	$this->db->order_by('offers.timestamp','ASC');
// 		  	return $this->db->get('offers')->result_array();
// 	    } 

// 	    public function get_offer($id){
//           	$this->db->select('offers.*, activity.name AS activity_name, hotel.name AS hotel_name, sub_activity.name AS sub_name');
//           	$this->db->join('activity','offers.activity_id = activity.id','left');
//           	$this->db->join('hotel','offers.hotel_id = hotel.id','left');
//           	$this->db->join('sub_activity','offers.sub_id = sub_activity.id','left');
// 	        $this->db->where('offers.id', $id);
// 		  	return $this->db->get('offers')->row_array();
// 	    } 

// 	    public function add_offer($data){
//           	$this->db->insert('offers', $data);
//           	return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
//         }  

// 		public function update_offer($data,$offer_id){
//           	$this->db->where('offers.id', $offer_id);
// 		  	$this->db->update('offers', $data);
// 			if ($this->db->affected_rows() >= 0) {
// 	            return $this->db->affected_rows();
// 	        } 
// 	    }

// 	    public function get_activities(){
//           	$this->db->select('activity.*');
// 	        $this->db->where('activity.deleted', 0);
// 	      	$this->db->order_by('activity.id','ASC');
// 		  	return $this->db->get('activity')->result_array();
// 	    }

// 	    public function get_hotels(){
//           	$this->db->select('hotel.*');
// 	        $this->db->where('hotel.deleted', 0);
// 	      	$this->db->order_by('hotel.id','ASC');
// 		  	return $this->db->get('hotel')->result_array();
// 	    } 

// 	    public function get_sub_activities(){
//           	$this->db->select('sub_activity.*');
// 	        $this->db->where('sub_activity.deleted', 0);
// 	      	$this->db->order_by('sub_activity.id','ASC');
// 		  	return $this->db->get('sub_activity')->result_array();
	    // } 

//------------------------------------------ slider manager -----------------------------------------
	    	public function get_all_sliders(){
	      		$this->db->order_by('slider.rank','ASC');
		  	return $this->db->get('slider')->result_array();
	     	} 

	    	function get_slider_item($item_id) {
          		$this->db->select('slider.*');
          		$this->db->where(array('slider.id'=>$item_id));
          		return $this->db->get('slider')->row_array();
        	}     

		public function add_slider($data){
	          	$this->db->insert('slider', $data);
	          	return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
	        }  

		public function update_slider($data,$slider_id){
          		$this->db->where('slider.id', $slider_id);
		  	$this->db->update('slider', $data);
			if ($this->db->affected_rows() >= 0) {
	            		return $this->db->affected_rows();
	         	} 
	     	}	 
			
		/**
		 ****************** Competition manager **********************
		*/ 
		public function get_competitions($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT competitions.* FROM competitions';
			if ($search) {
				$query .=' WHERE competitions.title like "'.$search.'%" 
				OR competitions.description like "'.$search.'%" 
				OR competitions.type like "'.$search.'%" 
				OR competitions.start_date like "'.$search.'%" 
				OR competitions.end_date like "'.$search.'%" ';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				return $records->result_array();
			} 
        }  

		public function get_all_competitions(){
			return $this->db->get('competitions')->result_array();
		}

		public function get_all_competitions_active(){
			$this->db->order_by('rank', 'DESC');
			return $this->db->get('competitions')->result_array();
		}

		public function add_competition($data){
				$this->db->insert('competitions', $data);
				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
			}  

		public function update_competition($data,$competition_id){
				$this->db->where('competitions.id', $competition_id);
			$this->db->update('competitions', $data);
			if ($this->db->affected_rows() >= 0) {
						return $this->db->affected_rows();
				} 
			}
		/*
		*  booking manager section
		* 
		* 
		*/ 
		public function get_bookings($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT booking.*, rooms.room As room_title, rooms.price As room_price,
							rooms.currency AS room_currency, rooms.tax AS room_tax , 
							offers.title As offer_title, offers.price As offer_price,
							offers.currency AS offer_currency, offers.tax AS offer_tax,
							countries.country_name,site_meta_data.header, 
							site_meta_data.title AS booking_status, site_meta_data.paragraph AS status_color
				FROM booking
				LEFT JOIN rooms ON booking.room_id = rooms.id
				LEFT JOIN offers ON booking.offer_id = offers.id
				LEFT JOIN countries ON booking.country = countries.country_code
				LEFT JOIN site_meta_data ON booking.status = site_meta_data.link';
			if ($search) {
				$query .=' WHERE booking.first_name like "'.$search.'%"  
				OR booking.last_name like "'.$search.'%" 
				OR booking.code like "'.$search.'%" 
				OR booking.address like "'.$search.'%" 
				OR booking.city like "'.$search.'%" 
				OR booking.email like "'.$search.'%" 
				OR booking.phone like "'.$search.'%"
				OR booking.country like "'.$search.'%"
				OR booking.payment_method like "'.$search.'%"  
				OR countries.country_name like "'.$search.'%" 
				OR rooms.room like "'.$search.'%" 
				OR offers.title like "'.$search.'%" 
				OR site_meta_data.title like "'.$search.'%"';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				return $records->result_array();
			} 
        }  

		public function get_all_bookings(){
			return $this->db->get('booking')->result_array();
		}

		function get_booking($booking_id) {
			$this->db->select('booking.*');
			$this->db->where(array('booking.id'=>$booking_id));
			return $this->db->get('booking')->row_array();
	  }
	  
	  /**
		 ****************** Room manager **********************
		*/ 
		public function get_rooms($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT rooms.* , site_meta_data.title AS room_category
			          FROM rooms
					  LEFT JOIN  site_meta_data ON rooms.category = site_meta_data.id';
			if ($search) {
				$query .=' WHERE rooms.room like "'.$search.'%"
				OR site_meta_data.title like "'.$search.'%"';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				$rooms = $records->result_array();
				foreach($rooms as $key => $room){
					$rooms[$key]['files'] = $this->db->get_where('site_files',[
						'module_id'=> 15,
						'form_id' => $room['id'],
					])->result_array();
				}
				return $rooms;
			} 
        }  

		public function get_all_rooms(){
			return $this->db->get('rooms')->result_array();
		}

		public function add_room($data){
				$this->db->insert('rooms', $data);
				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
			}  

		public function update_room($data,$room_id){
			$this->db->where('rooms.id', $room_id);
			$this->db->update('rooms', $data);
			if ($this->db->affected_rows() >= 0) {
				return $this->db->affected_rows();
			} 
		}

		public function get_room($id){
            $this->db->select('rooms.*, , site_meta_data.header AS category_name, site_meta_data.paragraph AS category_class');
            $this->db->join('site_meta_data', 'rooms.category = site_meta_data.id','left');
            $this->db->order_by('rooms.rank','ASC');
            $room = $this->db->get_where('rooms',['rooms.id' => $id, 'disabled'=>0, 'deleted'=>0])->row_array();
            $room['files'] = $this->db->get_where('site_files',[
                    'module_id'=> 15,
                    'form_id' => $room['id'],
                ])->result_array();
            $room['features'] = $this->db->get_where('room_features',[
                    'room_id' => $room['id'],
                ])->result_array();    
            return $room;
        }

		public function get_room_feature($id)
		{
			return $this->db->get_where('room_features',['room_features.id' => $id])->row_array();
		}

		public function add_room_feature($data){
			$this->db->insert('room_features', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		}  

		public function update_room_feature($data,$winner_id){
			$this->db->where('room_features.id', $winner_id);
			$this->db->update('room_features', $data);
			if ($this->db->affected_rows() >= 0) {
				return $this->db->affected_rows();
			} 
		}
	     
		/**
		 ****************** Offers manager **********************
		*/ 
		public function get_offers($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT offers.* , rooms.room AS offer_room
			          FROM offers
					  LEFT JOIN  rooms ON offers.room_id = rooms.id';
			if ($search) {
				$query .=' WHERE rooms.room like "'.$search.'%"
				OR offers.title like "%'.$search.'%"';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				$offers = $records->result_array();
				return $offers;
			} 
        }  

		public function get_all_offers(){
			return $this->db->get('offers')->result_array();
		}

		public function add_offer($data){
				$this->db->insert('offers', $data);
				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
			}  

		public function update_offer($data,$offer_id){
			$this->db->where('offers.id', $offer_id);
			$this->db->update('offers', $data);
			if ($this->db->affected_rows() >= 0) {
				return $this->db->affected_rows();
			} 
		}

		public function get_offer($id){
            $this->db->select('offers.*, , rooms.room AS offer_room');
            $this->db->join('rooms', 'offers.room_id = rooms.id','left');
            $offer = $this->db->get_where('offers',['offers.id' => $id, 'offers.active'=> 1, 'offers.deleted'=>0])->row_array();
            $offer['features'] = $this->db->get_where('offer_features',[
                    'offer_id' => $offer['id'],
                ])->result_array();    
            return $offer;
        }

		public function get_offer_feature($id)
		{
			return $this->db->get_where('offer_features',['offer_features.id' => $id])->row_array();
		}

		public function add_offer_feature($data){
			$this->db->insert('offer_features', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		}  

		public function update_offer_feature($data,$winner_id){
			$this->db->where('offer_features.id', $winner_id);
			$this->db->update('offer_features', $data);
			if ($this->db->affected_rows() >= 0) {
				return $this->db->affected_rows();
			} 
		}
	
		/*
		*  results manager section
		* 
		* 
		*/ 
		public function get_results($competition_id, $start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT projects.*, competitions.title As competition_title, competitions.price As competition_price,
							competitions.currency, competitors.name AS first_name,competitors.middle_name,
							competitors.last_name, competitors.email,competitors.phone, 
							site_meta_data.header, site_meta_data.title AS projects_status, 
							site_meta_data.paragraph AS status_color
				FROM projects
				LEFT JOIN competitions ON projects.competition_id = competitions.id
				LEFT JOIN competitors ON projects.competitor_id = competitors.id
				LEFT JOIN site_meta_data ON projects.status = site_meta_data.link
				WHERE projects.competition_id = "'.$competition_id.'"';
			if ($search) {
				$query .=' AND (projects.title like "'.$search.'%"  
				OR competitions.title like "'.$search.'%" 
				OR competitors.name like "'.$search.'%" 
				OR competitors.middle_name like "'.$search.'%" 
				OR competitors.last_name like "'.$search.'%" 
				OR competitors.school like "'.$search.'%" 
				OR competitors.email like "'.$search.'%"  
				OR site_meta_data.title like "'.$search.'%")';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				return $records->result_array();
			} 
        }  

		public function get_all_results($competition_id){
			return $this->db->get_where('projects', ['competition_id' => $competition_id])->result_array();
		}

		public function update_project($data, $project_id){
			$this->db->where('projects.id', $project_id);
			$this->db->update('projects', $data);
			if ($this->db->affected_rows() >= 0) {
				return $this->db->affected_rows();
			} 
		}

		public function get_competition_projects($competition_id){
			$query = 'SELECT projects.*, competitions.title As competition_title, competitions.price As competition_price,
							competitions.currency, competitors.name AS first_name,competitors.middle_name,
							competitors.last_name, competitors.email,competitors.phone,competitors.school
							, competitors.age,site_meta_data.header, site_meta_data.title AS projects_status, 
							site_meta_data.paragraph AS status_color
				FROM projects
				LEFT JOIN competitions ON projects.competition_id = competitions.id
				LEFT JOIN competitors ON projects.competitor_id = competitors.id
				LEFT JOIN site_meta_data ON projects.status = site_meta_data.link
				WHERE projects.competition_id = "'.$competition_id.'"';
				$query .=' ORDER BY total_score DESC';
				$records = $this->db->query($query);
				return $records->result_array();
        }  
		

		/**
		 ****************** Competitors manager **********************
		*/ 
		public function get_competitors($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT competitors.* FROM competitors';
			if ($search) {
				$query .=' WHERE competitors.name like "'.$search.'%" 
				OR competitors.middle_name like "'.$search.'%" 
				OR competitors.last_name like "'.$search.'%" 
				OR competitors.email like "'.$search.'%" 
				OR competitors.phone like "'.$search.'%" 
				OR competitors.school like "'.$search.'%" 
				OR competitors.city like "'.$search.'%" 
				OR competitors.birth_date like "'.$search.'%" ';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				return $records->result_array();
			} 
        }  

		public function get_all_competitors()
		{
			return $this->db->get('competitors')->result_array();
		}

		public function add_competitor($data)
		{
			$this->db->insert('competitors', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		}  

		public function update_competitor($data,$competitor_id)
		{
			$this->db->where('competitors.id', $competitor_id);
			$this->db->update('competitors', $data);
			if ($this->db->affected_rows() >= 0) {
						return $this->db->affected_rows();
				} 
		}

		/**
		 ****************** Voters manager **********************
		*/ 
		public function get_voters($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT voters.* FROM voters';
			if ($search) {
				$query .=' WHERE voters.first_name like "'.$search.'%" 
				OR voters.last_name like "'.$search.'%" 
				OR voters.email like "'.$search.'%" 
				OR voters.oauth_provider like "'.$search.'%"';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				return $records->result_array();
			} 
        }  

		public function get_all_voters()
		{
			return $this->db->get('voters')->result_array();
		}

		/**
		 ****************** winners manager **********************
		*/ 
		public function get_winners($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
			$query = 'SELECT winners.*,competitions.title as competition_title, competitors.name, competitors.middle_name, competitors.last_name,
			competitors.email, competitors.profile_picture
			 FROM winners
			 LEFT JOIN competitors ON winners.competitor_id = competitors.id
			 LEFT JOIN competitions ON winners.competition_id = competitions.id ';
			if ($search) {
				$query .=' WHERE competitors.name like "'.$search.'%" 
				OR competitors.middle_name like "'.$search.'%" 
				OR competitors.last_name like "'.$search.'%" 
				OR competitors.email like "'.$search.'%"  
				OR competitions.title like "'.$search.'%" ';
				}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$col_name.' '.$order.'';
				$query .=' LIMIT '.$start.','.$length.'';
				$records = $this->db->query($query);
				return $records->result_array();
			} 
        }  

		public function get_all_winners(){
			return $this->db->get('winners')->result_array();
		}

		public function add_winner($data){
				$this->db->insert('winners', $data);
				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
			}  

		public function update_winner($data,$winner_id){
			$this->db->where('winners.id', $winner_id);
			$this->db->update('winners', $data);
			if ($this->db->affected_rows() >= 0) {
			    return $this->db->affected_rows();
			} 
		}

		/**
		 * about us
		 */
		public function update_about_us($data,$about_id)
		{
			$this->db->where('site_meta_data.id', $about_id);
			$this->db->update('site_meta_data', $data);
			if ($this->db->affected_rows() >= 0) {
						return $this->db->affected_rows();
				} 
		}

		/**
		 * partners
		 */
		public function add_partner($data)
		{
			$this->db->insert('site_meta_data', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		}  

		public function update_partner($data,$partner_id)
		{
			$this->db->where('site_meta_data.id', $partner_id);
			$this->db->update('site_meta_data', $data);
			if ($this->db->affected_rows() >= 0) {
						return $this->db->affected_rows();
				} 
		}

		public function get_partner($id){
			return $this->db->get_where('site_meta_data', ['id' => $id])->row_array();
		}
		


//------------------------------------------ package manager -----------------------------------------
	    // 	public function get_all_packages(){
	    //   		$this->db->order_by('packages.rank','ASC');
		//   	return $this->db->get('packages')->result_array();
	    //  	} 

	    // 	function get_package_item($item_id) {
        //   		$this->db->select('packages.*');
        //   		$this->db->where(array('packages.id'=>$item_id));
        //   		return $this->db->get('packages')->row_array();
        // 	}     

		// public function add_package($data){
	    //       	$this->db->insert('packages', $data);
	    //       	return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
	    //     }  

		// public function update_package($data,$slider_id){
        //   		$this->db->where('packages.id', $slider_id);
		//   	$this->db->update('packages', $data);
		// 	if ($this->db->affected_rows() >= 0) {
	    //         		return $this->db->affected_rows();
	    //      	} 
	    //  	}	
//------------------------------------------ gallary manager -----------------------------------------
	    	public function get_all_gallarys(){
				$this->db->select('gallary.*, site_meta_data.title AS gallery_category');
				$this->db->join('site_meta_data','gallary.categories = site_meta_data.id','left');
	      		$this->db->order_by('gallary.rank','ASC');
		  	    return $this->db->get('gallary')->result_array();
	     	} 

	    	function get_gallary_item($item_id) {
          		$this->db->select('gallary.*');
          		$this->db->where(array('gallary.id'=>$item_id));
          		return $this->db->get('gallary')->row_array();
        	}     

		    public function add_gallary($data){
	          	$this->db->insert('gallary', $data);
	          	return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
	        }  

		    public function update_gallary($data,$slider_id){
          		$this->db->where('gallary.id', $slider_id);
		  	$this->db->update('gallary', $data);
			if ($this->db->affected_rows() >= 0) {
	            		return $this->db->affected_rows();
	         	} 
	     	}	

			public function get_gallery_unique(){
				$this->db->select('header, points');
				$this->db->order_by('site_meta_data.rank','ASC');
				$this->db->where(array('meta'=>'gallery'));
				$this->db->group_by('site_meta_data.points');
			   return $this->db->get_where('site_meta_data')->result_array();
			}
			 
//------------------------------------------ event manager -----------------------------------------
	    	public function get_all_events(){
	      		$this->db->order_by('events.rank','ASC');
		  	return $this->db->get('events')->result_array();
	     	} 

	    	function get_event_item($item_id) {
          		$this->db->select('events.*');
          		$this->db->where(array('events.id'=>$item_id));
          		return $this->db->get('events')->row_array();
        	}     

		public function add_event($data){
	          	$this->db->insert('events', $data);
	          	return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
	        }  

		public function update_event($data,$event_id){
          		$this->db->where('events.id', $event_id);
		  	$this->db->update('events', $data);
			if ($this->db->affected_rows() >= 0) {
	            		return $this->db->affected_rows();
	         	} 
	     	}	
//---------------------------------------- about  ----------------------------------------------------

        public function update_about($data){
          $this->db->where('site_meta_data.meta','about');
		  $this->db->update('site_meta_data', $data);
			if ($this->db->affected_rows() >= 0) {
	            return $this->db->affected_rows();
	         } 
	     }	

//--------------------------------------- Categories and products -----------------------------------------

	    public function add_category($data){
          $this->db->insert('site_groups', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
         }  

		public function update_category($data,$cat_id){
          $this->db->where('site_groups.id', $cat_id);
		  $this->db->update('site_groups', $data);
			if ($this->db->affected_rows() >= 0) {
	            return $this->db->affected_rows();
	         } 
	     }	 

        public function get_categories(){
        	$this->db->order_by('rank','ASC');
            return $this->db->get_where('site_groups',array('deleted'=>0))->result_array();

         }  

        public function get_category($id){
            return $this->db->get_where('site_groups',array('id'=>$id,'deleted'=>0))->row_array();

         }  

        public function get_category_items($cat_id){
         	$this->db->order_by('site_group_items.rank','ASC');
            return $this->db->get_where('site_group_items',array('group_id'=>$cat_id,'deleted'=>0))->result_array();
         }

//---------------------products
        
        public function get_products($gid,$start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
	      $query = 'SELECT site_group_items.* FROM site_group_items
		    	        WHERE group_id = '.$gid.'';
          	if ($search) {
          		$query .='  AND (item_name like "'.$search.'%" OR serial like "'.$search.'%" OR company like "'.$search.'%" OR description like "'.$search.'%" )';
            	}

          	if ($filter_count == 'count') { 
      	     	 $records = $this->db->query($query);
                 return $records->num_rows();
	           }else{
      	     	 $query .=' ORDER BY '.$col_name.' '.$order.'';
      	     	 $query .=' LIMIT '.$start.','.$length.'';
	             $records = $this->db->query($query);
	             return $records->result_array();

	            } 
		  }  

        public function get_all_products($gid){
	        $this->db->where('site_group_items.deleted !=',1);
			$this->db->where(array('deleted'=>0,'site_group_items.group_id'=>$gid));
			return $this->db->get('site_group_items')->result_array();
		  }  

		public function add_product($data){
			$this->db->insert('site_group_items', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		 }  

        public function update_product($item_id,$data){
		 	$this->db->where('site_group_items.id', $item_id);			
			$this->db->update('site_group_items', $data);
			 if ($this->db->affected_rows() >= 0) {                  
                   return $this->db->affected_rows();
              } 
          }	  
                     
		public function get_product($item_id){
	    	$this->db->select('site_group_items.*');
	        $this->db->where('site_group_items.id',$item_id);
            return $this->db->get("site_group_items")->row_array();
           } 

        public function get_product_by($serial){
			 $by_serial = $this->db->get_where('site_group_items',array('serial' => $serial))->row_array();
             return  $by_serial;
           } 

//---------------------clients requests manager

        public function get_clients_req($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
	      $query = 'SELECT clients_mail_queue.*,site_meta_data.header,site_meta_data.title AS mail_status,site_meta_data.paragraph AS status_color 
	                FROM clients_mail_queue
	                LEFT JOIN site_meta_data ON clients_mail_queue.status = site_meta_data.id';
          	if ($search) {
          		$query .='  WHERE site_meta_data.title like "'.$search.'%" OR client_name like "'.$search.'%" OR client_name like "'.$search.'%" OR subject like "'.$search.'%"  OR message like "'.$search.'%"';
            	}

          	if ($filter_count == 'count') { 
      	     	 $records = $this->db->query($query);
                 return $records->num_rows();
	           }else{
      	     	 $query .=' ORDER BY '.$col_name.' '.$order.'';
      	     	 $query .=' LIMIT '.$start.','.$length.'';
	             $records = $this->db->query($query);
	             return $records->result_array();

	            } 
		  }  

        public function get_all_clients_req(){
			return $this->db->get('clients_mail_queue')->result_array();
		  }  

//-------------------- client request manager
         
	    public function update_client_mail($item_id,$data){
		 	$this->db->where('clients_mail_queue.id', $item_id);			
			$this->db->update('clients_mail_queue', $data);
			 if ($this->db->affected_rows() >= 0) {                  
	               return $this->db->affected_rows();
	          } 
	      }

//=================================== language manager module
        
        public function get_languages($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
	      $query = 'SELECT languages.* FROM languages';
          	if ($search) {
          		$query .=' WHERE  english like "%'.$search.'%" OR german like "%'.$search.'%" OR french like "%'.$search.'%" OR arabic like "%'.$search.'%"';
            	}
          	if ($filter_count == 'count') { 
      	     	 $records = $this->db->query($query);
                 return $records->num_rows();
	           }else{
      	     	 $query .=' ORDER BY '.$col_name.' '.$order.'';
      	     	 $query .=' LIMIT '.$start.','.$length.'';
	             $records = $this->db->query($query);
	             return $records->result_array();

	            } 
		  }  

        public function get_all_languages(){
			return $this->db->get('languages')->result_array();
		  }  

        public function update_language($item_id,$data){
		 	$this->db->where('languages.id', $item_id);			
			$this->db->update('languages', $data);
			 if ($this->db->affected_rows() >= 0) {                  
                   return $this->db->affected_rows();
              } 
          }

        public function add_language($data){
			$this->db->insert('languages', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		 }    	  
                     
		public function get_language($item_id){
	    	$this->db->select('languages.*');
	        $this->db->where('languages.id',$item_id);
            return $this->db->get("languages")->row_array();
           } 


}
?>
