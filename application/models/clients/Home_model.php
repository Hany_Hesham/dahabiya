<?php

	class Home_model extends CI_Model{

        public function get_sliders(){
            $this->db->order_by('slider.rank','ASC');
            return $this->db->get_where('slider',array('deleted'=>0))->result_array();  
        }

        public function get_gallarys(){
            $this->db->order_by('gallary.rank','ASC');
            return $this->db->get_where('gallary',array('deleted'=>0))->result_array();  
        }

        public function get_events(){
            $this->db->order_by('events.rank','ASC');
            return $this->db->get_where('events',array('deleted'=>0, 'active'=>1))->result_array();  
        }
        
        public function get_rooms(){
            $this->db->select('rooms.*, , site_meta_data.header AS category_name, site_meta_data.paragraph AS category_class');
            $this->db->join('site_meta_data', 'rooms.category = site_meta_data.id','left');
            $this->db->order_by('rooms.rank','ASC');
            $rooms = $this->db->get_where('rooms',['disabled'=>0, 'deleted'=>0])->result_array();
            foreach($rooms as $key => $room){
                $rooms[$key]['files'] = $this->db->get_where('site_files',[
                    'module_id'=> 15,
                    'form_id' => $room['id'],
                ])->result_array();
            }
            return $rooms;
        }

        public function get_rooms_byLimit($limit){
            $this->db->select('rooms.*, , site_meta_data.header AS category_name, site_meta_data.paragraph AS category_class');
            $this->db->join('site_meta_data', 'rooms.category = site_meta_data.id','left');
            $this->db->order_by('rooms.rank','ASC');
            $this->db->limit($limit);
            $rooms = $this->db->get_where('rooms',['disabled'=>0,'deleted'=>0])->result_array();
            foreach($rooms as $key => $room){
                $rooms[$key]['files'] = $this->db->get_where('site_files',[
                    'module_id'=> 15,
                    'form_id' => $room['id'],
                ])->result_array();
            }
            return $rooms;
        }

        public function get_room($id){
            $this->db->select('rooms.*, , site_meta_data.header AS category_name, site_meta_data.paragraph AS category_class');
            $this->db->join('site_meta_data', 'rooms.category = site_meta_data.id','left');
            $this->db->order_by('rooms.rank','ASC');
            $room = $this->db->get_where('rooms',['rooms.id' => $id, 'disabled'=>0, 'deleted'=>0])->row_array();
            $room['files'] = $this->db->get_where('site_files',[
                    'module_id'=> 15,
                    'form_id' => $room['id'],
                ])->result_array();
            $room['features'] = $this->db->get_where('room_features',[
                    'room_id' => $room['id'],
                ])->result_array();    
            return $room;
        }

        public function get_available_rooms($search){   
            $this->db->select('rooms.*, , site_meta_data.header AS category_name, site_meta_data.paragraph AS category_class');
            $this->db->join('site_meta_data', 'rooms.category = site_meta_data.id','left');
            $this->db->order_by('rooms.rank','ASC');
            $rooms = $this->db->get_where('rooms',['disabled'=>0, 'deleted'=>0])->result_array();
            foreach($rooms as $key => $room){
                $query = '';
                $query = 'SELECT *, SUM(rooms) AS total_rooms FROM booking
                WHERE deleted = 0 AND room_id = '.$room['id'].' 
                AND (check_in BETWEEN "'.trim($search['check_in']).'" AND "'.trim($search['check_out']).'"
                OR check_out BETWEEN "'.trim($search['check_in']).'" AND "'.trim($search['check_out']).'")';
                $rooms[$key]['bookings'] = $this->db->query($query)->row_array();
                $rooms[$key]['available'] =  $room['inventory'] - $rooms[$key]['bookings']['total_rooms'];
                $rooms[$key]['files'] = $this->db->get_where('site_files',[
                    'module_id'=> 15,
                    'form_id' => $room['id'],
                ])->result_array();
            }
            return $rooms;
        }

        public function get_offers(){
            $this->db->select('offers.*');
            $this->db->order_by('offers.rank','ASC');
            $offers = $this->db->get_where('offers',['deleted' => 0, 'active' => 1])->result_array();
            return $offers;
        }

        public function get_offer($id){
            $this->db->order_by('offers.rank','ASC');
            $offer = $this->db->get_where('offers',['offers.id' => $id, 'deleted'=>0, 'active' => 1])->row_array();
            $offer['features'] = $this->db->get_where('offer_features',[
                    'offer_id' => $offer['id'],
                ])->result_array();    
            return $offer;
        }

        public function get_countries(){
            return $this->db->get_where('countries',array('deleted'=>0))->result_array();  
        }

        public function get_booking_byCode($code){
            return $this->db->get_where('booking',array('deleted'=>0, 'code' => $code))->row_array();  
        }
        public function get_booking($id){
            return $this->db->get_where('booking',array('deleted'=>0, 'id' => $id))->row_array();  
        }
        


        // public function get_boat_infos($boat_id){
        //     $this->db->select('boat_info.*, status.status_name');
        //     $this->db->join('status', 'boat_info.status = status.id','left');
        //     $this->db->order_by('boat_info.rank','ASC');
        //     return $this->db->get_where('boat_info',array('deleted'=>0, 'boat_id'=>$boat_id, 'status'=>2))->result_array();  
        // } 

        // public function get_boat_cabins($boat_id){
        //     $this->db->distinct();
        //     $this->db->select('cabins.*, status.status_name');
        //     $this->db->join('status', 'cabins.status = status.id','left');
        //     $this->db->order_by('cabins.rank','ASC');
        //     return $this->db->get_where('cabins',array('deleted'=>0, 'boat_id'=>$boat_id, 'status'=>2))->result_array();  
        // }  

        // public function get_boat_packages($boat_id){
        //     $this->db->select('packages.*, status.status_name');
        //     $this->db->join('status', 'packages.status = status.id','left');
        //     $this->db->order_by('packages.rank','ASC');
        //     return $this->db->get_where('packages',array('deleted'=>0, 'boat_id'=>$boat_id, 'status'=>2))->result_array();  
        // }  

        public function add_booking($data){
          $this->db->insert('booking', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
        } 

        public function update_booking($data,$booking_id){
            $this->db->where('booking.id', $booking_id);
            $this->db->update('booking', $data);
            if ($this->db->affected_rows() >= 0) {
                return $this->db->affected_rows();
            } 
        }

        public function get_posts(){
            $this->db->select('blog.*, status.status_name');
            $this->db->join('status', 'blog.status = status.id','left');
            $this->db->order_by('blog.timestamp','ASC');
            return $this->db->get_where('blog',array('deleted'=>0, 'status'=>2))->result_array();  
        }

        public function get_post($post_id){
            $this->db->select('blog.*, status.status_name');
            $this->db->join('status', 'blog.status = status.id','left');
            $this->db->where('blog.id',$post_id);
            return $this->db->get('blog')->row_array();
        }

        public function get_sit_metaData($meta,$type){
            $this->db->order_by('rank','ASC');
        	if ($type=='single') {
				return $this->db->get_where('site_meta_data',array('meta'=>$meta))->row_array();
        	}elseif($type=='multi'){
                return $this->db->get_where('site_meta_data',array('meta'=>$meta))->result_array();
        	}

         }     

        public function get_categories(){
            $this->db->order_by('rank','ASC');
            return $this->db->get_where('site_groups',array('deleted'=>0))->result_array();

         }  

         public function get_category($serial){
            return $this->db->get_where('site_groups',array('serial'=>$serial,'deleted'=>0))->row_array();

         }  

         public function get_category_items($cat_id){
         	$this->db->order_by('site_group_items.rank','ASC');
            return $this->db->get_where('site_group_items',array('group_id'=>$cat_id,'deleted'=>0))->result_array();
         } 

         public function get_gallery_unique(){
         	$this->db->select('header, points');
         	$this->db->order_by('site_meta_data.rank','ASC');
         	$this->db->where(array('meta'=>'gallery'));
         	$this->db->group_by('site_meta_data.points');
            return $this->db->get_where('site_meta_data')->result_array();
         }

         public function get_portfolio_images($limit){
                $this->db->limit($limit);
                return $this->db->get_where('site_meta_data',array('meta'=>'gallery'))->result_array();

         } 

        public function add_client_mail($data){
          $this->db->insert('clients_mail_queue', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
         }             

        // public function add_booking_trip($data){
        //   $this->db->insert('bookings', $data);
        //   return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
        //  } 

        // public function get_hotels(){
        //     $this->db->select('hotel.*');
        //     $this->db->where('hotel.deleted', 0);
        //     $this->db->order_by('hotel.id','ASC');
        //     return $this->db->get('hotel')->result_array();
        // }            


  }
?>	