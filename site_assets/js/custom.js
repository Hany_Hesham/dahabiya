/* 

1. Add your custom JavaScript code below
2. Place the this code in your template:
*/

$(document).ready(function(){
    let totalStars = 5;
    let stars = $('.stars-container').data("stars");
    let checkedStars = '';
    for (let index = 0; index < stars; index++) {
        checkedStars += `<span class="fa fa-star star-checked"></span>`; 
    }
    if(stars < 5){
        let restStars = totalStars - stars;
        for (let index = 0; index < restStars; index++) {
            checkedStars += `<span class="fa fa-star"></span>`; 
        }
    }
    $('.stars-container').html(checkedStars);

    $('#modalStripBottomLoader').click(function(){
            $('#modalStripBottom').toggle();
    });
});