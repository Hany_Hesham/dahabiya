 if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
          navigator.serviceWorker.register('../service-worker.js')
            .then((registration) => {
              console.log('Service worker registered successfully', registration);
            })
            .catch((error) => {
              console.log('Something went wrong', error);
            });
        });
      }