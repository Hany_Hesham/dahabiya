-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2022 at 02:21 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dahabiya`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name`, `deleted`) VALUES
(1, 'Aqua Center', 0),
(2, 'Diving', 0),
(3, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(5) NOT NULL,
  `category` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `post` longblob NOT NULL,
  `status` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `category`, `title`, `description`, `image`, `post`, `status`, `deleted`, `timestamp`) VALUES
(1, 'General', 'This is a Standard post with a Preview Image', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '17.jpg', '', 2, 0, '2021-10-24 13:58:13'),
(2, 'Diving', 'This is a Standard post with a Preview Image 2', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '1.jpg', '', 2, 0, '2021-10-24 13:58:13'),
(3, 'Torusim', 'This is a Standard post with a Preview Image 3', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '10.jpg', '', 2, 0, '2021-10-25 16:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(5) NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(5) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `check_in` varchar(50) NOT NULL,
  `check_out` varchar(50) NOT NULL,
  `adults` int(5) NOT NULL,
  `childs` int(5) NOT NULL,
  `rooms` int(11) NOT NULL,
  `nights` int(5) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) NOT NULL,
  `total_price` double(15,2) NOT NULL,
  `rate` double(15,2) NOT NULL,
  `payed` double(15,2) NOT NULL,
  `tax` double(10,2) NOT NULL,
  `total_rate` double(15,2) NOT NULL,
  `total_tax` double(15,2) NOT NULL,
  `currency` varchar(5) NOT NULL DEFAULT 'EGP',
  `payment_image` varchar(255) NOT NULL,
  `payment_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `code`, `room_id`, `offer_id`, `check_in`, `check_out`, `adults`, `childs`, `rooms`, `nights`, `first_name`, `last_name`, `email`, `address`, `city`, `state`, `post_code`, `country`, `phone`, `total_price`, `rate`, `payed`, `tax`, `total_rate`, `total_tax`, `currency`, `payment_image`, `payment_method`, `status`, `deleted`, `timestamp`) VALUES
(18, 'BOO202218', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'US', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-28 20:30:53'),
(19, 'BOO202219', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-21 15:58:49'),
(20, 'BOO202220', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-21 15:59:47'),
(21, 'BOO202221', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-21 16:00:55'),
(22, 'BOO202222', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-21 16:01:23'),
(23, 'BOO202223', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-21 16:01:48'),
(24, 'BOO202224', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 4, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 5280.00, 400.00, 0.00, 40.00, 4800.00, 480.00, 'EGP', '', '', 1, 0, '2022-01-21 16:03:03'),
(25, 'BOO202225', 1, 0, '2022-01-28', '2022-01-31', 3, 1, 2, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 2640.00, 400.00, 0.00, 40.00, 2400.00, 240.00, 'EGP', '40961.jpg', 'Cheque', 3, 0, '2022-01-21 17:38:28'),
(26, 'BOO202226', 2, 0, '2022-01-21', '2022-01-24', 1, 0, 1, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EC', '01002273237', 1335.00, 400.00, 0.00, 45.00, 1200.00, 135.00, 'EGP', 'af8d63a477078732b79ff9d9fc60873f.jpg', 'Cheque', 3, 0, '2022-01-21 17:45:34'),
(27, 'BOO202227', 1, 0, '2022-01-22', '2022-01-25', 1, 0, 1, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 1320.00, 400.00, 0.00, 40.00, 1200.00, 120.00, 'EGP', 'kids-learn-robotics.webp', 'paypal', 3, 0, '2022-01-22 00:14:33'),
(28, 'BOO202228', 1, 1, '2022-02-01', '2022-02-04', 3, 2, 2, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 2400.00, 400.00, 0.00, 0.00, 2400.00, 0.00, 'EGP', '', '', 1, 0, '2022-01-28 16:49:17'),
(29, 'BOO202229', 1, 1, '2022-02-01', '2022-02-04', 3, 2, 2, 3, 'mm', 'mm', 'mm@m', 'portsaid, portfouad', 'sadas', 'adasd', '', 'EG', '01002273237', 800.00, 400.00, 0.00, 0.00, 800.00, 0.00, 'EGP', '4096.jpg', 'Bank Transfer', 2, 0, '2022-01-28 20:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUN',
  `logo` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch_name`, `group_id`, `code`, `logo`, `deleted`) VALUES
(1, 'Zahabia Resort', 1, 'ZAR', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`, `lang`) VALUES
('1p6rp9vo7bjc2ds3osbpmnm1cu67qslo', '::1', 1643893920, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333839333932303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('6bgd9nm1ktv9hp4fvgalrhqsimin5kv5', '::1', 1643899817, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333839393831373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ado27bnb3go4a6acq8rhkj0pd0il0hfq', '::1', 1643906762, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333930303132353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('e7fqsq8vior08f2cj4ntc64m43ilbama', '::1', 1643894232, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333839343233323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('fb8hnqvreek1ntu3876evs5pdi8iorvk', '::1', 1643900125, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333930303132353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('h6h7pvnvumiqb7p698buim8jndt5ttnm', '::1', 1643899342, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333839393334323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('iu27mnkgphnnbt40ilkuk9dk31j01im3', '::1', 1643980892, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333937383934313b757365725f69647c733a333a22313136223b6e616d657c733a31343a226d61686d6f75642e6d6f68696572223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('jp956gio2cced6iapiu04486sdgm4ed2', '::1', 1643898845, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333839383834353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('nvotjcov2ljeus0msvu503avgi95sm1c', '::1', 1643978941, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333937383934313b757365725f69647c733a333a22313136223b6e616d657c733a31343a226d61686d6f75642e6d6f68696572223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('sq034uqfrvoj4ldoq66d4lm1f5thnc04', '::1', 1643977930, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634333937373933303b757365725f69647c733a333a22313136223b6e616d657c733a31343a226d61686d6f75642e6d6f68696572223b69735f61646d696e5f6c6f67696e7c623a313b, '');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cash` decimal(20,2) NOT NULL,
  `phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `updated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients_mail_queue`
--

CREATE TABLE `clients_mail_queue` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_info` int(11) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients_mail_queue`
--

INSERT INTO `clients_mail_queue` (`id`, `type`, `client_info`, `client_name`, `email`, `subject`, `message`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', NULL, 'mahmoud', 'mm@m', '', 'testy mest', 22, NULL, '2022-01-24 01:06:27'),
(2, '', NULL, 'mahmoud', 'mm@m', '', '$this->input->post(\'subject\')', 22, NULL, '2022-01-24 01:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(20) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `rank`, `deleted`) VALUES
(1, 'عز', 1, 0),
(2, 'بشاى', 2, 0),
(3, 'سعودى سابك', 3, 0),
(4, 'سرحان', 4, 0),
(5, 'سعودى راجحى', 5, 0),
(6, 'مراكبى', 6, 0),
(7, 'بيانكو', 7, 0),
(8, 'جيوشى', 8, 0),
(9, 'عنتر', 9, 0),
(10, 'مصريين', 10, 0),
(11, 'المصرية', 11, 0),
(12, 'السويدى', 12, 0),
(13, 'الجيش بنى سويف', 13, 0),
(14, 'الجيش العريش', 14, 0),
(15, 'القومية', 15, 0),
(16, 'طرة', 16, 0),
(17, 'المسلح', 17, 0),
(18, 'السويس', 18, 0),
(19, 'سينا', 19, 0),
(20, 'رويال', 20, 0),
(21, 'الدولية', 21, 0),
(22, 'البلاح', 22, 0),
(23, 'نجمة سيناء مصيص', 23, 0),
(24, 'عثمان', 24, 0),
(25, 'بدون', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `phone_code` int(5) NOT NULL,
  `country_code` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `country_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `phone_code`, `country_code`, `country_name`, `deleted`) VALUES
(1, 93, 'AF', 'Afghanistan', 0),
(2, 358, 'AX', 'Aland Islands', 0),
(3, 355, 'AL', 'Albania', 0),
(4, 213, 'DZ', 'Algeria', 0),
(6, 376, 'AD', 'Andorra', 0),
(7, 244, 'AO', 'Angola', 0),
(8, 1264, 'AI', 'Anguilla', 0),
(9, 672, 'AQ', 'Antarctica', 0),
(10, 1268, 'AG', 'Antigua and Barbuda', 0),
(11, 54, 'AR', 'Argentina', 0),
(12, 374, 'AM', 'Armenia', 0),
(13, 297, 'AW', 'Aruba', 0),
(14, 61, 'AU', 'Australia', 0),
(15, 43, 'AT', 'Austria', 0),
(16, 994, 'AZ', 'Azerbaijan', 0),
(17, 1242, 'BS', 'Bahamas', 0),
(18, 973, 'BH', 'Bahrain', 0),
(19, 880, 'BD', 'Bangladesh', 0),
(20, 1246, 'BB', 'Barbados', 0),
(21, 375, 'BY', 'Belarus', 0),
(22, 32, 'BE', 'Belgium', 0),
(23, 501, 'BZ', 'Belize', 0),
(24, 229, 'BJ', 'Benin', 0),
(25, 1441, 'BM', 'Bermuda', 0),
(26, 975, 'BT', 'Bhutan', 0),
(27, 591, 'BO', 'Bolivia', 0),
(28, 599, 'BQ', 'Bonaire, Sint Eustatius and Saba', 0),
(29, 387, 'BA', 'Bosnia and Herzegovina', 0),
(30, 267, 'BW', 'Botswana', 0),
(31, 55, 'BV', 'Bouvet Island', 0),
(32, 55, 'BR', 'Brazil', 0),
(33, 246, 'IO', 'British Indian Ocean Territory', 0),
(34, 673, 'BN', 'Brunei Darussalam', 0),
(35, 359, 'BG', 'Bulgaria', 0),
(36, 226, 'BF', 'Burkina Faso', 0),
(37, 257, 'BI', 'Burundi', 0),
(38, 855, 'KH', 'Cambodia', 0),
(39, 237, 'CM', 'Cameroon', 0),
(40, 1, 'CA', 'Canada', 0),
(41, 238, 'CV', 'Cape Verde', 0),
(42, 1345, 'KY', 'Cayman Islands', 0),
(43, 236, 'CF', 'Central African Republic', 0),
(44, 235, 'TD', 'Chad', 0),
(45, 56, 'CL', 'Chile', 0),
(46, 86, 'CN', 'China', 0),
(47, 61, 'CX', 'Christmas Island', 0),
(48, 672, 'CC', 'Cocos (Keeling) Islands', 0),
(49, 57, 'CO', 'Colombia', 0),
(50, 269, 'KM', 'Comoros', 0),
(51, 242, 'CG', 'Congo', 0),
(52, 242, 'CD', 'Congo, Democratic Republic of the Congo', 0),
(53, 682, 'CK', 'Cook Islands', 0),
(54, 506, 'CR', 'Costa Rica', 0),
(55, 225, 'CI', 'Cote D\'Ivoire', 0),
(56, 385, 'HR', 'Croatia', 0),
(57, 53, 'CU', 'Cuba', 0),
(58, 599, 'CW', 'Curacao', 0),
(59, 357, 'CY', 'Cyprus', 0),
(60, 420, 'CZ', 'Czech Republic', 0),
(61, 45, 'DK', 'Denmark', 0),
(62, 253, 'DJ', 'Djibouti', 0),
(63, 1767, 'DM', 'Dominica', 0),
(64, 1809, 'DO', 'Dominican Republic', 0),
(65, 593, 'EC', 'Ecuador', 0),
(66, 20, 'EG', 'Egypt', 0),
(67, 503, 'SV', 'El Salvador', 0),
(68, 240, 'GQ', 'Equatorial Guinea', 0),
(69, 291, 'ER', 'Eritrea', 0),
(70, 372, 'EE', 'Estonia', 0),
(71, 251, 'ET', 'Ethiopia', 0),
(72, 500, 'FK', 'Falkland Islands (Malvinas)', 0),
(73, 298, 'FO', 'Faroe Islands', 0),
(74, 679, 'FJ', 'Fiji', 0),
(75, 358, 'FI', 'Finland', 0),
(76, 33, 'FR', 'France', 0),
(77, 594, 'GF', 'French Guiana', 0),
(78, 689, 'PF', 'French Polynesia', 0),
(79, 262, 'TF', 'French Southern Territories', 0),
(80, 241, 'GA', 'Gabon', 0),
(81, 220, 'GM', 'Gambia', 0),
(82, 995, 'GE', 'Georgia', 0),
(83, 49, 'DE', 'Germany', 0),
(84, 233, 'GH', 'Ghana', 0),
(85, 350, 'GI', 'Gibraltar', 0),
(86, 30, 'GR', 'Greece', 0),
(87, 299, 'GL', 'Greenland', 0),
(88, 1473, 'GD', 'Grenada', 0),
(89, 590, 'GP', 'Guadeloupe', 0),
(90, 1671, 'GU', 'Guam', 0),
(91, 502, 'GT', 'Guatemala', 0),
(92, 44, 'GG', 'Guernsey', 0),
(93, 224, 'GN', 'Guinea', 0),
(94, 245, 'GW', 'Guinea-Bissau', 0),
(95, 592, 'GY', 'Guyana', 0),
(96, 509, 'HT', 'Haiti', 0),
(97, 0, 'HM', 'Heard Island and Mcdonald Islands', 0),
(98, 39, 'VA', 'Holy See (Vatican City State)', 0),
(99, 504, 'HN', 'Honduras', 0),
(100, 852, 'HK', 'Hong Kong', 0),
(101, 36, 'HU', 'Hungary', 0),
(102, 354, 'IS', 'Iceland', 0),
(103, 91, 'IN', 'India', 0),
(104, 62, 'ID', 'Indonesia', 0),
(105, 98, 'IR', 'Iran, Islamic Republic of', 0),
(106, 964, 'IQ', 'Iraq', 0),
(107, 353, 'IE', 'Ireland', 0),
(108, 44, 'IM', 'Isle of Man', 0),
(109, 972, 'IL', 'Israel', 1),
(110, 39, 'IT', 'Italy', 0),
(111, 1876, 'JM', 'Jamaica', 0),
(112, 81, 'JP', 'Japan', 0),
(113, 44, 'JE', 'Jersey', 0),
(114, 962, 'JO', 'Jordan', 0),
(115, 7, 'KZ', 'Kazakhstan', 0),
(116, 254, 'KE', 'Kenya', 0),
(117, 686, 'KI', 'Kiribati', 0),
(118, 850, 'KP', 'Korea, Democratic People\'s Republic of', 0),
(119, 82, 'KR', 'Korea, Republic of', 0),
(120, 381, 'XK', 'Kosovo', 0),
(121, 965, 'KW', 'Kuwait', 0),
(122, 996, 'KG', 'Kyrgyzstan', 0),
(123, 856, 'LA', 'Lao People\'s Democratic Republic', 0),
(124, 371, 'LV', 'Latvia', 0),
(125, 961, 'LB', 'Lebanon', 0),
(126, 266, 'LS', 'Lesotho', 0),
(127, 231, 'LR', 'Liberia', 0),
(128, 218, 'LY', 'Libyan Arab Jamahiriya', 0),
(129, 423, 'LI', 'Liechtenstein', 0),
(130, 370, 'LT', 'Lithuania', 0),
(131, 352, 'LU', 'Luxembourg', 0),
(132, 853, 'MO', 'Macao', 0),
(133, 389, 'MK', 'Macedonia, the Former Yugoslav Republic of', 0),
(134, 261, 'MG', 'Madagascar', 0),
(135, 265, 'MW', 'Malawi', 0),
(136, 60, 'MY', 'Malaysia', 0),
(137, 960, 'MV', 'Maldives', 0),
(138, 223, 'ML', 'Mali', 0),
(139, 356, 'MT', 'Malta', 0),
(140, 692, 'MH', 'Marshall Islands', 0),
(141, 596, 'MQ', 'Martinique', 0),
(142, 222, 'MR', 'Mauritania', 0),
(143, 230, 'MU', 'Mauritius', 0),
(144, 269, 'YT', 'Mayotte', 0),
(145, 52, 'MX', 'Mexico', 0),
(146, 691, 'FM', 'Micronesia, Federated States of', 0),
(147, 373, 'MD', 'Moldova, Republic of', 0),
(148, 377, 'MC', 'Monaco', 0),
(149, 976, 'MN', 'Mongolia', 0),
(150, 382, 'ME', 'Montenegro', 0),
(151, 1664, 'MS', 'Montserrat', 0),
(152, 212, 'MA', 'Morocco', 0),
(153, 258, 'MZ', 'Mozambique', 0),
(154, 95, 'MM', 'Myanmar', 0),
(155, 264, 'NA', 'Namibia', 0),
(156, 674, 'NR', 'Nauru', 0),
(157, 977, 'NP', 'Nepal', 0),
(158, 31, 'NL', 'Netherlands', 0),
(159, 599, 'AN', 'Netherlands Antilles', 0),
(160, 687, 'NC', 'New Caledonia', 0),
(161, 64, 'NZ', 'New Zealand', 0),
(162, 505, 'NI', 'Nicaragua', 0),
(163, 227, 'NE', 'Niger', 0),
(164, 234, 'NG', 'Nigeria', 0),
(165, 683, 'NU', 'Niue', 0),
(166, 672, 'NF', 'Norfolk Island', 0),
(167, 1670, 'MP', 'Northern Mariana Islands', 0),
(168, 47, 'NO', 'Norway', 0),
(169, 968, 'OM', 'Oman', 0),
(170, 92, 'PK', 'Pakistan', 0),
(171, 680, 'PW', 'Palau', 0),
(172, 970, 'PS', 'Palestinian Territory, Occupied', 0),
(173, 507, 'PA', 'Panama', 0),
(174, 675, 'PG', 'Papua New Guinea', 0),
(175, 595, 'PY', 'Paraguay', 0),
(176, 51, 'PE', 'Peru', 0),
(177, 63, 'PH', 'Philippines', 0),
(178, 64, 'PN', 'Pitcairn', 0),
(179, 48, 'PL', 'Poland', 0),
(180, 351, 'PT', 'Portugal', 0),
(181, 1787, 'PR', 'Puerto Rico', 0),
(182, 974, 'QA', 'Qatar', 0),
(183, 262, 'RE', 'Reunion', 0),
(184, 40, 'RO', 'Romania', 0),
(185, 70, 'RU', 'Russian Federation', 0),
(186, 250, 'RW', 'Rwanda', 0),
(187, 590, 'BL', 'Saint Barthelemy', 0),
(188, 290, 'SH', 'Saint Helena', 0),
(189, 1869, 'KN', 'Saint Kitts and Nevis', 0),
(190, 1758, 'LC', 'Saint Lucia', 0),
(191, 590, 'MF', 'Saint Martin', 0),
(192, 508, 'PM', 'Saint Pierre and Miquelon', 0),
(193, 1784, 'VC', 'Saint Vincent and the Grenadines', 0),
(194, 684, 'WS', 'Samoa', 0),
(195, 378, 'SM', 'San Marino', 0),
(196, 239, 'ST', 'Sao Tome and Principe', 0),
(197, 966, 'SA', 'Saudi Arabia', 0),
(198, 221, 'SN', 'Senegal', 0),
(199, 381, 'RS', 'Serbia', 0),
(200, 381, 'CS', 'Serbia and Montenegro', 0),
(201, 248, 'SC', 'Seychelles', 0),
(202, 232, 'SL', 'Sierra Leone', 0),
(203, 65, 'SG', 'Singapore', 0),
(204, 1, 'SX', 'Sint Maarten', 0),
(205, 421, 'SK', 'Slovakia', 0),
(206, 386, 'SI', 'Slovenia', 0),
(207, 677, 'SB', 'Solomon Islands', 0),
(208, 252, 'SO', 'Somalia', 0),
(209, 27, 'ZA', 'South Africa', 0),
(210, 500, 'GS', 'South Georgia and the South Sandwich Islands', 0),
(211, 211, 'SS', 'South Sudan', 0),
(212, 34, 'ES', 'Spain', 0),
(213, 94, 'LK', 'Sri Lanka', 0),
(214, 249, 'SD', 'Sudan', 0),
(215, 597, 'SR', 'Suriname', 0),
(216, 47, 'SJ', 'Svalbard and Jan Mayen', 0),
(217, 268, 'SZ', 'Swaziland', 0),
(218, 46, 'SE', 'Sweden', 0),
(219, 41, 'CH', 'Switzerland', 0),
(220, 963, 'SY', 'Syrian Arab Republic', 0),
(221, 886, 'TW', 'Taiwan, Province of China', 0),
(222, 992, 'TJ', 'Tajikistan', 0),
(223, 255, 'TZ', 'Tanzania, United Republic of', 0),
(224, 66, 'TH', 'Thailand', 0),
(225, 670, 'TL', 'Timor-Leste', 0),
(226, 228, 'TG', 'Togo', 0),
(227, 690, 'TK', 'Tokelau', 0),
(228, 676, 'TO', 'Tonga', 0),
(229, 1868, 'TT', 'Trinidad and Tobago', 0),
(230, 216, 'TN', 'Tunisia', 0),
(231, 90, 'TR', 'Turkey', 0),
(232, 7370, 'TM', 'Turkmenistan', 0),
(233, 1649, 'TC', 'Turks and Caicos Islands', 0),
(234, 688, 'TV', 'Tuvalu', 0),
(235, 256, 'UG', 'Uganda', 0),
(236, 380, 'UA', 'Ukraine', 0),
(237, 971, 'AE', 'United Arab Emirates', 0),
(238, 44, 'GB', 'United Kingdom', 0),
(239, 1, 'US', 'United States', 0),
(240, 1, 'UM', 'United States Minor Outlying Islands', 0),
(241, 598, 'UY', 'Uruguay', 0),
(242, 998, 'UZ', 'Uzbekistan', 0),
(243, 678, 'VU', 'Vanuatu', 0),
(244, 58, 'VE', 'Venezuela', 0),
(245, 84, 'VN', 'Viet Nam', 0),
(246, 1284, 'VG', 'Virgin Islands, British', 0),
(247, 1340, 'VI', 'Virgin Islands, U.s.', 0),
(248, 681, 'WF', 'Wallis and Futuna', 0),
(249, 212, 'EH', 'Western Sahara', 0),
(250, 967, 'YE', 'Yemen', 0),
(251, 260, 'ZM', 'Zambia', 0),
(252, 263, 'ZW', 'Zimbabwe', 0);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `symbol`, `name`, `isdefault`) VALUES
(1, '$', 'USD', 0),
(2, '€', 'EUR', 0),
(3, 'EGP', 'EGP', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `dep_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dep_name`, `code`, `rank`) VALUES
(1, 'Reservation', 'MG', 1),
(2, 'IT', 'IT', 2),
(4, 'Management', 'M&G', 3);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(5) NOT NULL,
  `location` varchar(100) NOT NULL,
  `event` varchar(250) NOT NULL,
  `describtion` text NOT NULL,
  `rank` int(5) NOT NULL,
  `image` varchar(100) NOT NULL,
  `toped` double(5,2) NOT NULL,
  `lefted` double(5,2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `location`, `event`, `describtion`, `rank`, `image`, `toped`, `lefted`, `active`, `deleted`) VALUES
(1, 'Luxor', '', 'Is a city on the east bank of the nile river in southern Egypt.', 1, 'Pointer.png', 34.50, 54.50, 1, 0),
(2, 'El Kab', '', 'Is an upper Egyptian site on the east bank of the Nile at the mounts of the wadi hillal.', 2, 'Pointer.png', 37.00, 52.50, 1, 0),
(3, 'Edfu', '', 'Is an Egyptian city, edfu is site of the Ptolemaic temple of Horus.', 3, 'Pointer.png', 51.50, 57.50, 1, 0),
(4, 'Gebel Silsila', '', 'Is 65km of aswan in upper Egypt where the cliffs on both sides close to the narrowest point along the length of the entire nile.', 4, 'Pointer.png', 57.70, 59.30, 1, 0),
(5, 'Kom Ombo', '', 'Is an agricultural town in Egypt famous for the temple of kom ombo, meaning city of gold.', 5, 'Pointer.png', 60.50, 60.00, 1, 0),
(6, 'Aswan', '', 'A city on the Nile river. Has been southern Egypt’s strategic and commercial gateway since antiquity.', 6, 'Pointer.png', 66.00, 57.70, 1, 0),
(7, 'Philae', '', 'Is an island in the reservoir of the aswan low dam.', 7, 'Pointer.png', 67.70, 58.40, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `font_icons`
--

CREATE TABLE `font_icons` (
  `id` int(11) NOT NULL,
  `icon_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `font_icons`
--

INSERT INTO `font_icons` (`id`, `icon_name`, `rank`) VALUES
(1, 'fas fa-arrow-alt-circle-left', 1),
(2, 'fa-music', 2),
(3, 'fa-search', 3),
(4, 'fa-envelope-o', 4),
(5, 'fa-heart', 5),
(6, 'fa-star', 6),
(7, 'fa-star-o', 7),
(8, 'fa-user', 8),
(9, 'fa-film', 9),
(10, 'fa-th-large', 10),
(11, 'fa-th', 11),
(12, 'fa-th-list', 12),
(13, 'fa-check', 13),
(14, 'fa-remove', 14),
(15, 'fa-close', 15),
(16, 'fa-times', 16),
(17, 'fa-search-plus', 17),
(18, 'fa-search-minus', 18),
(19, 'fa-power-off', 19),
(20, 'fa-signal', 20),
(21, 'fa-gear', 21),
(22, 'fa-cog', 22),
(23, 'fa-trash-o', 23),
(24, 'fa-home', 24),
(25, 'fa-file-o', 25),
(26, 'fa-clock-o', 26),
(27, 'fa-road', 27),
(28, 'fa-download', 28),
(29, 'fa-arrow-circle-o-do', 29),
(30, 'fa-arrow-circle-o-up', 30),
(31, 'fa-inbox', 31),
(32, 'fa-play-circle-o', 32),
(33, 'fa-rotate-right', 33),
(34, 'fa-repeat', 34),
(35, 'fa-refresh', 35),
(36, 'fa-list-alt', 36),
(37, 'fa-lock', 37),
(38, 'fa-flag', 38),
(39, 'fa-headphones', 39),
(40, 'fa-volume-off', 40),
(41, 'fa-volume-down', 41),
(42, 'fa-volume-up', 42),
(43, 'fa-qrcode', 43),
(44, 'fa-barcode', 44),
(45, 'fa-tag', 45),
(46, 'fa-tags', 46),
(47, 'fa-book', 47),
(48, 'fa-bookmark', 48),
(49, 'fa-print f', 49),
(50, 'fa-camera', 50),
(51, 'fa-font', 51),
(52, 'fa-bold', 52),
(53, 'fa-italic', 53),
(54, 'fa-text-height', 54),
(55, 'fa-text-width', 55),
(56, 'fa-align-left', 56),
(57, 'fa-align-center', 57),
(58, 'fa-align-right', 58),
(59, 'fa-align-justify', 59),
(60, 'fa-list', 60),
(61, 'fa-dedent', 61),
(62, 'fa-outdent', 62),
(63, 'fa-indent', 63),
(64, 'fa-video-camera', 64),
(65, 'fa-photo', 65),
(66, 'fa-image', 66),
(67, 'fa-picture-o', 67),
(68, 'fa-pencil', 68),
(69, 'fa-map-marker', 69),
(70, 'fa-adjust', 70),
(71, 'fa-tint', 71),
(72, 'fa-edit', 72),
(73, 'fa-pencil-square-o', 73),
(74, 'fa-share-square-o', 74),
(75, 'fa-check-square-o', 75),
(76, 'fa-arrows', 76),
(77, 'fa-step-backward', 77),
(78, 'fa-fast-backward', 78),
(79, 'fa-backward', 79),
(80, 'fa-play', 80),
(81, 'fa-pause', 81),
(82, 'fa-stop', 82),
(83, 'fa-forward', 83),
(84, 'fa-fast-forward', 84),
(85, 'fa-step-forward', 85),
(86, 'fa-eject', 86),
(87, 'fa-chevron-left', 87),
(88, 'fa-chevron-right', 88),
(89, 'fa-plus-circle', 89),
(90, 'fa-minus-circle', 90),
(91, 'fa-times-circle', 91),
(92, 'fa-check-circle', 92),
(93, 'fa-question-circle', 93),
(94, 'fa-info-circle', 94),
(95, 'fa-crosshairs', 95),
(96, 'fa-times-circle-o', 96),
(97, 'fa-check-circle-o', 97),
(98, 'fa-ban', 98),
(99, 'fa-arrow-left', 99),
(100, 'fa-arrow-right', 100),
(101, 'fa-arrow-up', 101),
(102, 'fa-arrow-down', 102),
(103, 'fa-mail-forward', 103),
(104, 'fa-share', 104),
(105, 'fa-expand', 105),
(106, 'fa-compress', 106),
(107, 'fa-plus', 107),
(108, 'fa-minus', 108),
(109, 'fa-asterisk', 109),
(110, 'fa-exclamation-circl', 110),
(111, 'fa-gift', 111),
(112, 'fa-leaf', 112),
(113, 'fa-fire', 113),
(114, 'fa-eye', 114),
(115, 'fa-eye-slash', 115),
(116, 'fa-warning', 116),
(117, 'fa-exclamation-trian', 117),
(118, 'fa-plane', 118),
(119, 'fa-calendar', 119),
(120, 'fa-random', 120),
(121, 'fa-comment', 121),
(122, 'fa-magnet', 122),
(123, 'fa-chevron-up', 123),
(124, 'fa-chevron-down', 124),
(125, 'fa-retweet', 125),
(126, 'fa-shopping-cart', 126),
(127, 'fa-folder', 127),
(128, 'fa-folder-open', 128),
(129, 'fa-arrows-v', 129),
(130, 'fa-arrows-h', 130),
(131, 'fa-bar-chart-o', 131),
(132, 'fa-bar-chart', 132),
(133, 'fa-twitter-square', 133),
(134, 'fa-facebook-square', 134),
(135, 'fa-camera-retro', 135),
(136, 'fa-key', 136),
(137, 'fa-gears', 137),
(138, 'fa-cogs', 138),
(139, 'fa-comments', 139),
(140, 'fa-thumbs-o-up', 140),
(141, 'fa-thumbs-o-down', 141),
(142, 'fa-star-half', 142),
(143, 'fa-heart-o', 143),
(144, 'fa-sign-out', 144),
(145, 'fa-linkedin-square', 145),
(146, 'fa-thumb-tack', 146),
(147, 'fa-external-link', 147),
(148, 'fa-sign-in', 148),
(149, 'fa-trophy', 149),
(150, 'fa-github-square', 150),
(151, 'fa-upload', 151),
(152, 'fa-lemon-o', 152),
(153, 'fa-phone', 153),
(154, 'fa-square-o', 154),
(155, 'fa-bookmark-o', 155),
(156, 'fa-phone-square', 156),
(157, 'fa-twitter', 157),
(158, 'fa-facebook-f', 158),
(159, 'fa-facebook', 159),
(160, 'fa-github', 160),
(161, 'fa-unlock', 161),
(162, 'fa-credit-card', 162),
(163, 'fa-rss', 163),
(164, 'fa-hdd-o', 164),
(165, 'fa-bullhorn', 165),
(166, 'fa-bell', 166),
(167, 'fa-certificate', 167),
(168, 'fa-hand-o-right', 168),
(169, 'fa-hand-o-left', 169),
(170, 'fa-hand-o-up', 170),
(171, 'fa-hand-o-down', 171),
(172, 'fa-arrow-circle-left', 172),
(173, 'fa-arrow-circle-righ', 173),
(174, 'fa-arrow-circle-up', 174),
(175, 'fa-arrow-circle-down', 175),
(176, 'fa-globe', 176),
(177, 'fa-wrench', 177),
(178, 'fa-tasks', 178),
(179, 'fa-filter', 179),
(180, 'fa-briefcase', 180),
(181, 'fa-arrows-alt', 181),
(182, 'fa-group', 182),
(183, 'fa-users', 183),
(184, 'fa-chain', 184),
(185, 'fa-link', 185),
(186, 'fa-cloud', 186),
(187, 'fa-flask', 187),
(188, 'fa-cut', 188),
(189, 'fa-scissors', 189),
(190, 'fa-copy', 190),
(191, 'fa-files-o', 191),
(192, 'fa-paperclip', 192),
(193, 'fa-save', 193),
(194, 'fa-floppy-o', 194),
(195, 'fa-square', 195),
(196, 'fa-navicon', 196),
(197, 'fa-reorder', 197),
(198, 'fa-bars', 198),
(199, 'fa-list-ul', 199),
(200, 'fa-list-ol', 200),
(201, 'fa-strikethrough', 201),
(202, 'fa-underline', 202),
(203, 'fa-table', 203),
(204, 'fa-magic', 204),
(205, 'fa-truck', 205),
(206, 'fa-pinterest', 206),
(207, 'fa-pinterest-square', 207),
(208, 'fa-google-plus-squar', 208),
(209, 'fa-google-plus', 209),
(210, 'fa-money', 210),
(211, 'fa-caret-down', 211),
(212, 'fa-caret-up', 212),
(213, 'fa-caret-left', 213),
(214, 'fa-caret-right', 214),
(215, 'fa-columns', 215),
(216, 'fa-unsorted', 216),
(217, 'fa-sort', 217),
(218, 'fa-sort-down', 218),
(219, 'fa-sort-desc', 219),
(220, 'fa-sort-up', 220),
(221, 'fa-sort-asc', 221),
(222, 'fa-envelope', 222),
(223, 'fa-linkedin', 223),
(224, 'fa-rotate-left', 224),
(225, 'fa-undo', 225),
(226, 'fa-legal', 226),
(227, 'fa-gavel', 227),
(228, 'fa-dashboard', 228),
(229, 'fa-tachometer', 229),
(230, 'fa-comment-o', 230),
(231, 'fa-comments-o', 231),
(232, 'fa-flash', 232),
(233, 'fa-bolt', 233),
(234, 'fa-sitemap', 234),
(235, 'fa-umbrella', 235),
(236, 'fa-paste', 236),
(237, 'fa-clipboard', 237),
(238, 'fa-lightbulb-o', 238),
(239, 'fa-exchange', 239),
(240, 'fa-cloud-download', 240),
(241, 'fa-cloud-upload', 241),
(242, 'fa-user-md', 242),
(243, 'fa-stethoscope', 243),
(244, 'fa-suitcase', 244),
(245, 'fa-bell-o', 245),
(246, 'fa-coffee', 246),
(247, 'fa-cutlery', 247),
(248, 'fa-file-text-o', 248),
(249, 'fa-building-o', 249),
(250, 'fa-hospital-o', 250),
(251, 'fa-ambulance', 251),
(252, 'fa-medkit', 252),
(253, 'fa-fighter-jet', 253),
(254, 'fa-beer', 254),
(255, 'fa-h-square', 255),
(256, 'fa-plus-square', 256),
(257, 'fa-angle-double-left', 257),
(258, 'fa-angle-double-righ', 258),
(259, 'fa-angle-double-up', 259),
(260, 'fa-angle-double-down', 260),
(261, 'fa-angle-left', 261),
(262, 'fa-angle-right', 262),
(263, 'fa-angle-up', 263),
(264, 'fa-angle-down', 264),
(265, 'fa-desktop', 265),
(266, 'fa-laptop', 266),
(267, 'fa-tablet', 267),
(268, 'fa-mobile-phone', 268),
(269, 'fa-mobile', 269),
(270, 'fa-circle-o', 270),
(271, 'fa-quote-left', 271),
(272, 'fa-quote-right', 272),
(273, 'fa-spinner', 273),
(274, 'fa-circle', 274),
(275, 'fa-mail-reply', 275),
(276, 'fa-reply', 276),
(277, 'fa-github-alt', 277),
(278, 'fa-folder-o', 278),
(279, 'fa-folder-open-o', 279),
(280, 'fa-smile-o', 280),
(281, 'fa-frown-o', 281),
(282, 'fa-meh-o', 282),
(283, 'fa-gamepad', 283),
(284, 'fa-keyboard-o', 284),
(285, 'fa-flag-o', 285),
(286, 'fa-flag-checkered', 286),
(287, 'fa-terminal', 287),
(288, 'fa-code', 288),
(289, 'fa-mail-reply-all', 289),
(290, 'fa-reply-all', 290),
(291, 'fa-star-half-empty', 291),
(292, 'fa-star-half-full', 292),
(293, 'fa-star-half-o', 293),
(294, 'fa-location-arrow', 294),
(295, 'fa-crop', 295),
(296, 'fa-code-fork', 296),
(297, 'fa-unlink', 297),
(298, 'fa-chain-broken', 298),
(299, 'fa-question', 299),
(300, 'fa-info', 300),
(301, 'fa-exclamation', 301),
(302, 'fa-superscript', 302),
(303, 'fa-subscript', 303),
(304, 'fa-eraser', 304),
(305, 'fa-puzzle-piece', 305),
(306, 'fa-microphone', 306),
(307, 'fa-microphone-slash', 307),
(308, 'fa-shield', 308),
(309, 'fa-calendar-o', 309),
(310, 'fa-fire-extinguisher', 310),
(311, 'fa-rocket', 311),
(312, 'fa-maxcdn', 312),
(313, 'fa-chevron-circle-le', 313),
(314, 'fa-chevron-circle-ri', 314),
(315, 'fa-chevron-circle-up', 315),
(316, 'fa-chevron-circle-do', 316),
(317, 'fa-html5', 317),
(318, 'fa-css3', 318),
(319, 'fa-anchor', 319),
(320, 'fa-unlock-alt', 320),
(321, 'fa-bullseye', 321),
(322, 'fa-ellipsis-h', 322),
(323, 'fa-ellipsis-v', 323),
(324, 'fa-rss-square', 324),
(325, 'fa-play-circle', 325),
(326, 'fa-ticket', 326),
(327, 'fa-minus-square', 327),
(328, 'fa-minus-square-o', 328),
(329, 'fa-level-up', 329),
(330, 'fa-level-down', 330),
(331, 'fa-check-square', 331),
(332, 'fa-pencil-square', 332),
(333, 'fa-external-link-squ', 333),
(334, 'fa-share-square', 334),
(335, 'fa-compass', 335),
(336, 'fa-toggle-down', 336),
(337, 'fa-caret-square-o-do', 337),
(338, 'fa-toggle-up', 338),
(339, 'fa-caret-square-o-up', 339),
(340, 'fa-toggle-right', 340),
(341, 'fa-caret-square-o-ri', 341),
(342, 'fa-euro', 342),
(343, 'fa-eur', 343),
(344, 'fa-gbp', 344),
(345, 'fa-dollar', 345),
(346, 'fa-usd', 346),
(347, 'fa-rupee', 347),
(348, 'fa-inr', 348),
(349, 'fa-cny', 349),
(350, 'fa-rmb', 350),
(351, 'fa-yen', 351),
(352, 'fa-jpy', 352),
(353, 'fa-ruble', 353),
(354, 'fa-rouble', 354),
(355, 'fa-rub', 355),
(356, 'fa-won', 356),
(357, 'fa-krw', 357),
(358, 'fa-bitcoin', 358),
(359, 'fa-btc', 359),
(360, 'fa-file', 360),
(361, 'fa-file-text', 361),
(362, 'fa-sort-alpha-asc', 362),
(363, 'fa-sort-alpha-desc', 363),
(364, 'fa-sort-amount-asc', 364),
(365, 'fa-sort-amount-desc', 365),
(366, 'fa-sort-numeric-asc', 366),
(367, 'fa-sort-numeric-desc', 367),
(368, 'fa-thumbs-up', 368),
(369, 'fa-thumbs-down', 369),
(370, 'fa-youtube-square', 370),
(371, 'fa-youtube', 371),
(372, 'fa-xing', 372),
(373, 'fa-xing-square', 373),
(374, 'fa-youtube-play', 374),
(375, 'fa-dropbox', 375),
(376, 'fa-stack-overflow', 376),
(377, 'fa-instagram', 377),
(378, 'fa-flickr', 378),
(379, 'fa-adn', 379),
(380, 'fa-bitbucket', 380),
(381, 'fa-bitbucket-square', 381),
(382, 'fa-tumblr', 382),
(383, 'fa-tumblr-square', 383),
(384, 'fa-long-arrow-down', 384),
(385, 'fa-long-arrow-up', 385),
(386, 'fa-long-arrow-left', 386),
(387, 'fa-long-arrow-right', 387),
(388, 'fa-apple', 388),
(389, 'fa-windows', 389),
(390, 'fa-android', 390),
(391, 'fa-linux', 391),
(392, 'fa-dribbble', 392),
(393, 'fa-skype', 393),
(394, 'fa-foursquare', 394),
(395, 'fa-trello', 395),
(396, 'fa-female', 396),
(397, 'fa-male', 397),
(398, 'fa-gittip', 398),
(399, 'fa-gratipay', 399),
(400, 'fa-sun-o', 400),
(401, 'fa-moon-o', 401),
(402, 'fa-archive', 402),
(403, 'fa-bug', 403),
(404, 'fa-vk', 404),
(405, 'fa-weibo', 405),
(406, 'fa-renren', 406),
(407, 'fa-pagelines', 407),
(408, 'fa-stack-exchange', 408),
(409, 'fa-arrow-circle-o-ri', 409),
(410, 'fa-arrow-circle-o-le', 410),
(411, 'fa-toggle-left', 411),
(412, 'fa-caret-square-o-le', 412),
(413, 'fa-dot-circle-o', 413),
(414, 'fa-wheelchair', 414),
(415, 'fa-vimeo-square', 415),
(416, 'fa-turkish-lira', 416),
(417, 'fa-try', 417),
(418, 'fa-plus-square-o', 418),
(419, 'fa-space-shuttle', 419),
(420, 'fa-slack', 420),
(421, 'fa-envelope-square', 421),
(422, 'fa-wordpress', 422),
(423, 'fa-openid', 423),
(424, 'fa-institution', 424),
(425, 'fa-bank', 425),
(426, 'fa-university', 426),
(427, 'fa-mortar-board', 427),
(428, 'fa-graduation-cap', 428),
(429, 'fa-yahoo', 429),
(430, 'fa-google', 430),
(431, 'fa-reddit', 431),
(432, 'fa-reddit-square', 432),
(433, 'fa-stumbleupon-circl', 433),
(434, 'fa-stumbleupon', 434),
(435, 'fa-delicious', 435),
(436, 'fa-digg', 436),
(437, 'fa-pied-piper', 437),
(438, 'fa-pied-piper-alt', 438),
(439, 'fa-drupal', 439),
(440, 'fa-joomla', 440),
(441, 'fa-language', 441),
(442, 'fa-fax', 442),
(443, 'fa-building', 443),
(444, 'fa-child', 444),
(445, 'fa-paw', 445),
(446, 'fa-spoon', 446),
(447, 'fa-cube', 447),
(448, 'fa-cubes', 448),
(449, 'fa-behance', 449),
(450, 'fa-behance-square', 450),
(451, 'fa-steam', 451),
(452, 'fa-steam-square', 452),
(453, 'fa-recycle', 453),
(454, 'fa-automobile', 454),
(455, 'fa-car', 455),
(456, 'fa-cab', 456),
(457, 'fa-taxi', 457),
(458, 'fa-tree', 458),
(459, 'fa-spotify', 459),
(460, 'fa-deviantart', 460),
(461, 'fa-soundcloud', 461),
(462, 'fa-database', 462),
(463, 'fa-file-pdf-o', 463),
(464, 'fa-file-word-o', 464),
(465, 'fa-file-excel-o', 465),
(466, 'fa-file-powerpoint-o', 466),
(467, 'fa-file-photo-o', 467),
(468, 'fa-file-picture-o', 468),
(469, 'fa-file-image-o', 469),
(470, 'fa-file-zip-o', 470),
(471, 'fa-file-archive-o', 471),
(472, 'fa-file-sound-o', 472),
(473, 'fa-file-audio-o', 473),
(474, 'fa-file-movie-o', 474),
(475, 'fa-file-video-o', 475),
(476, 'fa-file-code-o', 476),
(477, 'fa-vine', 477),
(478, 'fa-codepen', 478),
(479, 'fa-jsfiddle', 479),
(480, 'fa-life-bouy', 480),
(481, 'fa-life-buoy', 481),
(482, 'fa-life-saver', 482),
(483, 'fa-support', 483),
(484, 'fa-life-ring', 484),
(485, 'fa-circle-o-notch', 485),
(486, 'fa-ra', 486),
(487, 'fa-rebel', 487),
(488, 'fa-ge', 488),
(489, 'fa-empire', 489),
(490, 'fa-git-square', 490),
(491, 'fa-git', 491),
(492, 'fa-hacker-news', 492),
(493, 'fa-tencent-weibo', 493),
(494, 'fa-qq', 494),
(495, 'fa-wechat', 495),
(496, 'fa-weixin', 496),
(497, 'fa-send', 497),
(498, 'fa-paper-plane', 498),
(499, 'fa-send-o', 499),
(500, 'fa-paper-plane-o', 500),
(501, 'fa-history', 501),
(502, 'fa-genderless', 502),
(503, 'fa-circle-thin', 503),
(504, 'fa-header', 504),
(505, 'fa-paragraph', 505),
(506, 'fa-sliders', 506),
(507, 'fa-share-alt', 507),
(508, 'fa-share-alt-square', 508),
(509, 'fa-bomb', 509),
(510, 'fa-soccer-ball-o', 510),
(511, 'fa-futbol-o', 511),
(512, 'fa-tty', 512),
(513, 'fa-binoculars', 513),
(514, 'fa-plug', 514),
(515, 'fa-slideshare', 515),
(516, 'fa-twitch', 516),
(517, 'fa-yelp', 517),
(518, 'fa-newspaper-o', 518),
(519, 'fa-wifi', 519),
(520, 'fa-calculator', 520),
(521, 'fa-paypal', 521),
(522, 'fa-google-wallet', 522),
(523, 'fa-cc-visa', 523),
(524, 'fa-cc-mastercard', 524),
(525, 'fa-cc-discover', 525),
(526, 'fa-cc-amex', 526),
(527, 'fa-cc-paypal', 527),
(528, 'fa-cc-stripe', 528),
(529, 'fa-bell-slash', 529),
(530, 'fa-bell-slash-o', 530),
(531, 'fa-trash', 531),
(532, 'fa-copyright', 532),
(533, 'fa-at', 533),
(534, 'fa-eyedropper', 534),
(535, 'fa-paint-brush', 535),
(536, 'fa-birthday-cake', 536),
(537, 'fa-area-chart', 537),
(538, 'fa-pie-chart', 538),
(539, 'fa-line-chart', 539),
(540, 'fa-lastfm', 540),
(541, 'fa-lastfm-square', 541),
(542, 'fa-toggle-off', 542),
(543, 'fa-toggle-on', 543),
(544, 'fa-bicycle', 544),
(545, 'fa-bus', 545),
(546, 'fa-ioxhost', 546),
(547, 'fa-angellist', 547),
(548, 'fa-cc za', 548),
(549, 'fa-shekel', 549),
(550, 'fa-sheqel', 550),
(551, 'fa-ils', 551),
(552, 'fa-meanpath', 552),
(553, 'fa-buysellads', 553),
(554, 'fa-connectdevelop', 554),
(555, 'fa-dashcube', 555),
(556, 'fa-forumbee', 556),
(557, 'fa-leanpub', 557),
(558, 'fa-sellsy', 558),
(559, 'fa-shirtsinbulk', 559),
(560, 'fa-simplybuilt', 560),
(561, 'fa-skyatlas', 561),
(562, 'fa-cart-plus', 562),
(563, 'fa-cart-arrow-down', 563),
(564, 'fa-diamond', 564),
(565, 'fa-ship', 565),
(566, 'fa-user-secret', 566),
(567, 'fa-motorcycle', 567),
(568, 'fa-street-view', 568),
(569, 'fa-heartbeat', 569),
(570, 'fa-venus', 570),
(571, 'fa-mars', 571),
(572, 'fa-mercury', 572),
(573, 'fa-transgender', 573),
(574, 'fa-transgender-alt', 574),
(575, 'fa-venus-double', 575),
(576, 'fa-mars-double', 576),
(577, 'fa-venus-mars', 577),
(578, 'fa-mars-stroke', 578),
(579, 'fa-mars-stroke-v', 579),
(580, 'fa-mars-stroke-h', 580),
(581, 'fa-neuter', 581),
(582, 'fa-facebook-official', 582),
(583, 'fa-pinterest-p', 583),
(584, 'fa-whatsapp', 584),
(585, 'fa-server', 585),
(586, 'fa-user-plus', 586),
(587, 'fa-user-times', 587),
(588, 'fa-hotel', 588),
(589, 'fa-bed', 589),
(590, 'fa-viacoin', 590),
(591, 'fa-train', 591),
(592, 'fa-subway', 592),
(593, 'fa-medium', 593),
(594, 'fa-snowflake', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE `gallary` (
  `id` int(11) NOT NULL,
  `thumb` varchar(150) NOT NULL,
  `img` varchar(150) NOT NULL,
  `categories` int(11) DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallary`
--

INSERT INTO `gallary` (`id`, `thumb`, `img`, `categories`, `title`, `rank`, `deleted`) VALUES
(3, '', 'DSC_3104-min.jpg', 86, 'zahabia resort gallery images', 1, 0),
(4, '', 'DSC_3122-min.jpg', 86, 'zahabia resort gallery images', 2, 0),
(5, '', 'DSC_3115-min.jpg', 86, 'zahabia resort gallery images', 3, 0),
(6, '', 'DSC_3137-min.jpg', 86, 'zahabia resort gallery images', 4, 0),
(7, '', 'DSC_3140-min.jpg', 86, 'zahabia resort gallery images', 5, 0),
(8, '', 'DSC_3198-min.jpg', 86, 'zahabia resort gallery images', 6, 0),
(9, '', 'DSC_2969-min.jpg', 87, 'zahabia resort gallery images', 7, 0),
(10, '', 'DSC_3031-min.jpg', 87, 'zahabia resort gallery images', 8, 0),
(11, '', 'DSC_3091-min.jpg', 86, 'zahabia resort gallery images', 9, 0),
(12, '', 'DSC_3113-min.jpg', 86, 'zahabia resort gallery images', 10, 0),
(13, '', 'DSC_3257-min.jpg', 87, 'zahabia resort gallery images', 11, 0),
(14, '', 'DSC_3157-min.jpg', 86, 'zahabia resort gallery images', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `deleted`) VALUES
(1, 'Pharaoh Azur Beach Resort', 0),
(2, 'SUNRISE Garden Beach Resort -Select-', 0),
(3, 'SENTIDO Mamlouk Palace', 0),
(4, 'SUNRISE Crystal Bay Resort- Grand Select-', 0),
(5, 'SUNRISE Royal Makadi Aqua Resort', 0);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `english` text COLLATE utf8_unicode_ci NOT NULL,
  `german` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `french` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `arabic` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `english`, `german`, `french`, `arabic`, `updated_at`, `timestamp`) VALUES
(1, 'Dahabeya', 'Dahabeya', 'Dahabeya', 'دهبية', '', '2021-06-25 13:51:59'),
(2, 'Welcome To Queen-Farida', 'Willkommen bei Queen-Farida', 'Bienvenue à Reine-Farida', 'مرحبًا بكم في الملكة فريدة', '', '2021-06-25 13:53:48'),
(3, 'Queen Farida', 'Königin Farida', 'Reine Farida', 'الملكة فريدة', '', '2021-06-25 14:00:47'),
(4, 'English', 'Englisch', 'Anglais', 'الإنجليزية', '', '2021-06-25 14:00:47'),
(5, 'French', 'Französisch', 'Français', 'الفرنسية', '', '2021-06-25 14:00:47'),
(6, 'German', 'Deutsche', 'Allemand', 'الألمانية', '', '2021-06-25 14:00:47'),
(7, 'Arabic', 'Arabisch', 'Arabe', 'العربى', '', '2021-06-25 14:00:47'),
(8, 'Home', 'Zuhause', 'Domicile', 'الصفحة الرئيسية', '', '2021-06-25 14:00:47'),
(9, 'standy', NULL, NULL, NULL, '', '2022-01-29 00:31:14'),
(10, 'test required', NULL, NULL, NULL, '', '2022-01-29 00:31:14'),
(11, 'requiredrequired', NULL, NULL, NULL, '', '2022-01-29 00:31:14'),
(12, ' ', NULL, NULL, NULL, '', '2022-01-30 00:17:29'),
(13, 'test offer', NULL, NULL, NULL, '', '2022-01-30 01:15:31'),
(14, 'test offer test offer test offer', NULL, NULL, NULL, '', '2022-01-30 01:15:31'),
(15, 'testy mesty', NULL, NULL, NULL, '', '2022-01-30 01:29:58'),
(16, 'software test', NULL, NULL, NULL, '', '2022-01-30 01:30:07'),
(17, 'A2 Villa 1 King bedroom 1 Twin bedroom', NULL, NULL, NULL, '', '2022-01-30 01:37:28'),
(18, 'One king bed and two queen beds', NULL, NULL, NULL, '', '2022-01-30 01:37:28'),
(19, 'from 85m to 95m, including balcony', NULL, NULL, NULL, '', '2022-01-30 01:37:28'),
(20, 'About Us', NULL, NULL, NULL, '', '2022-01-30 01:44:01'),
(21, 'Zahabia Hotel and Beach Resort is not only a remarkable place for an amazing holidays but also it is a place filled with welcoming staff, lovely food, astonishing design and unforgettable atmosphere.....\r\nWe are an established hotel with the benefit of its own private beach overlooking the famous Giftun Island.\r\nThe development comprises an exclusive collection', NULL, NULL, NULL, '', '2022-01-30 01:44:01'),
(22, 'Zahabia Hotel and Beach Resort is not only a remarkable place for an amazing holidays but also it is a place filled with welcoming staff, lovely food, astonishing design and unforgettable atmosphere.\r\nWe are an established hotel with the benefit of its own private beach overlooking the famous Giftun Island.\r\nThe development comprises an exclusive collection', NULL, NULL, NULL, '', '2022-01-30 01:44:08'),
(23, 'About us points', NULL, NULL, NULL, '', '2022-01-30 17:45:58'),
(24, 'Located just 10 minutes drive from Hurghada International Airport, at the heart of Hurghada. Sheraton Street, the main shopping street in Hurghada, is just 20 minutes walk away, as is the new marina and marine port. Here you will find an abundance of shops, restaurants, clubs and bars, the majority with friendly English & Russian speaking staff.', NULL, NULL, NULL, '', '2022-01-30 17:46:10');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mini_target_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_data` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `new_items` text COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `action`, `module_id`, `target`, `target_id`, `mini_target_id`, `data`, `new_data`, `items`, `new_items`, `comments`, `ip`, `log_time`) VALUES
(1, 1, 'Create', 1, 'Sliders', '11', 0, '0', '{\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":0}', '0', '0', 'added slider: 11', '172.50.28.253()', '2021-07-07 15:15:21'),
(2, 1, 'Delete', 1, 'Sliders', '11', 0, '{\"id\":\"11\",\"thumb\":\"\",\"img\":\"\",\"video\":null,\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":\"0\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted slider:11', '172.50.28.253()', '2021-07-07 15:16:51'),
(3, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:22'),
(4, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:30'),
(5, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:39'),
(6, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:46'),
(7, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:39:18'),
(8, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:40:00'),
(9, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:40:09'),
(10, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:40:13'),
(11, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:41:06'),
(12, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:41:25'),
(13, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:41:31'),
(14, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:55:26'),
(15, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:55:29'),
(16, 1, 'Create', 6, 'العملاء', '8', 0, '0', '{\"location\":\"Enter Location\",\"describtion\":\"Enter Describtion\",\"rank\":0}', '0', '0', 'added Event: 8', '::1()', '2021-07-16 12:47:51'),
(17, 1, 'update', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"Enter Location\",\"event\":\"\",\"describtion\":\"Enter Describtion\",\"rank\":\"0\",\"image\":\"\",\"toped\":\"0\",\"lefted\":\"0\",\"active\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"toped\":\"0\",\"lefted\":\"0\",\"rank\":\"9\"}', '0', '0', 'updated Event: 8', '::1()', '2021-07-16 12:48:58'),
(18, 1, 'update', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"rank\":\"9\",\"image\":\"\",\"toped\":\"0\",\"lefted\":\"0\",\"active\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"toped\":\"0\",\"lefted\":\"0\",\"rank\":\"9\",\"image\":\"New_Project.png\"}', '0', '0', 'updated Event: 8', '::1()', '2021-07-16 12:49:38'),
(19, 1, 'update', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"rank\":\"9\",\"image\":\"New_Project.png\",\"toped\":\"0\",\"lefted\":\"0\",\"active\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"toped\":\"650\",\"lefted\":\"400\",\"rank\":\"9\"}', '0', '0', 'updated Event: 8', '::1()', '2021-07-16 12:50:19'),
(20, 1, 'Delete', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"rank\":\"9\",\"image\":\"New_Project.png\",\"toped\":\"650\",\"lefted\":\"400\",\"active\":\"1\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted Event:8', '::1()', '2021-07-16 12:53:01'),
(21, 1, 'Status Change', 5, 'Booking', '29', 0, '0', '0', '0', '0', 'Changed booking status:29', '::1()', '2022-01-28 20:06:01'),
(22, 1, 'Status Change', 5, 'Booking', '29', 0, '0', '0', '0', '0', 'Changed booking status:29', '::1()', '2022-01-28 20:06:06'),
(23, 1, 'Status Change', 5, 'Booking', '29', 0, '0', '0', '0', '0', 'Changed booking status:29', '::1()', '2022-01-28 20:08:11'),
(24, 1, 'Status Change', 5, 'Booking', '2', 0, '0', '0', '0', '0', 'Changed booking status:2', '::1()', '2022-01-28 20:14:00'),
(25, 1, 'Active Change', 15, 'Rooms', '9', 0, '0', '0', '0', '0', 'Changed room active:9', '::1()', '2022-01-28 22:48:37'),
(26, 1, 'Active Change', 15, 'Rooms', '9', 0, '0', '0', '0', '0', 'Changed room active:9', '::1()', '2022-01-28 22:49:10'),
(27, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"4096.jpg\"}', '0', '0', '0', 'added file:4096.jpg', '::1()', '2022-01-28 23:05:02'),
(28, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f.jpg', '::1()', '2022-01-29 00:29:54'),
(29, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"4096.jpg\"}', '0', '0', '0', 'added file:4096.jpg', '::1()', '2022-01-29 00:29:54'),
(30, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f1.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f1.jpg', '::1()', '2022-01-29 00:31:02'),
(31, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"40961.jpg\"}', '0', '0', '0', 'added file:40961.jpg', '::1()', '2022-01-29 00:31:02'),
(32, 1, 'Update', 15, 'Files', '10', 0, '[{\"id\":\"13\",\"module_id\":\"0\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"4096.jpg\",\"timestamp\":\"2022-01-29 01:05:02\"},{\"id\":\"14\",\"module_id\":\"0\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f.jpg\",\"timestamp\":\"2022-01-29 02:29:54\"},{\"id\":\"15\",\"module_id\":\"0\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"4096.jpg\",\"timestamp\":\"2022-01-29 02:29:54\"},{\"id\":\"16\",\"module_id\":\"0\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f1.jpg\",\"timestamp\":\"2022-01-29 02:31:02\"},{\"id\":\"17\",\"module_id\":\"0\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"40961.jpg\",\"timestamp\":\"2022-01-29 02:31:02\"}]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 10', '::1()', '2022-01-29 00:31:14'),
(33, 1, 'Create', 15, 'Rooms', '10', 0, '0', '{\"room\":\"standy\",\"inventory\":\"5\",\"category\":\"52\",\"rank\":\"1\",\"price\":\"1200\",\"tax\":\"120\",\"adult\":\"2\",\"child\":\"2\",\"bed\":\"test required\",\"space\":\"requiredrequired\"}', '0', '0', 'added Info: 10', '::1()', '2022-01-29 00:31:14'),
(34, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"4096.jpg\"}', '0', '0', '0', 'added file:4096.jpg', '::1()', '2022-01-29 00:36:47'),
(35, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"40961.jpg\"}', '0', '0', '0', 'added file:40961.jpg', '::1()', '2022-01-29 00:39:39'),
(36, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f.jpg', '::1()', '2022-01-29 00:42:35'),
(37, 1, 'Update', 15, 'Files', '11', 0, '[{\"id\":\"18\",\"module_id\":\"0\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"4096.jpg\",\"timestamp\":\"2022-01-29 02:36:46\"},{\"id\":\"19\",\"module_id\":\"15\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"40961.jpg\",\"timestamp\":\"2022-01-29 02:39:39\"},{\"id\":\"20\",\"module_id\":\"15\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f.jpg\",\"timestamp\":\"2022-01-29 02:42:35\"}]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 11', '::1()', '2022-01-29 00:42:39'),
(38, 1, 'Create', 15, 'Rooms', '11', 0, '0', '{\"room\":\"standy\",\"inventory\":\"2\",\"category\":\"53\",\"rank\":\"2\",\"price\":\"500\",\"tax\":\"55\",\"adult\":\"2\",\"child\":\"2\",\"bed\":\"test required\",\"space\":\"requiredrequired\"}', '0', '0', 'added Info: 11', '::1()', '2022-01-29 00:42:39'),
(39, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"40962.jpg\"}', '0', '0', '0', 'added file:40962.jpg', '::1()', '2022-01-29 19:02:19'),
(40, 1, 'Create', 15, 'Files', 'I20221', 0, '{\"table\":\"site_files\",\"file_name\":\"childrens-robotics-classes.webp\"}', '0', '0', '0', 'added file:childrens-robotics-classes.webp', '::1()', '2022-01-29 19:02:26'),
(41, 1, 'Update', 15, 'Files', '12', 0, '[{\"id\":\"21\",\"module_id\":\"15\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"40962.jpg\",\"timestamp\":\"2022-01-29 21:02:19\"},{\"id\":\"22\",\"module_id\":\"15\",\"form_id\":\"I20221\",\"user_id\":\"1\",\"file_name\":\"childrens-robotics-classes.webp\",\"timestamp\":\"2022-01-29 21:02:26\"}]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 12', '::1()', '2022-01-29 19:02:29'),
(42, 1, 'Create', 15, 'Rooms', '12', 0, '0', '{\"room\":\"standy\",\"inventory\":\"12\",\"category\":\"52\",\"rank\":\"1\",\"price\":\"100\",\"tax\":\"10\",\"adult\":\"2\",\"child\":\"2\",\"bed\":\"test required\",\"space\":\"requiredrequired\"}', '0', '0', 'added Info: 12', '::1()', '2022-01-29 19:02:29'),
(43, 1, 'Delete', 15, 'Files', '12', 22, '{\"id\":\"22\",\"module_id\":\"15\",\"form_id\":\"12\",\"user_id\":\"1\",\"file_name\":\"childrens-robotics-classes.webp\",\"timestamp\":\"2022-01-29 21:02:26\"}', '{\"table\":\"site_files\"}', '0', '0', 'Deleted file No#.22from  site_files', '::1()', '2022-01-29 19:02:54'),
(44, 1, 'Delete', 15, 'Files', '12', 21, '{\"id\":\"21\",\"module_id\":\"15\",\"form_id\":\"12\",\"user_id\":\"1\",\"file_name\":\"40962.jpg\",\"timestamp\":\"2022-01-29 21:02:19\"}', '{\"table\":\"site_files\"}', '0', '0', 'Deleted file No#.21from  site_files', '::1()', '2022-01-29 19:02:57'),
(45, 1, 'Updated', 15, 'Rooms', '12', 0, '0', '0', '0', '0', 'Changed room:', '::1()', '2022-01-29 19:05:09'),
(46, 1, 'Updated', 15, 'Rooms', '12', 0, '0', '0', '0', '0', 'Changed room:', '::1()', '2022-01-29 19:05:28'),
(47, 1, 'Create', 15, 'Files', '12', 0, '{\"table\":\"site_files\",\"file_name\":\"40963.jpg\"}', '0', '0', '0', 'added file:40963.jpg', '::1()', '2022-01-29 19:05:40'),
(48, 1, 'Updated', 15, 'Rooms', '156', 0, '0', '0', '0', '0', 'Changed room feature:156', '::1()', '2022-01-30 00:04:01'),
(49, 1, 'Create', 15, 'Rooms', '157', 0, '0', '{\"room_id\":\"12\",\"feature\":\"software test\",\"category\":\"room features\"}', '0', '0', 'added Info: 157', '::1()', '2022-01-30 00:05:11'),
(50, 1, 'Updated', 15, 'Rooms', '156', 0, '0', '0', '0', '0', 'Changed room feature:156', '::1()', '2022-01-30 00:07:07'),
(51, 1, 'Delete', 15, 'Rooms', '156', 0, '{\"id\":\"156\",\"room_id\":\"12\",\"feature\":\"testy mesty\",\"category\":\"room amenities\",\"active\":\"0\",\"timestamp\":\"0\"}', '0', '0', '0', 'Deleted room feature:156', '::1()', '2022-01-30 00:08:31'),
(52, 1, 'Create', 15, 'Rooms', '158', 0, '0', '{\"room_id\":\"12\",\"feature\":\"testy mesty\",\"category\":\"room amenities\"}', '0', '0', 'added Info: 158', '::1()', '2022-01-30 00:08:53'),
(53, 1, 'Create', 15, 'Rooms', '159', 0, '0', '{\"room_id\":\"12\",\"feature\":\"testy mesty\",\"category\":\"room features\"}', '0', '0', 'added Info: 159', '::1()', '2022-01-30 00:09:05'),
(54, 1, 'Create', 15, 'Rooms', '160', 0, '0', '{\"room_id\":\"12\",\"feature\":\"testy mesty\",\"category\":\"room amenities\"}', '0', '0', 'added Info: 160', '::1()', '2022-01-30 00:10:24'),
(55, 1, 'Active Change', 2, 'competitions', '3', 0, '0', '0', '0', '0', 'Changed offer active:3', '::1()', '2022-01-30 00:44:05'),
(56, 1, 'Active Change', 2, 'competitions', '3', 0, '0', '0', '0', '0', 'Changed offer active:3', '::1()', '2022-01-30 00:44:21'),
(57, 1, 'Delete', 15, 'Rooms', '158', 0, '{\"id\":\"158\",\"room_id\":\"12\",\"feature\":\"testy mesty\",\"category\":\"room amenities\",\"active\":\"0\",\"timestamp\":\"0\"}', '0', '0', '0', 'Deleted room feature:158', '::1()', '2022-01-30 01:11:19'),
(58, 1, 'Delete', 15, 'Rooms', '160', 0, '{\"id\":\"160\",\"room_id\":\"12\",\"feature\":\"testy mesty\",\"category\":\"room amenities\",\"active\":\"0\",\"timestamp\":\"0\"}', '0', '0', '0', 'Deleted room feature:160', '::1()', '2022-01-30 01:11:21'),
(59, 1, 'Create', 2, 'competitions', '5', 0, '0', '{\"title\":\"test offer\",\"description\":\"test offer test offer test offer\",\"price\":\"1500\",\"tax\":\"200\",\"currency\":\"USD\",\"nights\":\"3\",\"adults\":\"2\",\"childs\":\"1\",\"image\":false}', '0', '0', 'added offer: 5', '::1()', '2022-01-30 01:15:31'),
(60, 1, 'Active Change', 2, 'competitions', '5', 0, '0', '0', '0', '0', 'Changed offer active:5', '::1()', '2022-01-30 01:17:07'),
(61, 1, 'Updated', 2, 'competitions', '5', 0, '0', '0', '0', '0', 'Changed offer:', '::1()', '2022-01-30 01:19:26'),
(62, 1, 'Create', 2, 'competitions', '14', 0, '0', '{\"offer_id\":\"5\",\"feature\":\"testy mesty\",\"category\":\"included\"}', '0', '0', 'added Info: 14', '::1()', '2022-01-30 01:29:58'),
(63, 1, 'Create', 2, 'competitions', '15', 0, '0', '{\"offer_id\":\"5\",\"feature\":\"software test\",\"category\":\"conditions\"}', '0', '0', 'added Info: 15', '::1()', '2022-01-30 01:30:07'),
(64, 1, 'Updated', 2, 'competitions', '5', 0, '0', '0', '0', '0', 'Changed offer:', '::1()', '2022-01-30 01:33:02'),
(65, 1, 'Delete', 2, 'competitions', '14', 0, '{\"id\":\"14\",\"offer_id\":\"5\",\"feature\":\"testy mesty\",\"category\":\"included\",\"active\":\"0\",\"timestamp\":\"0\"}', '0', '0', '0', 'Deleted offer feature:14', '::1()', '2022-01-30 01:33:43'),
(66, 1, 'Create', 2, 'competitions', '16', 0, '0', '{\"offer_id\":\"5\",\"feature\":\"testy mesty\",\"category\":\"included\"}', '0', '0', 'added Info: 16', '::1()', '2022-01-30 01:33:52'),
(67, 1, 'Updated', 2, 'competitions', '16', 0, '0', '0', '0', '0', 'Changed offer feature:16', '::1()', '2022-01-30 01:33:59'),
(68, 1, 'Delete', 2, 'competitions', '16', 0, '{\"id\":\"16\",\"offer_id\":\"5\",\"feature\":\"testy mesty\",\"category\":\"conditions\",\"active\":\"0\",\"timestamp\":\"0\"}', '0', '0', '0', 'Deleted offer feature:16', '::1()', '2022-01-30 01:34:06'),
(69, 1, 'Delete', 2, 'competitions', '15', 0, '{\"id\":\"15\",\"offer_id\":\"5\",\"feature\":\"software test\",\"category\":\"conditions\",\"active\":\"0\",\"timestamp\":\"0\"}', '0', '0', '0', 'Deleted offer feature:15', '::1()', '2022-01-30 01:34:09'),
(70, 1, 'Create', 2, 'competitions', '17', 0, '0', '{\"offer_id\":\"5\",\"feature\":\"testy mesty\",\"category\":\"included\"}', '0', '0', 'added Info: 17', '::1()', '2022-01-30 01:34:17'),
(71, 1, 'Create', 2, 'competitions', '18', 0, '0', '{\"offer_id\":\"5\",\"feature\":\"software test\",\"category\":\"conditions\"}', '0', '0', 'added Info: 18', '::1()', '2022-01-30 01:34:24'),
(72, 1, 'Updated', 15, 'Rooms', '9', 0, '0', '0', '0', '0', 'Changed room:', '::1()', '2022-01-30 01:37:28'),
(73, 1, 'Updated', 15, 'Rooms', '9', 0, '0', '0', '0', '0', 'Changed room:', '::1()', '2022-01-30 01:38:23'),
(74, 1, 'Updated', 14, 'About Us', '46', 0, '0', '0', '0', '0', 'Changed winner:46', '::1()', '2022-01-30 01:44:01'),
(75, 1, 'Updated', 14, 'About Us', '46', 0, '0', '0', '0', '0', 'Changed winner:46', '::1()', '2022-01-30 01:44:08'),
(76, 1, 'Updated', 14, 'About Us', '47', 0, '0', '0', '0', '0', 'Changed winner:47', '::1()', '2022-01-30 17:45:58'),
(77, 1, 'Updated', 14, 'About Us', '47', 0, '0', '0', '0', '0', 'Changed winner:47', '::1()', '2022-01-30 17:46:10'),
(78, 1, 'update', 1, 'Sliders', '16', 0, '{\"id\":\"16\",\"thumb\":\"04-b_08.jpg\",\"img\":\"DSC_3131-min.jpg\",\"video\":null,\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"16\",\"first_title\":\"en\",\"second_title\":\"ennnnnnnnnnnnnn\"}', '0', '0', 'updated slider: 16', '::1()', '2022-01-30 18:04:10'),
(79, 1, 'update', 1, 'Sliders', '16', 0, '{\"id\":\"16\",\"thumb\":\"04-b_08.jpg\",\"img\":\"DSC_3131-min.jpg\",\"video\":null,\"first_title\":\"en\",\"second_title\":\"ennnnnnnnnnnnnn\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"16\",\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience\"}', '0', '0', 'updated slider: 16', '::1()', '2022-01-30 18:04:36'),
(80, 1, 'update', 1, 'Sliders', '16', 0, '{\"id\":\"16\",\"thumb\":\"04-b_08.jpg\",\"img\":\"DSC_3131-min.jpg\",\"video\":null,\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"16\",\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory  Enjoy A luxory Experience Enjoy A luxory Experience\"}', '0', '0', 'updated slider: 16', '::1()', '2022-01-30 18:15:26'),
(81, 1, 'update', 1, 'Sliders', '16', 0, '{\"id\":\"16\",\"thumb\":\"04-b_08.jpg\",\"img\":\"DSC_3131-min.jpg\",\"video\":null,\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory  Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"16\",\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience  Enjoy A luxory Experience Enjoy A luxory Experience\"}', '0', '0', 'updated slider: 16', '::1()', '2022-01-30 18:15:34'),
(82, 1, 'update', 1, 'Sliders', '16', 0, '{\"id\":\"16\",\"thumb\":\"04-b_08.jpg\",\"img\":\"DSC_3131-min.jpg\",\"video\":null,\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience  Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"16\",\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience  Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"5\"}', '0', '0', 'updated slider: 16', '::1()', '2022-01-30 18:33:36'),
(83, 1, 'update', 1, 'Sliders', '16', 0, '{\"id\":\"16\",\"thumb\":\"04-b_08.jpg\",\"img\":\"DSC_3131-min.jpg\",\"video\":null,\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience  Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"5\",\"deleted\":\"0\"}', '{\"id\":\"16\",\"first_title\":\"Enjoy A luxory Experience\",\"second_title\":\"Enjoy A luxory Experience  Enjoy A luxory Experience Enjoy A luxory Experience\",\"rank\":\"3\"}', '0', '0', 'updated slider: 16', '::1()', '2022-01-30 18:34:27'),
(84, 1, 'Create', 1, 'Sliders', '17', 0, '0', '{\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":0}', '0', '0', 'added slider: 17', '::1()', '2022-01-30 18:36:33'),
(85, 1, 'Delete', 1, 'Sliders', '17', 0, '{\"id\":\"17\",\"thumb\":\"\",\"img\":\"\",\"video\":null,\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":\"0\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted slider:17', '::1()', '2022-01-30 18:38:19'),
(86, 1, 'Create', 1, 'Sliders', '18', 0, '0', '{\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":0}', '0', '0', 'added slider: 18', '::1()', '2022-01-30 18:39:52'),
(87, 1, 'update', 1, 'Sliders', '18', 0, '{\"id\":\"18\",\"thumb\":\"\",\"img\":\"\",\"video\":null,\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"18\",\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":\"0\",\"img\":\"4096.jpg\"}', '0', '0', 'updated slider: 18', '::1()', '2022-01-30 18:40:06'),
(88, 1, 'Delete', 1, 'Sliders', '18', 0, '{\"id\":\"18\",\"thumb\":\"\",\"img\":\"4096.jpg\",\"video\":null,\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":\"0\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted slider:18', '::1()', '2022-01-30 18:40:27'),
(89, 1, 'Create', 22, 'gallery', '1', 0, '0', '{\"title\":\"test\",\"categories\":null,\"rank\":\"1\",\"img\":false}', '0', '0', 'added gallery: 1', '::1()', '2022-02-02 18:48:06'),
(90, 1, 'Delete', 4, 'Gallery', '1', 0, '{\"id\":\"1\",\"thumb\":\"\",\"img\":\"0\",\"categories\":null,\"title\":\"test\",\"rank\":\"1\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted gallery:1', '::1()', '2022-02-02 19:07:29'),
(91, 1, 'Create', 4, 'Gallery', '2', 0, '0', '{\"title\":\"test\",\"categories\":\"86\",\"rank\":\"1\",\"img\":\"4096.jpg\"}', '0', '0', 'added gallary: 2', '::1()', '2022-02-02 19:08:13'),
(92, 1, 'Updated', 4, 'Gallery', '2', 0, '0', '0', '0', '0', 'Changed gallary:2', '::1()', '2022-02-02 19:16:53'),
(93, 1, 'Updated', 4, 'Gallery', '2', 0, '0', '0', '0', '0', 'Changed gallary:2', '::1()', '2022-02-02 19:17:55'),
(94, 1, 'Delete', 4, 'Gallery', '2', 0, '{\"id\":\"2\",\"thumb\":\"\",\"img\":\"af8d63a477078732b79ff9d9fc60873f1.jpg\",\"categories\":\"86\",\"title\":\"test 2\",\"rank\":\"12\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted gallery:2', '::1()', '2022-02-02 19:37:09'),
(95, 1, 'Updated', 4, 'Gallery', '13', 0, '0', '0', '0', '0', 'Changed gallary:13', '::1()', '2022-02-02 19:49:09'),
(96, 1, 'Updated', 4, 'Gallery', '10', 0, '0', '0', '0', '0', 'Changed gallary:10', '::1()', '2022-02-02 19:49:21'),
(97, 1, 'Updated', 4, 'Gallery', '9', 0, '0', '0', '0', '0', 'Changed gallary:9', '::1()', '2022-02-02 19:49:36');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `activity_id`, `hotel_id`, `sub_id`, `rank`, `deleted`) VALUES
(1, 1, 1, 6, 1, 0),
(2, 3, 1, 7, 2, 0),
(3, 2, 1, 1, 3, 0),
(4, 2, 2, 1, 4, 0),
(5, 2, 2, 2, 5, 0),
(6, 2, 2, 3, 6, 0),
(7, 2, 2, 4, 7, 0),
(8, 2, 2, 5, 8, 0),
(9, 2, 3, 1, 9, 0),
(10, 2, 3, 2, 10, 0),
(11, 2, 3, 3, 11, 0),
(12, 2, 3, 4, 12, 0),
(13, 2, 3, 5, 13, 0),
(14, 2, 4, 1, 14, 0),
(15, 2, 4, 2, 15, 0),
(16, 2, 4, 3, 16, 0),
(17, 2, 4, 4, 17, 0),
(18, 2, 4, 5, 18, 0),
(19, 2, 5, 1, 19, 0),
(20, 2, 5, 2, 20, 0),
(21, 2, 5, 3, 21, 0),
(22, 2, 5, 4, 22, 0),
(23, 2, 5, 5, 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creat` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `remove` tinyint(1) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `creat`, `edit`, `view`, `remove`, `link`, `type`, `rank`, `deleted`) VALUES
(1, 'Sliders', 1, 1, 1, 1, '', 'site', 1, 0),
(2, 'offers', 1, 1, 1, 1, '', 'site', 2, 0),
(4, 'Gallery', 1, 1, 1, 1, '', 'site', 4, 0),
(5, 'Booking', 1, 1, 1, 1, 'admin/site_manager/booking_manager', 'store', 5, 0),
(6, 'users', 1, 1, 1, 1, 'admin/users', 'store', 5, 0),
(8, 'reports', 1, 1, 1, 1, 'reports', 'store', 6, 0),
(11, 'logs', 1, 1, 1, 1, 'admin/log_activity', 'store', 6, 0),
(13, 'Site manager', 1, 1, 1, 1, 'admin/site_manager', 'store', 10, 0),
(14, 'About Us', 1, 1, 1, 1, '', 'site', 17, 0),
(15, 'Rooms', 1, 1, 1, 1, 'admin/site_manager/rooms_manager', 'store', 10, 0),
(17, 'portfolio', 1, 1, 1, 1, '', 'site', 12, 0),
(18, 'competitors', 1, 1, 1, 1, '', 'site', 13, 0),
(19, 'contact', 1, 1, 1, 1, '', 'site', 14, 0),
(20, 'Footer', 1, 1, 1, 1, '', 'site', 14, 0),
(21, 'Client Requests', 1, 1, 1, 1, '', 'site', 15, 0),
(22, 'gallery', 1, 1, 1, 1, '', 'site', 12, 0),
(23, 'team', 1, 1, 1, 1, '', 'site', 13, 0),
(25, 'language', 1, 1, 1, 1, '', 'site', 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `to_branch_id` int(11) DEFAULT NULL,
  `to_dep_id` int(11) DEFAULT NULL,
  `readedby` int(11) DEFAULT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(3) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(5) NOT NULL,
  `room_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nights` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childs` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `image` varchar(150) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `special` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `room_id`, `title`, `description`, `nights`, `adults`, `childs`, `rooms`, `image`, `price`, `currency`, `tax`, `special`, `active`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'Offers Anniversary', '', 3, 3, 2, 1, 'DSC_3198-min.jpg', '400', 'EGP', '0.00', 0, 1, 1, 0, '2022-01-04 20:25:01'),
(2, 2, 'Offers honeymoon', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet. Ut nec metus a mi ullamcorper hendrerit. Nulla facilisi. Pellentesque sed nibh a quam accumsan dignissim quis quis urna. The most happiest time of the day!.', 3, 2, 0, 1, 'DSC_3122-min.jpg', '400', 'EGP', '0.00', 0, 1, 2, 0, '2022-01-04 20:25:01'),
(3, 3, 'Offers Relax', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet. Ut nec metus a mi ullamcorper hendrerit. Nulla facilisi. Pellentesque sed nibh a quam accumsan dignissim quis quis urna. The most happiest time of the day!.', 4, 2, 0, 1, 'DSC_3104-min.jpg', '400', 'EGP', '0.00', 0, 1, 3, 0, '2022-01-04 20:25:01');

-- --------------------------------------------------------

--
-- Table structure for table `offer_features`
--

CREATE TABLE `offer_features` (
  `id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `offer_features`
--

INSERT INTO `offer_features` (`id`, `offer_id`, `feature`, `category`, `active`, `timestamp`) VALUES
(1, 1, 'Daily breakfast for two', 'included', 0, 0),
(2, 1, 'Bottle of Wine', 'included', 0, 0),
(3, 1, 'Fruit Basket (once per stay)', 'included', 0, 0),
(4, 1, 'Candlelight Dinner', 'included', 0, 0),
(5, 1, 'Luggage Rack', 'included', 0, 0),
(6, 1, 'Rates for this offer are based on our best available Room Rate for the dates chosen', 'conditions', 0, 0),
(7, 1, 'The rate shown is the average rate per night after the complimentary night has been applied.', 'conditions', 0, 0),
(8, 1, ' Complimentary night(s) must be used in conjunction with initial stay.', 'conditions', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `time` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `location` varchar(250) NOT NULL,
  `price` double(15,2) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `boat_id`, `title`, `remarks`, `time`, `date`, `image`, `location`, `price`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'Normal Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '3.png', 'Aswan', 100.00, 2, 1, 0, '2021-07-09 11:50:37'),
(2, 1, 'Honeymooners Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '1.png', 'Aswan', 200.00, 2, 2, 0, '2021-07-09 11:50:40'),
(3, 1, 'VIP Boat Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '2.png', 'Aswan', 500.00, 2, 3, 0, '2021-07-09 11:50:42');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `inventory` int(10) NOT NULL,
  `bed` text NOT NULL,
  `space` text NOT NULL,
  `adult` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `currency` varchar(100) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `rank` int(11) NOT NULL,
  `disabled` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room`, `category`, `inventory`, `bed`, `space`, `adult`, `child`, `price`, `currency`, `tax`, `rank`, `disabled`, `deleted`, `timestamp`) VALUES
(1, 'Standard Twin', 51, 100, 'One king bed or two queen beds', 'from 27m to 34m, including balcony', 2, 0, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01'),
(2, 'Standard Large bed', 51, 69, 'One king bed', 'from 27m to 34m, including balcony', 2, 0, '400.00', 'EGP', '45.00', 0, 0, 0, '2021-12-29 12:01:01'),
(3, 'A1 Apartment King Bedroom', 52, 76, 'One king bed', 'from 45m to 57m, including balcony', 2, 2, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01'),
(4, 'A1 Apartment Twin Bedroom', 52, 19, 'One king bed or two queen beds', 'from 45m to 57m, including balcony', 2, 2, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01'),
(5, 'Standard Triple', 51, 2, 'three queen beds', 'from 41m to 52m, including balcony', 3, 0, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01'),
(6, 'A2 Apartment 1 King bedroom 1 Twin bedroom', 52, 47, 'four queen beds', 'from 65m to 74m, including balcony', 4, 2, '400.00', 'EGP', '45.00', 0, 0, 0, '2021-12-29 12:01:01'),
(7, 'Villa Standard King Bed \r\n', 53, 20, 'One king bed or two queen beds', 'from 25m to 35m, including balcony', 2, 0, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01'),
(8, 'A1 Villa 1 Twin bedroom', 53, 3, 'One king bed and one queen beds', 'from 65m to 74m, including balcony', 3, 1, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01'),
(9, 'A2 Villa 1 King bedroom 1 Twin bedroom', 53, 17, 'One king bed and two queen beds', 'from 85m to 95m, including balcony', 4, 2, '400.00', 'EGP', '40.00', 0, 0, 0, '2021-12-29 12:01:01');

-- --------------------------------------------------------

--
-- Table structure for table `room_features`
--

CREATE TABLE `room_features` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_features`
--

INSERT INTO `room_features` (`id`, `room_id`, `feature`, `category`, `active`, `timestamp`) VALUES
(1, 1, 'One king bed or two queen beds', 'room features', 0, 0),
(2, 1, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(3, 1, 'Terrace / Balcony', 'room features', 0, 0),
(4, 1, 'Air Conditioning', 'room features', 0, 0),
(5, 1, 'Luggage Rack', 'room features', 0, 0),
(6, 1, 'Connected Rooms available', 'room features', 0, 0),
(7, 1, 'Satellite LCD TV', 'room amenities', 0, 0),
(8, 1, 'Digital Safe Box', 'room amenities', 0, 0),
(9, 1, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(10, 1, 'Telephone', 'room amenities', 0, 0),
(11, 1, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(12, 1, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(13, 1, 'Equipped Kitchen', 'room amenities', 0, 0),
(15, 1, 'Sofa Bed', 'room features', 0, 0),
(16, 1, 'Sitting Room', 'room features', 0, 0),
(17, 1, 'Minibar', 'room amenities', 0, 0),
(18, 1, 'Safe Box', 'room amenities', 0, 0),
(20, 2, 'Satellite LCD TV', 'room amenities', 0, 0),
(21, 2, 'Digital Safe Box', 'room amenities', 0, 0),
(22, 2, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(23, 2, 'Telephone', 'room amenities', 0, 0),
(24, 2, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(25, 2, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(26, 2, 'Equipped Kitchen', 'room amenities', 0, 0),
(27, 2, 'Minibar', 'room amenities', 0, 0),
(28, 2, 'Safe Box', 'room amenities', 0, 0),
(29, 2, 'One king bed or two queen beds', 'room features', 0, 0),
(30, 2, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(31, 2, 'Terrace / Balcony', 'room features', 0, 0),
(32, 2, 'Air Conditioning', 'room features', 0, 0),
(33, 2, 'Luggage Rack', 'room features', 0, 0),
(34, 2, 'Connected Rooms available', 'room features', 0, 0),
(35, 2, 'Sofa Bed', 'room features', 0, 0),
(36, 2, 'Sitting Room', 'room features', 0, 0),
(37, 3, 'Satellite LCD TV', 'room amenities', 0, 0),
(38, 3, 'Digital Safe Box', 'room amenities', 0, 0),
(39, 3, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(40, 3, 'Telephone', 'room amenities', 0, 0),
(41, 3, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(42, 3, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(43, 3, 'Equipped Kitchen', 'room amenities', 0, 0),
(44, 3, 'Minibar', 'room amenities', 0, 0),
(45, 3, 'Safe Box', 'room amenities', 0, 0),
(46, 3, 'One king bed', 'room features', 0, 0),
(47, 3, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(48, 3, 'Terrace / Balcony', 'room features', 0, 0),
(49, 3, 'Air Conditioning', 'room features', 0, 0),
(50, 3, 'Luggage Rack', 'room features', 0, 0),
(51, 3, 'Connected Rooms available', 'room features', 0, 0),
(52, 3, 'Sofa Bed', 'room features', 0, 0),
(53, 3, 'Sitting Room', 'room features', 0, 0),
(54, 4, 'Satellite LCD TV', 'room amenities', 0, 0),
(55, 4, 'Digital Safe Box', 'room amenities', 0, 0),
(56, 4, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(57, 4, 'Telephone', 'room amenities', 0, 0),
(58, 4, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(59, 4, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(60, 4, 'Equipped Kitchen', 'room amenities', 0, 0),
(61, 4, 'Minibar', 'room amenities', 0, 0),
(62, 4, 'Safe Box', 'room amenities', 0, 0),
(63, 4, 'One king bed or two queen beds', 'room features', 0, 0),
(64, 4, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(65, 4, 'Terrace / Balcony', 'room features', 0, 0),
(66, 4, 'Air Conditioning', 'room features', 0, 0),
(67, 4, 'Luggage Rack', 'room features', 0, 0),
(68, 4, 'Connected Rooms available', 'room features', 0, 0),
(69, 4, 'Sofa Bed', 'room features', 0, 0),
(70, 4, 'Sitting Room', 'room features', 0, 0),
(71, 5, 'Satellite LCD TV', 'room amenities', 0, 0),
(72, 5, 'Digital Safe Box', 'room amenities', 0, 0),
(73, 5, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(74, 5, 'Telephone', 'room amenities', 0, 0),
(75, 5, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(76, 5, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(77, 5, 'Equipped Kitchen', 'room amenities', 0, 0),
(78, 5, 'Minibar', 'room amenities', 0, 0),
(79, 5, 'Safe Box', 'room amenities', 0, 0),
(80, 5, 'three queen beds', 'room features', 0, 0),
(81, 5, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(82, 5, 'Terrace / Balcony', 'room features', 0, 0),
(83, 5, 'Air Conditioning', 'room features', 0, 0),
(84, 5, 'Luggage Rack', 'room features', 0, 0),
(85, 5, 'Connected Rooms available', 'room features', 0, 0),
(86, 5, 'Sofa Bed', 'room features', 0, 0),
(87, 5, 'Sitting Room', 'room features', 0, 0),
(88, 6, 'Satellite LCD TV', 'room amenities', 0, 0),
(89, 6, 'Digital Safe Box', 'room amenities', 0, 0),
(90, 6, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(91, 6, 'Telephone', 'room amenities', 0, 0),
(92, 6, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(93, 6, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(94, 6, 'Equipped Kitchen', 'room amenities', 0, 0),
(95, 6, 'Minibar', 'room amenities', 0, 0),
(96, 6, 'Safe Box', 'room amenities', 0, 0),
(97, 6, 'four queen beds', 'room features', 0, 0),
(98, 6, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(99, 6, 'Terrace / Balcony', 'room features', 0, 0),
(100, 6, 'Air Conditioning', 'room features', 0, 0),
(101, 6, 'Luggage Rack', 'room features', 0, 0),
(102, 6, 'Connected Rooms available', 'room features', 0, 0),
(103, 6, 'Sofa Bed', 'room features', 0, 0),
(104, 6, 'Sitting Room', 'room features', 0, 0),
(105, 7, 'Satellite LCD TV', 'room amenities', 0, 0),
(106, 7, 'Digital Safe Box', 'room amenities', 0, 0),
(107, 7, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(108, 7, 'Telephone', 'room amenities', 0, 0),
(109, 7, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(110, 7, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(111, 7, 'Equipped Kitchen', 'room amenities', 0, 0),
(112, 7, 'Minibar', 'room amenities', 0, 0),
(113, 7, 'Safe Box', 'room amenities', 0, 0),
(114, 7, 'One king bed or two queen beds', 'room features', 0, 0),
(115, 7, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(116, 7, 'Terrace / Balcony', 'room features', 0, 0),
(117, 7, 'Air Conditioning', 'room features', 0, 0),
(118, 7, 'Luggage Rack', 'room features', 0, 0),
(119, 7, 'Connected Rooms available', 'room features', 0, 0),
(120, 7, 'Sofa Bed', 'room features', 0, 0),
(121, 7, 'Sitting Room', 'room features', 0, 0),
(122, 8, 'Satellite LCD TV', 'room amenities', 0, 0),
(123, 8, 'Digital Safe Box', 'room amenities', 0, 0),
(124, 8, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(125, 8, 'Telephone', 'room amenities', 0, 0),
(126, 8, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(127, 8, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(128, 8, 'Equipped Kitchen', 'room amenities', 0, 0),
(129, 8, 'Minibar', 'room amenities', 0, 0),
(130, 8, 'Safe Box', 'room amenities', 0, 0),
(131, 8, 'One king bed or two queen beds', 'room features', 0, 0),
(132, 8, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(133, 8, 'Terrace / Balcony', 'room features', 0, 0),
(134, 8, 'Air Conditioning', 'room features', 0, 0),
(135, 8, 'Luggage Rack', 'room features', 0, 0),
(136, 8, 'Connected Rooms available', 'room features', 0, 0),
(137, 8, 'Sofa Bed', 'room features', 0, 0),
(138, 8, 'Sitting Room', 'room features', 0, 0),
(139, 9, 'Satellite LCD TV', 'room amenities', 0, 0),
(140, 9, 'Digital Safe Box', 'room amenities', 0, 0),
(141, 9, 'Wi-Fi (free of charge)', 'room amenities', 0, 0),
(142, 9, 'Telephone', 'room amenities', 0, 0),
(143, 9, 'Iron & Ironing Board (upon request)', 'room amenities', 0, 0),
(144, 9, 'Laundry Services (chargeable)', 'room amenities', 0, 0),
(145, 9, 'Equipped Kitchen', 'room amenities', 0, 0),
(146, 9, 'Minibar', 'room amenities', 0, 0),
(147, 9, 'Safe Box', 'room amenities', 0, 0),
(148, 9, 'One king bed or two queen beds', 'room features', 0, 0),
(149, 9, 'Bathroom with Shower Cabin', 'room features', 0, 0),
(150, 9, 'Terrace / Balcony', 'room features', 0, 0),
(151, 9, 'Air Conditioning', 'room features', 0, 0),
(152, 9, 'Luggage Rack', 'room features', 0, 0),
(153, 9, 'Connected Rooms available', 'room features', 0, 0),
(154, 9, 'Sofa Bed', 'room features', 0, 0),
(155, 9, 'Sitting Room', 'room features', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_files`
--

CREATE TABLE `site_files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_files`
--

INSERT INTO `site_files` (`id`, `module_id`, `form_id`, `user_id`, `file_name`, `timestamp`) VALUES
(1, 15, '1', 0, 'DSC_2969-min.jpg', '2022-01-02 22:24:23'),
(2, 15, '1', 0, 'DSC_3010-min.jpg', '2022-01-02 22:24:23'),
(3, 15, '2', 0, 'DSC_3249-min.jpg', '2022-01-02 22:24:23'),
(4, 15, '2', 0, 'DSC_3250-min.jpg', '2022-01-02 22:24:23'),
(5, 15, '3', 0, 'DSC_3251-min.jpg', '2022-01-02 22:24:23'),
(6, 15, '4', 0, 'DSC_3031-min.jpg', '2022-01-02 22:24:23'),
(7, 15, '5', 0, 'DSC_2969-min.jpg', '2022-01-02 22:24:23'),
(8, 15, '9', 0, 'DSC_3010-min.jpg', '2022-01-02 22:24:23'),
(9, 15, '5', 0, 'DSC_3249-min.jpg', '2022-01-02 22:24:23'),
(10, 15, '6', 0, 'DSC_3250-min.jpg', '2022-01-02 22:24:23'),
(11, 15, '7', 0, 'DSC_3251-min.jpg', '2022-01-02 22:24:23'),
(12, 15, '8', 0, 'DSC_3031-min.jpg', '2022-01-02 22:24:23'),
(23, 15, '12', 1, '40963.jpg', '2022-01-29 19:05:40');

-- --------------------------------------------------------

--
-- Table structure for table `site_groups`
--

CREATE TABLE `site_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_groups`
--

INSERT INTO `site_groups` (`id`, `group_name`, `serial`, `img`, `rank`, `deleted`) VALUES
(1, 'HVAC ACCESSORIES', 'hva', 'hvac_accessories.jpg', 1, 0),
(2, 'VENTILATION FANS', 'ven', 'ventilation fans.jpg', 2, 0),
(3, 'ELECTRICAL MATERIALS', 'elec', 'ELECTRICAL MATERIALS.jpg', 3, 0),
(4, 'GRILLES & DIFFUSERS', 'gril', 'grilles diffusers.jpg', 4, 0),
(5, 'G.I. & ACCESSORIES', 'g.i.', 'G.I. & ACCESSORIES.jpg', 5, 0),
(6, 'P.I. & ACCESSORIES', 'p.i.', 'P.I. & ACCESSORIES.jpg', 6, 0),
(7, 'AIR CONDITION UNITS', 'air', 'AIR CONDITION UNITS.jpg', 7, 0),
(8, 'THERMOSTAT & CONTROL VALVES', 'ther', 'THERMOSTAT & CONTROL VALVES.jpg', 8, 0),
(9, 'AC FILTERS', 'ac', 'AC FILTERS.jpg', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_group_items`
--

CREATE TABLE `site_group_items` (
  `id` int(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_group_items`
--

INSERT INTO `site_group_items` (`id`, `group_id`, `item_name`, `serial`, `company`, `description`, `img`, `file`, `rank`, `timestamp`, `deleted`) VALUES
(1, 1, 'وش مبنى 100 سم', '00133E8', '1', 'Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit.', '1.jpg', '1.pdf', 1, '2019-04-18 14:46:10', 0),
(2, 1, 'حديد 4 لنية ', '002225B', '4', 'حديد 4 لنية ', '', '', 2, '2019-04-24 14:50:10', 0),
(3, 1, 'test', '009B11E', '4', 'حديد 3 لنية', '', '', 3, '2019-04-24 15:22:01', 0),
(4, 3, 'حديد 4 لنية', '00269BB', '1', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:23:21', 0),
(5, 3, 'حديد 4 لنية', '00F436C', '6', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:24:17', 0),
(6, 3, 'حديد 2.5 لنية أطوال', '00A6820', '7', 'حديد 2.5 لنية', '', '', 0, '2019-04-24 15:26:18', 0),
(7, 3, 'حديد 2 لنية لفف', '003988F', '1', 'حديد 2 لنية لفف', '', '', 0, '2019-04-24 15:29:51', 0),
(8, 7, 'أسمنت الجيش رتبة 52.5 بورتلاندى', '0091BED', '13', 'أسمنت الجيش رتبة 52.5 بورتلاندى', '', '', 0, '2019-04-24 15:36:18', 0),
(9, 7, 'أسمنت السويدى', '0009D57', '12', 'أسمنت السويدى', '', '', 0, '2019-04-24 16:10:52', 0),
(10, 7, 'أسمنت المصرية', '00C27C9', '11', 'أسمنت المصرية', '', '', 0, '2019-04-24 16:11:38', 0),
(11, 7, 'أسمنت سويتر', '00D982E', '25', 'أسمنت سويتر', '', '', 0, '2019-04-24 16:15:22', 0),
(12, 7, 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '00B3C18', '14', 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '', '', 0, '2019-04-24 16:18:53', 0),
(14, 1, 'test2', '008C60E', 'test2', 'test2 test2 test2 test2', '556a0152-fab8-428f-9010-215a63d9fa00.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c1.png', 0, '2019-10-04 20:23:52', 0),
(15, 1, 'test2w', '00A31BA', 'test2w', 'test2wtest2wtest2w test2wtest2w', '556a0152-fab8-428f-9010-215a63d9fa00.png', 'Book121.csv', 1, '2019-10-04 20:27:44', 0),
(16, 2, 'test2ww', '00F9BB8', 'test2w', 'test 2uhsd', '6bc837d5-1df8-4527-8e1a-61ba7a17c5a8.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c.png', 5, '2019-10-04 22:38:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_meta_data`
--

CREATE TABLE `site_meta_data` (
  `id` int(11) NOT NULL,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paragraph` text COLLATE utf8_unicode_ci NOT NULL,
  `points` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_meta_data`
--

INSERT INTO `site_meta_data` (`id`, `header`, `title`, `paragraph`, `points`, `meta`, `img`, `link`, `rank`) VALUES
(1, 'Zahabia', ' ', '', '', 'site_title_logo', 'logo.png', '', 1),
(8, 'Call', '+2065 / 3615852', '+2065 / 3615852', '', 'contact_us', 'fa fa-mobile', '', 1),
(9, 'Email', 'zahabia_resort@yahoo.com', 'zahabia_resort@yahoo.com', '', 'email_us', 'fa fa-envelope-o', '', 2),
(10, 'Location', 'Location', 'Zahabia Hotel & Beach Resort, Kornish sakala st،\nHurghada,', '', 'location', 'fa fa-map-marker', '', 3),
(11, '', '', 'Our purpose is to positively impact lifestyles of the community. We serve through consistent, timely, quality,  efficient and  value  added  delivery of  innovative joiness that  constantly exceed expectations.', '', 'footer_about', '', '', 1),
(12, '', '', 'Don\'t hesitate to contact us with any questions or inquiries.', '', 'footer_info', '', '', 1),
(16, 'CAT 1', 'CAT 1', '', 'cat_1', 'gallery', 'DSC_3104-min.jpg', '', 1),
(17, 'CAT 2', 'CAT 2', '', 'cat_2', 'gallery', 'DSC_3122-min.jpg', '', 1),
(18, 'CAT 3', 'CAT 3', '', 'cat_3', 'gallery', 'DSC_3115-min.jpg', '', 1),
(22, 'Sent Successfully', 'Sent', '#000000', '', 'clients_req_status', '', '', 1),
(23, 'Failed to sent', 'Failed', '#bd1212', '', 'clients_req_status', '', '', 1),
(24, 'Waiting Reply', 'Waiting', '#467b2b', '', 'clients_req_status', '', '', 1),
(25, 'Business City', '3.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'cabo-pulmo-diving-11.jpg', '', 1),
(26, 'Business City', '4.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'Discover-Scuba-Diving1.jpg', '', 3),
(28, 'Business City', '5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'scuba-diving2-960x11491.jpg', '', 2),
(29, 'Julien Miro', 'Software Developer', '', '', 'team', '11.jpg', '', 1),
(30, 'Joan Williams', 'Support Operator', '', '', 'team', '21.jpg', '', 2),
(32, 'Benedict Smith', 'Creative Director', '', '', 'team', '31.jpg', '', 3),
(33, 'Madlen Green', 'Sales Manager', '', '', 'team', '41.jpg', '', 4),
(34, 'Julien Miro', 'Julien Miro', '', '', 'team', 'Discover-Scuba-Diving.jpg', '', 5),
(35, 'Reservation Request', 'Reservation Request', '#bd1212', '', 'booking_status', '', '1', 1),
(36, 'waiting payment confirmation', 'waiting payment confirmation', '#ffa922', '', 'booking_status', '', '2', 1),
(37, 'payed & Confirmed', 'payed & Confirmed', '#1f8d5d', '', 'booking_status', '', '3', 1),
(46, 'About Us', 'About Us ', 'Zahabia Hotel and Beach Resort is not only a remarkable place for an amazing holidays but also it is a place filled with welcoming staff, lovely food, astonishing design and unforgettable atmosphere.\r\nWe are an established hotel with the benefit of its own private beach overlooking the famous Giftun Island.\r\nThe development comprises an exclusive collection', '', 'about', '', '', 1),
(47, 'About us points', 'About us points', 'Located just 10 minutes drive from Hurghada International Airport, at the heart of Hurghada. Sheraton Street, the main shopping street in Hurghada, is just 20 minutes walk away, as is the new marina and marine port. Here you will find an abundance of shops, restaurants, clubs and bars, the majority with friendly English & Russian speaking staff.', '', 'about_points', '', '', 1),
(49, 'About us points', 'About us points', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac.', '', 'about_images', 'DSC_3128.jpg', '', 1),
(50, 'About us points', 'About us points', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac', '', 'about_images', 'DSC_3122.jpg', '', 2),
(51, 'Rooms', 'Rooms', 'Rooms', '', 'rooms_categories', '', '', 1),
(52, 'Apartment', 'Apartment', 'Apartment', '', 'rooms_categories', '', '', 2),
(53, 'Villa', 'Villa', 'Villa', '', 'rooms_categories', '', '', 3),
(55, 'About us points', 'About us points', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac.', '', 'about_images', 'DSC_3115.jpg', '', 1),
(56, 'About us points', 'About us points', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac', '', 'about_images', 'DSC_3131.jpg', '', 2),
(57, 'About us points', 'About us points', 'Curabitur pulvinar euismod ante, ac sagittis ante posuere ac', '', 'about_images', 'DSC_3140.jpg', '', 2),
(58, 'CAT 1', 'CAT 1', '', 'cat_1', 'gallery', 'DSC_3137-min.jpg', '', 1),
(59, 'CAT 2', 'CAT 2', '', 'cat_2', 'gallery', 'DSC_3140-min.jpg', '', 1),
(60, 'CAT 3', 'CAT 3', '', 'cat_3', 'gallery', 'DSC_3198-min.jpg', '', 1),
(61, 'CAT 1', 'CAT 1', '', 'cat_1', 'gallery', 'DSC_2969-min.jpg', '', 1),
(62, 'CAT 2', 'CAT 2', '', 'cat_2', 'gallery', 'DSC_3031-min.jpg', '', 1),
(63, 'CAT 3', 'CAT 3', '', 'cat_3', 'gallery', 'DSC_3091-min.jpg', '', 1),
(64, 'CAT 1', 'CAT 1', '', 'cat_1', 'gallery', 'DSC_3113-min.jpg', '', 1),
(65, 'CAT 2', 'CAT 2', '', 'cat_2', 'gallery', 'DSC_3257-min.jpg', '', 1),
(66, 'CAT 3', 'CAT 3', '', 'cat_3', 'gallery', 'DSC_3157-min.jpg', '', 1),
(67, 'Restaurants', 'Restaurants', '', '', 'facilities', 'icon-check', '', 1),
(68, '24-hrs Concierge Service', '24-hrs Concierge Service', '', '', 'facilities', 'icon-check', '', 1),
(69, 'Coffee Shop', 'Coffee Shop', '', '', 'facilities', 'icon-check', '', 1),
(70, 'Pool Bar', 'Pool Bar', '', '', 'facilities', 'icon-check', '', 1),
(71, 'Take Away Service', 'Take Away Service', '', '', 'facilities', 'icon-check', '', 1),
(72, 'Laundry Service', 'Laundry Service', '', '', 'facilities', 'icon-check', '', 1),
(73, 'Wi-Fi', 'Wi-Fi', '', '', 'facilities', 'icon-check', '', 1),
(74, 'Kids Club', 'Kids Club', '', '', 'facilities', 'icon-check', '', 1),
(75, 'Sport Activities', 'Sport Activities', '', '', 'facilities', 'icon-check', '', 1),
(76, 'Gym', 'Gym', '', '', 'facilities', 'icon-check', '', 1),
(77, 'Private Beach', 'Private Beach', '', '', 'facilities', 'icon-check', '', 1),
(78, 'Taxi Service', 'Taxi Service', '', '', 'facilities', 'icon-check', '', 1),
(79, 'SPA', 'SPA', '', '', 'facilities', 'icon-check', '', 1),
(80, 'Water & Beach Sport Center', 'Water & Beach Sport Center', '', '', 'facilities', 'icon-check', '', 1),
(81, 'Doctor & Medical Service ( On Call )', 'Doctor & Medical Service ( On Call )', '', '', 'facilities', 'icon-check', '', 1),
(83, 'WELCOME TO Zahabia Resort', 'WELCOME TO Zahabia Resort', 'Zahabia Hotel and Beach Resort is not only a remarkable place for an amazing holidays but also it is a place filled with welcoming staff, lovely food, astonishing design and unforgettable atmosphere.', '', 'welcome_zahabia', '', '', 1),
(84, 'Call Center', '+2065 / 3615852', '', '', 'contact_us_call_center', 'fa fa-mobile', '', 1),
(85, 'facebook', 'facebook', 'https://www.facebook.com/ZahabiaHotelAndBeachResort', '', 'social_data_facebook', '', 'https://www.facebook.com/ZahabiaHotelAndBeachResort', 1),
(86, 'RESORT OVERVIEW', 'RESORT OVERVIEW', 'cat_1', 'cat_1', 'gallery_categories', '', '', 1),
(87, 'ACCOMMODATIONS', 'ACCOMMODATIONS', 'cat_2', 'cat_2', 'gallery_categories', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `thumb` varchar(150) NOT NULL,
  `img` varchar(150) NOT NULL,
  `video` varchar(150) DEFAULT NULL,
  `first_title` text NOT NULL,
  `second_title` text NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `thumb`, `img`, `video`, `first_title`, `second_title`, `rank`, `deleted`) VALUES
(14, '04-b_14.jpg', 'DSC_3128-min.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 1, 0),
(15, '04-b_08.jpg', 'DSC_3122-min.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 2, 0),
(16, '04-b_08.jpg', 'DSC_3131-min.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience  Enjoy A luxory Experience Enjoy A luxory Experience', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `text_color` varchar(150) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_name`, `status_color`, `text_color`, `rank`) VALUES
(1, 'Draft', '#000000', '#FFF', 2),
(2, 'Avalible', '#b88121', '#FFF', 4),
(3, 'Cancelled', '#0088cc', '#FFF', 6),
(4, 'Unavalible', '#1f8d5d', '#FFF', 8);

-- --------------------------------------------------------

--
-- Table structure for table `sub_activity`
--

CREATE TABLE `sub_activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_activity`
--

INSERT INTO `sub_activity` (`id`, `name`, `deleted`) VALUES
(1, 'Diving', 0),
(2, 'Courses', 0),
(3, 'Intro', 0),
(4, 'Daily Dive', 0),
(5, 'Speciality', 0),
(6, 'Aqua Center', 0),
(7, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `possion` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `to_deleted_bookings`
--

CREATE TABLE `to_deleted_bookings` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pax` int(5) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_id` int(11) NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_cost` decimal(20,2) NOT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `to_deleted_bookings`
--

INSERT INTO `to_deleted_bookings` (`id`, `type`, `first_name`, `last_name`, `date`, `pax`, `email`, `phone`, `offer_id`, `price`, `currency`, `total_cost`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', 'Menna Allah', 'hisham', '2020-01-16', 3, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '6000.00', 37, NULL, '2020-01-10 09:09:34'),
(2, '', 'hany', 'hisham', '2020-01-16', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', 'EGP', '4000.00', 35, NULL, '2020-01-10 08:55:52'),
(3, '', 'hany', 'hisham', '2020-01-17', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '4000.00', 36, NULL, '2020-01-10 09:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int(5) NOT NULL,
  `department` int(11) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `permission` tinyint(1) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `email`, `role_id`, `department`, `mobile_no`, `password`, `is_admin`, `permission`, `disabled`, `last_ip`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'hany.hisham', 'admin', 'mahmoud.mohier@gmail.com', 1, 1, '12345', '$2y$10$Q69gfBlPjFPL0n1EctX73.FzaJEQiw6icQgC2sQFaubkEX69Uvgza', 1, 1, 0, '', '2022-02-02 00:00:00', '2022-02-02 00:00:00', 0),
(115, 'mahmoud', 'mohier', 'mahmoud.mohier@gmail.com', 1, 1, '', '$2y$10$fI9g2/jZAWS2A5ximb70oeX0jD7WFlH9tuKysCUrLq2ErAMsuKPnK', 0, 1, 0, '', '2022-02-03 00:00:00', '2022-02-03 00:00:00', 0),
(116, 'mahmoud.mohier', 'mohier', 'mahmoud.mohier@gmail.com', 1, 2, '', '$2y$10$OLsSf6aMiuELhBUEsAV6newJnB4WJaSVuiiZlnzVaOKcmJe1q8j2C', 0, 1, 0, '', '2022-02-04 00:00:00', '2022-02-04 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_branches`
--

CREATE TABLE `user_branches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(6) NOT NULL,
  `role_id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_branches`
--

INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`, `role_id`) VALUES
(318, 1, 1, ''),
(319, 115, 1, ''),
(320, 116, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `permission`) VALUES
(1, 'Admin', 1),
(2, 'IT', 1),
(3, 'GMs', 0),
(4, 'Reservation', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups_permission`
--

CREATE TABLE `user_groups_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(6) NOT NULL,
  `module_id` int(11) NOT NULL,
  `g_view` tinyint(1) NOT NULL,
  `creat` tinyint(2) NOT NULL,
  `edit` tinyint(2) NOT NULL,
  `view` tinyint(2) NOT NULL,
  `remove` tinyint(2) NOT NULL,
  `pr_edit` tinyint(1) NOT NULL,
  `pr_approve` tinyint(1) NOT NULL,
  `recieve` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups_permission`
--

INSERT INTO `user_groups_permission` (`id`, `role_id`, `module_id`, `g_view`, `creat`, `edit`, `view`, `remove`, `pr_edit`, `pr_approve`, `recieve`) VALUES
(1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(12, 0, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(13, 0, 3, 1, 1, 1, 1, 1, 0, 0, 0),
(14, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0),
(22, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0),
(23, 1, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(24, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(32, 2, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(33, 2, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(34, 2, 4, 1, 0, 0, 0, 0, 0, 0, 0),
(35, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 2, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 2, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 2, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 2, 10, 1, 1, 1, 1, 0, 0, 0, 0),
(41, 6, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(42, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0),
(43, 6, 3, 0, 1, 0, 0, 0, 0, 0, 0),
(44, 6, 4, 0, 0, 1, 0, 0, 0, 0, 0),
(45, 6, 5, 0, 1, 0, 0, 0, 0, 0, 0),
(46, 6, 6, 0, 1, 0, 1, 0, 0, 0, 0),
(47, 6, 7, 1, 1, 0, 0, 0, 0, 0, 0),
(48, 6, 8, 0, 1, 0, 1, 0, 0, 0, 0),
(49, 6, 9, 0, 1, 1, 0, 0, 0, 0, 0),
(50, 6, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 25, 1, 1, 0, 0, 1, 0, 0, 0, 0),
(52, 25, 2, 1, 0, 0, 1, 0, 0, 0, 0),
(53, 25, 3, 1, 1, 0, 0, 0, 0, 0, 0),
(54, 25, 4, 1, 0, 1, 0, 0, 0, 0, 0),
(55, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(56, 26, 2, 1, 1, 1, 1, 0, 1, 0, 1),
(57, 26, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(58, 26, 4, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `creat` tinyint(4) NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `view` tinyint(4) NOT NULL,
  `remove` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `module_id`, `creat`, `edit`, `view`, `remove`, `timestamp`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(2, 1, 2, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(3, 1, 3, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(4, 1, 4, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(5, 1, 5, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(6, 1, 6, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(7, 1, 7, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(8, 1, 8, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(9, 1, 9, 1, 0, 0, 0, '2019-01-15 11:55:20'),
(10, 1, 11, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(11, 1, 12, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(12, 1, 13, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(13, 1, 14, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(14, 1, 15, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(15, 1, 16, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(16, 1, 17, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(17, 1, 18, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(18, 1, 19, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(19, 1, 20, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(20, 1, 21, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(21, 1, 22, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(22, 1, 23, 0, 1, 0, 1, '2019-01-15 11:55:20'),
(23, 1, 24, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(24, 1, 25, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(25, 1, 26, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(26, 1, 27, 1, 1, 1, 1, '2019-01-15 11:55:20'),
(27, 115, 1, 1, 0, 1, 0, '2022-02-03 14:55:25'),
(28, 115, 2, 1, 1, 1, 1, '2022-02-03 14:55:25'),
(29, 115, 4, 0, 0, 1, 0, '2022-02-03 14:55:25'),
(30, 115, 5, 0, 0, 1, 0, '2022-02-03 14:55:25'),
(31, 115, 6, 0, 0, 1, 0, '2022-02-03 14:55:25'),
(32, 115, 8, 0, 0, 1, 0, '2022-02-03 14:55:25'),
(33, 115, 11, 0, 0, 1, 0, '2022-02-03 14:55:25'),
(34, 115, 13, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(35, 115, 15, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(36, 115, 17, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(37, 115, 22, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(38, 115, 18, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(39, 115, 23, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(40, 115, 19, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(41, 115, 20, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(42, 115, 21, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(43, 115, 25, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(44, 115, 14, 0, 0, 0, 0, '2022-02-03 14:55:25'),
(45, 116, 1, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(46, 116, 2, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(47, 116, 4, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(48, 116, 5, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(49, 116, 6, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(50, 116, 8, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(51, 116, 11, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(52, 116, 13, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(53, 116, 15, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(54, 116, 17, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(55, 116, 22, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(56, 116, 18, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(57, 116, 23, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(58, 116, 19, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(59, 116, 20, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(60, 116, 21, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(61, 116, 25, 1, 1, 1, 1, '2022-02-04 12:22:47'),
(62, 116, 14, 1, 1, 1, 1, '2022-02-04 12:22:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supp_name` (`client_name`);

--
-- Indexes for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font_icons`
--
ALTER TABLE `font_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallary`
--
ALTER TABLE `gallary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_features`
--
ALTER TABLE `offer_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_features`
--
ALTER TABLE `room_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_files`
--
ALTER TABLE `site_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_groups`
--
ALTER TABLE `site_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_name` (`group_name`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_group_items`
--
ALTER TABLE `site_group_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_activity`
--
ALTER TABLE `sub_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `to_deleted_bookings`
--
ALTER TABLE `to_deleted_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_branches`
--
ALTER TABLE `user_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `font_icons`
--
ALTER TABLE `font_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;

--
-- AUTO_INCREMENT for table `gallary`
--
ALTER TABLE `gallary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `offer_features`
--
ALTER TABLE `offer_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `room_features`
--
ALTER TABLE `room_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `site_files`
--
ALTER TABLE `site_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `site_groups`
--
ALTER TABLE `site_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `site_group_items`
--
ALTER TABLE `site_group_items`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_activity`
--
ALTER TABLE `sub_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `to_deleted_bookings`
--
ALTER TABLE `to_deleted_bookings`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `user_branches`
--
ALTER TABLE `user_branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
