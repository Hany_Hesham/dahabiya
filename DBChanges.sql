"TRUNCATE `dahbia`.`slider`"
INSERT INTO `slider` (`id`, `thumb`, `img`, `video`, `first_title`, `second_title`, `rank`, `deleted`) VALUES
(1, 's1-thumb.jpg', 'slider 7.jpg', 'explore.mp4', 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 3, 1),
(2, '04-b_14.jpg', 'slider 6.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 4, 0),
(3, '04-b_08.jpg', 'slider 5.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 5, 1),
(12, '04-b_08.jpg', 'about_banner.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 1, 0),
(13, '04-b_08.jpg', 'banner_bg.jpg', NULL, 'Enjoy A luxory Experience', 'Enjoy A luxory Experience Enjoy A luxory Experience Enjoy A luxory Experience', 2, 0);
COMMIT;